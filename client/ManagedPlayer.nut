class ManagedPlayer
{
    static function getVirtualWorld()
    {
        return m_VWorld;
    }

    // ------------------------------------------------------------------- //

    static function setVirtualWorld(vWorld)
    {
        m_VWorld = vWorld;
    }

    // ------------------------------------------------------------------- //

    static m_VWorld = 0;
}
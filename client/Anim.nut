// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online Utility Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

enableEvent_Render(true);
addEvent("onBotChangeAni");

// ------------------------------------------------------------------- //

local aniTickInterval = 1000;
local nextAniTick = getTickCount() + aniTickInterval;

local lastAnis = {};

// ------------------------------------------------------------------- //

addEventHandler("onRender", function()
{
    local tick = getTickCount();
    if (tick >= nextAniTick)
    {
        foreach (key, bot in Bot.m_Bots)
        {
            if (bot != null && bot.isSpawned())
            {
                local id = bot.getID();
                local currAni = getPlayerAni(id);

                if (!(id in lastAnis))
                {
                    lastAnis[id] <- currAni;
                    return;
                }

                local lastAni = lastAnis[id];

                if (lastAni != currAni)
                {
                    callEvent("onBotChangeAni", id, currAni);
                    lastAnis[id] = currAni;
                    bot.m_AniChanged = true;
                }
            }
        }

        nextAniTick = tick + aniTickInterval;
    }
});

// ------------------------------------------------------------------- //

/*addEventHandler("onBotChangeAni", function(bid, ani)
{
    local bot = Bot.getBot(bid);
    bot.m_AniChanged = true;
});*/

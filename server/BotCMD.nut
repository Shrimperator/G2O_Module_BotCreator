// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

local function CMDPos(pid)
{
    local pos = getPlayerPosition(pid);
    sendMessageToPlayer(pid, 200, 200, 200, (pos.x + "|" + pos.y + "|" + pos.z));
}

// ------------------------------------------------------------------- //

local function CMDAngle(pid)
{
    local angle = getPlayerAngle(pid);
    sendMessageToPlayer(pid, 200, 200, 200, angle.tostring());
}

// ------------------------------------------------------------------- //

local function CMDIdle(params)
{
    local args = sscanf("d", params);
    local bot = Bot.getBot(args[0]);

    if (bot != null)
        bot.idle();
}

// ------------------------------------------------------------------- //

local function CMDSchedule(params)
{
    local args = sscanf("d", params);
    local bot = Bot.getBot(args[0]);

    if (bot != null)
        bot.getScheduleComp().returnToSchedule(getTime());
}

// ------------------------------------------------------------------- //

local function CMDSend(pid, params)
{
    local args = sscanf("d", params);
    local pos = getPlayerPosition(pid);
    local angle = getPlayerAngle(pid);
    local bot = Bot.getBot(args[0]);

    if (bot != null)
    {
        bot.getActionComp().addGoto(pos.x, pos.y, pos.z);
        bot.getActionComp().addTurn(angle);
    }
}

// ------------------------------------------------------------------- //

local function CMDTurn(pid, params)
{
    local args = sscanf("d", params);
    local angle = getPlayerAngle(pid);
    local bot = Bot.getBot(args[0]);

    if (bot != null)
        bot.getActionComp().addTurn(angle);
}

// ------------------------------------------------------------------- //

local function CMDstopAni(params)
{
    local args = sscanf("d", params);
    local bot = Bot.getBot(args[0]);

    if (bot != null)
        bot.stopAni();
}

// ------------------------------------------------------------------- //

local function CMDRun(params)
{
    local args = sscanf("dd", params);

    local bot = Bot.getBot(args[0]);
    local mode = true;
    if (args[1] != 1)
        mode = false;

    if (bot != null)
    {
        if (mode)
            bot.m_MovementMode = EMovementMode.running;
        else
            bot.m_MovementMode = EMovementMode.walking;
    }
}

// ------------------------------------------------------------------- //

local function CMDDestroy(params)
{
    local args = sscanf("d", params);
    local bid = args[0];
    Bot.destroyBot(bid);
}

// ------------------------------------------------------------------- //

local function CMDTeleportBot(params)
{
    local args = sscanf("dd", params);

    local idFrom = args[0];
    local idTo = args[1];

    if (Bot.isBot(idFrom) && Bot.isPlayer(idTo))
    {
        local bot = Bot.getBot(idFrom);
        local pos = getPlayerPosition(idTo);
        local world = getPlayerWorld(idTo);

        bot.teleport(pos.x, pos.y, pos.z);

        if (world != bot.getWorld())
        {
            bot.setWorld(world);
        }
    }
    else if (Bot.isPlayer(idFrom) && Bot.isBot(idTo))
    {
        local bot = Bot.getBot(idTo);
        local pos = bot.getPosition();
        local world = bot.getWorld();

        setPlayerPosition(idFrom, pos.x, pos.y, pos.z);

        if (getPlayerWorld(idFrom) != world)
        {
            setPlayerWorld(idFrom, world);
        }
    }
}

// ------------------------------------------------------------------- //

addEventHandler("onPlayerCommand", function(pid, cmd, params)
{
	cmd = cmd.tolower();

	switch (cmd)
	{
            // -- /botidle <botID> -- //
        case "botidle":
        case "idlebot":
            CMDIdle(params);
            break;

            // -- /botschedule <botID> -- //
        case "botschedule":
        case "schedulebot":
            CMDSchedule(params);
            break;

            // -- /botgoto <botID> -- //
        case "botgoto":
        case "gotobot":
        case "sendbot":
        case "botsend":
            CMDSend(pid, params);
            break;

            // -- /botturn <botID> -- //
        case "botturn":
        case "botangle":
            CMDTurn(pid, params);
            break;

            // -- /botstopani <botID> -- //
        case "botstopani":
        case "botstand":
        case "stopanibot":
        case "standbot":
            CMDstopAni(params);
            break;

            // -- /botrun <botID> <0-1> -- //
        case "botrun":
        case "runbot":
            CMDRun(params);
            break;

            // -- /destroybot <botID> -- //
        case "destroybot":
        case "botdestroy":
            CMDDestroy(params);
            break;

            // -- /pos  -- //
        case "pos":
            CMDPos(pid);
            break;

            // -- /angle -- //
        case "angle":
        case "rot":
            CMDAngle(pid);
            break;

            // -- /tpbot <fromId> <toId> -- //
        case "tpbot":
            CMDTeleportBot(params);
            break;
    }
});

WayPoints.FPs["OLDWORLD\\OLDWORLD.ZEN"] <-
{
	["FP_ITEM_XARDASALTERTURM_02"] = {x = -12173.9775, y = 3672.76074, z = -34231.8086, adjacent =
	[
	]},
	["FP_ROAM_PLATEAU_ROUND02_CAVE_MOVE_01"] = {x = 14187.1641, y = 6676.8374, z = -30104.5586, adjacent =
	[
	]},
	["FP_ROAM_PLATEAU_ROUND02_CAVE_MOVE_02"] = {x = 13822.8066, y = 6688.37744, z = -30096.2871, adjacent =
	[
	]},
	["FP_ITEM_OW_02"] = {x = -6096.63037, y = 867.08374, z = -12157.2666, adjacent =
	[
	]},
	["FP_ROAM_MC_11"] = {x = 2777.25488, y = 2118.24316, z = -30700.4102, adjacent =
	[
	]},
	["FP_ROAM_MC_10"] = {x = 2135.91626, y = 2285.21606, z = -30389.2949, adjacent =
	[
	]},
	["FP_ROAM_MC_09"] = {x = 2720.01099, y = 2121.21606, z = -31205.0352, adjacent =
	[
	]},
	["FP_ROAM_MC_08"] = {x = 1964.13916, y = 2248.21606, z = -31061.9121, adjacent =
	[
	]},
	["FP_ROAM_MC_07"] = {x = 464.55838, y = 2548, z = -28267.293, adjacent =
	[
	]},
	["FP_ROAM_MC_15"] = {x = 1982.31104, y = 2476.6228, z = -28810.1855, adjacent =
	[
	]},
	["FP_ROAM_MC_14"] = {x = 2514.90796, y = 2423.89526, z = -28428.2773, adjacent =
	[
	]},
	["FP_ROAM_MC_13"] = {x = 2184.47583, y = 2406.35327, z = -29187.873, adjacent =
	[
	]},
	["FP_ROAM_MC_12"] = {x = 2700.33862, y = 2335.35327, z = -28900.1699, adjacent =
	[
	]},
	["FP_ROAM_MC_05"] = {x = 823.810486, y = 2539.00024, z = -27928.1172, adjacent =
	[
	]},
	["FP_ROAM_MC_06"] = {x = 1605.10559, y = 2528, z = -28020.6309, adjacent =
	[
	]},
	["FP_ROAM_MC_04"] = {x = -1489.68811, y = 2724, z = -27193.2109, adjacent =
	[
	]},
	["FP_ROAM_MC_03"] = {x = 1929.41577, y = 2496, z = -28229.5918, adjacent =
	[
	]},
	["FP_ROAM_MC_02"] = {x = 585.740356, y = 2530, z = -27749.418, adjacent =
	[
	]},
	["FP_ROAM_MC_01"] = {x = -671.481934, y = 2539, z = -28033.9219, adjacent =
	[
	]},
	["FP_ROAM_GB_02"] = {x = -31186.4219, y = 697.611938, z = 4581.50684, adjacent =
	[
	]},
	["FP_ROAM_GB_01"] = {x = -30681.9629, y = 695.611938, z = 4422.08398, adjacent =
	[
	]},
	["FP_ROAM_BRIDGE_04"] = {x = -31673.6641, y = 653.518311, z = 6501.03223, adjacent =
	[
	]},
	["FP_ROAM_BRIDGE_03"] = {x = -32096.7344, y = 735.574829, z = 7299.8457, adjacent =
	[
	]},
	["FP_ROAM_BRIDGE_02"] = {x = -31521.832, y = 520.743591, z = 12976.7676, adjacent =
	[
	]},
	["FP_ROAM_BRIDGE_01"] = {x = -31139.0039, y = 522.953552, z = 14004.5596, adjacent =
	[
	]},
	["FOP_ROAM_ORC_GATE_06"] = {x = -12676, y = 267, z = -9742, adjacent =
	[
	]},
	["FOP_ROAM_ORC_GATE_05"] = {x = -12993, y = 336, z = -10443, adjacent =
	[
	]},
	["FOP_ROAM_ORC_GATE_04"] = {x = -12177, y = 76, z = -8880, adjacent =
	[
	]},
	["FOP_ROAM_ORC_GATE_03"] = {x = -12273, y = 223, z = -9452, adjacent =
	[
	]},
	["FOP_ROAM_ORC_GATE_02"] = {x = -12388, y = 169, z = -9165, adjacent =
	[
	]},
	["FOP_ROAM_ORC_GATE_01"] = {x = -11824, y = 160, z = -9341, adjacent =
	[
	]},
	["FP_STAND_GUARDING_MINE2_02"] = {x = -25867.1563, y = -574.66394, z = 20613.3906, adjacent =
	[
	]},
	["FP_STAND_GUARDING_MINE2_01"] = {x = -29133.3301, y = -933.805176, z = 20765.2852, adjacent =
	[
	]},
	["FP_ROAM_SCAVENGER_10"] = {x = 11023.4297, y = -1347.80029, z = 10606.5215, adjacent =
	[
	]},
	["FP_ROAM_SCAVENGER_09"] = {x = 10649.6963, y = -1295.70349, z = 10711.7969, adjacent =
	[
	]},
	["FP_ROAM_SCAVENGER_08"] = {x = 10720.9502, y = -1370.72864, z = 11201.0801, adjacent =
	[
	]},
	["FP_ROAM_SCAVENGER_07"] = {x = 10432.707, y = -1357.38953, z = 10868.5928, adjacent =
	[
	]},
	["FP_ROAM_SCAVENGER_06"] = {x = 11165.8672, y = -1344.53552, z = 11018.5215, adjacent =
	[
	]},
	["FP_ROAM_SCAVENGER_05"] = {x = 10081.2393, y = -1349.02783, z = 10607.2529, adjacent =
	[
	]},
	["FP_ROAM_SCAVENGER_04"] = {x = 10371.8486, y = -1348.80493, z = 10261.9688, adjacent =
	[
	]},
	["FP_ROAM_SCAVENGER_03"] = {x = 9585.16113, y = -1321.01953, z = 10468.6689, adjacent =
	[
	]},
	["FP_ROAM_SCAVENGER_02"] = {x = 9600.05664, y = -1333.95374, z = 9987.80078, adjacent =
	[
	]},
	["FP_ROAM_SCAVENGER_01"] = {x = 10134.4668, y = -1380.2688, z = 9906.51855, adjacent =
	[
	]},
	["FP_STAND_GUARDING_MARCOS"] = {x = -16505.3555, y = -1234.93872, z = 6102.69482, adjacent =
	[
	]},
	["FP_ROAM_OW_MOVETUNING_03"] = {x = -8116.80469, y = -629.545837, z = -7660.46631, adjacent =
	[
	]},
	["FP_ROAM_TUNING_4"] = {x = -6390.99902, y = -532.296631, z = -8852.67676, adjacent =
	[
	]},
	["FP_ROAM_TUNING_3"] = {x = -6687.20605, y = -1317.04297, z = 6640.25586, adjacent =
	[
	]},
	["FP_ROAM_TUNING_2"] = {x = -4714.43262, y = -1164.30237, z = 7631.58984, adjacent =
	[
	]},
	["FP_ROAM_TUNING"] = {x = -4535.74316, y = -1169.57312, z = 7323.77686, adjacent =
	[
	]},
	["FP_CAMPFIRE_LOCATION_23_CAVE_01"] = {x = -44001.0625, y = 12.5482435, z = -4358.04541, adjacent =
	[
	]},
	["FP_CAMPFIRE_LOCATION_23_CAVE_03"] = {x = -43974.6797, y = 12.0884399, z = -4055.82031, adjacent =
	[
	]},
	["FP_CAMPFIRE_LOCATION_23_CAVE_02"] = {x = -43768.6992, y = 41.6082916, z = -4431.95313, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_04_02_01"] = {x = -34819.543, y = 97.7893524, z = 1191.35535, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_04_02_02"] = {x = -34427.125, y = 37.7893562, z = 1198.29346, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_17_03"] = {x = -16914.7754, y = -1317.69604, z = -9115.69043, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_17_02"] = {x = -16572.6875, y = -1337.69604, z = -9510.66992, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_17_01"] = {x = -16088.5547, y = -1296.85266, z = -9695.78906, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_16_01"] = {x = -19864.6484, y = -1280.60181, z = -8004.84326, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_15_01"] = {x = -23489.9238, y = -1257.14099, z = -9968.72168, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_15_02"] = {x = -23626.1602, y = -1284.60242, z = -10390.2412, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_14_01"] = {x = -22695.8516, y = -1270.8092, z = -10871.9717, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_14_02"] = {x = -22360.584, y = -1295.59106, z = -11334.8359, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_13_01"] = {x = -21732.8262, y = -1280.60181, z = -10302.8389, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_13_02"] = {x = -21757.5664, y = -1280.60181, z = -9800.49316, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_12_03"] = {x = -19596.5273, y = -1292.17029, z = -9385.7334, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_12_01"] = {x = -20409.1895, y = -1278.9342, z = -9667.41016, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_12_02"] = {x = -20238.5879, y = -1295.75903, z = -8991.02344, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_11_03"] = {x = -21388.3848, y = -1280.60181, z = -9581.28906, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_11_02"] = {x = -21575.8828, y = -1280.60181, z = -9124.05078, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_11_01"] = {x = -21857.5371, y = -1280.60181, z = -9379.90332, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_10_02"] = {x = -19819.7188, y = -1318.63879, z = -9503.65918, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_10_01"] = {x = -19526.6816, y = -1318.73267, z = -9593.70996, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_10_03"] = {x = -19340.9844, y = -1270.05322, z = -9121.75488, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_9_01"] = {x = -15238.9912, y = -1295.19666, z = -5555.07324, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_9_02"] = {x = -14830.959, y = -1295.19666, z = -5778.1084, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_9_03"] = {x = -14487.1543, y = -1295.19666, z = -5465.82129, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_8_01"] = {x = -13640.9678, y = -1295.19666, z = -7286.16113, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_8_02"] = {x = -13867.3457, y = -1295.19666, z = -6977.83496, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_8_03"] = {x = -13963.6553, y = -1295.19666, z = -7623.90332, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_7_03"] = {x = -15147.4883, y = -1295.19666, z = -7964.88135, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_7_01"] = {x = -15484.5742, y = -1295.19666, z = -8046.97949, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_7_02"] = {x = -15307.4502, y = -1295.19666, z = -7679.13965, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_6_03"] = {x = -15315.5938, y = -1295.19666, z = -9032.52832, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_6_02"] = {x = -15687.0166, y = -1295.19666, z = -8943.29297, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_6_01"] = {x = -15415.668, y = -1295.19666, z = -8693.12891, adjacent =
	[
	]},
	["FP_STAND_OW_ICEREGION_SYLVIO_WAIT_02"] = {x = -39640.9219, y = 896.375244, z = 5736.65527, adjacent =
	[
	]},
	["FP_STAND_OW_ICEREGION_SYLVIO_WAIT_01"] = {x = -39381.4297, y = 873.375244, z = 5867.46973, adjacent =
	[
	]},
	["FP_STAND_OW_DJG_STARTCAMP_02"] = {x = 2768.06226, y = 2402.84033, z = 20439.8594, adjacent =
	[
	]},
	["FP_SMALLTALK_OW_DJG_STARTCAMP_04"] = {x = 1995.16357, y = 2693.23608, z = 21398.7324, adjacent =
	[
	]},
	["FP_SMALLTALK_OW_DJG_STARTCAMP_03"] = {x = 1868.70532, y = 2642.64697, z = 21264.8145, adjacent =
	[
	]},
	["FP_SMALLTALK_OW_DJG_STARTCAMP_02"] = {x = 1483.41248, y = 2708.61255, z = 21371.4434, adjacent =
	[
	]},
	["FP_SMALLTALK_OW_DJG_STARTCAMP_01"] = {x = 1293.14355, y = 2725.03955, z = 21406.9141, adjacent =
	[
	]},
	["FP_STAND_OW_DJG_STARTCAMP_01"] = {x = 1350.23535, y = 2781.14429, z = 21087.3359, adjacent =
	[
	]},
	["FP_ROAM_OW_WATERFALL_GOBBO10_04"] = {x = 19653.2031, y = 1140.89929, z = -17715.0801, adjacent =
	[
	]},
	["FP_CAMPFIRE_DJG_ROCKCAMP_02"] = {x = 20531.9863, y = 4451.03564, z = -24392.8184, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_2_01"] = {x = -16339.6484, y = -1295.19666, z = -8513.28223, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_5_01"] = {x = -16392.8848, y = -1295.19666, z = -8189.41748, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_5_03"] = {x = -16814.8086, y = -1312.37585, z = -8315.52148, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_4_01"] = {x = -18042.8223, y = -1295.19666, z = -10319.5869, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_4_02"] = {x = -18192.4766, y = -1295.19666, z = -9788.25488, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_4_03"] = {x = -18698.959, y = -1295.19666, z = -9845.38184, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_3_01"] = {x = -17796.5195, y = -1295.19666, z = -8490.375, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_3_03"] = {x = -18460.5703, y = -1295.19666, z = -8445.43848, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_3_02"] = {x = -18244.668, y = -1295.19666, z = -7994.53809, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_2_02"] = {x = -19606.4785, y = -1295.19666, z = -6996.12451, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_2_03"] = {x = -19420.8496, y = -1295.19666, z = -6553.60986, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_03"] = {x = -20043.4785, y = -1295.19666, z = -6480.70947, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_02"] = {x = -20423.9707, y = -1295.19666, z = -6761.33496, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_01"] = {x = -20575.6875, y = -1295.19666, z = -6240.64209, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_ENTRANCE_02"] = {x = -18320.041, y = -1295.19666, z = -4763.67871, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_ENTRANCE_03"] = {x = -18133.2285, y = -1295.19666, z = -5045.13867, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_ENTRANCE_01"] = {x = -18862.959, y = -1274.66687, z = -4362.19531, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_09_2"] = {x = 11413.0938, y = 6802.93164, z = -23832.7676, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_09_1"] = {x = 10839.8721, y = 7049.0708, z = -24581.9141, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_09"] = {x = 11343.1865, y = 6881.00098, z = -24192.8711, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_08_2"] = {x = 11875.7246, y = 6957.87744, z = -25936.3184, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_08_1"] = {x = 11449.9668, y = 7026.89404, z = -26021.1895, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_08"] = {x = 11636.0811, y = 6936.89404, z = -26424.4375, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_06_2"] = {x = 13574.1494, y = 6727.45361, z = -28334.9629, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_07_2"] = {x = 13014.8623, y = 6938.60693, z = -25304.9082, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_07_1"] = {x = 12966.667, y = 7020.35205, z = -24993.834, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_07"] = {x = 13326.085, y = 6970.32568, z = -25229.8027, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_04_2"] = {x = 15329.9092, y = 6349.38184, z = -27421.0137, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_05_2"] = {x = 16609.0078, y = 6468.29395, z = -28168.2773, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_05_1"] = {x = 16625.4297, y = 6468.29395, z = -28493.2422, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_05"] = {x = 16140.7441, y = 6468.29395, z = -28190.6602, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_03_2"] = {x = 17051.8984, y = 6371.854, z = -27240.3477, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_03_1"] = {x = 17389.3672, y = 6375.49268, z = -27274.7539, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_03"] = {x = 17191.2832, y = 6460.58447, z = -27509.8887, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_02_2"] = {x = 17048.3242, y = 6339.35645, z = -26115.2793, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_02_1"] = {x = 17118.8652, y = 6477.49805, z = -25528.8457, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_01_2"] = {x = 18463.457, y = 6385.63281, z = -25886.1289, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_01_1"] = {x = 18867.6133, y = 6443.38721, z = -26396.6035, adjacent =
	[
	]},
	["FP_CAMPFIRE_OW_WOLF_WOODRUIN2"] = {x = -26818.8438, y = -215.699341, z = 2417.5564, adjacent =
	[
	]},
	["FP_ROAM_ORC_17"] = {x = -16788.1445, y = 2388.33887, z = -20391.7578, adjacent =
	[
	]},
	["FP_ROAM_ORC_16"] = {x = -16648.6758, y = 2348.99121, z = -20864.291, adjacent =
	[
	]},
	["FP_ROAM_ORC_15"] = {x = -16417.8457, y = 2329.7959, z = -20209.6738, adjacent =
	[
	]},
	["FP_ROAM_ORC_14"] = {x = -2383.51904, y = 3006.14722, z = -17919.1621, adjacent =
	[
	]},
	["FP_ROAM_ORC_18"] = {x = -1758.37, y = 3225.9541, z = -17920.4043, adjacent =
	[
	]},
	["FP_ROAM_ORC_13"] = {x = -14256.0693, y = 904.924194, z = -12117.916, adjacent =
	[
	]},
	["FP_ROAM_ORC_12"] = {x = -14679.3203, y = 978.125854, z = -11624.1602, adjacent =
	[
	]},
	["FP_ROAM_ORC_11"] = {x = -14490.1816, y = 972.619019, z = -11987.5654, adjacent =
	[
	]},
	["FP_ROAM_ORC_10"] = {x = -12221.5498, y = 716.719604, z = -12648.1797, adjacent =
	[
	]},
	["FP_ROAM_ORC_09"] = {x = -12477.6582, y = 778.152771, z = -12954.5957, adjacent =
	[
	]},
	["FP_ROAM_ORC_08"] = {x = -11967.0156, y = 763.697937, z = -12713.3428, adjacent =
	[
	]},
	["FP_ROAM_ORC_07"] = {x = -23792.9238, y = 1554.20276, z = -14269.7646, adjacent =
	[
	]},
	["FP_ROAM_ORC_06"] = {x = -22872.6191, y = 1402.33191, z = -14088.6748, adjacent =
	[
	]},
	["FP_ROAM_ORC_05"] = {x = -22997.8926, y = 1415.66223, z = -14367.126, adjacent =
	[
	]},
	["FP_ROAM_ORC_04"] = {x = -2973.78638, y = 512.252686, z = -15724.876, adjacent =
	[
	]},
	["FP_ROAM_ORC_03"] = {x = -3346.94556, y = 723.338318, z = -16245.6455, adjacent =
	[
	]},
	["FP_ROAM_ORC_02"] = {x = -2879.63843, y = 531.380127, z = -15448.8936, adjacent =
	[
	]},
	["FP_ROAM_ORC_01"] = {x = -4927.13232, y = 1185.5105, z = -16689.1582, adjacent =
	[
	]},
	["FP_ROAM_A_OW_OC_NC_AMBIENTNSC3"] = {x = -4526.56982, y = -1159.76172, z = 7812.21777, adjacent =
	[
	]},
	["FP_SMALLTALK_A_START_PATH_1_5_2"] = {x = -5574.19238, y = -908.582275, z = 15257.2432, adjacent =
	[
	]},
	["FP_ROAM_OW_BLACKWOLF_02_22"] = {x = -12353.4111, y = 764.138062, z = -12935.9307, adjacent =
	[
	]},
	["FP_ROAM_OW_BLACKWOLF_02_21"] = {x = -12485.9678, y = 830.76239, z = -13389.6963, adjacent =
	[
	]},
	["FP_ROAM_OW_BLACKWOLF_02_20"] = {x = -11225.7734, y = 1098.90784, z = -13796.4219, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_CAVE1_4"] = {x = -27355.3652, y = -638.456848, z = 9338.48926, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_CAVE1_1"] = {x = -27393.2656, y = -655.720398, z = 9928.79004, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_CAVE1_2"] = {x = -26853.2871, y = -533.93866, z = 9934.86719, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_CAVE1_3"] = {x = -26854.9766, y = -466.87146, z = 9274.69922, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_A_6_NC7"] = {x = -33579.2461, y = 684.874329, z = 15154.4775, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERATS_WOOD_OM5"] = {x = -23823.209, y = -170.152664, z = 21016.7578, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERATS_WOOD_OM4"] = {x = -23636.6367, y = -200.793076, z = 20430.8145, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERATS_WOOD_OM3"] = {x = -24232.3086, y = -243.689651, z = 20699.9805, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERATS_WOOD_OM2"] = {x = -23364.8887, y = -193.444641, z = 20695.4785, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERATS_WOOD_OM"] = {x = -24005.9258, y = -237.526733, z = 20502.9531, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_WOOD10_05"] = {x = -17475.3828, y = -23.5918846, z = 19285.6074, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_WOOD10_04"] = {x = -17593.2266, y = -8.97551155, z = 19614.2305, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_WOOD10_03"] = {x = -18432.2578, y = -62.8733902, z = 19122.8711, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_WOOD10_02"] = {x = -17898.9473, y = 5.64086103, z = 19975.5586, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_WOOD10_01"] = {x = -17836.25, y = -67.4399033, z = 19032.7988, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_01_DEMONT5"] = {x = -4301.35938, y = 2606.93457, z = -30076.9941, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_01_DEMONT3"] = {x = -3412.47461, y = 2831.07837, z = -30458.8047, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_01_DEMONT2"] = {x = -3504.49512, y = 2873.38013, z = -30685.541, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_01_DEMONT"] = {x = -4363.06982, y = 2724.38037, z = -30525.1309, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_NC_02"] = {x = -39825.6484, y = 321.702667, z = 1212.19983, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_NC_03"] = {x = -40416.0547, y = 385.287872, z = 1259.69507, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_NC_04"] = {x = -40590.4023, y = 234.215729, z = 538.082214, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_NC_05"] = {x = -40085.1367, y = 270.221344, z = 609.7901, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_RIVER2_BEACH6"] = {x = 12955.7471, y = -1432.05603, z = 13421.126, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_RIVER2_BEACH5"] = {x = 12748.293, y = -1405.92029, z = 13940.1279, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_RIVER2_BEACH4"] = {x = 12519.251, y = -1382.4447, z = 13517.6543, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_RIVER2_BEACH3"] = {x = 11690.7783, y = -1435.77356, z = 14438.5771, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_RIVER2_BEACH2"] = {x = 11367.6104, y = -1405.83105, z = 14115.749, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_RIVER2_BEACH"] = {x = 11173.499, y = -1398.03357, z = 14392.7813, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_BANDIT_02_2"] = {x = -14276.6865, y = -761.917236, z = 7721.70313, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_BANDIT_02"] = {x = -13699.0342, y = -809.059814, z = 7512.6001, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_OW_ORC_01"] = {x = -711.269531, y = 201.901031, z = -15089.5313, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_OW_ORC1"] = {x = -1077.29224, y = 276.005096, z = -15320.5342, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_OW_ORC3"] = {x = -1585.58911, y = 340.00412, z = -15356.7949, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_OW_ORC2"] = {x = -1372.32031, y = 295.935425, z = -15535.4756, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_AL_ORC4"] = {x = -10730.3311, y = -701.607849, z = -5579.17236, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_AL_ORC3"] = {x = -10238.375, y = -677.607849, z = -5537.04639, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_AL_ORC2"] = {x = -10755.0605, y = -677.607849, z = -6009.04736, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_AL_ORC"] = {x = -10145.4863, y = -661.607849, z = -5965.07715, adjacent =
	[
	]},
	["FP_ROAM_TOTURIAL_CHICKEN_2"] = {x = 93.8484344, y = 2670.46924, z = 21099.2988, adjacent =
	[
	]},
	["FP_ROAM_OW_MAETBUG_01_01"] = {x = -5072.42725, y = -1078.2384, z = 14448.5469, adjacent =
	[
	]},
	["FP_ROAM_OW_STARTSCAVENGER_02_02"] = {x = -9799.31934, y = -822.391235, z = 11422.4697, adjacent =
	[
	]},
	["FP_ROAM_OW_STARTSCAVENGER_02_01"] = {x = -9384.15918, y = -745.492798, z = 11124.4209, adjacent =
	[
	]},
	["FP_ROAM_OW_STARTSCAVNGERBO_01_02"] = {x = -8139.95313, y = -1172.84534, z = 12774.5244, adjacent =
	[
	]},
	["FP_ROAM_OW_STARTSCAVNGERBO_01_01"] = {x = -8633.18457, y = -1185.47766, z = 12540.9463, adjacent =
	[
	]},
	["FP_ROAM_O_SCAVENGER_OCWOODL3"] = {x = 7848.43506, y = -1244.71936, z = 8263.76172, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERATT_04_PSI6"] = {x = 18567.1406, y = 1933.7373, z = -12388.5537, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERATT_04_PSI1"] = {x = 18001.7227, y = 1905.68237, z = -12074.666, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_LONE_WALD_OC3"] = {x = 15129.6426, y = 39.3730927, z = 5023.77295, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_LONE_WALD_OC2"] = {x = 15443.9795, y = 49.1782837, z = 5092.17773, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_LONE_WALD_OC1"] = {x = 15516.7139, y = 40.8992348, z = 4775.55957, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF2_WALD_OC8"] = {x = 14494.1055, y = 31.012085, z = 5387.625, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF2_WALD_OC6"] = {x = 13959.0811, y = -80.8584671, z = 6091.18652, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF2_WALD_OC4"] = {x = 13423.1465, y = 104.940971, z = 5544.229, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF2_WALD_OC2"] = {x = 13990.2861, y = 103.94101, z = 5072.37158, adjacent =
	[
	]},
	["FP_ROAM_A_OW_OC_NC_AMBIENTNSC2"] = {x = -4140.04785, y = -1164.06262, z = 7510.33643, adjacent =
	[
	]},
	["FP_ROAM_A_OW_OC_NC_AMBIENTNSC"] = {x = -4174.33984, y = -1143.85254, z = 7824.54297, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERATN_PSIWOOD_3"] = {x = 18249.459, y = 1118.53479, z = -4652.25635, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_PSIWOOD_5"] = {x = 16960.3164, y = 992.251282, z = -5906.6748, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_OCWOODEND2"] = {x = -26300.7891, y = -671.348816, z = 19633.7246, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_OCWOODEND"] = {x = -26071.7227, y = -655.690735, z = 19295.9492, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_WATERFALLCAVE_5"] = {x = -28683.625, y = -585.208923, z = 14914.7188, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_WATERFALLCAVE_4"] = {x = -29125.9277, y = -500.490845, z = 15079.0303, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_WATERFALLCAVE_3"] = {x = -29183.998, y = -566.573425, z = 14708.3223, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_WATERFALLCAVE_2"] = {x = -28798.0137, y = -490.573792, z = 15214.3398, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_WATERFALLCAVE_1"] = {x = -29228.1914, y = -399.575653, z = 15419.3438, adjacent =
	[
	]},
	["FP_ROAM_O_SCAVENGER_OCWOODL2"] = {x = 7697.2583, y = -1257.91431, z = 7532.6665, adjacent =
	[
	]},
	["FP_ROAM_O_SCAVENGER_OCWOODL"] = {x = 8828.23535, y = -1312.74817, z = 7946.17822, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_OLDWOOD1_M3"] = {x = -13108.6572, y = -709.05072, z = 15260.6064, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_OLDWOOD1_M2"] = {x = -13269.2861, y = -672.606506, z = 14757.834, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_OLDWOOD1_M"] = {x = -12888.6514, y = -651.016724, z = 14862.1982, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_OCWOOD5"] = {x = -16460.6191, y = -500.226929, z = 13529.0195, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_OCWOOD4"] = {x = -16356.5859, y = -476.225281, z = 12790.1025, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_OCWOOD3"] = {x = -15866.9131, y = -572.224121, z = 13612.415, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_OCWOOD2"] = {x = -15749.8477, y = -572.224121, z = 12945.6934, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_OCWOOD"] = {x = -15856.8223, y = -522.220703, z = 12533.9043, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_OCWOOD_OC5"] = {x = -19099.0449, y = -433.88089, z = 13483.9707, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_OCWOOD_OC4"] = {x = -19553.8672, y = -401.603271, z = 13048.957, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_OCWOOD_OC3"] = {x = -19275.9238, y = -417.254639, z = 12984.6406, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_OCWOOD_OC1"] = {x = -19014.8867, y = -432.906006, z = 13148.5801, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_OCWOOD_OC2"] = {x = -19608.666, y = -385.862183, z = 13485.4766, adjacent =
	[
	]},
	["FP_ROAM_OW_BLACKWOLF_02_01"] = {x = -11805.5957, y = 894.43988, z = -13706.8252, adjacent =
	[
	]},
	["FP_ROAM_OW_BLACKWOLF_02_02"] = {x = -12260.125, y = 931.208008, z = -14156.627, adjacent =
	[
	]},
	["FP_ROAM_OW_BLACKWOLF_02_05"] = {x = -11702.6094, y = 994.279785, z = -14064.627, adjacent =
	[
	]},
	["FP_ROAM_OW_BLACKWOLF_02_06"] = {x = -12535.8691, y = 849.674194, z = -13663.3984, adjacent =
	[
	]},
	["FP_SMALLTALK_A_START_PATH_GUARD1"] = {x = -12247.3643, y = 39.5637894, z = -8700.75195, adjacent =
	[
	]},
	["FP_SMALLTALK_A_START_PATH_GUARD2"] = {x = -11936.1592, y = 14.5588531, z = -8829.55469, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_WOOD05_05"] = {x = -20959.7559, y = -195.286865, z = 15498.2998, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_WOOD05_04"] = {x = -21223.6582, y = -202.587509, z = 15313.8643, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_WOOD05_02"] = {x = -21472.9707, y = -231.827927, z = 14978.042, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_WOOD05_03"] = {x = -20997.7246, y = -202.578217, z = 15071.8691, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_WOOD05_05"] = {x = -20751.3066, y = -268.307434, z = 17686.9668, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_WOOD05_04"] = {x = -20944.1719, y = -192.540237, z = 18563.3418, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_WOOD05_03"] = {x = -21092.5898, y = -317.775299, z = 17561.0547, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_WOOD05_02"] = {x = -21563.0469, y = -223.845947, z = 18194.5938, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_WOOD05_01"] = {x = -21334.5293, y = -169.020309, z = 18592.4473, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_WOOD05_01"] = {x = -16420.9609, y = -957.633545, z = 16830.9941, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_WOOD05_02"] = {x = -16864.9023, y = -933.688721, z = 17051.4121, adjacent =
	[
	]},
	["FP_SLEEP_DEADWOOD_WOLF_02"] = {x = -23849.707, y = 436.903381, z = -4186.83447, adjacent =
	[
	]},
	["FP_SLEEP_DEADWOOD_WOLF_04"] = {x = -23590.1699, y = 465.337402, z = -4124.17432, adjacent =
	[
	]},
	["FP_SLEEP_DEADWOOD_WOLF_05"] = {x = -23301.0605, y = 447.512329, z = -3964.76831, adjacent =
	[
	]},
	["FP_ROAM_DEADWOOD_WOLF_05"] = {x = -23066.1563, y = 497.032654, z = -4657.07666, adjacent =
	[
	]},
	["FP_ROAM_DEADWOOD_WOLF_04"] = {x = -24119.9121, y = 481.18927, z = -4874.73291, adjacent =
	[
	]},
	["FP_ROAM_DEADWOOD_WOLF_03"] = {x = -23515.7305, y = 468.315521, z = -4478.47803, adjacent =
	[
	]},
	["FP_ROAM_DEADWOOD_WOLF_02"] = {x = -23965.9316, y = 504.956268, z = -5162.70752, adjacent =
	[
	]},
	["FP_ROAM_DEADWOOD_WOLF_01"] = {x = -23510.3164, y = 497.395996, z = -4988.18652, adjacent =
	[
	]},
	["FP_SLEEP_OW_LURKER_NC_LAKE_02"] = {x = -34578.2266, y = -5.87207794, z = -6617.47363, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_NC_LAKE_03"] = {x = -35993.2617, y = -20.3164482, z = -6770.65918, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_NC_LAKE_02"] = {x = -35553.5859, y = -33.4089203, z = -6217.85547, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_NC_LAKE_01"] = {x = -34676.9883, y = -46.4121513, z = -6106.07227, adjacent =
	[
	]},
	["FP_ROAM_WATER_BLOODFLY_04_05"] = {x = -31931.3477, y = -91.2006302, z = -2777.46387, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_07_02"] = {x = -19290.1836, y = -1002.30536, z = 7435.74072, adjacent =
	[
	]},
	["FP_CAMPFIRE_CAVALORN_01"] = {x = -14428.8184, y = -1366.51514, z = 2818.4375, adjacent =
	[
	]},
	["FP_SLEEP_OW_SCAVENGER_TREE2"] = {x = -17499.541, y = -781.629517, z = 3410.93726, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_CAVE5"] = {x = -19442.4102, y = -769.413879, z = 2679.42383, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_CAVE4"] = {x = -18918.1855, y = -781.657837, z = 2625.51221, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_CAVE3"] = {x = -19554.7852, y = -691.965454, z = 2927.29541, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_CAVE2"] = {x = -19083.0215, y = -852.839905, z = 2077.09326, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_CAVE"] = {x = -19887.9961, y = -798.911499, z = 2426.43872, adjacent =
	[
	]},
	["FP_SLEEP_OW_SCAVENGER_AL_NL_02"] = {x = -13345.9639, y = -1952.24536, z = 473.281281, adjacent =
	[
	]},
	["FP_SLEEP_OW_SCAVENGER_AL_NL_01"] = {x = -13747.1328, y = -1921.60327, z = 398.734436, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_AL_NL_03"] = {x = -13111.6641, y = -2021.45703, z = -3.0834341, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_AL_NL_02"] = {x = -13763.1699, y = -1950.87158, z = 149.924957, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_AL_NL_01"] = {x = -13290.4941, y = -1976.05896, z = 196.813385, adjacent =
	[
	]},
	["FP_SLEEP_OW_WARAN_G"] = {x = -31391.4004, y = 96.9062195, z = 1251.30115, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_G2"] = {x = -30735.3301, y = -43.6906967, z = 1505.62683, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_G"] = {x = -30662.6211, y = -17.7986832, z = 1085.26526, adjacent =
	[
	]},
	["FP_ROAM_OW_GREENGOBBO_SAWHUT_4"] = {x = -15388.6396, y = -1376.18701, z = 3181.52563, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_16_03"] = {x = -19716.7559, y = -1322, z = -5380.09229, adjacent =
	[
	]},
	["FP_ROAM_SWAMP_16_02"] = {x = -19618.4922, y = -1312, z = -5229.63623, adjacent =
	[
	]},
	["FP_ROAM_OW_GREEGOBBO_SAWHUT_3"] = {x = -14977.0146, y = -1353, z = 2887.44214, adjacent =
	[
	]},
	["FP_ROAM_OW_GREEGOBBO_SAWHUT_2"] = {x = -15317.0166, y = -1323.78442, z = 3483.05591, adjacent =
	[
	]},
	["FP_ROAM_OW_GREEGOBBO_SAWHUT"] = {x = -14960.7227, y = -1350.21558, z = 3324.11646, adjacent =
	[
	]},
	["FP_SMALLTALK_A_START_PATH_1_5_1"] = {x = -5687.85303, y = -916.09729, z = 15211.8027, adjacent =
	[
	]},
	["FP_ROAM_A_START_01"] = {x = 3667.33936, y = 6485.11963, z = 32197.9746, adjacent =
	[
	]},
	["FP_GUARDPASSAGE_A_OW_02"] = {x = -11333.6279, y = -919.759949, z = 10469.46, adjacent =
	[
	]},
	["FP_GUARDPASSAGE_A_OW_01"] = {x = -10754.0957, y = -909.531799, z = 10531.4941, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_10_10"] = {x = -30366.1875, y = 2498.01758, z = -14907.1133, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_10_09"] = {x = -31335, y = 2062, z = -14162, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_10_08"] = {x = -26469, y = 1856, z = -13758, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_10_06"] = {x = -29791, y = 2534, z = -14616, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_10_05"] = {x = -30177.3906, y = 2500.10474, z = -14351.1318, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_10_02"] = {x = -29073, y = 2425, z = -14148, adjacent =
	[
	]},
	["FP_SLEEP_OW_SHADOWBEAST_10_03"] = {x = -28896.084, y = 2398.10474, z = -13698.4561, adjacent =
	[
	]},
	["FP_SLEEP_OW_SCAVENGER_10_01"] = {x = -29346, y = 2322, z = -13488, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_03_05"] = {x = -42204.9688, y = 189.587433, z = 33.1753578, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_03_04"] = {x = -41960, y = 269, z = -200, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_03_03"] = {x = -42747, y = 185, z = -63, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_03_02"] = {x = -42344, y = 318, z = -843, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_03_01"] = {x = -43057.8164, y = 84.178833, z = -916.167297, adjacent =
	[
	]},
	["FP_SLEEP_OW_MOLERAT_03_02"] = {x = -43134, y = 31, z = -1499, adjacent =
	[
	]},
	["FP_SLEEP_OW_MOLERAT_03_01"] = {x = -43003, y = 0, z = -2101, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_09_06"] = {x = -38623.2617, y = 11.9190769, z = -1700.33252, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_09_05"] = {x = -38345.9727, y = 104.42775, z = -992.997864, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_09_03"] = {x = -37751.3633, y = 27.2274532, z = -1180.00085, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_09_02"] = {x = -38374.7695, y = 108, z = -2189.10815, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_09_01"] = {x = -38220.1367, y = 40.491333, z = -1427.71643, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_05_07"] = {x = -40546.9453, y = 690.683167, z = 9854.30859, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_05_06"] = {x = -41800.6992, y = 647.596313, z = 8924.15039, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_05_05"] = {x = -40005.4609, y = 618.760803, z = 9245.03125, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_05_04"] = {x = -41242.8945, y = 706.221985, z = 9046.9541, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_05_03"] = {x = -41189.4258, y = 700.716797, z = 9731.16895, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_05_02"] = {x = -41457.8477, y = 681.284607, z = 8530.98145, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_05_01"] = {x = -41873.7695, y = 674.316223, z = 8334.61133, adjacent =
	[
	]},
	["FP_SLEEP_OW_BLOODFLY_05_03"] = {x = -40616.1055, y = 659.232483, z = 9163.68262, adjacent =
	[
	]},
	["FP_SLEEP_OW_BLOODFLY_05_02"] = {x = -41111.6875, y = 669.696411, z = 8195.29004, adjacent =
	[
	]},
	["FP_SLEEP_OW_BLOODFLY_05_01"] = {x = -40601.3125, y = 665.965454, z = 8589.2793, adjacent =
	[
	]},
	["FP_SLEEP_OW_BLOODFLY_04_02"] = {x = -31711.8672, y = 81.618576, z = -3171.57056, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_04_05"] = {x = -30377, y = 453.999176, z = -3165, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_04_04"] = {x = -30676, y = 406, z = -3956, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_04_03"] = {x = -31037, y = 346, z = -3357, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_04_02"] = {x = -31801, y = 110, z = -3514, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_04_01"] = {x = -32013.4316, y = -95.0097122, z = -3155.40601, adjacent =
	[
	]},
	["FP_OW_BLOODFLY04_SPAWN01"] = {x = -31386, y = 283, z = -3104, adjacent =
	[
	]},
	["FP_ROAM_OW_MEATBUG_04_03"] = {x = -8304, y = -1231, z = 7584, adjacent =
	[
	]},
	["FP_ROAM_OW_MEATBUG_04_02"] = {x = -8290.34961, y = -1314.96558, z = 6917.98047, adjacent =
	[
	]},
	["FP_ROAM_OW_MEATBUG_04_01"] = {x = -8820.3584, y = -1359.81091, z = 6372.41748, adjacent =
	[
	]},
	["FP_ROAM_OW_MEATBUG_03_03"] = {x = -10266.958, y = -1164.64673, z = 6666.89844, adjacent =
	[
	]},
	["FP_SLEEP_OW_MEATBUG_03_01"] = {x = -10418.3389, y = -1122.32104, z = 6401.46826, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_01_04"] = {x = -10348, y = -384, z = 18138, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_01_01"] = {x = -11054.0967, y = -219.389206, z = 18281.1289, adjacent =
	[
	]},
	["FP_SLEEP_OW_SCAVENGER_01_01"] = {x = -10849, y = -180, z = 18991, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_04_10"] = {x = 21161, y = 258, z = 30812, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_04_05"] = {x = 21070, y = -218, z = 28097, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_03_05"] = {x = 20934, y = -1744, z = 21573, adjacent =
	[
	]},
	["ENTRANCE_SURFACE_ABANDONEDMINE"] = {x = 4567.80371, y = 5934, z = 27783.8711, adjacent =
	[
	]},
	["FP_CONVINCECORRISTO_KEY"] = {x = -32093.4688, y = -478.069946, z = 14188.9355, adjacent =
	[
	]},
	["FP_CONVINCECORRISTO_SCROLL"] = {x = -31956.3066, y = -502.069946, z = 14222.1006, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_02_05"] = {x = -15479.3398, y = 995.708374, z = -13623.9824, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_02_06"] = {x = -16363.0938, y = 1008.58691, z = -13650, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_02_11"] = {x = -17787.4512, y = 940.880371, z = -14654.3389, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_02_12"] = {x = -17340.252, y = 962.646729, z = -13938.1465, adjacent =
	[
	]},
	["FP_SLEEP_OW_SNAPPER_02_01"] = {x = -6290.15137, y = 1859.74048, z = -26324.7188, adjacent =
	[
	]},
	["FP_SLEEP_OW_SNAPPER_02_03"] = {x = -6598, y = 1784, z = -26550, adjacent =
	[
	]},
	["FP_SLEEP_OW_SNAPPER_02_04"] = {x = -6198, y = 1924, z = -26770, adjacent =
	[
	]},
	["FP_SLEEP_OW_SNAPPER_02_05"] = {x = -6178, y = 1944, z = -27190, adjacent =
	[
	]},
	["FP_SLEEP_OW_SNAPPER_02_06"] = {x = -6458, y = 1924, z = -27250, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_02_01"] = {x = -7042.20117, y = 1664, z = -26517.9258, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_02_02"] = {x = -8238, y = 1530.0144, z = -26830, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_02_04"] = {x = -7393.88916, y = 1505.85425, z = -26002.3867, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_02_05"] = {x = -7440, y = 1717, z = -26733, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_02_06"] = {x = -6967.37354, y = 1837, z = -27120.8789, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_02_07"] = {x = -6773, y = 1917.24097, z = -27822, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_02_08"] = {x = -7773, y = 1657, z = -27802, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_02_10"] = {x = -7273, y = 1903.44641, z = -28322, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_02_11"] = {x = -6678.60889, y = 1857, z = -27052.8633, adjacent =
	[
	]},
	["FP_ROAM_OW_MEATBUG_01_05"] = {x = 4372, y = 6122, z = 31079, adjacent =
	[
	]},
	["FP_ROAM_OW_MEATBUG_01_01"] = {x = 4165.43408, y = 6080, z = 30503.0547, adjacent =
	[
	]},
	["FP_ROAM_OW_MEATBUG_01_03"] = {x = 4150, y = 6122, z = 31069, adjacent =
	[
	]},
	["FP_ROAM_OW_MEATBUG_01_04"] = {x = 4428, y = 6074.70703, z = 29895, adjacent =
	[
	]},
	["FP_SLEEP_OW_MEATBUG_02_04"] = {x = 4790.29004, y = 5947, z = 27768.834, adjacent =
	[
	]},
	["FP_ROAM_OW_MEATBUG_02_02"] = {x = 4495.19434, y = 5947, z = 28197.8359, adjacent =
	[
	]},
	["FP_ROAM_OW_MEATBUG_02_04"] = {x = 4709.59814, y = 5946.66992, z = 27564.2422, adjacent =
	[
	]},
	["FP_ROAM_OW_MEATBUG_02_07"] = {x = 3974, y = 5773.84717, z = 27332, adjacent =
	[
	]},
	["FP_ROAM_OW_MEATBUG_02_05"] = {x = 3784, y = 5770.12109, z = 27693, adjacent =
	[
	]},
	["FP_ROAM_OW_MEATBUG_02_06"] = {x = 3357, y = 5607, z = 27318, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_05_01"] = {x = 4537.10742, y = -1458.58179, z = 11232.293, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_05_03"] = {x = 5947.71338, y = -1472.05505, z = 11437.876, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_05_05"] = {x = 6511.95557, y = -1433.11072, z = 12364.0752, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_05_02"] = {x = 8867.36035, y = -1258.48438, z = 9796.58594, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_05_04"] = {x = 7281.63623, y = -1325.10962, z = 9192.57031, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_05_06"] = {x = 8627.19922, y = -1492.92615, z = 11681.3262, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_05_07"] = {x = 7821.30811, y = -1527.29858, z = 11124.75, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_05_08"] = {x = 7411.68555, y = -1294.32532, z = 9585.2959, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_05_09"] = {x = 9643.00586, y = -1432.77844, z = 11596.748, adjacent =
	[
	]},
	["FP_SLEEP_OW_BLOODFLY_01_01"] = {x = 7835.45313, y = -1293.75159, z = 14441.6133, adjacent =
	[
	]},
	["FP_SLEEP_OW_BLOODFLY_01_02"] = {x = 7231, y = -1287, z = 14671, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_01_01"] = {x = 8392.20703, y = -1361.13159, z = 14840.459, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_01_02"] = {x = 9340, y = -1369, z = 14887, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_01_03"] = {x = 8364.18945, y = -1406.72375, z = 14285.2529, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_01_04"] = {x = 10288.7285, y = -1390.93274, z = 15003.4355, adjacent =
	[
	]},
	["FP_SLEEP_OW_SCAVENGER_03_01"] = {x = 18830.9141, y = -229.657562, z = 5705.26611, adjacent =
	[
	]},
	["FP_SLEEP_OW_SCAVENGER_03_02"] = {x = 19012.4805, y = -397.788025, z = 6572.23047, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_03_01"] = {x = 18146.9824, y = -100.474609, z = 4756.57031, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_03_02"] = {x = 19207.7656, y = -210.670898, z = 5215.12012, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_03_03"] = {x = 19237.0098, y = -246.670898, z = 5713.54492, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_03_04"] = {x = 19604, y = -483, z = 6884, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_03_05"] = {x = 18668.8477, y = -161.990662, z = 4409.69434, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_01_05"] = {x = -8432, y = -559, z = 18775, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_01_06"] = {x = -8775.2373, y = -627.893066, z = 17913.125, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_01_07"] = {x = -9339, y = -606, z = 18515, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_01_01"] = {x = 8463.24609, y = 7412.9624, z = 23083.1504, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_01_06"] = {x = 8965.47852, y = 7490.96436, z = 22643.9121, adjacent =
	[
	]},
	["FP_SLEEP_OW_MOLERAT_01_01"] = {x = 3226, y = -776, z = -12600, adjacent =
	[
	]},
	["FP_SLEEP_OW_MOLERAT_01_02"] = {x = 2604, y = -812, z = -12550, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_01_01"] = {x = 3385.99658, y = -743, z = -11724.5811, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_01_02"] = {x = 3707.78662, y = -783, z = -10962.5615, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_01_03"] = {x = 3301.3418, y = -790.241821, z = -11050.999, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_01_04"] = {x = 4450.92871, y = -668.310913, z = -10399.3779, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_01_05"] = {x = 3387.01025, y = -661.513184, z = -10015.0059, adjacent =
	[
	]},
	["FP_OW_GOBBO01_SPAWN01"] = {x = 8443.28906, y = 7502.95996, z = 23614.9727, adjacent =
	[
	]},
	["FP_SLEEP_OW_MOLERAT_02_01"] = {x = -187, y = 6089, z = 26836, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_02_01"] = {x = -61.8633385, y = 6149, z = 27257.3906, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_02_03"] = {x = 1556.50989, y = 6149.12598, z = 27098.332, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_06_01"] = {x = -6822.89941, y = -555.730164, z = -8468.85742, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_06_02"] = {x = -6272.61035, y = -495.210388, z = -9131.95313, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_06_03"] = {x = -6084.54932, y = -652.282654, z = -8348.64844, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_06_04"] = {x = -6871.02051, y = -446.69574, z = -8898.82129, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_06_05"] = {x = -7278.48438, y = -597.130371, z = -8112.66357, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_06_06"] = {x = -7985.81689, y = -467.365723, z = -8248.17969, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_06_07"] = {x = -6592.80176, y = -449.579926, z = -9100.5752, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_07_01"] = {x = -8081, y = 1803, z = -15982, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_07_02"] = {x = -7891, y = 1738, z = -15250, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_07_03"] = {x = -7279, y = 1738, z = -15787, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_07_04"] = {x = -6360, y = 1618, z = -16449, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_07_05"] = {x = -7284, y = 1964, z = -16880, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_07_06"] = {x = -9033, y = 1930, z = -16505, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_07_07"] = {x = -11127.1621, y = 1864.52258, z = -17739.6426, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_07_08"] = {x = -8136, y = 2103, z = -17565, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_07_09"] = {x = -6478, y = 1986, z = -17426, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_07_11"] = {x = -11262, y = 1809, z = -17312, adjacent =
	[
	]},
	["FP_ROAM_INTRO_CHICKEN_1"] = {x = 2241.3252, y = 2544.00269, z = 20852.2539, adjacent =
	[
	]},
	["FP_ROAM_INTRO_CHICKEN_2"] = {x = 2978.95337, y = 2518.52783, z = 20820.9805, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_07_02"] = {x = -6938.48535, y = -374.050446, z = 17919.209, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_07_03"] = {x = -7235.1958, y = -257.816254, z = 18435.375, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_07_04"] = {x = -7834.92822, y = -503.857147, z = 18243.584, adjacent =
	[
	]},
	["FP_SLEEP_OW_SCA_05_01"] = {x = -12964.6348, y = -591.864258, z = 12328.3525, adjacent =
	[
	]},
	["FP_ROAM_OW_SCA_05_01"] = {x = -12619.0527, y = -668.223145, z = 11645.8096, adjacent =
	[
	]},
	["FP_ROAM_OW_SCA_05_02"] = {x = -12720.6611, y = -585.242371, z = 11999.6309, adjacent =
	[
	]},
	["FP_ROAM_OW_SCA_05_03"] = {x = -12158.8447, y = -714.36908, z = 12419.8848, adjacent =
	[
	]},
	["FP_SLEEP_OW_BLOODFLY_06_01"] = {x = -15164.1924, y = -486.837219, z = 11245.4004, adjacent =
	[
	]},
	["FP_SLEEP_OW_BLOODFLY_06_02"] = {x = -14851.4932, y = -445.462067, z = 11352.4053, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_06_01"] = {x = -14988.4023, y = -500.082947, z = 10997.3271, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_06_02"] = {x = -14623.8623, y = -510.354797, z = 10749.9512, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_06_03"] = {x = -14793.5137, y = -647.006958, z = 10203.2041, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_06_04"] = {x = -15391, y = -651, z = 10628, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_06_05"] = {x = -14259.8242, y = -599.808472, z = 10367.6777, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_06_06"] = {x = -15714, y = -541.647034, z = 11242, adjacent =
	[
	]},
	["FP_SLEEP_OW_SCAVENGER_12_02"] = {x = -28920.332, y = -305.510406, z = 17420.3926, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_12_01"] = {x = -29606.8711, y = -331.072632, z = 18054.8984, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_12_02"] = {x = -29212.7402, y = -445.546082, z = 17899.1035, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_12_03"] = {x = -28807.457, y = -369.498474, z = 17658.3652, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_12_04"] = {x = -28896.7461, y = -423.179138, z = 18205.9551, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_12_05"] = {x = -29057.752, y = -529.471497, z = 18484.6953, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_12_06"] = {x = -29411.3672, y = -227.423859, z = 17475.627, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_12_07"] = {x = -28571.9316, y = -316.308807, z = 17356.4512, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_08_01"] = {x = -15137.3848, y = -454.266235, z = 19193.6992, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_08_02"] = {x = -13443.1572, y = -42.5728607, z = 19894.5742, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_08_04"] = {x = -13395.458, y = -12.3996515, z = 18326.3613, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_08_05"] = {x = -14043.0938, y = -117.731071, z = 19169.8066, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_08_06"] = {x = -14124.54, y = -63.7370949, z = 18468.0527, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_08_07"] = {x = -14602.1045, y = -57.5750427, z = 18246.418, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_08_08"] = {x = -14523.5352, y = -137.465927, z = 18776.7734, adjacent =
	[
	]},
	["FP_SLEEP_OW_WOLF_08_03"] = {x = -15578.2256, y = -592.55896, z = 19357.9844, adjacent =
	[
	]},
	["FP_SLEEP_OW_WOLF_08_01"] = {x = -15207.917, y = -501.780029, z = 19812.2734, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_OCWOOD_OLDMINE"] = {x = -19889.0254, y = -1020.30969, z = 18401.957, adjacent =
	[
	]},
	["FP_SLEEP_OW_SCAVENGER_13_01"] = {x = -30432, y = 904, z = 9462, adjacent =
	[
	]},
	["FP_SLEEP_OW_SCAVENGER_13_02"] = {x = -30800.1289, y = 911, z = 9165.37207, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_13_01"] = {x = -30354, y = 846, z = 8842, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_13_02"] = {x = -31873.3164, y = 930.383301, z = 9264.7998, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_13_03"] = {x = -31196, y = 848, z = 8705, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_13_04"] = {x = -31702.9941, y = 983.427917, z = 9779.22656, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_13_05"] = {x = -31076.7168, y = 921.875427, z = 9641.9502, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_13_06"] = {x = -30784.5938, y = 880.766602, z = 10236.8633, adjacent =
	[
	]},
	["FP_SLEEP_OW_BLOODFLY_07_01"] = {x = -19332.1465, y = -856.794556, z = 7091.28418, adjacent =
	[
	]},
	["FP_SLEEP_OW_BLOODFLY_07_02"] = {x = -18909.9102, y = -970.050354, z = 7237.76563, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_07_01"] = {x = -19586.1719, y = -1011.9104, z = 7517.56982, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_07_03"] = {x = -19098, y = -883, z = 7190, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_07_04"] = {x = -18938.7988, y = -1080.32886, z = 7486.08936, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_07_05"] = {x = -18669.0488, y = -946.66394, z = 7142.83887, adjacent =
	[
	]},
	["FP_SLEEP_OW_WARAN_06_01"] = {x = -27406.7441, y = 624.3396, z = -4748.97803, adjacent =
	[
	]},
	["FP_SLEEP_OW_WARAN_06_02"] = {x = -28027.5859, y = 701.233459, z = -4748.69287, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_06_05"] = {x = -29200.125, y = 817.988037, z = -4737.07568, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_06_04"] = {x = -28652.1934, y = 726.158142, z = -4357.87158, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_06_03"] = {x = -29504.2207, y = 861.230225, z = -5434.84814, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_06_02"] = {x = -28547.8828, y = 767.899475, z = -4879.81787, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_06_01"] = {x = -29000.459, y = 820.5578, z = -5349.10449, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_WALD_OC"] = {x = 14649.1113, y = -115.468498, z = 7011.32861, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_WALD_OC2"] = {x = 15436.832, y = -83.026268, z = 7472.54053, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_WALD_OC3"] = {x = 15426.9492, y = -34.3629227, z = 6754.28955, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_WALD_OC4"] = {x = 15888.8857, y = -20.1827145, z = 6916.63037, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_WALD_OC5"] = {x = 15563.7422, y = -34.2604179, z = 6078.60059, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_SAWHUT"] = {x = -15994.5088, y = -1282.3894, z = 5715.24561, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_SAWHUT2"] = {x = -16396.0762, y = -1274.3894, z = 5936.35303, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_SAWHUT3"] = {x = -16310.4951, y = -1275.3894, z = 6235.53564, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_SAWHUT4"] = {x = -15997.0215, y = -1327.88354, z = 5988.84229, adjacent =
	[
	]},
	["FP_SLEEP_OW_MOLERAT_SWAHUT1"] = {x = -16950.7793, y = -1249.61548, z = 6565.03027, adjacent =
	[
	]},
	["FP_SLEEP_OW_MOLERAT_SWAHUT"] = {x = -16939.9199, y = -1281.61548, z = 6224.09766, adjacent =
	[
	]},
	["FP_ROAM_OW_MEATBUG_OLDWOOD_3"] = {x = -14725.6162, y = -1282.24438, z = 4074.78931, adjacent =
	[
	]},
	["FP_ROAM_OW_MEATBUG_OLDWOOD"] = {x = -14968.3076, y = -1289.83997, z = 4157.79443, adjacent =
	[
	]},
	["FP_ROAM_OW_MEATBUG_OLDWOOD_2"] = {x = -14903.9199, y = -1296.44336, z = 3997.7395, adjacent =
	[
	]},
	["FP_CAMPFIRE_OW_WOLF_WOODRUIN"] = {x = -26174.7344, y = 26.4260521, z = 1131.83411, adjacent =
	[
	]},
	["FP_CAMPFIRE_OW_WOLF_WOODRUIN2_1"] = {x = -25537.2637, y = -248.974731, z = 2618.89404, adjacent =
	[
	]},
	["FP_CAMPFIRE_OW_WOLF_WOODRUIN3"] = {x = -25952.8086, y = 57.1033478, z = 935.2052, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_PLACE"] = {x = -29765.3867, y = 615.517578, z = 3856.47876, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_PLACE2"] = {x = -29928.2734, y = 629.406433, z = 4024.13599, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_PLACE3"] = {x = -29706.2188, y = 621.716003, z = 3661.38574, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_PLACE4"] = {x = -30099.5996, y = 650.315613, z = 3581.48877, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_PLACE5"] = {x = -30125.2148, y = 670.834656, z = 3739.70483, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_CAVE"] = {x = -21132.3066, y = -717.0672, z = 5095.26465, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_CAVE2"] = {x = -20750.6758, y = -679.673279, z = 5173.45654, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_CAVE3"] = {x = -20677.4492, y = -740.232544, z = 5492.80078, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_CAVE4"] = {x = -20673.7871, y = -697.228943, z = 4926.9165, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_CAVE5"] = {x = -20404.7305, y = -727.992004, z = 5141.35303, adjacent =
	[
	]},
	["FP_SLEEP_OW_MOLERAT_CAVE2"] = {x = -21227.0723, y = -759.243286, z = 5344.52832, adjacent =
	[
	]},
	["FP_SLEEP_OW_MOLERAT_CAVE3"] = {x = -21021.166, y = -755.402222, z = 5470.40625, adjacent =
	[
	]},
	["FP_SLEEP_OW_MOLERAT_CAVE4"] = {x = -21362.2598, y = -786.162476, z = 5693.63623, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_TREE"] = {x = -17470.7363, y = -788.765564, z = 3276.57031, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_TREE2"] = {x = -17749.4336, y = -760.511169, z = 3330.58252, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_ENTRANCE_01"] = {x = -15711.6826, y = -1664.28931, z = -1434.97144, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_ENTRANCE_02"] = {x = -15663.6787, y = -1641.53442, z = -1526.59009, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_ENTRANCE_03"] = {x = -15709.8076, y = -1659.66638, z = -1711.71021, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_CAVE_1"] = {x = -19690.7461, y = -596.616089, z = 4130.54541, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_CAVE_2"] = {x = -19729.3281, y = -590.94812, z = 3657.86328, adjacent =
	[
	]},
	["FP_CAMPFIRE_OW_WOLF_WOODRUIN_FOR1"] = {x = -24228.9609, y = -363.104156, z = 2550.71533, adjacent =
	[
	]},
	["FP_CAMPFIRE_OW_WOLF_WOODRUIN_FOR"] = {x = -23955.7988, y = -344.973999, z = 2599.4231, adjacent =
	[
	]},
	["FP_CAMPFIRE_OW_WOLF_WOODRUIN_FOR2"] = {x = -24204.2754, y = -414.601532, z = 2803.81934, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_AL_NL_02_1"] = {x = -11044.417, y = -556.06189, z = 3116.89087, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_AL_NL_02_2"] = {x = -10909.5303, y = -600.930725, z = 3311.18994, adjacent =
	[
	]},
	["FP_SLEEP_OW_SCAVENGER_AL_NL_02_1"] = {x = -11143.1475, y = -546.730347, z = 3343.05835, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_CAVE1_OC"] = {x = -12863.3926, y = -831.016846, z = 5963.17969, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_CAVE1_OC2"] = {x = -12827.668, y = -787.166687, z = 5721.05518, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_CAVE1_OC3"] = {x = -12280.3213, y = -874.173584, z = 6090.25049, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_CAVE1_OC4"] = {x = -12409.1143, y = -890.176758, z = 6380.53223, adjacent =
	[
	]},
	["FP_SLEEP_OW_MOLERAT_CAVE1_OC4"] = {x = -12765.1387, y = -766.977478, z = 5396.04346, adjacent =
	[
	]},
	["FP_SLEEP_OW_MOLERAT_CAVE1_OC1"] = {x = -13071.7051, y = -777.292725, z = 5423.96436, adjacent =
	[
	]},
	["FP_SLEEP_OW_MOLERAT_CAVE1_OC2"] = {x = -13243.8047, y = -806.526489, z = 5701.60791, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_C2"] = {x = -24578.2051, y = -567.22168, z = 14655.0645, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_C_1"] = {x = -25292.9258, y = -695.239014, z = 14672.1094, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_C3"] = {x = -25136.0234, y = -511.221313, z = 15081.0625, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_C4"] = {x = -24832.0195, y = -814.366333, z = 14031.7051, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_C5"] = {x = -25525.332, y = -837.729797, z = 14464.543, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_C6"] = {x = -25839.3457, y = -797.833923, z = 14648.7432, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_A_1"] = {x = -34695.4844, y = 686.108643, z = 11408.5654, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_A_2"] = {x = -33649.3633, y = 705.434204, z = 11459.0332, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_A_3"] = {x = -33936.5547, y = 625.70105, z = 11855.2539, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_A_4"] = {x = -34283.1289, y = 618.618958, z = 11853.9199, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_A_5"] = {x = -33450.4336, y = 647.614319, z = 11750.2471, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_A_6"] = {x = -32954.8281, y = 569.384277, z = 12158.3389, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_E_1"] = {x = -3461.94775, y = -1431.06628, z = 11635.9199, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_E_2"] = {x = -3866.12915, y = -1464.69019, z = 11727.3223, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_E_3"] = {x = -3901.57617, y = -1437.39783, z = 11143.7383, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_E_5"] = {x = -4873.93652, y = -1448.87231, z = 11291.8389, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_E_4"] = {x = -4280.32617, y = -1448.87231, z = 11129.7295, adjacent =
	[
	]},
	["FP_SMALLTALK_A_START_PATH_ERPRESSER"] = {x = -31565.9707, y = 612.355591, z = 5776.58447, adjacent =
	[
	]},
	["FP_SMALLTALK_A_START_PATH_ERPRESSER2"] = {x = -31362.7266, y = 593.29834, z = 5882.88037, adjacent =
	[
	]},
	["FP_ROAM_OW_BLACKGOBBO_NEARCAVE1_1"] = {x = -28279.6426, y = -717.56189, z = 7798.75879, adjacent =
	[
	]},
	["FP_ROAM_OW_BLACKGOBBO_NEARCAVE1_2"] = {x = -28673.1074, y = -894.306396, z = 8271.7998, adjacent =
	[
	]},
	["FP_ROAM_OW_BLACKGOBBO_NEARCAVE1_3"] = {x = -28617.8184, y = -793.385132, z = 7777.4165, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_NEARBGOBBO_1"] = {x = -26617.8633, y = -481.880646, z = 11489.4365, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_NEARBGOBBO_2"] = {x = -26712.8945, y = -586.632324, z = 12104.6885, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_NEARBGOBBO_3"] = {x = -27156.8633, y = -551.068848, z = 11779.4873, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_NEARBGOBBO_4"] = {x = -26222.3379, y = -582.279175, z = 12035.3076, adjacent =
	[
	]},
	["FP_SLEEP_OW_LURKER_NEARBGOBBO_1"] = {x = -27583.0684, y = -547.899231, z = 11777.6387, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_MINICOAST_1"] = {x = -21218.0605, y = -989.146973, z = 10166.1074, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_MINICOAST_2"] = {x = -21713.5215, y = -979.479614, z = 10454.4824, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_MINICOAST_3"] = {x = -22216.8145, y = -911.19696, z = 10204.7559, adjacent =
	[
	]},
	["FP_SLEEP_OW_LURKER_MINICOAST_3"] = {x = -21774.9883, y = -906.155518, z = 9978.08105, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_WALD_OC1"] = {x = 13857.4775, y = 100.591011, z = 4094.2688, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_WALD_OC2"] = {x = 13788.1787, y = 114.691284, z = 3681.08154, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_WALD_OC3"] = {x = 13291.7881, y = 139.627228, z = 3486.63965, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_WALD_OC4"] = {x = 14342.75, y = 105.442291, z = 3881.40527, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_WALD_OC5"] = {x = 14252.9688, y = 149.61203, z = 3359.12158, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_CAVE03_01"] = {x = 20131.0313, y = 858.840698, z = -23348.9355, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_CAVE03_02"] = {x = 19439.3105, y = 1015.15845, z = -22906.5371, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_CAVE03_03"] = {x = 20370.5078, y = 847.801331, z = -22693.8086, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_CAVE03_04"] = {x = 20552.4219, y = 983.613525, z = -23396.6621, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_CAVE03_05"] = {x = 20424.5645, y = 1011.97485, z = -24294.6797, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_OCWOOD_OLDMINE2"] = {x = -20324.8887, y = -1074.82825, z = 18431.832, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_OCWOOD_OLDMINE3"] = {x = -20135.877, y = -1034.23047, z = 18579.9883, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_OCWOOD_OLDMINE4"] = {x = -20504.7363, y = -1083.38342, z = 18165.1719, adjacent =
	[
	]},
	["FP_SMALLTALK_A_TALL_PATH_BANDITOS"] = {x = -18699.1367, y = -142.675751, z = 4570.32666, adjacent =
	[
	]},
	["FP_SMALLTALK_A_TALL_PATH_BANDITOS2"] = {x = -18772.3184, y = -149.175827, z = 4668.14404, adjacent =
	[
	]},
	["FP_ROAM_A_TALL_PATH_BANDITOS2"] = {x = -18227.5898, y = -429.020355, z = 5532.45166, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_NEAR_SHAADOW"] = {x = -29535.5137, y = 1576.43799, z = -9282.7168, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_NEAR_SHADOW2"] = {x = -30027.9629, y = 1600.42078, z = -9141.05957, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_NEAR_SHADOW3"] = {x = -29884.0039, y = 1488.78052, z = -8532.23145, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_NEAR_SHADOW4"] = {x = -29399.1777, y = 1456.8031, z = -8669.66602, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_NEAR_SHADOW5"] = {x = -29739.0039, y = 1424.82605, z = -8240.98242, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_NEAR_SHADOW6"] = {x = -28992.9883, y = 1437.724, z = -8968.7334, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_NEAR_SHADOW_01"] = {x = -30731.1621, y = 2568.21118, z = -12175.5371, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF_NEAR_SHADOW3"] = {x = -31037.4023, y = 2568.29199, z = -11870.5361, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_OC_WOOD01"] = {x = 1139.58643, y = -1361.07373, z = 9670.25195, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_OC_WOOD02"] = {x = 753.724121, y = -1341.51636, z = 9707.42578, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_OC_WOOD03"] = {x = 399.536133, y = -1322.07153, z = 9769.7334, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_OC_WOOD04"] = {x = -621.355713, y = -1321.67627, z = 9705.96875, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODFLY_OC_WOOD05"] = {x = -181.020187, y = -1346.01392, z = 9762.06934, adjacent =
	[
	]},
	["FP_ROAM_O_SCAVENGER_05_01"] = {x = 2962.51733, y = -1123.99341, z = 8458.77148, adjacent =
	[
	]},
	["FP_ROAM_O_SCAVENGER_05_02"] = {x = 2598.13501, y = -1090.35498, z = 7522.10986, adjacent =
	[
	]},
	["FP_ROAM_O_SCAVENGER_05_03"] = {x = 3859.27002, y = -1224.69678, z = 7686.27539, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_WALD_OC"] = {x = 17367.7207, y = 202.90773, z = 6370.50537, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_WALD_OC2"] = {x = 17474.9355, y = 217.419708, z = 5950.98047, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_WALD_OC3"] = {x = 17020.709, y = 216.682098, z = 5883.93115, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_INWALD_OC2"] = {x = 12496.9502, y = -362.147217, z = 7780.6709, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_INWALD_OC1"] = {x = 12396.6943, y = -209.060699, z = 7050.32666, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_INWALD_OC7"] = {x = 12821.5029, y = -247.026794, z = 7502.18848, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF2_WALD_OC5"] = {x = 12868.6367, y = 312.599823, z = 892.689209, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF2_WALD_OC3"] = {x = 12865.9297, y = 293.474152, z = 155.090302, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF2_WALD_OC1"] = {x = 12233.6455, y = 293.474152, z = 1031.41797, adjacent =
	[
	]},
	["FP_ROAM_OW_WOLF2_WALD_OC7"] = {x = 12434.6914, y = 285.474915, z = 690.313843, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT2_WALD_OC3"] = {x = 12620.376, y = 18.3624001, z = 2692.71216, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT2_WALD_OC2"] = {x = 11950.9307, y = -40.152916, z = 2653.88159, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT2_WALD_OC1"] = {x = 12437.8467, y = 68.5901947, z = 3428.55957, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT2_WALD_OC4"] = {x = 11581.6201, y = 16.7848778, z = 3010.41553, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT2_WALD_OC5"] = {x = 11924.9658, y = 107.413689, z = 3660.81812, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_RIVER"] = {x = 12192.2217, y = -1216.59973, z = 10797.4893, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_RIVER2"] = {x = 12580.0732, y = -1316.47534, z = 10986.6045, adjacent =
	[
	]},
	["FP_ROAM_OW_LURKER_RIVER3"] = {x = 12049.2412, y = -1355.37598, z = 11245.8682, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_INCAVE_DM2"] = {x = -10981.0303, y = 2615.17505, z = -20132.5449, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_INCAVE_DM1"] = {x = -11588.1523, y = 2607.41504, z = -19915.709, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_INCAVE_DM3"] = {x = -11481.1875, y = 2622.33008, z = -19347.791, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_INCAVE_DM3_01"] = {x = -11149.75, y = 3040.00195, z = -18309.666, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_INCAVE_DM4"] = {x = -12300.5566, y = 2579.43262, z = -19558.8535, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOCKGOBBO_CAVE_DM1"] = {x = -12994.6104, y = 345.863037, z = -28477.5957, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOCKGOBBO_CAVE_DM2"] = {x = -12229.8643, y = 281.221466, z = -28807.2578, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOCKGOBBO_CAVE_DM5"] = {x = -12527.9473, y = 339.983246, z = -28712.4375, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOCKGOBBO_CAVE_DM6"] = {x = -12723.5967, y = 374.030975, z = -28343.8555, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOCKGOBBO_CAVE_DM7"] = {x = -12855.4395, y = 284.871246, z = -28771.0664, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOCKGOBBO_CAVE_DM8"] = {x = -12613.6445, y = 314.390137, z = -28505.7227, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_WOOD05_02"] = {x = -20461.8594, y = 124.073746, z = 22965.2539, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_WOOD05_03"] = {x = -20253.7656, y = 140.112259, z = 22506.3223, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_WOOD05_05"] = {x = -20557.6113, y = 75.7790604, z = 22519.7109, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_WOOD05_04"] = {x = -20512.4824, y = 112.594337, z = 21577.2969, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_WOODOLDMINE"] = {x = -22788.5234, y = -141.370239, z = 19711.4609, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_WOODOLDMINE2"] = {x = -23011.9219, y = -120.360947, z = 19505.1621, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_06_CAVE_GUARD"] = {x = -10340.6143, y = 32.515686, z = -10783.9961, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_06_CAVE_GUARD2"] = {x = -10606.2988, y = 48.5473557, z = -10193.4316, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_06_CAVE_GUARD3"] = {x = -10059.2822, y = 19.7337723, z = -10315.9863, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_06_CAVE_GUARD4"] = {x = -10093.9082, y = -4.25694466, z = -10064.9414, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_OCWOOD1_05_01"] = {x = 16945.4453, y = -1048.35754, z = 9998.3418, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_OCWOOD1_05_02"] = {x = 17306.0176, y = -1078.04419, z = 10099.2324, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_OCWOOD1_05_03"] = {x = 17276.6152, y = -969.037537, z = 9724.12891, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_OCWOOD1_05_04"] = {x = 17849.9336, y = -934.0354, z = 9409.99805, adjacent =
	[
	]},
	["FP_SIT_OW2"] = {x = -10346.6318, y = -700.080383, z = -152.475479, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_OC_PSI1"] = {x = 8877.72949, y = 333.677795, z = -10758.4678, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_OC_PSI2"] = {x = 8639.16992, y = 205.868744, z = -10221.1348, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_OC_PSI3"] = {x = 8020.02295, y = 173.868744, z = -10973.0547, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_OC_PSI4"] = {x = 7995.42725, y = 85.8687439, z = -10343.335, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_OC_PSI5"] = {x = 8491.93359, y = 297.186584, z = -11208.334, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_OC_PSI_RUIN1"] = {x = 8209.09961, y = -82.1266937, z = -8331.38086, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_OC_PSI_RUIN2"] = {x = 8069.98877, y = -60.1268997, z = -8816.07129, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_OC_PSI_RUIN3"] = {x = 7519.24951, y = -258.125031, z = -8416.05957, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_OC_PSI_RUIN4"] = {x = 7562.05273, y = -170.125626, z = -8841.23828, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_ORC_02"] = {x = -10879.6514, y = 367.488373, z = -25968.9063, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_ORC_03"] = {x = -10323.9697, y = 510.227356, z = -25323.7207, adjacent =
	[
	]},
	["FP_ROAM_OW_SCAVENGER_ORC_04"] = {x = -10766.6172, y = 390.721558, z = -25484.752, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_ORC_01"] = {x = -11795.6992, y = 1573.61414, z = -16783.4375, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_ORC_02"] = {x = -12133.2598, y = 1557.61511, z = -16847.6953, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_ORC_03"] = {x = -11992.5908, y = 1469.62048, z = -16501.8398, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_ORC_04"] = {x = -12569.6826, y = 1464.6272, z = -16643.0879, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_ORC_04"] = {x = -14509.4336, y = 2031.86511, z = -20311.0391, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_ORC_03"] = {x = -14785.373, y = 2011.28235, z = -20590.6113, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_ORC_02"] = {x = -14810.0039, y = 2084.59717, z = -19866.2188, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_ORC_01"] = {x = -15247.2178, y = 2150.89429, z = -20019.0195, adjacent =
	[
	]},
	["FP_ROAM_OW_MAETBUG_ROOT_01"] = {x = -6840.14404, y = -822.629089, z = 14658.4063, adjacent =
	[
	]},
	["FP_ROAM_OW_MAETBUG_ROOT_02"] = {x = -7123.65771, y = -856.039429, z = 14166.8574, adjacent =
	[
	]},
	["FP_ROAM_OW_MAETBUG_ROOT_03"] = {x = -7329.10645, y = -877.061401, z = 14561.6221, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_DEMON_01"] = {x = -16338.2119, y = 2535.97705, z = -30219.3789, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_DEMON_02"] = {x = -15884.7139, y = 2545.34839, z = -29964.7344, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_DEMON_03"] = {x = -16091.3037, y = 2481.7771, z = -30648.707, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_DEMON_04"] = {x = -15971.3223, y = 2524.15796, z = -30436.3828, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_DEMON_05"] = {x = -15691.3037, y = 2545.34839, z = -30312.9063, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_DEMON_02_02"] = {x = -16438.7148, y = 1402.63098, z = -33435.2656, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_DEMON_02_03"] = {x = -15902.5615, y = 1427.40686, z = -33322.8359, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_EBENE_02_01"] = {x = 24966.5801, y = 4076.68726, z = -24736.1875, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_EBENE_02_02"] = {x = 24755.4551, y = 4047.85864, z = -24257.0176, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_EBENE_02_03"] = {x = 24303.4199, y = 4079.56494, z = -23919.4844, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_EBENE_02_04"] = {x = 25569.4727, y = 3907.08008, z = -24255.0859, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_EBENE_02_05"] = {x = 24927.1113, y = 3967.83325, z = -23872.3711, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_EBENE_02_06"] = {x = 23871.7402, y = 3986.33228, z = -22793.8457, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_EBENE_02_07"] = {x = 23853.082, y = 3931.92651, z = -22231.9219, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_EBENE_02_08"] = {x = 24482.459, y = 3898.00439, z = -23127.2148, adjacent =
	[
	]},
	["FP_ROAM_OW_WARAN_EBENE_02_09"] = {x = 24635.5117, y = 3756.38208, z = -22325.9531, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_06_1"] = {x = 13121.4199, y = 6705.45068, z = -28082.5801, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_06"] = {x = 13522.6152, y = 6687.70703, z = -27713.4551, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_04"] = {x = 15245.0869, y = 6294.15381, z = -27941.0234, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_04_1"] = {x = 14926.4805, y = 6345.61963, z = -27781.7578, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_02"] = {x = 17514.0098, y = 6367.54443, z = -25944.0664, adjacent =
	[
	]},
	["FP_ROAM_OW_ROCK_DRACONIAN_01"] = {x = 18121.4063, y = 6389.46875, z = -26396.8828, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_10_04"] = {x = -32971.7969, y = 2754.44434, z = -17534.1367, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_10_03"] = {x = -33464.1914, y = 2768.52808, z = -18219.6289, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_10_01"] = {x = -33433.9609, y = 2665.18115, z = -16719.9961, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_CANYONCAVE"] = {x = -43860.25, y = 3389.2937, z = -1771.82629, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_CANYONCAVE2"] = {x = -43233.1758, y = 3338.80981, z = -1646.67029, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_CANYONCAVE3"] = {x = -43151.6992, y = 3368.07959, z = -2092.66821, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_CANYONCAVE4"] = {x = -42830.7266, y = 3307.11255, z = -1791.69214, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_CANYONCAVE5"] = {x = -42801.332, y = 3248.80859, z = -1414.30066, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODHOUND_CANYONCAVE3"] = {x = -45312.7539, y = 3292.3584, z = -1270.71948, adjacent =
	[
	]},
	["FP_ROAM_OW_BLOODHOUND_CANYONCAVE2"] = {x = -44690.6133, y = 3344.12769, z = -1533.79089, adjacent =
	[
	]},
	["FP_ROAM_BLOODHOUND_PLATEAU_NEARCASTLERUIN"] = {x = 9664.50195, y = 5623.48291, z = -21204.6875, adjacent =
	[
	]},
	["FP_ROAM_BLOODHOUND_PLATEAU_NEARCASTLERUIN2"] = {x = 9420.23145, y = 5580.84033, z = -20664.5, adjacent =
	[
	]},
	["FP_ROAM_BLOODHOUND_PLATEAU_NEARCASTLERUIN3"] = {x = 9367.24316, y = 5562.02441, z = -20272.0781, adjacent =
	[
	]},
	["FP_ROAM_BLOODHOUND_PLATEAU_NEARCASTLERUIN4"] = {x = 7740.0459, y = 5835.03955, z = -21203.8535, adjacent =
	[
	]},
	["FP_ROAM_BLOODHOUND_PLATEAU_NEARCASTLERUIN5"] = {x = 8216.26953, y = 5798.68213, z = -20704.7813, adjacent =
	[
	]},
	["FP_ROAM_BLOODHOUND_PLATEAU_NEARCASTLERUIN6"] = {x = 7645.12354, y = 5919.79834, z = -20505.1543, adjacent =
	[
	]},
	["FP_ROAM_ORC_2_1"] = {x = -8482.65625, y = 2181.04761, z = -18839.9961, adjacent =
	[
	]},
	["FP_ROAM_ORC_2_2"] = {x = -8413.13281, y = 2181.04761, z = -18514.752, adjacent =
	[
	]},
	["FP_ROAM_ORC_2_3"] = {x = -8649.8125, y = 2131.60327, z = -18180.3164, adjacent =
	[
	]},
	["FP_ROAM_ORC_2_4"] = {x = -8838.0332, y = 2103.34937, z = -18013.6113, adjacent =
	[
	]},
	["FP_ROAM_ORC_2_5"] = {x = -8248.63477, y = 2164.70801, z = -18160.4395, adjacent =
	[
	]},
	["FP_ROAM_ORC_2_1_2"] = {x = -12904.4775, y = 1153.80725, z = -15480.2393, adjacent =
	[
	]},
	["FP_ROAM_ORC_2_1_3"] = {x = -13063.0264, y = 1154.4281, z = -15463.8438, adjacent =
	[
	]},
	["FP_ROAM_ORC_2_1_4"] = {x = -13633.2969, y = 1158.18726, z = -15352.0762, adjacent =
	[
	]},
	["FP_ROAM_ORC_2_1_5"] = {x = -13497.8418, y = 1154.80603, z = -15381.498, adjacent =
	[
	]},
	["FP_ROAM_ORC_2_1_6"] = {x = -20683.8926, y = 1244.33044, z = -13765.3848, adjacent =
	[
	]},
	["FP_ROAM_ORC_2_1_7"] = {x = -19011.584, y = 1157.76636, z = -13610.8262, adjacent =
	[
	]},
	["FP_ROAM_ORC_2_1_8"] = {x = -20847.1348, y = 1251.51074, z = -14222.5986, adjacent =
	[
	]},
	["FP_ROAM_ORC_2_1_9"] = {x = -19660.8906, y = 1130.63501, z = -14474.3906, adjacent =
	[
	]},
	["FP_ROAM_ORC_20"] = {x = -16294.5664, y = 2347.40161, z = -20750.1563, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_OW_ORC4"] = {x = -913.487183, y = 235.495789, z = -15562.0195, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_OW_ORC5"] = {x = -1251.56628, y = 259.056396, z = -13912.6914, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_OW_ORC6"] = {x = -450.458405, y = 227.608185, z = -15230.1846, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_OW_ORC7"] = {x = -824.287537, y = 276.078461, z = -13845.2617, adjacent =
	[
	]},
	["FP_ROAM_OW_SNAPPER_OW_ORC8"] = {x = -602.502197, y = 216.569016, z = -15436.2373, adjacent =
	[
	]},
	["FP_CAMPFIRE_PATH_BANDITOS2_03_01"] = {x = -18351.3613, y = -136.171906, z = 4135.57666, adjacent =
	[
	]},
	["FP_CAMPFIRE_PATH_BANDITOS2_03_02"] = {x = -18544.4863, y = -136.171906, z = 4141.19727, adjacent =
	[
	]},
	["FP_CAMPFIRE_DJG_ROCKCAMP_01"] = {x = 20746.541, y = 4514.94092, z = -24117.1836, adjacent =
	[
	]},
	["FP_CAMPFIRE_OW_NEWMINE_01"] = {x = -14267.3701, y = 914.078735, z = -25100.2637, adjacent =
	[
	]},
	["FP_CAMPFIRE_OW_NEWMINE_02"] = {x = -14081.4395, y = 914.051819, z = -25395.1563, adjacent =
	[
	]},
	["FP_CAMPFIRE_OW_NEWMINE_03"] = {x = -14304.6289, y = 825.050354, z = -25517.3691, adjacent =
	[
	]},
	["FP_ROAM_DRAGONSNAPPER_OW_PATH_187_01"] = {x = -12068.5635, y = 1465.72351, z = 23440.0547, adjacent =
	[
	]},
	["FP_ROAM_DRAGONSNAPPER_OW_PATH_187_02"] = {x = -12358.583, y = 1425.72351, z = 23539.375, adjacent =
	[
	]},
	["FP_ROAM_DRAGONSNAPPER_OW_PATH_187_03"] = {x = -12168.208, y = 1419.12549, z = 23312.0391, adjacent =
	[
	]},
	["FP_ROAM_DRAGONSNAPPER_OW_PATH_190_01"] = {x = -13130.0654, y = 2147.57568, z = 25647.6289, adjacent =
	[
	]},
	["FP_ROAM_DRAGONSNAPPER_OW_PATH_190_02"] = {x = -13373.6006, y = 1921.90771, z = 25363.2891, adjacent =
	[
	]},
	["FP_ROAM_DRAGONSNAPPER_OW_PATH_190_03"] = {x = -13056.4375, y = 1912.6001, z = 24987.9023, adjacent =
	[
	]},
	["FP_ROAM_DRAGONSNAPPER_OW_PATH_190_04"] = {x = -12912.4033, y = 2059.43677, z = 25276.8164, adjacent =
	[
	]},
	["FP_ROAM_DRAGONSNAPPER_OW_PATH_189_01"] = {x = -14110.252, y = 1707.03296, z = 24763.8887, adjacent =
	[
	]},
	["FP_ROAM_DRAGONSNAPPER_OW_PATH_189_02"] = {x = -13862.7021, y = 1707.03296, z = 24859.875, adjacent =
	[
	]},
	["FP_ROAM_DRAGONSNAPPER_OW_PATH_189_03"] = {x = -13934.0566, y = 1743.80652, z = 25268.2207, adjacent =
	[
	]},
	["FP_ROAM_DRAGONSNAPPER_OW_PATH_185_01"] = {x = -13269.3545, y = 1163.53552, z = 22792.1055, adjacent =
	[
	]},
	["FP_ROAM_DRAGONSNAPPER_OW_PATH_185_02"] = {x = -13411.3691, y = 858.692627, z = 22210.2148, adjacent =
	[
	]},
	["FP_ROAM_DRAGONSNAPPER_OW_PATH_185_03"] = {x = -12758.4717, y = 1002.38892, z = 22329.2246, adjacent =
	[
	]},
	["FP_ROAM_OW_WATERFALL_GOBBO10_01"] = {x = 20120.3438, y = 1223.33887, z = -17826.7383, adjacent =
	[
	]},
	["FP_ROAM_OW_WATERFALL_GOBBO10_02"] = {x = 19297.459, y = 1097.10999, z = -18105.3887, adjacent =
	[
	]},
	["FP_ROAM_OW_WATERFALL_GOBBO10_03"] = {x = 20633.4004, y = 1254.74841, z = -18161.0234, adjacent =
	[
	]},
	["FP_ROAM_MT16_03"] = {x = 7214.64014, y = 5477.72559, z = -28173.4082, adjacent =
	[
	]},
	["FP_ROAM_MT16_01"] = {x = 6530.47021, y = 5358.021, z = -27917.9063, adjacent =
	[
	]},
	["FP_ROAM_MT16_02"] = {x = 6864.34375, y = 5556.67529, z = -28780.1777, adjacent =
	[
	]},
	["FP_ROAM_MT15_01"] = {x = 7095.8999, y = 5634.4082, z = -30128.5762, adjacent =
	[
	]},
	["FP_ROAM_MT15_02"] = {x = 7560.56494, y = 5644.74072, z = -30596.4824, adjacent =
	[
	]},
	["FP_ROAM_MT15_03"] = {x = 7662.7627, y = 5727.98682, z = -29744.9688, adjacent =
	[
	]},
	["FP_ROAM_MT08_01"] = {x = 1716.16528, y = 5439.02637, z = -33842.043, adjacent =
	[
	]},
	["FP_ROAM_MT08_02"] = {x = 1100.4552, y = 5497.83203, z = -34098.3516, adjacent =
	[
	]},
	["FP_ROAM_MT09_01"] = {x = 1747.39661, y = 6026.52246, z = -31431.2363, adjacent =
	[
	]},
	["FP_ROAM_MT09_02"] = {x = 1306.52148, y = 6035.4585, z = -32027.4316, adjacent =
	[
	]},
	["FP_ROAM_OW_PATH_210_01"] = {x = 1401.33948, y = 5837.31055, z = -25808.8789, adjacent =
	[
	]},
	["FP_ROAM_OW_PATH_210_02"] = {x = 1028.62048, y = 5810.14795, z = -26389.2051, adjacent =
	[
	]},
	["FP_ROAM_OW_PATH_210_03"] = {x = 821.65332, y = 5744.93213, z = -26010.666, adjacent =
	[
	]},
	["FP_ROAM_OW_DT_BLOODFLY_01"] = {x = -18396.1016, y = 3427.88916, z = -40461.2734, adjacent =
	[
	]},
	["FP_ROAM_OW_DT_BLOODFLY_02"] = {x = -17185.5781, y = 3169.76978, z = -40856.4961, adjacent =
	[
	]},
	["FP_ROAM_OW_DT_BLOODFLY_03"] = {x = -15744.4893, y = 3019.40063, z = -40717.8438, adjacent =
	[
	]},
	["FP_ROAM_OW_DT_BLOODFLY_04"] = {x = -19125.2012, y = 3948.56396, z = -39466.5078, adjacent =
	[
	]},
	["FP_ROAM_OW_PATH_333_01"] = {x = -17063.6621, y = 2617.28369, z = -23947.0723, adjacent =
	[
	]},
	["FP_ROAM_OW_PATH_333_02"] = {x = -16948.8438, y = 2524.74097, z = -24451.1719, adjacent =
	[
	]},
	["FP_ROAM_OW_PATH_333_03"] = {x = -17642.6133, y = 2819.24536, z = -24212.8438, adjacent =
	[
	]},
	["FP_ROAM_TOTURIAL_CHICKEN_2_4"] = {x = 450.458008, y = 2766.30151, z = 20732.9766, adjacent =
	[
	]},
	["FP_ROAM_INTRO_CHICKEN_3"] = {x = 2588.67505, y = 2481.35986, z = 20736.4844, adjacent =
	[
	]},
	["FP_ROAM_INTRO_CHICKEN_4"] = {x = 2692.17236, y = 2683.14697, z = 21375.2441, adjacent =
	[
	]},
	["FP_ROAM_OW_PATH_1_5_3_01"] = {x = 1612.32312, y = 2805.43945, z = 21869.75, adjacent =
	[
	]},
	["FP_ROAM_OW_PATH_1_5_3_02"] = {x = 1855.75537, y = 2956.38403, z = 22255.3066, adjacent =
	[
	]},
	["FP_ROAM_OW_PATH_1_5_3_03"] = {x = 1139.95959, y = 2902.65356, z = 22293.3574, adjacent =
	[
	]},
	["FP_ROAM_OW_ORCBARRIER_08_01"] = {x = 14885.5762, y = 1420.86487, z = -10999.4746, adjacent =
	[
	]},
	["FP_ROAM_OW_ORCBARRIER_08_02"] = {x = 15116.7188, y = 1454.15149, z = -11479.4932, adjacent =
	[
	]},
	["FP_ROAM_OW_ORCBARRIER_08_03"] = {x = 15256.7012, y = 1574.84119, z = -11107.9141, adjacent =
	[
	]},
	["FP_ROAM_OW_ORCBARRIER_08_04"] = {x = 15105.8281, y = 1341.48047, z = -10224.0029, adjacent =
	[
	]},
	["FP_ROAM_OW_ORCBARRIER_08_05"] = {x = 14324.4199, y = 1312.89587, z = -11578.3906, adjacent =
	[
	]},
	["FP_ROAM_OW_ORCBARRIER_08_06"] = {x = 14131.4961, y = 1230.50757, z = -11389.4424, adjacent =
	[
	]},
	["FP_ROAM_OW_ORCBARRIER_04_01"] = {x = 16894.8164, y = 1785.56274, z = -13686.332, adjacent =
	[
	]},
	["FP_ROAM_OW_ORCBARRIER_04_02"] = {x = 17519.0859, y = 1836.05237, z = -13287.6016, adjacent =
	[
	]},
	["FP_ROAM_OW_ORCBARRIER_04_03"] = {x = 17392.4473, y = 1590.15149, z = -14631.9043, adjacent =
	[
	]},
	["FP_ROAM_OW_ORCBARRIER_04_05"] = {x = 16833.4316, y = 1610.15149, z = -14306.9385, adjacent =
	[
	]},
	["FP_ROAM_OW_ORCBARRIER_04_04"] = {x = 16455.4902, y = 1586.31421, z = -13896.6074, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_A_6_NC7_01"] = {x = -33003.3281, y = 655.675659, z = 15192.6992, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_A_6_NC7_02"] = {x = -33492.2031, y = 616.049988, z = 14528.4053, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_A_6_NC7_03"] = {x = -32744.7891, y = 536.614197, z = 14552.0352, adjacent =
	[
	]},
	["FP_ROAM_OW_MOLERAT_A_6_NC7_04"] = {x = -32462.9609, y = 566.614197, z = 15289.0703, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_07_05"] = {x = -6606.24512, y = -411.399719, z = 17450.0313, adjacent =
	[
	]},
	["FP_ROAM_OW_GOBBO_07_06"] = {x = -7294.65625, y = -499.659027, z = 17386.5586, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_10_10_01"] = {x = -30020.0664, y = 2458.13184, z = -15133.7471, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_10_10_02"] = {x = -30771.2695, y = 2513.11597, z = -14608.4863, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_10_10_03"] = {x = -31428.7324, y = 2589.11597, z = -14683.6592, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_10_10_04"] = {x = -30381.5, y = 2514.02368, z = -15261.9561, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_10_06_01"] = {x = -28499.3711, y = 2324.38721, z = -13645.5439, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_10_06_02"] = {x = -28035.1309, y = 2254.86353, z = -14122.4443, adjacent =
	[
	]},
	["FP_ROAM_OW_SHADOWBEAST_10_10_05"] = {x = -28765.6895, y = 2367.38086, z = -14183.3623, adjacent =
	[
	]},
	["FP_ROAM_ORC_14_1"] = {x = -1929.26318, y = 3014.76514, z = -17299.3457, adjacent =
	[
	]},
	["FP_ROAM_ORC_14_2"] = {x = -2633.95752, y = 2926.7312, z = -17487.2344, adjacent =
	[
	]},
	["FP_ROAM_ORC_14_3"] = {x = -2245.69849, y = 3116.32959, z = -18333.1934, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_8_1"] = {x = 8820.66992, y = 4864.21289, z = -17786.293, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_8_2"] = {x = 9432.75391, y = 4679.21777, z = -17553.0117, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_8_3"] = {x = 8362.07617, y = 4864.21338, z = -17283.5059, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_8_4"] = {x = 8881.28711, y = 4651.63818, z = -17265.5879, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_8_5"] = {x = 9366.62891, y = 5042.90479, z = -18274.8652, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_8_01"] = {x = 5432.40869, y = 5299.92529, z = -17223.6836, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_8_02"] = {x = 6154.50049, y = 5154.82568, z = -17340.8438, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_8_03"] = {x = 5869.60498, y = 4870.30713, z = -16611.8926, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_8_05"] = {x = 4491.88525, y = 5056.23438, z = -17155.7832, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_8_04"] = {x = 4661.75, y = 5022.20264, z = -16976.5508, adjacent =
	[
	]},
	["FP_ROAM_BLOODHOUND_PLATEAU_NEARCASTLERUIN_01"] = {x = 10235.2852, y = 5521.3916, z = -20817.584, adjacent =
	[
	]},
	["FP_ROAM_BLOODHOUND_PLATEAU_NEARCASTLERUIN_02"] = {x = 9770.55078, y = 5390.54297, z = -19803.8301, adjacent =
	[
	]},
	["FP_ROAM_BLOODHOUND_PLATEAU_NEARCASTLERUIN6_01"] = {x = 7959.63623, y = 5875.37988, z = -20363.5684, adjacent =
	[
	]},
	["FP_ROAM_BLOODHOUND_PLATEAU_NEARCASTLERUIN6_02"] = {x = 8839.25781, y = 5875.37988, z = -21122.707, adjacent =
	[
	]},
	["FP_ROAM_BLOODHOUND_PLATEAU_NEARCASTLERUIN6_03"] = {x = 7976.02832, y = 5875.37988, z = -21759.9805, adjacent =
	[
	]},
	["FP_CAMPFIRE_OW_WOLF_WOODRUIN_01"] = {x = -25973.7188, y = -107.892914, z = 1517.65088, adjacent =
	[
	]},
	["FP_CAMPFIRE_OW_WOLF_WOODRUIN_02"] = {x = -26443.8438, y = 107.693115, z = 926.133057, adjacent =
	[
	]},
	["FP_CAMPFIRE_OW_WOLF_WOODRUIN2_01"] = {x = -26515.543, y = -203.245529, z = 2506.2417, adjacent =
	[
	]},
	["FP_ROAM_ORC_01_01"] = {x = -4543.10205, y = 1250.93188, z = -17178.6465, adjacent =
	[
	]},
	["FP_ROAM_ORC_01_02"] = {x = -5343.60449, y = 1438.38245, z = -17221.5078, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_3_01"] = {x = -4961.6123, y = 2163.59131, z = -19572.8613, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_3_02"] = {x = -4652.45166, y = 2009.78833, z = -19334.2676, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_3_03"] = {x = -4462.13916, y = 2234.93896, z = -19980.5078, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_4_01"] = {x = -3317.45825, y = 2276.31885, z = -19789.8496, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_4_02"] = {x = -3476.51025, y = 2346.31885, z = -20370.5469, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_4_03"] = {x = -3741.68408, y = 2233.06079, z = -19997.8164, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_4_04"] = {x = -3111.10596, y = 2427.11743, z = -20038.207, adjacent =
	[
	]},
	["FP_ROAM_CASTLE_4_05"] = {x = -2664.48901, y = 2666.02222, z = -19415.0859, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_ENTRANCE_ICEGOLEM_02"] = {x = -38910.0273, y = 897.638, z = 5306.57275, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_ENTRANCE_ICEGOLEM_01"] = {x = -39316.7148, y = 915.309143, z = 4994.68848, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_1_01"] = {x = -41042.3164, y = 658.822876, z = 11459.6602, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_1_02"] = {x = -40384.3047, y = 658.822876, z = 11434.0898, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_1_03"] = {x = -41261.1445, y = 658.822876, z = 11008.3623, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_2_03"] = {x = -43633.9648, y = 1036.14697, z = 9220.07617, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_2_02"] = {x = -43316.3242, y = 1036.14697, z = 9735.28418, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_2_01"] = {x = -43118.8164, y = 1036.14697, z = 9263.70313, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_3_01"] = {x = -44436.9336, y = 1354.86975, z = 7389.6958, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_3_02"] = {x = -44061.3203, y = 1354.86975, z = 7192.5625, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_4_01"] = {x = -45189.7461, y = 1424.92883, z = 9186.20996, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_4_02"] = {x = -45291.5352, y = 1424.92883, z = 8951.64551, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_5_01"] = {x = -44493.8828, y = 1424.92883, z = 10058.7109, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_5_02"] = {x = -44466.8242, y = 1424.92883, z = 10588.5234, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_5_03"] = {x = -44291.4727, y = 1424.92883, z = 10324.4922, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_6_01"] = {x = -45542.7266, y = 1570.02625, z = 10026.3896, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_6_02"] = {x = -45729.4063, y = 1570.02625, z = 10481.5752, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_6_03"] = {x = -45356.5273, y = 1570.02625, z = 10390.7754, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_7_01"] = {x = -47134.0742, y = 1700.52515, z = 10196.4951, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_7_02"] = {x = -47149.4609, y = 1700.52515, z = 10672.3135, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_8_01"] = {x = -46464.4336, y = 1767.20728, z = 12406.8955, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_8_02"] = {x = -46217, y = 1837.20728, z = 12898.9609, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_9_01"] = {x = -48561.9336, y = 2678.74048, z = 13168.0459, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_9_02"] = {x = -48395.8672, y = 2678.74048, z = 13500.6289, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_10_01"] = {x = -49507.5781, y = 2722.23511, z = 12754.5459, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_10_02"] = {x = -49266.3789, y = 2722.23511, z = 12548.9004, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_10_03"] = {x = -49406.2031, y = 2722.23511, z = 12404.8701, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_11_01"] = {x = -50899.8008, y = 2537.24146, z = 12370.2139, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_11_02"] = {x = -50418.0313, y = 2537.24146, z = 12156.998, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_11_03"] = {x = -50508.8984, y = 2537.24146, z = 11808.6934, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_12_01"] = {x = -50869.4531, y = 2537.24146, z = 15067.2744, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_12_02"] = {x = -50557.1289, y = 2537.24146, z = 15100.6152, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_12_03"] = {x = -50638.5352, y = 2537.24146, z = 14715.9482, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_13_01"] = {x = -52800.9063, y = 2537.24146, z = 17837.2559, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_13_02"] = {x = -52306.5898, y = 2537.24146, z = 17838.9785, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_13_03"] = {x = -52473.0781, y = 2537.24146, z = 17407.1211, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_14_01"] = {x = -54417.5078, y = 2537.24146, z = 16053.001, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_13_04"] = {x = -54178.8672, y = 2537.24146, z = 16634.5293, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_13_05"] = {x = -54741.7539, y = 2537.24146, z = 16431.4824, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_14_03"] = {x = -56832.125, y = 2537.24146, z = 16940.5098, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_14_02"] = {x = -56645.5273, y = 2537.24146, z = 17465.6992, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_15_01"] = {x = -59428.9453, y = 2537.24146, z = 15604.8779, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_15_02"] = {x = -58849.1406, y = 2537.24146, z = 15778.4795, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_15_03"] = {x = -58769.7305, y = 2537.24146, z = 15195.6182, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_16_01"] = {x = -61442.6914, y = 2537.24146, z = 12004.7314, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_16_03"] = {x = -61706.7188, y = 2537.24146, z = 12459.9707, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_16_02"] = {x = -61170.0664, y = 2537.24146, z = 12480.585, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_17_01"] = {x = -58233.1484, y = 2537.24146, z = 12528.6914, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_17_02"] = {x = -58632.5039, y = 2537.24146, z = 12551.8613, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_17_03"] = {x = -58661.6289, y = 2537.24146, z = 13131.5771, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_18_01"] = {x = -56872.9961, y = 2537.24146, z = 12218.3994, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_18_02"] = {x = -57470.9141, y = 2537.24146, z = 11943.7236, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_18_03"] = {x = -57223.5898, y = 2537.24146, z = 12414.7051, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_19_01"] = {x = -60516.625, y = 3798.7981, z = 10130.8193, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_19_02"] = {x = -60040.7813, y = 3701.23682, z = 9823.90723, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_20_01"] = {x = -56382.6992, y = 2966.93579, z = 9349.43848, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_20_02"] = {x = -56305.1016, y = 2966.93579, z = 9964, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_20_03"] = {x = -55852.8125, y = 2853.57593, z = 9735.80859, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_21_01"] = {x = -54767.2031, y = 2620.78809, z = 5755.86182, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_21_02"] = {x = -53327.6055, y = 2571.89575, z = 7085.76514, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_21_03"] = {x = -52767.5391, y = 2498.3269, z = 8168.11475, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_22_01"] = {x = -55647.2422, y = 2569.3584, z = 6948.0415, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_22_02"] = {x = -55758.0313, y = 2596.17163, z = 6353.86816, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_22_03"] = {x = -55409.8828, y = 2577.80908, z = 6625.20361, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_23_01"] = {x = -54000.4258, y = 2500.0957, z = 6193.46387, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_23_02"] = {x = -54132.7031, y = 2509.37402, z = 5766.95752, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_23_03"] = {x = -54442.1406, y = 2541.12256, z = 6303.41943, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_24_01"] = {x = -54603.6133, y = 2636.66528, z = 4111.6084, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_24_02"] = {x = -54432.3672, y = 2636.66528, z = 4375.60645, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_25_01"] = {x = -53777.4219, y = 2636.66528, z = 4807.75439, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_25_02"] = {x = -53579.6563, y = 2599.57837, z = 5060.11914, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_26_01"] = {x = -55024.457, y = 2918.82593, z = 2672.39038, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_26_02"] = {x = -54748.8867, y = 2918.82593, z = 2874.41528, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_27_01"] = {x = -57298.1758, y = 2723.51465, z = 4589.67139, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_27_02"] = {x = -57523.5781, y = 2723.51465, z = 4309.60254, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_28_02"] = {x = -56923.543, y = 2683.33252, z = 5176.44141, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_28_03"] = {x = -56589.9883, y = 2683.33252, z = 5495.82861, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_28_01"] = {x = -57113.7109, y = 2692.97339, z = 5496.5835, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_29_01"] = {x = -40719.6211, y = 875.794983, z = 7113.24707, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_29_02"] = {x = -40966.4063, y = 875.794983, z = 7264.93555, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_30_01"] = {x = -39886.2734, y = 875.794983, z = 7778.61816, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_30_02"] = {x = -39640.9258, y = 875.794983, z = 7420.5874, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_31_01"] = {x = -58022.1914, y = 3223.49707, z = 16909.6465, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_31_02"] = {x = -57967.9766, y = 3223.49707, z = 17373.582, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_32_01"] = {x = -59171.5547, y = 3759.73022, z = 19903.0801, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_32_02"] = {x = -58862.7813, y = 3759.73022, z = 20646.4336, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_33_01"] = {x = -52973.5547, y = 3341.69727, z = 12970.165, adjacent =
	[
	]},
	["FP_ROAM_OW_ICEREGION_33_02"] = {x = -53381.4766, y = 3477.67236, z = 14200.8447, adjacent =
	[
	]},
	["FP_ROAM_STONES_01"] = {x = -34387.8164, y = 2904.87866, z = -12252.0801, adjacent =
	[
	]},
	["FP_ROAM_STONES_02"] = {x = -34708.2188, y = 2904.87866, z = -11988.3896, adjacent =
	[
	]},
	["FP_ROAM_OW_PATH_092_01"] = {x = -36380.3242, y = 3452.42236, z = -9734.18945, adjacent =
	[
	]},
	["FP_ROAM_OW_PATH_092_02"] = {x = -36144.7813, y = 3432.42236, z = -9687.47754, adjacent =
	[
	]},
	["FP_ROAM_OW_PATH_07_03_01"] = {x = -39195.4648, y = 3553.40845, z = -13052.3682, adjacent =
	[
	]},
	["FP_ROAM_OW_PATH_092_03"] = {x = -39032.9219, y = 3553.40845, z = -12917.9219, adjacent =
	[
	]},
	["FP_ROAM_OW_PATH_07_04_02"] = {x = -39950.3438, y = 3583.40845, z = -11614.71, adjacent =
	[
	]},
	["FP_ROAM_OW_PATH_07_04_01"] = {x = -40156.3672, y = 3573.40845, z = -11610.3467, adjacent =
	[
	]},
	["FP_ROAM_OW_PATH_3_13_01"] = {x = -35064.6406, y = 2931.58057, z = -13025.1523, adjacent =
	[
	]},
	["FP_ROAM_OW_PATH_3_13_02"] = {x = -35497.3164, y = 2971.58057, z = -12637.3486, adjacent =
	[
	]},
	["FP_ROAM_LOCATION_19_03_PATH_RUIN_01"] = {x = 21403.3789, y = 7616.37939, z = -34774.3242, adjacent =
	[
	]},
	["FP_ROAM_LOCATION_19_03_PATH_RUIN_02"] = {x = 21098.4863, y = 7596.37939, z = -34509.4297, adjacent =
	[
	]},
	["FP_ROAM_LOCATION_19_03_PATH_RUIN_03"] = {x = 21300.582, y = 7616.37939, z = -35336.0195, adjacent =
	[
	]},
	["FP_ROAM_LOCATION_19_03_PATH_RUIN_04"] = {x = 21063.7207, y = 7788.82617, z = -36719.6211, adjacent =
	[
	]},
	["FP_ROAM_LOCATION_19_03_PATH_RUIN_05"] = {x = 21167.2793, y = 7828.82617, z = -36193.4727, adjacent =
	[
	]},
	["FP_ROAM_LOCATION_19_03_PATH_RUIN_06"] = {x = 19396.5293, y = 7818.82617, z = -36530.3828, adjacent =
	[
	]},
	["FP_ROAM_LOCATION_19_03_PATH_RUIN_07"] = {x = 20027.8301, y = 7798.82617, z = -36204.4023, adjacent =
	[
	]},
	["FP_ROAM_LOCATION_19_03_PATH_RUIN_08"] = {x = 20321.7871, y = 7818.82617, z = -36575.8828, adjacent =
	[
	]},
	["FP_ROAM_LOCATION_19_03_PATH_RUIN_09"] = {x = 20422.291, y = 7613.87549, z = -34582.418, adjacent =
	[
	]},
	["FP_ROAM_LOCATION_19_03_PATH_RUIN_10"] = {x = 16997.3594, y = 5749.50879, z = -36864.2578, adjacent =
	[
	]},
	["FP_ROAM_LOCATION_19_03_PATH_RUIN_11"] = {x = 20017.0977, y = 5060.18799, z = -33426.8125, adjacent =
	[
	]},
	["FP_ROAM_LOCATION_19_03_PATH_RUIN_12"] = {x = 18922.5547, y = 5097.0415, z = -34403.668, adjacent =
	[
	]},
	["FP_ROAM_LOCATION_19_03_PATH_RUIN_12_1"] = {x = 18462.8457, y = 5167.0415, z = -34372.9063, adjacent =
	[
	]},
	["FP_ROAM_LOCATION_19_03_PATH_RUIN_13"] = {x = 19167.4004, y = 5157.0415, z = -33940.7969, adjacent =
	[
	]},
	["FP_ROAM_LOCATION_19_03_PATH_RUIN_14"] = {x = 18040.6035, y = 5618.80518, z = -32228.2754, adjacent =
	[
	]},
	["FP_ROAM_LOCATION_19_03_PATH_RUIN_15"] = {x = 17759.1602, y = 5454.31689, z = -32816.6992, adjacent =
	[
	]},
	["FP_OW_GORNS_VERSTECK"] = {x = 2925.39673, y = -818.155396, z = -12837.6123, adjacent =
	[
	]},
	["FP_CAMPFIRE_OW_WP_INTRO12"] = {x = 8096.52002, y = 5518.91943, z = 36220.0156, adjacent =
	[
	]},
	["FP_CAMPFIRE_OW_TOWER_JERGAN"] = {x = -17440.9844, y = -690.695496, z = -1319.15674, adjacent =
	[
	]},
	["FP_STAND_OW_DJG_VORPOSTEN_01"] = {x = -28059.5566, y = -534.45874, z = 11876.1348, adjacent =
	[
	]},
	["FP_SMALLTALK_OW_DJG_VORPOSTEN_01"] = {x = -28684.8438, y = -771.365295, z = 9903.73145, adjacent =
	[
	]},
	["FP_SMALLTALK_OW_DJG_VORPOSTEN_02"] = {x = -28740.2324, y = -711.365295, z = 10173.3467, adjacent =
	[
	]},
	["FP_ROAM_OW_UNDEAD_DUNGEON_01"] = {x = -34310.1055, y = 2326.23047, z = -15128.5918, adjacent =
	[
	]},
	["FP_ROAM_OW_UNDEAD_DUNGEON_02"] = {x = -34247.1719, y = 2336.23047, z = -14491.5635, adjacent =
	[
	]},
	["FP_ROAM_OW_UNDEAD_DUNGEON_03"] = {x = -35061.4727, y = 2316.23047, z = -15045.1143, adjacent =
	[
	]},
	["FP_ROAM_OW_UNDEAD_DUNGEON_04"] = {x = -35161.7539, y = 2316.23047, z = -14410.208, adjacent =
	[
	]},
	["FP_CAMPFIRE_OW_FALL"] = {x = 6341.02734, y = 6463.21436, z = 39311.1992, adjacent =
	[
	]},
	["FP_ROAM_HOSHPAK_01"] = {x = 3564.34277, y = 1417.05151, z = -11934.6982, adjacent =
	[
	]},
	["FP_ROAM_HOSHPAK_02"] = {x = 3015.88159, y = 1424.96155, z = -11242.5752, adjacent =
	[
	]},
	["FP_ROAM_HOSHPAK_03"] = {x = 3422.58179, y = 1396.19873, z = -11048.3867, adjacent =
	[
	]},
	["FP_CAMPFIRE_HOSHPAK_01"] = {x = 1582.0415, y = 1522.37903, z = -11002.793, adjacent =
	[
	]},
	["FP_ROAM_HOSHPAK_04"] = {x = 998.102295, y = 1596.09436, z = -12171.1758, adjacent =
	[
	]},
	["FP_ROAM_HOSHPAK_05"] = {x = 1246.62476, y = 1619.21643, z = -11547.1055, adjacent =
	[
	]},
	["FP_ROAM_HOSHPAK_06"] = {x = 1703.80933, y = 1481.28796, z = -12611.7285, adjacent =
	[
	]},
	["FP_ROAM_HOSHPAK_07"] = {x = 2589.38086, y = 1374.68689, z = -12779.4863, adjacent =
	[
	]},
	["FP_CAMPFIRE_HOSHPAK_02"] = {x = 1257.87329, y = 1530.37036, z = -11047.29, adjacent =
	[
	]},
	["FP_OW_ITEM_01"] = {x = -12786.5742, y = -743.650391, z = 5231.81592, adjacent =
	[
	]},
	["FP_OW_ITEM_02"] = {x = -15203.4375, y = -1241.65308, z = 4716.10059, adjacent =
	[
	]},
	["FP_OW_ITEM_03"] = {x = -27328.1348, y = -131.910995, z = 2941.46729, adjacent =
	[
	]},
	["FP_OW_ITEM_04"] = {x = -52423.0469, y = 3108.02271, z = 11718.6182, adjacent =
	[
	]},
	["FP_OW_ITEM_05"] = {x = -44238.7344, y = -24.1839066, z = -4465.14941, adjacent =
	[
	]},
	["FP_OW_ITEM_06"] = {x = -15393.6895, y = 813.822571, z = -25245.2578, adjacent =
	[
	]},
	["FP_OW_ITEM_07"] = {x = 2359.90747, y = 2215, z = -30884.5273, adjacent =
	[
	]},
	["FP_OW_ITEM_08"] = {x = 1734.58105, y = 1515.07996, z = -11323.5938, adjacent =
	[
	]},
	["FP_OW_ITEM_09"] = {x = 15668.833, y = 912.901489, z = -6297.93213, adjacent =
	[
	]},
	["START_OW_ABANDONEDMINE"] = {x = 4944.46484, y = 5925.84619, z = 26411.168, adjacent =
	[
	]},
	["START_OW_ORETRAIL"] = {x = 6660.63916, y = 6519.19287, z = 41072.2461, adjacent =
	[
	]},
	["FP_ROAM_BF_01"] = {x = -30087.1152, y = 622.796021, z = 5196.06445, adjacent =
	[
	]},
	["FP_ROAM_BF_02"] = {x = -30339.4375, y = 662.796021, z = 5138.30176, adjacent =
	[
	]},
	["FP_ROAM_BF_03"] = {x = -30282.9316, y = 642.796021, z = 4779.84668, adjacent =
	[
	]},
	["FP_ROAM_BF_04"] = {x = -29891.1836, y = 602.796021, z = 4816.44189, adjacent =
	[
	]},
	["FP_STAND_GUARDING_JERGAN_01"] = {x = -2622, y = -1129, z = 13623, adjacent =
	[
	]},
	["FPFP_STAND_GUARDING_JERGAN_02"] = {x = -27307, y = -757.000061, z = 20708, adjacent =
	[
	]},
	["FP_STAND_GUARDING_TENGRON"] = {x = -15435.4658, y = 792.404602, z = -26208.0879, adjacent =
	[
	]},
	["FP_STAND_GUARDING_MARCOS_GUARD1"] = {x = -16154.9336, y = -1308.04138, z = 6074.60107, adjacent =
	[
	]},
	["FP_STAND_GUARDING_MARCOS_GUARD2"] = {x = -16178.1797, y = -1306.04138, z = 5809.27344, adjacent =
	[
	]},
	["FP_ITEM_XARDASALTERTURM_01"] = {x = -12186.4248, y = 3675.37476, z = -34117.8164, adjacent =
	[
	]},
	["FP_CAMPFIRE_BILGOT_OUT"] = {x = 4287.52295, y = 5832.5376, z = 27114.4336, adjacent =
	[
	]},
	["OW_ITEM_SWAMPHORT_01"] = {x = -26115.7891, y = -1387.37195, z = -11287.4102, adjacent =
	[
	]},
	["OW_ITEM_SWAMPHORT_02"] = {x = -26261.6289, y = -1413.86707, z = -11171.168, adjacent =
	[
	]},
	["OW_ITEM_ROCKHORT_01"] = {x = 25703.6973, y = 9512.52441, z = -42572.5234, adjacent =
	[
	]},
	["OW_ITEM_ROCKHORT_02"] = {x = 25763.9531, y = 9553.36816, z = -42977.4258, adjacent =
	[
	]},
	["OW_ITEM_FIREHORT_01"] = {x = 2277.56665, y = 7719.67188, z = -23022.8555, adjacent =
	[
	]},
	["OW_ITEM_FIREHORT_02"] = {x = 2256.47046, y = 7716.71631, z = -22935.9238, adjacent =
	[
	]},
	["OW_ITEM_ICEHORT_01"] = {x = -56724.9258, y = 2865.38599, z = 2273.69409, adjacent =
	[
	]},
	["OW_ITEM_ICEHORT_02"] = {x = -56996.1133, y = 2860.3667, z = 2161.75854, adjacent =
	[
	]},
	["FP_ITEM_OW_01"] = {x = -58638.6758, y = 3006.31201, z = 5757.58936, adjacent =
	[
	]},
	["FP_ROAM_ITEM_SPECIAL_01"] = {x = -45375.7734, y = 3265.19116, z = -907.474854, adjacent =
	[
	]},
	["FP_ROAM_ITEM_SPECIAL_02"] = {x = 2499.15527, y = -855.61145, z = -12403.8936, adjacent =
	[
	]},
	["FP_ROAM_ITEM_SPECIAL_03"] = {x = 14230.415, y = 6691.34863, z = -30415.8281, adjacent =
	[
	]},
	["FP_ROAM_ITEM_SPECIAL_04"] = {x = -13117.3379, y = -821.660583, z = 5931.29102, adjacent =
	[
	]},
	["FP_ROAM_ITEM_SPECIAL_05"] = {x = -32161.7207, y = -529.444336, z = 12885.125, adjacent =
	[
	]},
	["FP_STAND_GUARDING_TANDOR"] = {x = 1869.37476, y = 157.646271, z = 320.74585, adjacent =
	[
	]},
	["FP_ITEM_GORN"] = {x = 6113.07275, y = -1449.93103, z = -2918.99585, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_01_2"] = {x = -2700.93481, y = -622.553345, z = 4963.44287, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_08_2"] = {x = -834.932251, y = -805.969299, z = -7390.19629, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_10_2"] = {x = -4243.71777, y = -503.948547, z = -5546.71191, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_09_2"] = {x = -5687.92139, y = -460.198273, z = -5852.78271, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_14_2"] = {x = -6967.66504, y = -475.41629, z = -6252.22607, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_26_2"] = {x = 8331.24414, y = -745.533997, z = 3081.42383, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_12_2"] = {x = 7550.87939, y = -784.19165, z = -1639.97839, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_11_2"] = {x = 7580.10498, y = -812.507141, z = -990.909424, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_27_3"] = {x = 8126.76758, y = -856.885742, z = -1536.62988, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_27_2"] = {x = 8349.58594, y = -850.184998, z = -1597.18188, adjacent =
	[
	]},
	["FP_CAMPFIRE_ORK_OC_12_1"] = {x = 5031.78906, y = -877.73938, z = -7060.57568, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_12_2"] = {x = 4682.88867, y = -946.076782, z = -7107.10156, adjacent =
	[
	]},
	["FP_OC_KEROLOTHS_GELDBEUTEL"] = {x = 647.305054, y = 152.944244, z = 676.520569, adjacent =
	[
	]},
	["FP_STAND_FEROS"] = {x = 49.1562271, y = 126.768829, z = 648.823853, adjacent =
	[
	]},
	["FP_STAND_OC_KNECHTCAMP_01"] = {x = -4057.4043, y = -266.188965, z = -1844.86292, adjacent =
	[
	]},
	["FP_CAMPFIRE_OC_KNECHTCAMP_04"] = {x = -3760.43701, y = -206.18895, z = -1519.25256, adjacent =
	[
	]},
	["FP_CAMPFIRE_OC_KNECHTCAMP_03"] = {x = -3563.34204, y = -196.18895, z = -1485.27795, adjacent =
	[
	]},
	["FP_CAMPFIRE_OC_KNECHTCAMP_02"] = {x = -3671.85376, y = -286.188965, z = -1971.86414, adjacent =
	[
	]},
	["FP_CAMPFIRE_OC_KNECHTCAMP_01"] = {x = -3537.94043, y = -256.188965, z = -1954.96936, adjacent =
	[
	]},
	["FP_SMALLTALK_OC_KNECHTCAMP_02"] = {x = -3559.47559, y = -322.959229, z = -2461.52148, adjacent =
	[
	]},
	["FP_SMALLTALK_OC_KNECHTCAMP_01"] = {x = -3723.27856, y = -332.959229, z = -2515.13086, adjacent =
	[
	]},
	["FP_SMALLTALK_OC_TO_MAGE_02"] = {x = -301.782928, y = 131.692596, z = -503.051636, adjacent =
	[
	]},
	["FP_SMALLTALK_OC_TO_MAGE_01"] = {x = -164.36731, y = 131.914154, z = -632.740479, adjacent =
	[
	]},
	["FP_STAND_OW_OC_ANGAR"] = {x = -414.317963, y = 169.810425, z = -844.963318, adjacent =
	[
	]},
	["FP_CAMPFIRE_ORK_OC_19"] = {x = 1874.79456, y = -816.863403, z = -8050.1626, adjacent =
	[
	]},
	["FP_CAMPFIRE_ORK_OC_18"] = {x = 1406.61255, y = -823.301331, z = -8087.89063, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_31"] = {x = -5740.49023, y = -1104.31995, z = 5776.23926, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_30"] = {x = -5755.96729, y = -1214.50598, z = 6558.93457, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_14"] = {x = 6987.95068, y = -714.029358, z = -5784.98975, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_13"] = {x = 7179.62793, y = -662.630554, z = -5560.03857, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_12"] = {x = 7340.13623, y = -725.382568, z = -1761.78174, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_11"] = {x = 7328.27148, y = -785.382568, z = -916.905823, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_10"] = {x = 8030.48291, y = -772.493713, z = 4824.11963, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_09"] = {x = 8131.66211, y = -838.872437, z = 5198.20361, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_08"] = {x = 5416.3916, y = -1027.83423, z = 6714.9873, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_07"] = {x = 4423.54834, y = -1027.83423, z = 6672.35693, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_06"] = {x = 1337.68115, y = -772.353943, z = 5312.375, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_05"] = {x = 2283.13892, y = -747.934448, z = 5184.18604, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_04"] = {x = -340.09082, y = -1195.52075, z = 7045.61426, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_08"] = {x = -1105.57837, y = -813.586487, z = -7595.63818, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_07"] = {x = 6179.91553, y = -595.572937, z = -4791.40967, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_09"] = {x = 6807.08154, y = -711.371582, z = 3208.13306, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_06"] = {x = 6692.25244, y = -798.805664, z = 3490.44507, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_05"] = {x = 5244.42773, y = -978.980896, z = 4526.0332, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_08"] = {x = 7433.69336, y = -878.826416, z = 5458.37549, adjacent =
	[
	]},
	["FP_CAMPFIRE_ORK_OC_07"] = {x = 7676.60498, y = -868.850769, z = 5130.37842, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_02"] = {x = -4859.17041, y = -1096.52356, z = 7050.23584, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_06"] = {x = -2326.58643, y = -1030.47925, z = 7005.64746, adjacent =
	[
	]},
	["FP_CAMPFIRE_ORK_OC_05"] = {x = -2583.03882, y = -991.356689, z = 6707.74951, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_04"] = {x = -2147.23535, y = -1003.6427, z = 6731.93408, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_04"] = {x = -7916.52002, y = -1357.07483, z = 5343.74756, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_03"] = {x = -7339.39111, y = -1337.07483, z = 5876.82275, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_02"] = {x = -4809.07178, y = -880.491638, z = 4479.14697, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_01"] = {x = -2870.73999, y = -626.825867, z = 5348.38086, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_03"] = {x = -930.827759, y = -1091.71643, z = 7125.56152, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_01"] = {x = -4319.55176, y = -1050.37683, z = 7134.93896, adjacent =
	[
	]},
	["FP_CAMPFIRE_ORK_OC_03"] = {x = -5375.79736, y = -1153.62, z = 6415.16211, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_02"] = {x = -5318.59082, y = -1116.84277, z = 5906.81445, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_01"] = {x = -4893.625, y = -1109.93164, z = 6497.74658, adjacent =
	[
	]},
	["FP_GUARD_OC_GUARDSROOM_01"] = {x = -391.826019, y = 158.674652, z = 518.404846, adjacent =
	[
	]},
	["FP_CAMPFIRE_OC_PRISON_01"] = {x = -6023.34424, y = -312.712585, z = -980.038086, adjacent =
	[
	]},
	["FP_CAMPFIRE_OC_PRISON_03"] = {x = -5327.00098, y = -319.073608, z = 757.180237, adjacent =
	[
	]},
	["FP_GUARD_OC_THRONE_02"] = {x = -3223.79468, y = -33.666687, z = -2933.03955, adjacent =
	[
	]},
	["FP_GUARD_OC_THRONE_01"] = {x = -3243.83691, y = -26.666687, z = -3361.3335, adjacent =
	[
	]},
	["FP_CAMPFIRE_OC_OUT_02"] = {x = 2377, y = 192, z = -1185, adjacent =
	[
	]},
	["FP_CAMPFIRE_OC_OUT_01"] = {x = 2205, y = 172, z = -1043, adjacent =
	[
	]},
	["FP_CAMPFIRE_OC_GUARDROOM_02"] = {x = -912.937988, y = 175.524994, z = 1221.08752, adjacent =
	[
	]},
	["FP_CAMPFIRE_OC_GUARDROOM_01"] = {x = -1202.01208, y = 164.135696, z = 1253.06116, adjacent =
	[
	]},
	["FP_SMALLTALK_OC_EBR_01"] = {x = -2162.70947, y = -23.2743988, z = -3250.13428, adjacent =
	[
	]},
	["FP_SMALLTALK_OC_EBR_02"] = {x = -2123.36328, y = 6.7256012, z = -3078.79395, adjacent =
	[
	]},
	["FP_CAMPFIRE_OC_PRISON_05"] = {x = -5149.87256, y = -306.374878, z = -1127.18176, adjacent =
	[
	]},
	["FP_CAMPFIRE_OC_PRISON_02"] = {x = -5875.90918, y = -306.374878, z = -725.78186, adjacent =
	[
	]},
	["FP_CAMPFIRE_OC_PRISON_04"] = {x = -5119.69629, y = -330.769745, z = 832.71521, adjacent =
	[
	]},
	["FP_CAMPFIRE_OC_OUT_03"] = {x = 2037.38025, y = 178.49292, z = -1309.32434, adjacent =
	[
	]},
	["FP_CAMPFIRE_OC_OUT_04"] = {x = 2208.96118, y = 181.552917, z = -1494.5249, adjacent =
	[
	]},
	["FP_REST_OC_SHEEP_02"] = {x = -4195.11719, y = -298.464355, z = 412.146637, adjacent =
	[
	]},
	["FP_REST_OC_SHEEP_03"] = {x = -3618.96777, y = -307.647949, z = 728.924866, adjacent =
	[
	]},
	["FP_SLEEP_OC_SHEEP_01"] = {x = -3877.9917, y = -307.9953, z = 609.875183, adjacent =
	[
	]},
	["FP_SLEEP_OC_SHEEP_02"] = {x = -4047.75488, y = -294.123505, z = 803.348633, adjacent =
	[
	]},
	["FP_SLEEP_OC_SHEEP_03"] = {x = -3854.99829, y = -294.019958, z = 371.3797, adjacent =
	[
	]},
	["FP_REST_OC_SHEEP_01"] = {x = -3941.63086, y = -298.471313, z = 830.163513, adjacent =
	[
	]},
	["FP_GUARD_OC_EBR_01"] = {x = -1992.63525, y = 729.417603, z = -2257.3877, adjacent =
	[
	]},
	["FP_GUARD_OC_EBR_02"] = {x = -3262.45557, y = 709.417603, z = -2568.6665, adjacent =
	[
	]},
	["FP_GUARD_OC_EBR_03"] = {x = -1998.66504, y = 161.406082, z = -2417.72485, adjacent =
	[
	]},
	["FP_CAMPFIRE_OC_CHEST_01"] = {x = -4785.82471, y = -353.964783, z = -1881.31897, adjacent =
	[
	]},
	["FP_GUARD_OC_OPEN_ROOM_01"] = {x = -5292.7832, y = 196.148468, z = -191.821869, adjacent =
	[
	]},
	["FP_CAMPFIRE_ORK_OC_10"] = {x = -4380.02686, y = -815.938599, z = -7854.87402, adjacent =
	[
	]},
	["FP_CAMPFIRE_ORK_OC_11"] = {x = -4728.37305, y = -774.722656, z = -7770.95264, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_12"] = {x = -4570.75439, y = -802.722656, z = -8186.47461, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_13"] = {x = -4817.57959, y = -772.722656, z = -8024.18848, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_14"] = {x = -5741.02539, y = -759.722656, z = -7822.24268, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_15"] = {x = -6017.62598, y = -771.957581, z = -7719.32129, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_16"] = {x = -5602.70215, y = -772.722656, z = -7588.08447, adjacent =
	[
	]},
	["FP_CAMPFIRE_ORK_OC_17"] = {x = -5766.10986, y = -765.65155, z = -7392.58789, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_09"] = {x = -5333.82031, y = -499.486328, z = -5878.86377, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_10"] = {x = -4120.7124, y = -509.140625, z = -5759.50098, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_11"] = {x = -7594.07617, y = -415.646484, z = -4592.88574, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_12"] = {x = 4960.5166, y = -919.30957, z = -6782.37305, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_15"] = {x = 3022.6814, y = -878.36554, z = -7803.04346, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_16"] = {x = 2969.69971, y = -867.475342, z = -8181.26953, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_17"] = {x = 2162.44458, y = -496.931641, z = -6142.23389, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_18"] = {x = 1676.14368, y = -476.824921, z = -5938.43555, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_13"] = {x = -519.655518, y = -1024.8075, z = -8564.65527, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_19"] = {x = -4050.9209, y = -891.324341, z = -8509.90527, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_20"] = {x = -3222.79785, y = -961.398193, z = -8663.86816, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_14"] = {x = -7250.36328, y = -404.828308, z = -6008.90234, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_21"] = {x = -9203.47461, y = -693.275269, z = -5994.6958, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_22"] = {x = -8653.91504, y = -636.422241, z = -6422.61328, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_15"] = {x = -8141.19678, y = -554.397461, z = -3863.08691, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_23"] = {x = -7433.08643, y = -518.082642, z = -129.864029, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_24"] = {x = -7536.74121, y = -477.044067, z = -442.333221, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_25"] = {x = -9816.86426, y = -836.346008, z = 2783.25586, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_26"] = {x = -9727.10938, y = -886.346008, z = 3177.42139, adjacent =
	[
	]},
	["FP_ROAM_WARG_OC_01"] = {x = -9454.30078, y = -841.694275, z = -462.518921, adjacent =
	[
	]},
	["FP_ROAM_WARG_OC_02"] = {x = -9497.57129, y = -848.236755, z = -821.770935, adjacent =
	[
	]},
	["FP_ROAM_WARG_OC_03"] = {x = -8035.07324, y = -605.340698, z = 2222.56812, adjacent =
	[
	]},
	["FP_ROAM_WARG_OC_04"] = {x = -8252.29297, y = -490.433868, z = 1494.89478, adjacent =
	[
	]},
	["FP_ROAM_WARG_OC_05"] = {x = -5749.05859, y = -1042.98462, z = 3658.65112, adjacent =
	[
	]},
	["FP_ROAM_WARG_OC_06"] = {x = -6501.91553, y = -1044.47705, z = 3550.28125, adjacent =
	[
	]},
	["FP_ROAM_WARG_OC_07"] = {x = -5866.4751, y = -941.412476, z = 3173.60303, adjacent =
	[
	]},
	["FP_ROAM_WARG_OC_08"] = {x = -6410.54883, y = -1160.20703, z = 4230.51367, adjacent =
	[
	]},
	["FP_ROAM_WARG_OC_09"] = {x = -5178.89502, y = -924.901733, z = 4232.27979, adjacent =
	[
	]},
	["FP_ROAM_WARG_OC_10"] = {x = -134.734024, y = -994.758667, z = 4754.45361, adjacent =
	[
	]},
	["FP_ROAM_WARG_OC_11"] = {x = 354.005798, y = -930.887085, z = 4899.06592, adjacent =
	[
	]},
	["FP_ROAM_WARG_OC_12"] = {x = -75.0864563, y = -960.887085, z = 5127.12451, adjacent =
	[
	]},
	["FP_ROAM_WARG_OC_13"] = {x = 6070.10107, y = -595.234619, z = 696.691711, adjacent =
	[
	]},
	["FP_ROAM_WARG_OC_14"] = {x = 6657.28711, y = -605.234619, z = 1074.85791, adjacent =
	[
	]},
	["FP_ROAM_WARG_OC_15"] = {x = 6481.24121, y = -625.234619, z = 694.035706, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_16"] = {x = -3685.35986, y = -727.084412, z = 5246.82959, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_17"] = {x = 1871.84851, y = -1073.02686, z = 6804.42578, adjacent =
	[
	]},
	["FP_REST_ORK_OC_18"] = {x = 3455.78076, y = -1521.85144, z = 2708.22559, adjacent =
	[
	]},
	["FP_SLEEP_SHEEP_ORK"] = {x = 3407.45288, y = -1511.85144, z = 2956.13379, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_19"] = {x = -4338.11719, y = -773.329773, z = -7523.00537, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_20"] = {x = -5602.02783, y = -685.326172, z = -7165.98877, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_21"] = {x = -8872.45605, y = -607.94281, z = -6190.60352, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_22"] = {x = -9398.14063, y = -903.104492, z = -3008.65845, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_23"] = {x = -1811.99377, y = -970.491211, z = 6410.69922, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_24"] = {x = -1876.21533, y = -1021.49048, z = 7082.87549, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_25"] = {x = 5663.42236, y = -924.510803, z = 4524.27344, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_26"] = {x = 8265.06738, y = -752.382568, z = 2814.44116, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_27"] = {x = 8294.97461, y = -792.834106, z = -1263.1084, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_28"] = {x = 881.75116, y = -548.380737, z = -7028.17822, adjacent =
	[
	]},
	["FP_REST_ORK_OC_29"] = {x = 2015.04297, y = -529.473267, z = -6646.19385, adjacent =
	[
	]},
	["FP_SMALLTALK_OC_MAGE_01"] = {x = 621.664063, y = 280.693848, z = -3341.34229, adjacent =
	[
	]},
	["FP_SMALLTALK_OC_MAGE_02"] = {x = 597.339722, y = 280.693848, z = -3140.62378, adjacent =
	[
	]},
	["FP_STAND_OC_RETHON"] = {x = 815.854858, y = 179.072739, z = -79.086853, adjacent =
	[
	]},
	["FP_SMALLTALK_OC_CENTER_01"] = {x = -2638.48901, y = -104.124565, z = 17.632679, adjacent =
	[
	]},
	["FP_SMALLTALK_OC_CENTER_02"] = {x = -2605.32349, y = -142.716705, z = 203.255981, adjacent =
	[
	]},
	["FP_STAND_OC_TO_GUARD"] = {x = -1659.67358, y = -63.5958862, z = 838.207214, adjacent =
	[
	]},
	["FP_SMALLTALK_OC_CENTER_03"] = {x = -3874.89551, y = -238.745651, z = -622.975525, adjacent =
	[
	]},
	["FP_SMALLTALK_OC_CENTER_04"] = {x = -3745.61694, y = -194.599625, z = -759.745667, adjacent =
	[
	]},
	["FP_ROAM_BRUTUS_MEATBUGS_01"] = {x = -5260.20361, y = -389.84082, z = -3253.43945, adjacent =
	[
	]},
	["FP_ROAM_BRUTUS_MEATBUGS_02"] = {x = -5009.78662, y = -392.047363, z = -3029.43701, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_TUNING_01"] = {x = -4639.52002, y = -1056.21179, z = 6143.39697, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_TUNING_02"] = {x = -5110.93701, y = -984.941528, z = 5444.61914, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_TUNING_03"] = {x = -6212.85156, y = -1115.18469, z = 3876.33667, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_TUNING_04"] = {x = -3870.6394, y = -704.360107, z = 4198.05566, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_TUNING_05"] = {x = -3576.93555, y = -686.766663, z = 4135.9248, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_TUNING_07"] = {x = -3904.21851, y = -895.064331, z = 5585.58105, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_TUNING_08"] = {x = -4112.46484, y = -858.063599, z = 5219.21484, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_TUNING_09"] = {x = -5241.86914, y = -1161.49988, z = 6658.92529, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_TUNING_10"] = {x = -5500.31445, y = -1185.03528, z = 6226.02393, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_TUNING_11"] = {x = -6045.98828, y = -1204.97827, z = 6435.84912, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_TUNING_12"] = {x = -2564.84277, y = -1010.22943, z = 6875.37793, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_TUNING_13"] = {x = -2471.57056, y = -970.081482, z = 6490.18896, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_TUNING_15"] = {x = 2005.58679, y = -729.044006, z = 5064.37207, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_13_2"] = {x = -361.72168, y = -989.45874, z = -8428.66992, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_13_3"] = {x = -140.351486, y = -1004.01373, z = -8477.94824, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_20_2"] = {x = -3582.58228, y = -965.904663, z = -8591.5791, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_15_2"] = {x = -8063.16162, y = -585.066711, z = -3481.08521, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_11_2"] = {x = -7368.73389, y = -376.73111, z = -4755.42529, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_22_2"] = {x = -9339.63086, y = -974.309814, z = -2650.37158, adjacent =
	[
	]},
	["FP_ROAM_WARG_OC_04_2"] = {x = -8130.3623, y = -573.430298, z = 1837.16479, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_03_2"] = {x = -7585.9165, y = -1433.19641, z = 5958.2832, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_03_3"] = {x = -7896.44873, y = -1412.51465, z = 5775.68164, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_07_2"] = {x = 4806.75049, y = -1092.7301, z = 6769.50293, adjacent =
	[
	]},
	["FP_CAMPFIRE_ORK_OC_09"] = {x = 7001.4126, y = -777.924561, z = 3268.50439, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_08_2"] = {x = 5287.02148, y = -1127.95947, z = 7013.76172, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_08_3"] = {x = 5106.40771, y = -1085.06201, z = 6716.28369, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_17_2"] = {x = 2305.53662, y = -1051.65576, z = 6856.65186, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_17_3"] = {x = 1952.86438, y = -1112.17493, z = 7195.14502, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_05_2"] = {x = 2039.6543, y = -690.38739, z = 5237.0918, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_05_3"] = {x = 1732.99194, y = -734.052002, z = 5313.36084, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_04_2"] = {x = -579.924561, y = -1200.09973, z = 6769.51953, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_04_3"] = {x = -901.093872, y = -1113.53687, z = 6773.00195, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_14_2"] = {x = 7024.9917, y = -698.919434, z = -6013.59277, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_13_2"] = {x = 7428.23389, y = -686.841309, z = -5668.48438, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_07_2"] = {x = 5817.56396, y = -640.730957, z = -4728.34326, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_15_2"] = {x = 3282.21265, y = -901.186096, z = -7965.6626, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_16_2"] = {x = 2686.82349, y = -855.994934, z = -8231.23145, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_28_2"] = {x = 659.091858, y = -614.777283, z = -7233.86182, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_28_3"] = {x = 1063.03137, y = -584.667053, z = -7164.95703, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_27_4"] = {x = 7424.40723, y = -931.5578, z = 6221.58496, adjacent =
	[
	]},
	["FP_ROAM_ORK_OC_27_5"] = {x = 7246.8833, y = -948.046326, z = 6449.47363, adjacent =
	[
	]},
	["FP_CAMPFIRE_ORK_OC_07"] = {x = 7681.72754, y = -865.564758, z = 5376.13818, adjacent =
	[
	]},
	["FP_ITEM_OC_01"] = {x = -1415.88696, y = 778.77887, z = -1997.56824, adjacent =
	[
	]},
	["FP_ITEM_OC_02"] = {x = 606.383118, y = 773.326477, z = 2065.72607, adjacent =
	[
	]},

}

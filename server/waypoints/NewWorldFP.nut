WayPoints.FPs["NEWWORLD\\NEWWORLD.ZEN"] <-
{
	["FP_SHELLSPAWN_CITY_25"] = {x = -4453.91943, y = -615.886597, z = -19296.5488, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_24"] = {x = -2841.25024, y = -684.564941, z = -22754.5098, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_23"] = {x = -4035.14038, y = -541.204834, z = -19659.1738, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_22"] = {x = -5785.26172, y = -840.61377, z = -17330.4199, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_21"] = {x = -8088.2998, y = -701.154541, z = -11651.0879, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_20"] = {x = -8676.62305, y = -459.072021, z = -9814.10059, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_19"] = {x = -7442.21289, y = -721.50177, z = -4140.84229, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_18"] = {x = -8888.19629, y = -630.420349, z = -4902.49268, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_17"] = {x = -10379.2852, y = -753.990356, z = -5728.5, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_16"] = {x = -23967.1523, y = -768.151672, z = 1522.22937, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_15"] = {x = -25800.5195, y = -841.098328, z = 5899.46484, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_14"] = {x = -20963.2793, y = -530.860352, z = 8633.19141, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_13"] = {x = 1542.4939, y = -950.207947, z = 25888.5684, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_12"] = {x = 1966.80017, y = -645.158447, z = 24693.3887, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_11"] = {x = -147.33374, y = -408.061066, z = 24192.8047, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_10"] = {x = -3340.47314, y = -72.1065674, z = 18038.0156, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_09"] = {x = -1618.19067, y = -201.344406, z = 20185.8008, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_08"] = {x = -2449.40283, y = -423.671173, z = 11760.1279, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_07"] = {x = -857.872314, y = -260.88562, z = 13605.4102, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_06"] = {x = -1276.98718, y = -192.465881, z = 13836.0732, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_05"] = {x = -2907.21436, y = -388.023407, z = 13466.9902, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_04"] = {x = -3727.41895, y = -537.173218, z = 11728.2148, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_03"] = {x = -3528.88867, y = -430.302124, z = 11207.3672, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_02"] = {x = -3394.25195, y = -328.23526, z = 9373.24512, adjacent =
	[
	]},
	["FP_SHELLSPAWN_CITY_01"] = {x = -4014.25928, y = -471.007385, z = 8494.46582, adjacent =
	[
	]},
	["FP_PICK_CITY_PALCAMP_02"] = {x = -5296.9668, y = 153.58255, z = -5490.26611, adjacent =
	[
	]},
	["FP_PICK_CITY_PALCAMP_01"] = {x = -5194.60205, y = 153.58255, z = -5491.18945, adjacent =
	[
	]},
	["FP_STAND_NW_CITY_MERCHANT_SHOP03_FRONT_02"] = {x = 7308.63037, y = 296.084717, z = 3637.66772, adjacent =
	[
	]},
	["FP_SMALLTALK_GERBRANDT_02"] = {x = 1053.65833, y = -185.644958, z = -4372.87793, adjacent =
	[
	]},
	["FP_SMALLTALK_GERBRANDT_01"] = {x = 925.471802, y = -180.644958, z = -4515.43604, adjacent =
	[
	]},
	["FP_SMALLTALK_PECK_02"] = {x = 2193.49316, y = 109.824417, z = -3286.23242, adjacent =
	[
	]},
	["FP_SMALLTALK_PECK_01"] = {x = 2340.03467, y = 105.824417, z = -3289.4126, adjacent =
	[
	]},
	["FP_SWEEP_ALTE_02"] = {x = 10141.7451, y = 911.874756, z = -2798.98926, adjacent =
	[
	]},
	["FP_SWEEP_OV_05"] = {x = 11313.7627, y = 905.339111, z = 2241.15869, adjacent =
	[
	]},
	["FP_SWEEP_CITY_OV_03"] = {x = 10814.4531, y = 913.775146, z = 2026.44165, adjacent =
	[
	]},
	["FP_SWEEP_OV_04"] = {x = 10956.3594, y = 909.612183, z = -88.9907303, adjacent =
	[
	]},
	["FP_SWEEP_CITY_OV_02"] = {x = 11966.1367, y = 905.331787, z = -5138.72607, adjacent =
	[
	]},
	["FP_SWEEP_CITY_OV_01"] = {x = 12551.6982, y = 909.331787, z = -4770.79639, adjacent =
	[
	]},
	["FP_SWEEP_RICHTER_01"] = {x = 16706.2891, y = 908.556396, z = -1917.51758, adjacent =
	[
	]},
	["FP_PEE_CITY_OV_01"] = {x = 16814.9473, y = 951.226074, z = -3372.41504, adjacent =
	[
	]},
	["FP_STAND_GUARDING_OV_05"] = {x = 11854.5508, y = 922.772461, z = 1553.86194, adjacent =
	[
	]},
	["FP_STAND_GUARDING_OV_04"] = {x = 11935.334, y = 955.282288, z = 2275.27539, adjacent =
	[
	]},
	["FP_PICK_RICHTER_06"] = {x = 14406.2744, y = 1098.54639, z = 671.425171, adjacent =
	[
	]},
	["FP_SIT_CAMPFIRE_OV_03"] = {x = 14084.7461, y = 965.923218, z = 2261.88672, adjacent =
	[
	]},
	["FP_SIT_CAMPFIRE_OV_02"] = {x = 15534.8779, y = 932.048584, z = -3642.80322, adjacent =
	[
	]},
	["FP_SIT_CAMPFIRE_OV_01"] = {x = 14946.5732, y = 922.048584, z = -3343.76758, adjacent =
	[
	]},
	["FP_SWEEP_CITY_OV_04"] = {x = 10775.9277, y = 909.787842, z = -3551.40259, adjacent =
	[
	]},
	["FP_STAND_GUARDING_OV_02"] = {x = 12607.3359, y = 916.994751, z = -2072.23242, adjacent =
	[
	]},
	["FP_STAND_GUARDING_OV_01"] = {x = 13213.0811, y = 929.915649, z = -3525.20435, adjacent =
	[
	]},
	["FP_SWEEP_OV_03"] = {x = 11792.6494, y = 918.336304, z = 951.76239, adjacent =
	[
	]},
	["FP_SWEEP_OV_02"] = {x = 15194.6963, y = 919.333496, z = -2579.91162, adjacent =
	[
	]},
	["FP_SWEEP_OV_01"] = {x = 12178.3828, y = 925.333496, z = -2245.40381, adjacent =
	[
	]},
	["FP_PICK_RICHTER_03"] = {x = 16007.8281, y = 1073.0304, z = -1137.51624, adjacent =
	[
	]},
	["FP_PICK_RICHTER_02"] = {x = 16425.3262, y = 1023.0304, z = -1190.71033, adjacent =
	[
	]},
	["FP_PICK_RICHTER_01"] = {x = 16640.7871, y = 1142.0304, z = -456.649658, adjacent =
	[
	]},
	["FP_SIT_CAMPFIRE_KASERN"] = {x = 3024.2207, y = 767.141907, z = 8053.14453, adjacent =
	[
	]},
	["FP_STAND_GUARDING_ANDRE_GUARD"] = {x = 3454.77539, y = 763.497986, z = 6997.21631, adjacent =
	[
	]},
	["FP_ROAM_KANAL_RATTEN_10"] = {x = 1743.08826, y = -602.949097, z = 6893.10059, adjacent =
	[
	]},
	["FP_ROAM_KANAL_RATTEN_09"] = {x = 1451.45459, y = -622.949097, z = 6697.64941, adjacent =
	[
	]},
	["FP_ROAM_KANAL_RATTEN_08"] = {x = 1648.10376, y = -622.949097, z = 6053.66748, adjacent =
	[
	]},
	["FP_ROAM_KANAL_RATTEN_07"] = {x = 1788.96997, y = -612.949097, z = 6285.96191, adjacent =
	[
	]},
	["FP_ROAM_KANAL_RATTEN_05"] = {x = 2678.13184, y = -625.707092, z = 5676.8374, adjacent =
	[
	]},
	["FP_ROAM_KANAL_RATTEN_06"] = {x = 3008.64185, y = -624.53833, z = 5754.39258, adjacent =
	[
	]},
	["FP_ROAM_KANAL_RATTEN_04"] = {x = 2714.67798, y = -635.566895, z = 6541.53857, adjacent =
	[
	]},
	["FP_ROAM_KANAL_RATTEN_03"] = {x = 3031.80176, y = -605.381897, z = 6361.67725, adjacent =
	[
	]},
	["FP_ROAM_KANAL_RATTEN_02"] = {x = 2270.1377, y = -634.903076, z = 5955.51855, adjacent =
	[
	]},
	["FP_ROAM_KANAL_RATTEN_01"] = {x = 2133.50342, y = -614.903076, z = 6325.90039, adjacent =
	[
	]},
	["FP_SIT_CAMPFIRE_JOE"] = {x = 7798.60352, y = 274.953644, z = -803.233643, adjacent =
	[
	]},
	["FP_STAND_GUARDING_RAMIREZ"] = {x = 9212.03418, y = -632.675659, z = 2500.96265, adjacent =
	[
	]},
	["FP_STAND_GUARDING_JESPER"] = {x = 6851.61914, y = -634.312134, z = 2474.40674, adjacent =
	[
	]},
	["FP_STAND_GUARDING_HALVOR"] = {x = -289.488739, y = -191.913589, z = -4903.51807, adjacent =
	[
	]},
	["FP_SWEEP_ALTE_01"] = {x = 3331.85376, y = -109.982117, z = -2490.93628, adjacent =
	[
	]},
	["FP_PRAY_VATRAS"] = {x = 10136.2559, y = 278.815308, z = -708.996643, adjacent =
	[
	]},
	["FP_PEE_WIRT"] = {x = 5719.96289, y = 327.123993, z = 2017.71008, adjacent =
	[
	]},
	["FP_STAND_GUARDING_HANNA"] = {x = 7679.30176, y = 295.756012, z = 3026.7644, adjacent =
	[
	]},
	["FP_STAND_GUARDING_ZURIS"] = {x = 8782.67676, y = 278.480408, z = 3992.65015, adjacent =
	[
	]},
	["FP_STAND_GUARDING_MARKT_01"] = {x = 9835.81543, y = 292.739319, z = 3299.07666, adjacent =
	[
	]},
	["FP_SMALLTALK_SCHREINER_02"] = {x = 6535.01074, y = 289.671417, z = -2838.53564, adjacent =
	[
	]},
	["FP_SMALLTALK_SCHREINER_01"] = {x = 6444.6626, y = 290.575226, z = -2926.55396, adjacent =
	[
	]},
	["FP_SWEEP_BOGNER_02"] = {x = 7072.4751, y = 308.26825, z = -3875.20093, adjacent =
	[
	]},
	["FP_SWEEP_GRITTA"] = {x = 7668.21338, y = 275.317963, z = -2038.88477, adjacent =
	[
	]},
	["FP_ROAM_CITY_SHEEP_06"] = {x = 5386.25879, y = -169.738007, z = 252.037323, adjacent =
	[
	]},
	["FP_ROAM_CITY_SHEEP_05"] = {x = 5626.70068, y = -177.738007, z = 502.299377, adjacent =
	[
	]},
	["FP_ROAM_CITY_SHEEP_04"] = {x = 5367.69775, y = -174.738007, z = 696.120972, adjacent =
	[
	]},
	["FP_ROAM_CITY_SHEEP_03"] = {x = 5588.21729, y = -167.738007, z = 150.234863, adjacent =
	[
	]},
	["FP_ROAM_CITY_SHEEP_02"] = {x = 5728.67334, y = -177.738007, z = 688.507202, adjacent =
	[
	]},
	["FP_ROAM_CITY_SHEEP_01"] = {x = 5524.59619, y = -177.738007, z = 895.14624, adjacent =
	[
	]},
	["FP_STAND_CITY_BEER_03"] = {x = 5706.01074, y = 323.516113, z = 3007.63892, adjacent =
	[
	]},
	["FP_STAND_CITY_BEER_02"] = {x = 6097.91992, y = 323.516113, z = 2594.89917, adjacent =
	[
	]},
	["FP_STAND_CITY_BEER_01"] = {x = 6021.25293, y = 318.41156, z = 2836.13965, adjacent =
	[
	]},
	["FP_STAND_CITY_REGIS"] = {x = 7290.8916, y = 285.249207, z = -4945.22705, adjacent =
	[
	]},
	["FP_STAND_CITY_ANDRE"] = {x = 7922.0459, y = 289.430969, z = -5395.52783, adjacent =
	[
	]},
	["FP_PEE_CITY_02"] = {x = 6852.51953, y = 283.219543, z = -5933.28613, adjacent =
	[
	]},
	["FP_SWEEP_CITY_12"] = {x = 7340.22363, y = 279.32428, z = -5210.62305, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_36"] = {x = 12800.1465, y = 919.125122, z = 1814.80225, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_35"] = {x = 12867.8604, y = 915.937805, z = 1687.27478, adjacent =
	[
	]},
	["FP_STAND_CITY_32"] = {x = 12267.2158, y = 914.41748, z = 519.994812, adjacent =
	[
	]},
	["FP_STAND_RANGAR"] = {x = 13848.0176, y = 933.900391, z = -2464.03638, adjacent =
	[
	]},
	["FP_STAND_RAUL"] = {x = 11924.4727, y = 919.981262, z = -1691.40344, adjacent =
	[
	]},
	["FP_STAND_FERNANDO"] = {x = 12617.9727, y = 932.022339, z = -3700.04688, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_33"] = {x = 11502.7969, y = 925.969482, z = 1658.64478, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_32"] = {x = 11574.1699, y = 915.51825, z = 1514.9469, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_30"] = {x = 11447.4834, y = 941.973328, z = -2929.34521, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_31"] = {x = 11625.291, y = 940.973328, z = -2956.11548, adjacent =
	[
	]},
	["FP_STAND_GIRION"] = {x = 13289.5117, y = 1098.56665, z = -453.201843, adjacent =
	[
	]},
	["FP_STAND_LOTHAR"] = {x = 11915.4658, y = 924.566895, z = -3448.90405, adjacent =
	[
	]},
	["FP_STAND_HAGEN"] = {x = 15553.792, y = 1048.67944, z = 49.7171059, adjacent =
	[
	]},
	["FP_STAND_NADJA"] = {x = 1126.07385, y = -133.818405, z = -2903.44141, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_29"] = {x = 610.149597, y = -151.793747, z = -895.099487, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_28"] = {x = 624.228699, y = -146.797409, z = -1073.67993, adjacent =
	[
	]},
	["FP_STAND_TAVERN01_02"] = {x = 1274.18384, y = -173.7612, z = -686.345642, adjacent =
	[
	]},
	["FP_SMALLTALK_TAVERN01_02"] = {x = 1645.70862, y = -176.741241, z = -555.027527, adjacent =
	[
	]},
	["FP_SMALLTALK_TAVERN01_01"] = {x = 1789.85925, y = -178.826355, z = -551.117004, adjacent =
	[
	]},
	["FP_STAND_TAVERN01_01"] = {x = 1698.60779, y = -172.698303, z = -943.362732, adjacent =
	[
	]},
	["FP_STAND_HAKON"] = {x = 10340.4707, y = 299.403259, z = 3904.67578, adjacent =
	[
	]},
	["FP_STAND_BALTRAM"] = {x = 9066.55859, y = 293.015625, z = 4458.28516, adjacent =
	[
	]},
	["FP_STAND_ZURIS"] = {x = 8587.7207, y = 287.862518, z = 4445.20996, adjacent =
	[
	]},
	["FP_STAND_SARAH"] = {x = 9209.33496, y = 300.756866, z = 5862.98291, adjacent =
	[
	]},
	["FP_STAND_JORA"] = {x = 10578.8252, y = 287.822815, z = 4694.58887, adjacent =
	[
	]},
	["FP_STAND_CITY_12"] = {x = 8094.47559, y = 270.918732, z = -3462.94678, adjacent =
	[
	]},
	["FP_STAND_MATTEO"] = {x = 5755.5376, y = 286.041992, z = -4369.08691, adjacent =
	[
	]},
	["FP_STAND_CITY_11"] = {x = 7025.84229, y = 310.358337, z = -5156.96387, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_27"] = {x = 1516.99915, y = 322.769318, z = 4017.79419, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_26"] = {x = 1368.1499, y = 319.389252, z = 3953.00439, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_23"] = {x = 2719.94019, y = -166.876785, z = 975.014282, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_22"] = {x = 2578.41895, y = -170.907135, z = 971.381775, adjacent =
	[
	]},
	["FP_STAND_CITY_16"] = {x = 6251.06738, y = 553.635254, z = 4349.3623, adjacent =
	[
	]},
	["FP_STAND_CITY_13"] = {x = 5724.1792, y = 291.617706, z = -2511.94141, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_14"] = {x = 7942.44287, y = 293.093109, z = -4399.54053, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_12"] = {x = 7826.62402, y = 289.159882, z = -4442.27197, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_13"] = {x = 7354.43262, y = 169.878754, z = 275.712891, adjacent =
	[
	]},
	["FP_STAND_CITY_020"] = {x = 612.66217, y = -163.37442, z = 1071.58057, adjacent =
	[
	]},
	["FP_STAND_CITY_01"] = {x = 1170.31897, y = -171.998367, z = -1326.18225, adjacent =
	[
	]},
	["FP_PEE_CITY_01"] = {x = -739.051025, y = -160.148636, z = -2648.37012, adjacent =
	[
	]},
	["FP_STAND_CITY_05"] = {x = 384.538208, y = -170.096207, z = -2963.42139, adjacent =
	[
	]},
	["FP_STAND_CITY_06"] = {x = -74.5207367, y = -169.930115, z = -5180.15869, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_04_01"] = {x = 830.61322, y = -169.930115, z = -1733.53613, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_04_02"] = {x = 731.806335, y = -169.930115, z = -1574.37854, adjacent =
	[
	]},
	["FP_STAND_CITY_30"] = {x = 2800.85327, y = -166.198318, z = -4375.61377, adjacent =
	[
	]},
	["FP_STAND_CITY_31"] = {x = 2948.06421, y = -161.307648, z = -4620.89355, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_11"] = {x = 7340.61426, y = 149.214478, z = 107.310196, adjacent =
	[
	]},
	["FP_STAND_CITY_17"] = {x = 9556.95313, y = 294.533142, z = -1645.82935, adjacent =
	[
	]},
	["FP_STAND_CITY_18"] = {x = 6588.02393, y = 159.104904, z = 1051.80212, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_06"] = {x = 2874.27319, y = -96.9131165, z = -4075.41406, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_07"] = {x = 3051.33545, y = -103.386475, z = -4025.29004, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_20"] = {x = -330.284271, y = -153.898514, z = -4311.36621, adjacent =
	[
	]},
	["FP_SMALLTALK_21"] = {x = -176.925476, y = -156.566254, z = -4187.2041, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_19"] = {x = 2995.09814, y = -133.724518, z = -1510.58569, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_18"] = {x = 2925.66138, y = -128.147781, z = -1659.6626, adjacent =
	[
	]},
	["FP_STAND_CITY_10"] = {x = 1885.37402, y = -155.492722, z = -1861.2085, adjacent =
	[
	]},
	["FP_STAND_KASERN_01"] = {x = 4519.61426, y = 776.325134, z = 7063.45264, adjacent =
	[
	]},
	["FP_STAND_WAREHOUSE_01"] = {x = 2386.44604, y = 128.792404, z = 3288.41455, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_CITY_MERCHANT_01"] = {x = 9334.91699, y = 278.029053, z = 5166.77344, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_CITY_MERCHANT_02"] = {x = 9345.50488, y = 298.418945, z = 4961.229, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_MERCHANT_03"] = {x = 9670.43848, y = 299.73407, z = 4513.65869, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_MERCHANT_04"] = {x = 9906.50586, y = 279.73407, z = 4506.604, adjacent =
	[
	]},
	["FP_STAND_KARDIF"] = {x = 1734.11633, y = -169.490829, z = -1184.19275, adjacent =
	[
	]},
	["FP_STAND_PUFF_COUNTER"] = {x = 850.14856, y = -162.588348, z = -2739.58154, adjacent =
	[
	]},
	["FP_SMALLTALK_BARRACK02_01"] = {x = 3206.60474, y = 760.129944, z = 8262.62109, adjacent =
	[
	]},
	["FP_SMALLTALK_BARRACK02_02"] = {x = 3360.96729, y = 758.97467, z = 8265.75, adjacent =
	[
	]},
	["FP_STAND_TAVERN01_03"] = {x = 1566.31006, y = -159.772629, z = -902.048767, adjacent =
	[
	]},
	["FP_STAND_PUFF_02"] = {x = 865.407837, y = -161.255402, z = -2994.12598, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_37"] = {x = 11688.3672, y = 959.027222, z = 1843.0957, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_38"] = {x = 11836.5889, y = 959.625732, z = 1923.44714, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_40"] = {x = 13230.2793, y = 925.297485, z = -3025.12256, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_39"] = {x = 13259.3545, y = 920.368835, z = -3185.00586, adjacent =
	[
	]},
	["FP_STAND_CITY_TAVERN_01"] = {x = 8133.38916, y = 136.194458, z = 1269.21582, adjacent =
	[
	]},
	["FP_STAND_CITY_TAVERN_02"] = {x = 8052.20703, y = 137.194458, z = 1148.45654, adjacent =
	[
	]},
	["FP_STAND_CITY_TAVERN_03"] = {x = 7850.12109, y = 155.194458, z = 1363.55054, adjacent =
	[
	]},
	["FP_ROAM_CITY_RATS_01"] = {x = 4948.91553, y = -48.8025665, z = -3148.35132, adjacent =
	[
	]},
	["FP_ROAM_CITY_RATS_02"] = {x = 4675.89307, y = -85.8025665, z = -3276.677, adjacent =
	[
	]},
	["FP_ROAM_CITY_RATS_03"] = {x = 4569.54688, y = -72.8025665, z = -3046.96118, adjacent =
	[
	]},
	["FP_ROAM_CITY_RATS_04"] = {x = 4261.72998, y = -65.8025665, z = -3053.25439, adjacent =
	[
	]},
	["FP_PICK_CITY_01"] = {x = 3925.45435, y = -150.569244, z = 1451.86145, adjacent =
	[
	]},
	["FP_PICK_CITY_02"] = {x = 4227.91992, y = -177.081497, z = 1065.17627, adjacent =
	[
	]},
	["FP_PICK_CITY_03"] = {x = 4195.73926, y = -156.585876, z = 1326.18115, adjacent =
	[
	]},
	["FP_STAND_WEAPON_02"] = {x = 9450.4541, y = 303.325867, z = 4315.88916, adjacent =
	[
	]},
	["FP_STAND_WEAPON_03"] = {x = 10045.959, y = 299.484894, z = 4120.31641, adjacent =
	[
	]},
	["FP_STAND_WEAPON_04"] = {x = 10203.6807, y = 298.195068, z = 4747.00732, adjacent =
	[
	]},
	["FP_STAND_WEAPON_01"] = {x = 9115.5625, y = 284.705505, z = 5368.01416, adjacent =
	[
	]},
	["FP_STAND_RENGARU"] = {x = 8628.78125, y = 299.338806, z = 5177.50244, adjacent =
	[
	]},
	["FP_STAND_WEAPON_05"] = {x = 8296.48926, y = 309.412354, z = 4655.03564, adjacent =
	[
	]},
	["FP_STAND_DARON"] = {x = 9722.19922, y = 323.89386, z = 5623.03223, adjacent =
	[
	]},
	["FP_STAND_CITY_BEER_04"] = {x = 5516.69482, y = 354.818481, z = 2949.00415, adjacent =
	[
	]},
	["FP_SMALLTALK_BEER_01"] = {x = 5782.3457, y = 325.726227, z = 3313.95264, adjacent =
	[
	]},
	["FP_SMALLTALK_BEER_02"] = {x = 5815.10498, y = 330.303711, z = 3466.23145, adjacent =
	[
	]},
	["FP_SMALLTALK_BLUBBER_01"] = {x = 6368.71875, y = 336.220459, z = 3332.82837, adjacent =
	[
	]},
	["FP_SMALLTALK_BLUBBER_02"] = {x = 6225.0459, y = 344.961792, z = 3409.04321, adjacent =
	[
	]},
	["FP_STAND_VATRAS_01"] = {x = 8650.04297, y = 158.846329, z = -622.242676, adjacent =
	[
	]},
	["FP_STAND_VATRAS_02"] = {x = 8839.89746, y = 190.360504, z = -1148.5282, adjacent =
	[
	]},
	["FP_STAND_VATRAS_03"] = {x = 8826.33691, y = 191.131165, z = -764.893311, adjacent =
	[
	]},
	["FP_STAND_VATRAS_04"] = {x = 8731.28906, y = 191.239655, z = -957.02124, adjacent =
	[
	]},
	["FP_STAND_VATRAS_05"] = {x = 8824.39453, y = 160.890442, z = -472.979126, adjacent =
	[
	]},
	["FP_STAND_VATRAS_06"] = {x = 8727.74902, y = 180.338867, z = -290.215607, adjacent =
	[
	]},
	["FP_SMALLTALK_CITYKNEIPE_01"] = {x = 7964.44287, y = 188.241974, z = 104.6381, adjacent =
	[
	]},
	["FP_SMALLTALK_CITYKNEIPE_02"] = {x = 8130.74951, y = 184.463928, z = 39.9245186, adjacent =
	[
	]},
	["FP_SMALLTALK_CITYKNEIPE_03"] = {x = 8503.99414, y = 144.80661, z = 674.909851, adjacent =
	[
	]},
	["FP_SMALLTALK_CITYKNEIPE_04"] = {x = 8639.73438, y = 149.661652, z = 620.168335, adjacent =
	[
	]},
	["FP_STAND_LARES"] = {x = -266.469635, y = -104.790848, z = -926.253723, adjacent =
	[
	]},
	["FP_STAND_CITYKNEIPE_OUT"] = {x = 7375.26953, y = 146.305145, z = 743.10675, adjacent =
	[
	]},
	["FP_STAND_PUFFLADEN"] = {x = 1990.21838, y = -151.41217, z = -2201.68335, adjacent =
	[
	]},
	["FP_STAND_CITY_FREIBIERWIRT_01"] = {x = 5745.55371, y = 387.401062, z = 2622.37012, adjacent =
	[
	]},
	["FP_STAND_WATCH_01"] = {x = 3834.25098, y = -150.410156, z = 2958.42358, adjacent =
	[
	]},
	["FP_STAND_WATCH_02"] = {x = 4039.58032, y = -169.461502, z = 3126.39233, adjacent =
	[
	]},
	["FP_STAND_WATCH_03"] = {x = 4046.8291, y = -128.648712, z = 2848.95679, adjacent =
	[
	]},
	["FP_STAND_HAFEN_01"] = {x = 386.54776, y = -165.72963, z = -1163.67798, adjacent =
	[
	]},
	["FP_STAND_HAFEN_03"] = {x = 899.217041, y = -134.379776, z = -2041.66296, adjacent =
	[
	]},
	["FP_STAND_HAFEN_02"] = {x = 629.850525, y = -153.63736, z = -1334.28125, adjacent =
	[
	]},
	["FP_STAND_TAVERN01_04"] = {x = 1858.68994, y = -167.923111, z = -937.557495, adjacent =
	[
	]},
	["FP_STAND_TAVERN01_05"] = {x = 1990.62878, y = -168.714752, z = -908.448486, adjacent =
	[
	]},
	["FP_STAND_PUFF_01"] = {x = 722.947693, y = -164.823547, z = -2998.81396, adjacent =
	[
	]},
	["FP_STAND_PUFF_02"] = {x = 1005.66077, y = -153.976944, z = -3005.23438, adjacent =
	[
	]},
	["FP_STAND_CITY_20"] = {x = 3547.68896, y = -73.9384689, z = -1560.85303, adjacent =
	[
	]},
	["FP_SMALLTALK_HAFEN_01"] = {x = 248.191757, y = -131.597, z = 221.453262, adjacent =
	[
	]},
	["FP_SMALLTALK_HAFEN_02"] = {x = 364.139648, y = -145.271301, z = -7.27391338, adjacent =
	[
	]},
	["FP_SMALLTALK_HAFEN_03"] = {x = 47.5463066, y = -130.657303, z = -1812.34009, adjacent =
	[
	]},
	["FP_SMALLTALK_HAFEN_03"] = {x = 105.164932, y = -165.01236, z = -2020.73535, adjacent =
	[
	]},
	["FP_STAND_CITY_CAULDRON_01"] = {x = 2712.68774, y = -166.578232, z = -4536.39111, adjacent =
	[
	]},
	["FP_PEE_CITY_03"] = {x = 4804.69043, y = -64.3319931, z = -4342.70166, adjacent =
	[
	]},
	["FP_STAND_EATING_01"] = {x = 2245.39063, y = -93.5571289, z = -6172.88916, adjacent =
	[
	]},
	["FP_STAND_ALRIK"] = {x = 3619.86499, y = -120.005829, z = 3420.15137, adjacent =
	[
	]},
	["FP_SIT_CAMPFIRE_HALVOR"] = {x = 3947.29419, y = 753.88562, z = 4796.55859, adjacent =
	[
	]},
	["FP_STAND_WAITFORSHIP_01"] = {x = -8008.88965, y = -222.988632, z = -13008.418, adjacent =
	[
	]},
	["FP_SMALLTALK_WAITFORSHIP_01"] = {x = -7750.72559, y = -214.710144, z = -13215.3604, adjacent =
	[
	]},
	["FP_SMALLTALK_WAITFORSHIP_02"] = {x = -7657.89209, y = -195.548584, z = -12502.5449, adjacent =
	[
	]},
	["FP_SMALLTALK_WAITFORSHIP_03"] = {x = -7802.6665, y = -190.637207, z = -12628.4492, adjacent =
	[
	]},
	["FP_SMALLTALK_WAITFORSHIP_04"] = {x = -7592.96436, y = -217.766205, z = -13123.876, adjacent =
	[
	]},
	["FP_STAND_WAITFORSHIP_02"] = {x = -7789.24512, y = -205.098938, z = -12876.1582, adjacent =
	[
	]},
	["FP_SMALLTALK_WAITFORSHIP_05"] = {x = -6938.19141, y = -203.138184, z = -11793.376, adjacent =
	[
	]},
	["FP_SMALLTALK_WAITFORSHIP_06"] = {x = -7090.26855, y = -217.924164, z = -12129.1074, adjacent =
	[
	]},
	["FP_STAND_WAITFORSHIP_03"] = {x = -7807.57227, y = -190.591858, z = -12174.8838, adjacent =
	[
	]},
	["FP_STAND_WAITFORSHIP_04"] = {x = -7443.37061, y = -187.386459, z = -11988.1758, adjacent =
	[
	]},
	["FP_STAND_WAITFORSHIP_05"] = {x = -6884.66504, y = -185.277176, z = -12571.0234, adjacent =
	[
	]},
	["FP_STAND_WAITFORSHIP_06"] = {x = -6984.79395, y = -197.88739, z = -12669.7764, adjacent =
	[
	]},
	["FP_STAND_WAITFORSHIP_07"] = {x = -7086.16309, y = -214.523163, z = -11719.9248, adjacent =
	[
	]},
	["FP_STAND_WAITFORSHIP_08"] = {x = -6931.12451, y = -215.370087, z = -12077.9238, adjacent =
	[
	]},
	["FP_STAND_CITY_PALCAMP_01"] = {x = -4240.27734, y = 180.477417, z = -5353.83252, adjacent =
	[
	]},
	["FP_STAND_CITY_PALCAMP_03"] = {x = -6431.69141, y = 157.691345, z = -6211.02686, adjacent =
	[
	]},
	["FP_STAND_CITY_PALCAMP_02"] = {x = -6461.2998, y = 160.268509, z = -5916.49463, adjacent =
	[
	]},
	["FP_STAND_CITY_PALCAMP_04"] = {x = -3727.12915, y = 160.837891, z = -5656.4668, adjacent =
	[
	]},
	["FP_STAND_CITY_PALCAMP_05"] = {x = -3869.64746, y = 148.139664, z = -5774.09229, adjacent =
	[
	]},
	["FP_STAND_CITY_PALCAMP_06"] = {x = -4051.81494, y = 151.251633, z = -5667.66309, adjacent =
	[
	]},
	["FP_STAND_CITY_PALCAMP_07"] = {x = -4069.77417, y = 152.120926, z = -5137.14063, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_PALCAMP_02"] = {x = -3719.94019, y = 153.893036, z = -5321.62939, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_PALCAMP_01"] = {x = -3830.28491, y = 148.796692, z = -5207.53955, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_PALCAMP_03"] = {x = -4971.31787, y = 151.448776, z = -5907.65576, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_PALCAMP_04"] = {x = -4850.15576, y = 165.242493, z = -5789.70605, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_PALCAMP_06"] = {x = -4401.24854, y = 148.36998, z = -5113.81689, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_PALCAMP_05"] = {x = -4390.70752, y = 155.310547, z = -5275.91309, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_PALCAMP_08"] = {x = -3050.79492, y = 160.911896, z = -5419.97119, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_PALCAMP_07"] = {x = -2888.79663, y = 137.328018, z = -5482.19043, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_PALCAMP_09"] = {x = -1352.77039, y = -155.891342, z = -3986.03076, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_PALCAMP_10"] = {x = -1211.70435, y = -161.496506, z = -3815.62988, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_PALCAMP_11"] = {x = -1608.90613, y = -165.527466, z = -4228.07813, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_PALCAMP_12"] = {x = -1734.06848, y = -165.527466, z = -4320.89941, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_PALCAMP_13"] = {x = -1410.45593, y = -131.529434, z = -4878.60107, adjacent =
	[
	]},
	["FP_SMALLTALK_CITY_PALCAMP_14"] = {x = -1365.06934, y = -140.5466, z = -5036.95215, adjacent =
	[
	]},
	["FP_STAND_GUARDING_MORTIS"] = {x = 6490.47949, y = 765.909119, z = 8785.51074, adjacent =
	[
	]},
	["FP_SMALLTALK_BARRACK02_03"] = {x = 2895.41064, y = 775.561035, z = 7595.35059, adjacent =
	[
	]},
	["FP_SMALLTALK_BARRACK02_04"] = {x = 3063.5835, y = 769.561035, z = 7585.21924, adjacent =
	[
	]},
	["FP_STAND_GUARDING_BOLTAN"] = {x = 4281.24805, y = 766.737732, z = 5614.07568, adjacent =
	[
	]},
	["FP_SMALLTALK_WULFGAR"] = {x = 3412.69922, y = 756.737732, z = 6849.64453, adjacent =
	[
	]},
	["FP_SMALLTALK_ANDRE"] = {x = 3364.46436, y = 759.737732, z = 6687.99023, adjacent =
	[
	]},
	["FP_STAND_KASERN_02"] = {x = 3995.85986, y = 760.55426, z = 6767.32324, adjacent =
	[
	]},
	["FP_SMALLTALK_PALADIN_01"] = {x = 13920.2295, y = 908.326416, z = -3234.26465, adjacent =
	[
	]},
	["FP_SMALLTALK_PALADIN_02"] = {x = 14111.6514, y = 909.326416, z = -3246.67749, adjacent =
	[
	]},
	["FP_SMALLTALK_PALADIN_03"] = {x = 14566.7236, y = 911.478516, z = -4032.27612, adjacent =
	[
	]},
	["FP_SMALLTALK_PALADIN_04"] = {x = 14495.2646, y = 911.326416, z = -4175.96924, adjacent =
	[
	]},
	["FP_ROAM_INSEL_01"] = {x = -23857.5684, y = -495.29071, z = 4518.22461, adjacent =
	[
	]},
	["FP_ROAM_INSEL_02"] = {x = -23752.4688, y = -471.29071, z = 4872.07715, adjacent =
	[
	]},
	["FP_ROAM_INSEL_03"] = {x = -22890.502, y = -151.29071, z = 5488.78955, adjacent =
	[
	]},
	["FP_ROAM_INSEL_04"] = {x = -22541.7227, y = -481.29071, z = 5448.97119, adjacent =
	[
	]},
	["FP_ROAM_INSEL_05"] = {x = -21329.7188, y = -276.699554, z = 2182.24146, adjacent =
	[
	]},
	["FP_ROAM_INSEL_06"] = {x = -21338.4277, y = -314.699554, z = 1908.44531, adjacent =
	[
	]},
	["FP_ROAM_INSEL_07"] = {x = -22716.8887, y = -534.699585, z = 1612.06982, adjacent =
	[
	]},
	["FP_ROAM_INSEL_08"] = {x = -22632.7988, y = -475.699585, z = 1928.02856, adjacent =
	[
	]},
	["FP_ROAM_INSEL_09"] = {x = -20222.168, y = -321.366638, z = 2518.71167, adjacent =
	[
	]},
	["FP_ROAM_INSEL_10"] = {x = -20038.8789, y = -328.366638, z = 2324.4397, adjacent =
	[
	]},
	["FP_ROAM_INSEL_11"] = {x = -19766.0723, y = -325.366638, z = 2914.19946, adjacent =
	[
	]},
	["FP_ROAM_INSEL_12"] = {x = -19525.2266, y = -320.366638, z = 2441.44116, adjacent =
	[
	]},
	["FP_ROAM_INSEL_13"] = {x = -23795.7617, y = -656.366638, z = 548.042847, adjacent =
	[
	]},
	["FP_ROAM_INSEL_14"] = {x = -23172.9668, y = -656.219177, z = 463.920074, adjacent =
	[
	]},
	["FP_ROAM_SHIPWRECK_01"] = {x = 1514.68958, y = -328.259705, z = 23942.748, adjacent =
	[
	]},
	["FP_ROAM_SHIPWRECK_02"] = {x = 711.528259, y = -100.46653, z = 23371.4316, adjacent =
	[
	]},
	["FP_ROAM_SHIPWRECK_03"] = {x = 1236.17053, y = -571.85614, z = 24609.3027, adjacent =
	[
	]},
	["FP_ROAM_SHIPWRECK_04"] = {x = -495.179932, y = -127.767303, z = 23416.7773, adjacent =
	[
	]},
	["FP_ROAM_FISHERCOAST_01"] = {x = -3182.36865, y = -422.08139, z = 12419.5254, adjacent =
	[
	]},
	["FP_ROAM_FISHERCOAST_02"] = {x = -2998.52075, y = -421.08139, z = 12009.8164, adjacent =
	[
	]},
	["FP_ROAM_FISHERCOAST_03"] = {x = -3381.40063, y = -420.831451, z = 12703.1641, adjacent =
	[
	]},
	["FP_ROAM_FISHERMAN_01"] = {x = -10040.374, y = -616.083618, z = -6153.96484, adjacent =
	[
	]},
	["FP_ROAM_FISHERMAN_02"] = {x = -5434.6001, y = -180.825638, z = -10671.8418, adjacent =
	[
	]},
	["FP_ROAM_FISHERMAN_03"] = {x = -5832.08936, y = -180.825638, z = -10957.1104, adjacent =
	[
	]},
	["FP_ROAM_FISHERMAN_04"] = {x = -3818.1145, y = -423.788177, z = -18550.8926, adjacent =
	[
	]},
	["FP_ROAM_FISHERMAN_05"] = {x = -4145.83496, y = -455.793121, z = -18279.9102, adjacent =
	[
	]},
	["FP_CITY_WEAPON_01"] = {x = 8924.22852, y = 367.425842, z = -6537.84326, adjacent =
	[
	]},
	["FP_CITY_WEAPON_02"] = {x = 7124.84619, y = 373.425842, z = -6166.08398, adjacent =
	[
	]},
	["FP_CITY_WEAPON_03"] = {x = 10837.8721, y = 371.425842, z = 5380.49561, adjacent =
	[
	]},
	["FP_CITY_WEAPON_04"] = {x = 10037.8477, y = 368.425842, z = 6622.41846, adjacent =
	[
	]},
	["FP_CITY_WEAPON_05"] = {x = 7629.1792, y = 376.425842, z = -907.031189, adjacent =
	[
	]},
	["FP_CITY_WEAPON_06"] = {x = 5514.82178, y = 293.178833, z = -3349.52124, adjacent =
	[
	]},
	["FP_STAND_TAVERN01_02_B"] = {x = 1174.04907, y = -174.335663, z = -587.012329, adjacent =
	[
	]},
	["FP_ITEM_LAGERHAUS_01"] = {x = 2081.92578, y = -91.9524536, z = 2917.76904, adjacent =
	[
	]},
	["FP_ITEM_OV_01"] = {x = 10823.876, y = 1059.89124, z = 2897.38379, adjacent =
	[
	]},
	["FP_ITEM_GAERTNER_01"] = {x = 16749.4707, y = 1095.66296, z = -857.6026, adjacent =
	[
	]},
	["FP_ITEM_GAERTNER_02"] = {x = 16975.7715, y = 1107.66296, z = -183.845978, adjacent =
	[
	]},
	["FP_ITEM_GAERTNER_03"] = {x = 16419.9102, y = 1103.66296, z = -20.9575157, adjacent =
	[
	]},
	["FP_ITEM_GAERTNER_04"] = {x = 16611.7793, y = 1105.66296, z = 508.198547, adjacent =
	[
	]},
	["FP_ITEM_GAERTNER_05"] = {x = 16241.2129, y = 1097.66296, z = 705.127808, adjacent =
	[
	]},
	["FP_ITEM_OV_02"] = {x = 11021.8662, y = 1390.51538, z = 998.859558, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_CITY_PIRATESCAMP_01"] = {x = -3214.87915, y = -401.124573, z = 11687.4199, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_CITYWALL_RIGHT_04_04"] = {x = 13042.6582, y = 941.251831, z = -7373.66699, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_CITYWALL_RIGHT_04_03"] = {x = 13002.8193, y = 945.164429, z = -6652.83496, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_CITYWALL_RIGHT_04_02"] = {x = 12801.3564, y = 927.557739, z = -7067.94043, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_CITYWALL_RIGHT_04_01"] = {x = 13337.8574, y = 947.120728, z = -7185.47168, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_18"] = {x = 8260.75488, y = 525.259644, z = -10007.4521, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_17"] = {x = 7392.71826, y = 549.413025, z = -11405.7725, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_35"] = {x = 17397.2207, y = 2015.14453, z = -14622.3174, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_34"] = {x = 8020.56299, y = 428.464996, z = -8651.96387, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_33"] = {x = 16036.9883, y = 2422.22705, z = -10173.4268, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_32"] = {x = 15816.9736, y = 2403.44727, z = -10016.9541, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_31"] = {x = 16131.6943, y = 2363.2793, z = -10546.1504, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_30"] = {x = -2914.98804, y = 419.590668, z = -14401.3574, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_29"] = {x = -3391.06909, y = 253.350525, z = -14304.5293, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_28"] = {x = 4093.42651, y = 412.272675, z = -23392.666, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_27"] = {x = 4183.97656, y = 441.12677, z = -21596.6758, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_26"] = {x = 4349.69141, y = 467.195526, z = -23616.0254, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_16"] = {x = 8301.07324, y = 1322.87842, z = -21208.5664, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_25"] = {x = 8614.54688, y = 1357.6438, z = -20712.3555, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_24"] = {x = 8080.60059, y = 1317.6438, z = -21183.6836, adjacent =
	[
	]},
	["FP_ROAM_FARM1_GOBBO_01"] = {x = 7617.22754, y = 1324.60059, z = -20478.4219, adjacent =
	[
	]},
	["FP_ROAM_FARM1_WOLF_04"] = {x = 9582.4834, y = 785.4729, z = -10192.7158, adjacent =
	[
	]},
	["FP_ROAM_FARM1_WOLF_03"] = {x = 9300.16406, y = 745.4729, z = -10032.4141, adjacent =
	[
	]},
	["FP_ROAM_FARM1_WOLF_02"] = {x = 9261.11914, y = 735.4729, z = -10470.4863, adjacent =
	[
	]},
	["FP_ROAM_FARM1_WOLF_01"] = {x = 9463.99414, y = 775.4729, z = -10470.7041, adjacent =
	[
	]},
	["FP_ROAM_FARM1_BLOODFLY_04"] = {x = 7469.16064, y = 819.445129, z = -13763.8232, adjacent =
	[
	]},
	["FP_ROAM_FARM1_BLOODFLY_03"] = {x = 7458.87842, y = 871.386292, z = -14083.7783, adjacent =
	[
	]},
	["FP_ROAM_FARM1_BLOODFLY_02"] = {x = 7759.80566, y = 799.445129, z = -13753.2324, adjacent =
	[
	]},
	["FP_ROAM_FARM1_BLOODFLY_01"] = {x = 7705.33936, y = 872.386536, z = -14130.2393, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_MILL_01_04"] = {x = 16718.209, y = 1984.65051, z = -13705.6602, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_MILL_01_03"] = {x = 16361.4922, y = 1954.65051, z = -13665.3252, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_MILL_01_02"] = {x = 16172.917, y = 1942.05078, z = -13090.2773, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_MILL_01_01"] = {x = 16823.7344, y = 1989.30725, z = -13001.4658, adjacent =
	[
	]},
	["FP_STAND_NW_FARM1_LOBART"] = {x = 13641.1973, y = 1770.71997, z = -15698.3291, adjacent =
	[
	]},
	["FP_PICK_NW_FARM1_FIELD_03_01"] = {x = 11047.9443, y = 1477.79272, z = -14496.5703, adjacent =
	[
	]},
	["FP_PICK_NW_FARM1_FIELD_03_01"] = {x = 11523.8936, y = 1427.14978, z = -14660.4951, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_06_07"] = {x = 9223.9541, y = 1375.31421, z = -15360.1963, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_06_06"] = {x = 8587.96875, y = 1160.4165, z = -14897.2061, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_CITYWALL_RIGHT_02_03"] = {x = 9901.5, y = 914.834229, z = -8733.99609, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_CITYWALL_RIGHT_02"] = {x = 10464.6582, y = 944.834229, z = -8391.2041, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_CITYWALL_RIGHT_02_01"] = {x = 10564.042, y = 944.834229, z = -8920.65625, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_OUT_13_03"] = {x = 13821.0527, y = 1685.21399, z = -10105.5752, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_OUT_13_02"] = {x = 13656.5908, y = 1685.21399, z = -9108.75586, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_OUT_13_01"] = {x = 13014.9238, y = 1685.21399, z = -9623.1123, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_OUT_15_04"] = {x = 18584.4512, y = 2301.74072, z = -8903.86328, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_OUT_15_03"] = {x = 18030.8125, y = 2424.48462, z = -9655.7002, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_OUT_15_02"] = {x = 17807.7656, y = 2566.93726, z = -9053.51563, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_OUT_15_01"] = {x = 17463.5137, y = 2616.89697, z = -9596.56738, adjacent =
	[
	]},
	["FP_PICK_FARM1_FIELD_06_01"] = {x = 11853.8145, y = 1462.43823, z = -11600.8174, adjacent =
	[
	]},
	["FP_PICK_NW_FARM1_FIELD_04_02"] = {x = 9716.62891, y = 1365.6792, z = -13635.5801, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SHEEP_05_03"] = {x = 12965.4092, y = 1625.23047, z = -18796.2852, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SHEEP_05_01"] = {x = 13282.2725, y = 1688.71509, z = -19157.8281, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SHEEP_05_02"] = {x = 12672.0967, y = 1668.71509, z = -18794.4121, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SHEEP_01_04"] = {x = 9670.34863, y = 1342.93567, z = -15985.5732, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SHEEP_01_03"] = {x = 9539.41016, y = 1390.43054, z = -15640.6855, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SHEEP_01_02"] = {x = 9468.85449, y = 1371.2793, z = -15847.8525, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SHEEP_01_01"] = {x = 9884.6709, y = 1382.28467, z = -15846.082, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SHEEP_02_05"] = {x = 12447.6543, y = 1682.36462, z = -18733.4883, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SHEEP_02_04"] = {x = 12346.8184, y = 1660.4707, z = -18518.5586, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SHEEP_02_01"] = {x = 13095.3682, y = 1655.14355, z = -19365.2461, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SHEEP_04_04"] = {x = 11753.624, y = 1622.16052, z = -18551.5742, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SHEEP_04_03"] = {x = 12044.4512, y = 1657.27026, z = -18447.9219, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SHEEP_04_02"] = {x = 11861.4277, y = 1628.40234, z = -18948.9258, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SHEEP_04_01"] = {x = 12273.1133, y = 1660.40405, z = -18993.3555, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_06_05"] = {x = 8993.32227, y = 1230.55981, z = -14898.2217, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_05_03"] = {x = 8532.24219, y = 522.469116, z = -11605.8867, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_05_02"] = {x = 8375.44141, y = 562.318054, z = -11174.0781, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_05_01"] = {x = 8232.54688, y = 564.523926, z = -11759.1895, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_03_03"] = {x = 9574.31836, y = 1571.20129, z = -20289.5918, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_03_02"] = {x = 9327.83008, y = 1467.4574, z = -20946.4902, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_03_01"] = {x = 8929.375, y = 1486.1145, z = -20436.2852, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_01_03"] = {x = 10540.2881, y = 1639.21814, z = -21370.4453, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_01_02"] = {x = 11261.2012, y = 1658.92859, z = -21271.8965, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_01_01"] = {x = 10836.3896, y = 1599.3855, z = -21299.4336, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_02_05"] = {x = 11010.8906, y = 1654.521, z = -20888.002, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_02_02"] = {x = 11054.4922, y = 1646.04053, z = -21296.498, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_02_01"] = {x = 11166.9727, y = 1690.39014, z = -21152.4707, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_02_04"] = {x = 10763.8936, y = 1628.47766, z = -21124.4883, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_07_01"] = {x = 9185.66309, y = 1602.99695, z = -22990.1523, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_07_02"] = {x = 8871.63184, y = 1451.26013, z = -22875.1504, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_07_03"] = {x = 8978.71387, y = 1455.50085, z = -23390.2266, adjacent =
	[
	]},
	["FP_PICK_NW_FARM1_FIELD_01_01"] = {x = 11392.6514, y = 1500.96069, z = -14340.9746, adjacent =
	[
	]},
	["FP_PICK_NW_FARM1_FIELD_01_01"] = {x = 10599.667, y = 1439.76038, z = -14460.7881, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM1_MILL_01"] = {x = 15894.6455, y = 1953.11206, z = -14283.3076, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM1_MILL_01"] = {x = 15771.0176, y = 1959.11206, z = -14197.9346, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM1_MILL_01"] = {x = 15908.751, y = 1951.8645, z = -14502.165, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_01"] = {x = 12642.3076, y = 1652.70056, z = -19045.5273, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_02"] = {x = 12889.3555, y = 1662.70056, z = -19417.8633, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_MILL_01_05"] = {x = 16142.8047, y = 1957.00659, z = -13476.6445, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_MILL_01_06"] = {x = 16893.791, y = 2017.00659, z = -13473.1221, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_03"] = {x = 12737.5771, y = 1692.69824, z = -19738.8809, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_04"] = {x = 12314.667, y = 1690.92822, z = -19646.2227, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_05"] = {x = 12304.3818, y = 1673.65588, z = -19333.2559, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_06"] = {x = 12540.1123, y = 1667.40857, z = -19764.6191, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_07"] = {x = 12707.4092, y = 1717.08398, z = -19534.6348, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_08"] = {x = 12579.4297, y = 1688.67651, z = -19225.4121, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_09"] = {x = 12875.9541, y = 1673.67468, z = -15869.4385, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_10"] = {x = 13193.0977, y = 1713.67468, z = -15856.3164, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_11"] = {x = 12825.0176, y = 1683.67468, z = -15552.9883, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_12"] = {x = 13112.5029, y = 1713.67468, z = -15554.6445, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_13"] = {x = 12750.2734, y = 1673.73425, z = -14840.6523, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_14"] = {x = 12465.873, y = 1523.67468, z = -14429.5293, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_15"] = {x = 12391.542, y = 1613.67468, z = -14767.2393, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_16"] = {x = 12787.0156, y = 1660.31921, z = -14527.1396, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_17"] = {x = 12435.2246, y = 1550.76733, z = -15929.1211, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_18"] = {x = 12232.5889, y = 1550.76733, z = -15551.6465, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_19"] = {x = 12014.1006, y = 1470.76733, z = -15763.3418, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_20"] = {x = 12503.624, y = 1591.22717, z = -15598.2568, adjacent =
	[
	]},
	["FP_STAND_FARM1_HIRTE"] = {x = 11963.4707, y = 1633.96204, z = -18730.8555, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_21"] = {x = 13079.4014, y = 1777.03064, z = -14852.6406, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_22"] = {x = 13286.2168, y = 1737.03064, z = -14492.6475, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_23"] = {x = 13273.6299, y = 1788.03064, z = -15000.8057, adjacent =
	[
	]},
	["FP_ROAM_FARM1_SHEEP_24"] = {x = 13454.9277, y = 1757.03064, z = -14673.2266, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM1_PATH_SPAWN_07_04"] = {x = 8584.52344, y = 1426.30139, z = -23105.1406, adjacent =
	[
	]},
	["FP_STAND_FARM1_CANTHAR"] = {x = 8150.63672, y = 1150.44519, z = -17440.1875, adjacent =
	[
	]},
	["FP_ROAM_FARM1_GOBBO_02"] = {x = 5407.94238, y = 396.151306, z = -22879.3379, adjacent =
	[
	]},
	["FP_ROAM_FARM1_GOBBO_03"] = {x = 5290.40674, y = 436.151306, z = -23287.7305, adjacent =
	[
	]},
	["FP_ROAM_FARM1_GOBBO_04"] = {x = 4990.43262, y = 426.151306, z = -23135.9941, adjacent =
	[
	]},
	["FP_ROAM_FARM1_GOBBO_05"] = {x = 4487.80615, y = 396.151306, z = -22644.8574, adjacent =
	[
	]},
	["FP_ROAM_FARM1_GOBBO_06"] = {x = 5465.46436, y = 366.151306, z = -22481.6465, adjacent =
	[
	]},
	["FP_ROAM_FARM1_BLOODFLY_05"] = {x = 4331.14697, y = 259.067383, z = -9453.95117, adjacent =
	[
	]},
	["FP_ROAM_FARM1_BLOODFLY_06"] = {x = 4408.95264, y = 219.067383, z = -8967.38867, adjacent =
	[
	]},
	["FP_ROAM_FARM1_BLOODFLY_07"] = {x = 3974.87769, y = 229.067383, z = -9141.05859, adjacent =
	[
	]},
	["FP_ROAM_FARM1_BLOODFLY_08"] = {x = 4719.52002, y = 286.039551, z = -9351.34863, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_WOLF_01"] = {x = 3095.71436, y = 343.720795, z = -13055.709, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_WOLF_02"] = {x = 3178.58447, y = 313.720795, z = -13359.0938, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_WOLF_03"] = {x = 3642.05469, y = 333.720795, z = -13800.9746, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_WOLF_04"] = {x = 4090.27881, y = 241.967316, z = -12946.252, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_WOLF_05"] = {x = 3819.44019, y = 225.052887, z = -12630.9229, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_WOLF_06"] = {x = 3428.21826, y = 273.720795, z = -13079.3506, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_WOLF_07"] = {x = 3706.65381, y = 322.481201, z = -13383.04, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_WOLF_08"] = {x = 4363.65674, y = 253.720795, z = -13350.1787, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_WOLF_09"] = {x = 4331.30811, y = 243.720795, z = -13676.7734, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_WOLF_10"] = {x = 3845.15308, y = 243.720795, z = -12948.3076, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_SHADOW_01"] = {x = 2924.11548, y = 365.50531, z = -15289.7061, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_SHADOW_02"] = {x = 3012.22192, y = 375.50531, z = -15861.5742, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_SHADOW_03"] = {x = 3484.8457, y = 423.915527, z = -15879.9697, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_BLOODFLY_01"] = {x = 3957.29248, y = 543.430176, z = -18187.0684, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_BLOODFLY_02"] = {x = 3695.04321, y = 543.430176, z = -17541.4648, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_BLOODFLY_03"] = {x = 3528.27905, y = 568.603027, z = -18022.3945, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_BLOODFLY_04"] = {x = 4612.92285, y = 543.430176, z = -17808.7012, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_BLOODFLY_05"] = {x = 4101.34082, y = 489.990417, z = -16647.1797, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_BLOODFLY_06"] = {x = 3797.6814, y = 483.430176, z = -17058.7285, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_BLOODFLY_07"] = {x = 4920.49268, y = 493.430176, z = -17296.0215, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_BLOODFLY_08"] = {x = 4263.3999, y = 483.430176, z = -17350.5879, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_BLOODFLY_09"] = {x = 5037.01123, y = 580.08252, z = -17944.6719, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_BLOODFLY_10"] = {x = 4700.91602, y = 626.846741, z = -18315.3496, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_SNAPPER_01"] = {x = 342.771881, y = -17.0381165, z = -7682.86816, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_SNAPPER_02"] = {x = 431.944916, y = 72.9618835, z = -8372.15625, adjacent =
	[
	]},
	["FP_ROAM_FARM1_FORREST_SNAPPER_03"] = {x = -42.1589508, y = 32.9618835, z = -7940.76904, adjacent =
	[
	]},
	["FP_ROAM_FARM1_ORC_SCOUT_01"] = {x = 2762.88013, y = 642.333191, z = -19599.4063, adjacent =
	[
	]},
	["FP_PICK_FARM1_FIELD_06_02"] = {x = 12290.1631, y = 1481.47803, z = -11781.0791, adjacent =
	[
	]},
	["FP_PICK_FARM1_FIELD_06_03"] = {x = 11511.875, y = 1441.89233, z = -11823.1035, adjacent =
	[
	]},
	["FP_PICK_FARM1_FIELD_06_04"] = {x = 11011.5449, y = 1486.30249, z = -11291.0762, adjacent =
	[
	]},
	["FP_PICK_FARM1_FIELD_06_05"] = {x = 12044.1689, y = 1467.83655, z = -11933.709, adjacent =
	[
	]},
	["FP_PICK_FARM1_FIELD_06_06"] = {x = 11282.1523, y = 1385.80359, z = -11924.0215, adjacent =
	[
	]},
	["FP_PICK_FARM1_FIELD_06_07"] = {x = 11670.9805, y = 1370.32617, z = -12138.8623, adjacent =
	[
	]},
	["FP_PICK_FARM1_FIELD_06_08"] = {x = 10171.9326, y = 1446.2605, z = -14464.2246, adjacent =
	[
	]},
	["FP_PICK_FARM1_FIELD_06_09"] = {x = 10499.6484, y = 1414.83655, z = -14690.207, adjacent =
	[
	]},
	["FP_ITEM_FARM1_01"] = {x = 16458.0566, y = 2572.30713, z = -9135.64258, adjacent =
	[
	]},
	["FP_ITEM_FARM1_02"] = {x = -1453.88916, y = 654.599609, z = -8700.08887, adjacent =
	[
	]},
	["FP_ITEM_FARM1_03"] = {x = 12658.418, y = 1590.39734, z = -21934.0781, adjacent =
	[
	]},
	["FP_ITEM_FARM1_04"] = {x = 14805.1484, y = 1924.79077, z = -16229.6309, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM1_BANDITS_CAVE_08"] = {x = -1880.06836, y = 567.858521, z = -8745.85938, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM1_BANDITS_CAVE_07"] = {x = -1760.48889, y = 558.880554, z = -9193.00586, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM1_BANDITS_CAVE_07"] = {x = -1986.35559, y = 556.766724, z = -8979.44043, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM1_BANDITS_CAVE_03"] = {x = -1774.92456, y = 597.573303, z = -10643.4688, adjacent =
	[
	]},
	["FP_EVENT_STONEGUARDIAN_ORNAMENT_EFFECT_FARM_01"] = {x = 16566.5625, y = 2567.23071, z = -9144.95215, adjacent =
	[
	]},
	["FP_EVENT_SPAWN_STONEGUARDIAN_ORNAMENT_FARM_01"] = {x = 16572.0352, y = 2602.23071, z = -9137.99902, adjacent =
	[
	]},
	["FP_SPAWN_KELCH"] = {x = 32303.4844, y = 3457.28442, z = 792.795166, adjacent =
	[
	]},
	["FP_ITEM_FOREST_BANDITTRADER_01"] = {x = 31532.5703, y = 4230.68848, z = -656.98938, adjacent =
	[
	]},
	["FP_STAND_CITY_TO_FOREST_43"] = {x = 30673.834, y = 4153.08691, z = -428.359467, adjacent =
	[
	]},
	["FP_STAND_NW_TAVERNE_IN_RANGERMEETING_05"] = {x = 36886.5664, y = 3800.34814, z = -2626.91675, adjacent =
	[
	]},
	["FP_STAND_NW_TAVERNE_IN_RANGERMEETING_04"] = {x = 36815.6602, y = 3796.18408, z = -2285.8916, adjacent =
	[
	]},
	["FP_STAND_NW_TAVERNE_IN_RANGERMEETING_03"] = {x = 37016.0391, y = 3807.38672, z = -2628.9375, adjacent =
	[
	]},
	["FP_STAND_NW_TAVERNE_IN_RANGERMEETING_02"] = {x = 36658.9648, y = 3802.72852, z = -2308.07007, adjacent =
	[
	]},
	["FP_STAND_NW_TAVERNE_IN_RANGERMEETING_01"] = {x = 36773.9961, y = 3804.99463, z = -2617.5498, adjacent =
	[
	]},
	["FP_STAND_NW_TAVERNE"] = {x = 38621.2695, y = 3834.29736, z = -2076.51489, adjacent =
	[
	]},
	["FP_ITEM_HERB_11"] = {x = 20655.1641, y = 1796.80823, z = -7270.02832, adjacent =
	[
	]},
	["FP_STAND_TAVERNE_LUTE_01"] = {x = 37532.3086, y = 3824.4165, z = -1932.26477, adjacent =
	[
	]},
	["FP_SMALLTALK_FLUECHTLING_02"] = {x = 37334.5469, y = 3798.4165, z = -2379.98145, adjacent =
	[
	]},
	["FP_SMALLTALK_FLUECHTLING_01"] = {x = 37344.5391, y = 3804.09009, z = -2511.66235, adjacent =
	[
	]},
	["FP_ITEM_HERB_10"] = {x = 8453.05469, y = 561.498901, z = 9620.99023, adjacent =
	[
	]},
	["FP_ITEM_HERB_09"] = {x = 10869.6309, y = 272.048065, z = 7421.58643, adjacent =
	[
	]},
	["FP_ITEM_HERB_08"] = {x = 12655.9346, y = 14.2667542, z = 7944.8457, adjacent =
	[
	]},
	["FP_ITEM_HERB_07"] = {x = 12558.6436, y = -28.7332458, z = 8479.81641, adjacent =
	[
	]},
	["FP_ITEM_HERB_06"] = {x = 7431.03516, y = 280.779419, z = 9073.71289, adjacent =
	[
	]},
	["FP_ITEM_HERB_05"] = {x = 7641.27637, y = 276.779572, z = 8702.86621, adjacent =
	[
	]},
	["FP_ITEM_HERB_04"] = {x = 9229.2793, y = 419.850708, z = 8885.47363, adjacent =
	[
	]},
	["FP_ITEM_HERB_03"] = {x = 9287.37305, y = 339.849731, z = 8420.49121, adjacent =
	[
	]},
	["FP_ITEM_HERB_02"] = {x = 10463.8242, y = 263.641815, z = 8497.33496, adjacent =
	[
	]},
	["FP_ITEM_HERB_01"] = {x = 10773.2861, y = 283.006592, z = 7974.16797, adjacent =
	[
	]},
	["FP_ROAM_NW_CITYFOREST_CAVE_06_05"] = {x = 34287.957, y = -2347.79956, z = 3466.15454, adjacent =
	[
	]},
	["FP_ROAM_NW_CITYFOREST_CAVE_06_04"] = {x = 34020.0195, y = -2342.78906, z = 3768.14453, adjacent =
	[
	]},
	["FP_ROAM_NW_CITYFOREST_CAVE_06_03"] = {x = 34276.1133, y = -2348.0874, z = 4003.49146, adjacent =
	[
	]},
	["FP_ROAM_NW_CITYFOREST_CAVE_06_02"] = {x = 34663.6914, y = -2334.83862, z = 3774.43213, adjacent =
	[
	]},
	["FP_ROAM_NW_CITYFOREST_CAVE_06_01"] = {x = 34461.3281, y = -2330.61499, z = 4147.01123, adjacent =
	[
	]},
	["FP_ROAM_NW_CITYFOREST_CAVE_04_03"] = {x = 33368.1953, y = -2166.57544, z = 4943.44922, adjacent =
	[
	]},
	["FP_ROAM_NW_CITYFOREST_CAVE_04_02"] = {x = 33225.0391, y = -2154.5564, z = 4710.81299, adjacent =
	[
	]},
	["FP_ROAM_NW_CITYFOREST_CAVE_04_01"] = {x = 33112.1055, y = -2151.3645, z = 4883.29541, adjacent =
	[
	]},
	["FP_ROAM_NW_CITYFOREST_CAVE_A01_01"] = {x = 33046.8789, y = -1949.62146, z = 2379.60645, adjacent =
	[
	]},
	["FP_ROAM_NW_CITYFOREST_CAVE_A01_01"] = {x = 33049.4531, y = -1946.97339, z = 2197.9436, adjacent =
	[
	]},
	["FP_ROAM_NW_CITYFOREST_CAVE_A06_03"] = {x = 31152.0645, y = -1671.96936, z = 2525.45532, adjacent =
	[
	]},
	["FP_ROAM_NW_CITYFOREST_CAVE_A06_02"] = {x = 30982.0352, y = -1671.96936, z = 2832.05396, adjacent =
	[
	]},
	["FP_ROAM_NW_CITYFOREST_CAVE_A06_01"] = {x = 30876.8672, y = -1681.81165, z = 2441.67432, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TAVERNCAVE1_02_05"] = {x = 33047.9219, y = 4132.37061, z = -11963.8936, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TAVERNCAVE1_02_04"] = {x = 33128.3008, y = 4129.38086, z = -11791.1563, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TAVERNCAVE1_02_03"] = {x = 32884.4297, y = 4137.22607, z = -11849.5928, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TAVERNCAVE1_02_02"] = {x = 32972.8633, y = 4134.35498, z = -12160.0039, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TAVERNCAVE1_02_01"] = {x = 33166.2109, y = 4137.63721, z = -12001.7959, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_03_M_04"] = {x = 3745.87036, y = 101.495056, z = 10921.5674, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_03_M_03"] = {x = 3593.08911, y = 60.2421417, z = 10528.0156, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_03_M_02"] = {x = 3134.61548, y = 46.4270554, z = 10781.3184, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_03_M_01"] = {x = 3174.56909, y = 18.6994324, z = 10480.0439, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_14_MONSTER_02"] = {x = 29578.6055, y = 2126.10791, z = 13830.9434, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_14_MONSTER_01"] = {x = 29179.1309, y = 2083.771, z = 14097.7314, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_FARM2_05_MOV5_04"] = {x = 27196.1074, y = 2671.42163, z = 9280.74707, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_FARM2_05_MOV5_03"] = {x = 27438.127, y = 2607.47339, z = 9792.52344, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_FARM2_05_MOV5_02"] = {x = 27635.5762, y = 2632.64087, z = 9470.60449, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_FARM2_05_MOV5_01"] = {x = 27071.5, y = 2621.67773, z = 9564.12598, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_5_04"] = {x = 23992.584, y = 1912.93103, z = 11357.1113, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_5_03"] = {x = 24333.6523, y = 1873.80518, z = 11174.8643, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_5_02"] = {x = 24116.2129, y = 1927.60449, z = 11624.2852, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_5_01"] = {x = 24345.9238, y = 1869.89478, z = 11372.2012, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENTF_04"] = {x = 16592.998, y = 2263.58716, z = 29125.1348, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENTF_03"] = {x = 16874.3809, y = 2232.82642, z = 29098.8066, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENTF_02"] = {x = 16700.7148, y = 2250.12915, z = 28716.0684, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENTF_01"] = {x = 17028.8086, y = 2229.57983, z = 28717.3691, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT8_M_04"] = {x = 11478.6543, y = 2407.14844, z = 27224.2871, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT8_M_03"] = {x = 11626.6768, y = 2389.45361, z = 27473.3848, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT8_M_01"] = {x = 11645, y = 2433.41675, z = 27013.9277, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT8_M_02"] = {x = 11823.5078, y = 2417.44189, z = 27227.2324, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_35_01_MONSTER_04"] = {x = 5608.37598, y = 3039.10889, z = 22600.9121, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_35_01_MONSTER_03"] = {x = 5472.31885, y = 3071.69482, z = 22805.3672, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_35_01_MONSTER_02"] = {x = 5806.95215, y = 3084.04248, z = 22679.543, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_35_01_MONSTER_01"] = {x = 5794.21289, y = 3090.75977, z = 22912.75, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_57_04"] = {x = 24624.5566, y = 2577.66968, z = 20490.5469, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_57_03"] = {x = 24507.209, y = 2557.94263, z = 20224.0742, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_57_02"] = {x = 24836.4805, y = 2592.69849, z = 20315.6992, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_57_01"] = {x = 24751.9238, y = 2601.15137, z = 19956.2012, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_62_M_04"] = {x = 27944.2422, y = 4188.20947, z = 24632.6504, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_62_M_03"] = {x = 28340.8223, y = 4276.43213, z = 24832.9395, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_62_M_02"] = {x = 28364.7617, y = 4258.35156, z = 24548.1152, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_62_M_01"] = {x = 28155.9688, y = 4211.89355, z = 24434.75, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_66_M_04"] = {x = 31205.332, y = 4125.68311, z = 27097.6035, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_66_M_04"] = {x = 30882.2012, y = 4126.64502, z = 26962.1387, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_66_M_02"] = {x = 31200.9531, y = 4094.94385, z = 27282.5898, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_66_M_01"] = {x = 30890.8691, y = 4117.52734, z = 27343.8047, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_82_M_04"] = {x = 23141.5313, y = 2713.43164, z = 29263.3047, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_82_M_03"] = {x = 22781.3301, y = 2714.8667, z = 28945.209, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_82_M_02"] = {x = 23090.168, y = 2742.19385, z = 29000.9863, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_82_M_01"] = {x = 22743.9551, y = 2665.42114, z = 29287.7715, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_82_05"] = {x = 22752.582, y = 2748.44238, z = 28610.793, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_82_04"] = {x = 22719.7324, y = 2768.6228, z = 28390.3613, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_82_03"] = {x = 22870.1973, y = 2749.40625, z = 28735.6816, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_82_02"] = {x = 22985.291, y = 2813.78101, z = 28300.4941, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_82_01"] = {x = 23138.959, y = 2781.11499, z = 28713.7461, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_801_04"] = {x = 18597.9844, y = 2139.40796, z = 29887.3008, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_801_03"] = {x = 18270.5527, y = 2100.97119, z = 29739.0254, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_801_02"] = {x = 18368.6367, y = 2142.29004, z = 30225.5313, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_801_01"] = {x = 18009.9004, y = 2118.27026, z = 30044.9219, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_79_04"] = {x = 19309.1172, y = 2160.35889, z = 27581.2188, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_79_03"] = {x = 18997.5469, y = 2117.11523, z = 27738.0059, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_79_02"] = {x = 19473.2988, y = 2148.82544, z = 27719.3965, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_79_01"] = {x = 19185.4551, y = 2120.53271, z = 28009.8262, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_75_2_MONSTER_04"] = {x = 21280.0938, y = 2780.80518, z = 22854.332, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_75_2_MONSTER_03"] = {x = 21135.3262, y = 2746.89771, z = 23043.1777, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_75_2_MONSTER_02"] = {x = 21462.8828, y = 2877.57495, z = 23294.5703, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_75_2_MONSTER_01"] = {x = 21301.8086, y = 2810.31177, z = 23231.0742, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_75_2_04"] = {x = 20463.4648, y = 2538.03491, z = 22333.4121, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_75_2_03"] = {x = 20439.248, y = 2573.58594, z = 22517.4512, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_75_2_02"] = {x = 20734.6387, y = 2620.66821, z = 22327.1563, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_75_2_01"] = {x = 20749.543, y = 2638.67139, z = 22598.7695, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_CAVE1_01_02"] = {x = 19340.7734, y = 2651.98584, z = 10315.9707, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_CAVE1_01_01"] = {x = 19050.8613, y = 2676.96704, z = 10320.3193, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_FOREST_04_02"] = {x = 14847.6377, y = 353.532196, z = 4762.49414, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_FOREST_04_01"] = {x = 15047.207, y = 369.352875, z = 4764.93311, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER5_04"] = {x = 34197.6289, y = 3897.97998, z = -5863.35059, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER5_03"] = {x = 33880.6953, y = 3928.73096, z = -6030.14209, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER5_02"] = {x = 34223.5, y = 3898.93994, z = -6018.00928, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER5_01"] = {x = 34021.8555, y = 3905.66797, z = -5776.66406, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER4_04"] = {x = 33762.6289, y = 4117.1499, z = -8667.18457, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER4_03"] = {x = 34019.6367, y = 4122.91504, z = -8716.43359, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER4_02"] = {x = 33654.1914, y = 4156.54932, z = -8940.33887, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER4_01"] = {x = 33977.7578, y = 4160.39307, z = -9002.24316, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER3_04"] = {x = 32828.3164, y = 4082.92822, z = -7003.94727, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER3_03"] = {x = 32648.4473, y = 4118.48389, z = -7135.09961, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER3_02"] = {x = 32959.3711, y = 4083.8877, z = -7427.30127, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER3_01"] = {x = 33030.5195, y = 4065.32568, z = -7111.41895, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER2_04"] = {x = 31732.0195, y = 4099.37598, z = -5725.14063, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER2_03"] = {x = 31521.0801, y = 4121.479, z = -5768.32129, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER2_02"] = {x = 31541.5449, y = 4164.72412, z = -6050.31689, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER2_01"] = {x = 31841.0605, y = 4128.20752, z = -5943.95068, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER_04"] = {x = 30699.0703, y = 4117.66162, z = -4458.85107, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER_03"] = {x = 30947.4668, y = 4082.10156, z = -4380.34473, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER_02"] = {x = 30738.1016, y = 4149.37354, z = -4615.00391, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_TO_TAVERN_09_MONSTER_01"] = {x = 31054.7676, y = 4114.27686, z = -4623.67285, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_21_MONSTER_04"] = {x = 17480.5195, y = 2741.4375, z = 17332.5293, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_21_MONSTER_03"] = {x = 17159.1133, y = 2743.05762, z = 17250.2207, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_21_MONSTER_02"] = {x = 17476.0859, y = 2720.40527, z = 17167.5488, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_21_MONSTER_01"] = {x = 17234.6953, y = 2733.99536, z = 17047.1914, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_MONSTER_04"] = {x = 8947.92871, y = 3215.47803, z = 21012.3535, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_MONSTER_03"] = {x = 8699.37109, y = 3268.45947, z = 20742.5508, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_MONSTER_02"] = {x = 8707.80469, y = 3256.58496, z = 20969.0898, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_MONSTER_01"] = {x = 9070.67969, y = 3219.76611, z = 20834.5996, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_35_01_04"] = {x = 4265.08936, y = 2452.677, z = 20947.3438, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_35_01_03"] = {x = 4497.18262, y = 2423.2146, z = 20686.623, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_35_01_02"] = {x = 4480.67236, y = 2506.06885, z = 21113.6641, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_35_01_01"] = {x = 4722.01855, y = 2469.46973, z = 20809.8262, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_LIGHTHOUSE_13_MONSTER_05"] = {x = 3362.52173, y = 2037.64551, z = 14149.0957, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_LIGHTHOUSE_13_MONSTER_03"] = {x = 3749.49683, y = 2080.46191, z = 14245.5322, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_LIGHTHOUSE_13_MONSTER_02"] = {x = 3229.7998, y = 2075.03979, z = 14425.207, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_LIGHTHOUSE_13_MONSTER_01"] = {x = 3537.22852, y = 2096.54443, z = 14421.25, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_38_MONSTER_04"] = {x = 2316.12207, y = 2125.52441, z = 15719.4707, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_38_MONSTER_03"] = {x = 2153.0459, y = 2178.91797, z = 15881.2861, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_38_MONSTER_02"] = {x = 2593.06909, y = 2145.75, z = 15973.7188, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_38_MONSTER_01"] = {x = 2326.31079, y = 2198.7373, z = 16068.3721, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_FOREST_04_05_9"] = {x = 18932.6152, y = 805.411743, z = 5311.93848, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_FOREST_04_05_8"] = {x = 18902.043, y = 807.241333, z = 5599.24561, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_FOREST_04_05_7"] = {x = 19374.6484, y = 796.276611, z = 5062.27686, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_FOREST_04_05_6"] = {x = 19082.832, y = 798.103455, z = 4876.81934, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_FOREST_04_05_5"] = {x = 19333.6816, y = 799.016724, z = 4859.22754, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_FOREST_04_05_4"] = {x = 19125.6367, y = 801.757751, z = 5120.54688, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_FOREST_04_05_3"] = {x = 18675.7129, y = 801.759888, z = 5428.71484, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_FOREST_04_05_2"] = {x = 18689.9766, y = 798.10498, z = 5107.28418, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_FOREST_04_05_1"] = {x = 19023.7598, y = 802.19873, z = 5415.62256, adjacent =
	[
	]},
	["FP_CAMPFIRE"] = {x = 20751.0039, y = 823.144775, z = 5579.11377, adjacent =
	[
	]},
	["FP_CAMPFIRE"] = {x = 20603.1094, y = 828.144775, z = 5333.19727, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_35_06_04"] = {x = 1299.19556, y = 2863.44214, z = 18486.7402, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_35_06_03"] = {x = 1624.23279, y = 2861.25049, z = 18875.5586, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_35_06_02"] = {x = 1350.86267, y = 2858.33276, z = 18815.1289, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_35_06_01"] = {x = 1610.85718, y = 2859.94458, z = 18549.8164, adjacent =
	[
	]},
	["FP_ITEM_NW_VINOKELLEREI"] = {x = 30506.6563, y = 3236.729, z = 29213.6621, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_VINOSKELLEREI_03"] = {x = 28063.0645, y = 3139.88403, z = 28960.9375, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_VINOSKELLEREI_02"] = {x = 28610.2734, y = 3139.88403, z = 28650.0703, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_VINOSKELLEREI_01"] = {x = 28493.1484, y = 3129.88403, z = 29049.5684, adjacent =
	[
	]},
	["FP_ITEM_NW_FARM2_03"] = {x = 25175.668, y = 4339.79639, z = -3286.06519, adjacent =
	[
	]},
	["FP_ITEM_NW_FARM2_02"] = {x = 31215.8125, y = 2767.37524, z = 20707.0996, adjacent =
	[
	]},
	["FP_ITEM_NW_FARM2_01"] = {x = 32002.3887, y = 3538.06006, z = 8369.74219, adjacent =
	[
	]},
	["FP_ITEM_NW_TAVERNE_04"] = {x = 36442.1094, y = 4230.21338, z = -2143.27832, adjacent =
	[
	]},
	["FP_ITEM_NW_TAVERNE_03"] = {x = 36585.2852, y = 3796.57813, z = -2179.07617, adjacent =
	[
	]},
	["FP_ITEM_NW_TAVERNE_02"] = {x = 28643.9824, y = 4338.8584, z = -8266.84668, adjacent =
	[
	]},
	["FP_ITEM_NW_TAVERNE_01"] = {x = 36999.1602, y = 4244.4585, z = -3221.39233, adjacent =
	[
	]},
	["FP_STAND_NW_LIGHTHOUSE_01"] = {x = -808.568054, y = 2428.79297, z = 16183.5127, adjacent =
	[
	]},
	["FP_STAND_NW_LIGHTHOUSE_01"] = {x = -321.264038, y = 2354.63647, z = 15760.9521, adjacent =
	[
	]},
	["FP_STAND_NW_LIGHTHOUSE_01"] = {x = -596.992859, y = 2329.19507, z = 15401.2939, adjacent =
	[
	]},
	["FP_STAND_TAVERNE_02"] = {x = 37555.8047, y = 3832.7605, z = -2507.0127, adjacent =
	[
	]},
	["FP_STAND_TAVERNE_01"] = {x = 37556.6094, y = 3831.79028, z = -2656.9751, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_18"] = {x = 38338.918, y = 3825.9502, z = -2946.43774, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_RUMBOLD_FLUCHT3_01"] = {x = 37639.2422, y = 3807.26025, z = -4454.98486, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_RUMBOLD_FLUCHT3_01"] = {x = 37708.3281, y = 3827.01196, z = -4587.71924, adjacent =
	[
	]},
	["FP_STAND_GUARDING_AKIL_B"] = {x = 29549.418, y = 3243.19897, z = 8509.56934, adjacent =
	[
	]},
	["FP_STAND_GUARDING_AKIL"] = {x = 32391.4043, y = 3668.23486, z = 6913.93799, adjacent =
	[
	]},
	["FP_SIT_CAMPFIRE_PAUSE"] = {x = 32574.7422, y = 3630.4939, z = 7375.20947, adjacent =
	[
	]},
	["FP_STAND_GUARDING_RANDOLPH"] = {x = 31992.5898, y = 3686.87915, z = 6366.58984, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_08_02"] = {x = 4274.95068, y = 141.832001, z = 15986.5771, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FOREST_02"] = {x = 3274.56763, y = 107.660507, z = 16060.8252, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FOREST_01"] = {x = 3546.82031, y = 115.999916, z = 16392.5449, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_55"] = {x = 17833.8086, y = 1113.07446, z = 2800.46582, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_54"] = {x = 18166.584, y = 1158.28967, z = 2915.81616, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_38"] = {x = 20976.8887, y = -710.877808, z = -4122.68018, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_37"] = {x = 21283.8184, y = -779.408936, z = -4536.00732, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_33"] = {x = 21457.3262, y = -539.390686, z = -2133.54419, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_34"] = {x = 21721.1641, y = -354.061798, z = -1745.69543, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_35"] = {x = 20542.3809, y = -266.598511, z = -1565.34949, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_36"] = {x = 21315.377, y = -258.714142, z = -1279.43701, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_28"] = {x = 22787.5645, y = -450.319061, z = -2373.78149, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_29"] = {x = 22746.4688, y = -627.098389, z = -2838.10645, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_30"] = {x = 22081.2148, y = -532.190674, z = -2736.48755, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_31"] = {x = 23087.9434, y = -484.172852, z = -2358.61963, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_32"] = {x = 22311.748, y = -439.433624, z = -2372.27515, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_23"] = {x = 23742.1387, y = -854.581238, z = -3945.13208, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_24"] = {x = 23633.4668, y = -727.601318, z = -3135.87354, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_25"] = {x = 23463.8457, y = -772.624878, z = -3514.64722, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_26"] = {x = 22945.3633, y = -834.993225, z = -3788.3501, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_27"] = {x = 23098.4746, y = -752.387878, z = -3338.75269, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_19"] = {x = 31646.7285, y = -2305.68701, z = -265.30835, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_20"] = {x = 31627.7246, y = -2369.2666, z = -522.277466, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_21"] = {x = 31916.3359, y = -2360.8042, z = -434.838287, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_22"] = {x = 31764.8535, y = -2351.03589, z = -676.739136, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_18"] = {x = 27489.166, y = -1652.56519, z = 1027.64856, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_17"] = {x = 27850.1426, y = -1772.31689, z = 1221.50366, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_16"] = {x = 27422.2109, y = -1695.59729, z = 1347.39661, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_15"] = {x = 27744.8477, y = -1852.05225, z = 1531.79089, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_14"] = {x = 27118.0391, y = -1690.60559, z = 1522.53296, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_13"] = {x = 27892.0859, y = -1330.00574, z = -952.861511, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_12"] = {x = 27978.5059, y = -1256.58179, z = -1463.72949, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_11"] = {x = 27620.0332, y = -1174.65491, z = -1632.82568, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_10"] = {x = 27355.3086, y = -1157.0647, z = -947.698181, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_09"] = {x = 26877.0684, y = -1053.91443, z = -1242.37268, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_08"] = {x = 33592.7344, y = -2217.59204, z = 403.354156, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_07"] = {x = 34407.0625, y = -2298.31177, z = 601.374451, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_06"] = {x = 34576.0742, y = -2330.19775, z = 190.451462, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_05"] = {x = 33767.2109, y = -2261.56274, z = -133.062698, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_03"] = {x = 29404.8906, y = -1992.75439, z = -404.623596, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_02"] = {x = 29600.0156, y = -2048.29199, z = -132.97467, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_01"] = {x = 29113.875, y = -1924.17908, z = -99.6485367, adjacent =
	[
	]},
	["FP_ROAM_CITYFOREST_KAP3_04"] = {x = 29293.3281, y = -2009.04028, z = 239.425552, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_53"] = {x = 26570.0488, y = 2495.56714, z = 5160.31543, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_48"] = {x = 28644.7617, y = 2643.125, z = 4570.47217, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_49"] = {x = 29793.3457, y = 2734.45557, z = 4545.57178, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_50"] = {x = 29143.3281, y = 2694.90771, z = 4377.45557, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_47"] = {x = 30049.3555, y = 2785.56372, z = 4028.7229, adjacent =
	[
	]},
	["FP_CAMPFIRE_CITY_TO_FOREST_46"] = {x = 30296.5313, y = 4219.24023, z = -1031.29211, adjacent =
	[
	]},
	["FP_STAND_CITY_TO_FOREST_45"] = {x = 30823.1973, y = 4216.60742, z = -654.414307, adjacent =
	[
	]},
	["FP_CAMPFIRE_CITY_TO_FOREST_44"] = {x = 30601.1543, y = 4195.60938, z = -974.100037, adjacent =
	[
	]},
	["FP_CAMPFIRE_CITY_TO_FOREST_43"] = {x = 30695.4414, y = 4173.76318, z = 115.407585, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_39"] = {x = 32001.3223, y = 3383.72754, z = 847.142517, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_42"] = {x = 31748.0059, y = 3329.82959, z = 1138.68774, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_41"] = {x = 32507.0078, y = 3474.55835, z = 1086.04919, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_40"] = {x = 31355.9004, y = 3250.02515, z = 545.028198, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_38"] = {x = 19169.9531, y = 1157.45789, z = 1493.32483, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_37"] = {x = 19402.3203, y = 1166.03687, z = 1406.24109, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_36"] = {x = 15897.2949, y = 752.129211, z = 2638.81055, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_33"] = {x = 16089.2031, y = 758.990417, z = 2303.97363, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_34"] = {x = 16361.6631, y = 753.188721, z = 2270.35352, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_35"] = {x = 16477.7656, y = 731.785461, z = 1962.64465, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_32"] = {x = 24596.1973, y = 4373.77344, z = -4454.50049, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_31"] = {x = 24169.4746, y = 4417.91797, z = -4725.02246, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_30"] = {x = 25176.0234, y = 4413.10352, z = -4192.05664, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_29"] = {x = 24337.8223, y = 4339.69531, z = -3813.87817, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_28"] = {x = 24007.6934, y = 4361.45605, z = -4373.47852, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_27"] = {x = 20591.3945, y = 1142.26855, z = 2165.57251, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_25"] = {x = 20191.8789, y = 1133.19873, z = 2997.47241, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_24"] = {x = 20950.707, y = 1115.07458, z = 2701.06055, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_23"] = {x = 21549.6992, y = 1073.67981, z = 3663.6123, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_22"] = {x = 21475.4551, y = 1066.00964, z = 3128.65552, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_21"] = {x = 20943.1211, y = 1062.27686, z = 3612.46533, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_20"] = {x = 16695.1914, y = 750.979431, z = 4397.97656, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_19"] = {x = 16473.6445, y = 710.979431, z = 4656.98828, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_18"] = {x = 14066.8271, y = 122.054527, z = 3988.82373, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_17"] = {x = 14419.5146, y = 162.797256, z = 3872.44507, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_16"] = {x = 14726.0771, y = 209.295151, z = 3866.17773, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_15"] = {x = 9211.26563, y = 531.485352, z = 9525.93066, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_14"] = {x = 8948.22461, y = 501.329407, z = 9194.19434, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_13"] = {x = 8749.01563, y = 599.67749, z = 9617.41797, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_11"] = {x = 7598.58154, y = 490.028503, z = 12288.6191, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_12"] = {x = 7146.16309, y = 494.233551, z = 12437.623, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_10"] = {x = 6954.86963, y = 448.440247, z = 12874.834, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_09"] = {x = 10936.584, y = 229.966293, z = 8974.28125, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_08"] = {x = 10625.5645, y = 278.134857, z = 8714.88379, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_07"] = {x = 12524.7393, y = 45.7639389, z = 8227.05371, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_06"] = {x = 10321.0117, y = 262.324768, z = 9507.15332, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_05"] = {x = 12370.2129, y = 118.72319, z = 7660.38428, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_04"] = {x = 11879.3154, y = 156.42421, z = 8032.87939, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_03"] = {x = 19469.998, y = 1215.48633, z = 2320.17529, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_02"] = {x = 19076.8652, y = 1198.30933, z = 2281.51099, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_01"] = {x = 19166.3105, y = 1219.69824, z = 2534.77661, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_09_04"] = {x = 4440.34082, y = 140.785843, z = 15119.3721, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_09_03"] = {x = 4340.82471, y = 140.680984, z = 15277.7178, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_09_02"] = {x = 4138.17334, y = 142.952667, z = 15333.6445, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_09_01"] = {x = 4300.27148, y = 139.349396, z = 15440.1279, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_05_03"] = {x = 2583.42432, y = 107.151939, z = 13951.3867, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_05_02"] = {x = 2515.84399, y = 104.371857, z = 13683.8086, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_05_01"] = {x = 2780.49634, y = 105.17057, z = 13723.1738, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_03_05"] = {x = 4802.92041, y = 85.9999542, z = 11750.3945, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_03_04"] = {x = 5068.479, y = -0.451163292, z = 11729.2578, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_03_03"] = {x = 5275.89648, y = -43.2152863, z = 11839.792, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_03_02"] = {x = 5302.09863, y = 45.4701996, z = 11491.6279, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_03_01"] = {x = 4966.28467, y = 96.6795044, z = 11383.9746, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_FARM2_TO_TAVERN_08_01"] = {x = 30533.1836, y = 4220.50342, z = -1596.53333, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_FARM2_TO_TAVERN_08_02"] = {x = 30604.2773, y = 4188.65088, z = -1392.97571, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_FARM2_SLD_ANGRIFF_02"] = {x = 31663.0547, y = 3696.05005, z = 6252.72656, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_FARM2_SLD_ANGRIFF_01"] = {x = 31453.4883, y = 3705.86792, z = 6440.43994, adjacent =
	[
	]},
	["FP_STAND_NW_FARM2_SLD_ANGRIFF_02"] = {x = 31763.416, y = 3621, z = 7100.12598, adjacent =
	[
	]},
	["FP_STAND_NW_FARM2_SLD_ANGRIFF_01"] = {x = 31418.4766, y = 3727.41699, z = 6223.58008, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM2_REST_03"] = {x = 32133.3379, y = 3486.28174, z = 9810.1123, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM2_REST_02"] = {x = 31611.9258, y = 3494.62256, z = 9948.89453, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM2_REST_01"] = {x = 32012.4004, y = 3476.91162, z = 10007.835, adjacent =
	[
	]},
	["FP_PICK_NW_FARM2_FIELD_11"] = {x = 32510.3652, y = 3707.24316, z = 6776.37988, adjacent =
	[
	]},
	["FP_PICK_NW_FARM2_FIELD_10"] = {x = 33158.5586, y = 3661.07617, z = 7709.07959, adjacent =
	[
	]},
	["FP_PICK_NW_FARM2_FIELD_09"] = {x = 32898.0039, y = 3735.98853, z = 6846.45117, adjacent =
	[
	]},
	["FP_PICK_NW_FARM2_FIELD_08"] = {x = 32804.1602, y = 3817.71509, z = 6336.38721, adjacent =
	[
	]},
	["FP_PICK_NW_FARM2_FIELD_07"] = {x = 28822.0566, y = 3165.49243, z = 8036.92139, adjacent =
	[
	]},
	["FP_PICK_NW_FARM2_FIELD_06"] = {x = 28723.834, y = 3168.35498, z = 7459.19727, adjacent =
	[
	]},
	["FP_PICK_NW_FARM2_FIELD_05"] = {x = 29135.6875, y = 3238.99707, z = 7634.69531, adjacent =
	[
	]},
	["FP_PICK_NW_FARM2_FIELD_04"] = {x = 29251.1523, y = 3141.69531, z = 9567.9043, adjacent =
	[
	]},
	["FP_PICK_NW_FARM2_FIELD_03"] = {x = 28710.75, y = 3066.57617, z = 9573.61133, adjacent =
	[
	]},
	["FP_PICK_NW_FARM2_FIELD_02"] = {x = 28862.5957, y = 3125.98047, z = 9017.7168, adjacent =
	[
	]},
	["FP_PICK_NW_FARM2_FIELD_01"] = {x = 29276.8691, y = 3195.73804, z = 9067.875, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_SHEEP_01"] = {x = 29739.0605, y = 3265.75366, z = 10772.7363, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_SHEEP_02"] = {x = 29668.0039, y = 3242.10132, z = 11061.7871, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM2_SHEEP_03"] = {x = 30199.9355, y = 3238.52124, z = 11061.458, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_01"] = {x = 25535.3125, y = 1904.26489, z = 12442.3594, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_02"] = {x = 24672.168, y = 1940.04944, z = 12361.8525, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_03"] = {x = 25740.5293, y = 1977.89185, z = 11486.3184, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_04"] = {x = 24274.8613, y = 1949.73389, z = 11870.8916, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_05"] = {x = 27900.5254, y = 2572.59351, z = 11141.5068, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_06"] = {x = 27627.9355, y = 2612.59351, z = 10798.5127, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_07"] = {x = 25249.0977, y = 2704.60498, z = 7119.0166, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_08"] = {x = 25554.6563, y = 2634.60132, z = 7293.17334, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_09"] = {x = 25904.0547, y = 2684.49146, z = 6944.71143, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_10"] = {x = 23534.3105, y = 2555.74756, z = 5791.30078, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_11"] = {x = 24103.2188, y = 2737.23193, z = 5968.95898, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_12"] = {x = 23318.7715, y = 2456.12695, z = 6860.17139, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_13"] = {x = 20835.6816, y = 3123.93677, z = 7785.71924, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_14"] = {x = 21377.4707, y = 2985.70337, z = 8129.02246, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_15"] = {x = 20959.8262, y = 3200.26196, z = 9053.39063, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_16"] = {x = 20456.4121, y = 3244.02783, z = 9205.29297, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_17"] = {x = 20899.1973, y = 3247.69849, z = 9791.38574, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_21"] = {x = 22315.8105, y = 2491.79297, z = 12790.3643, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_22"] = {x = 22501.0449, y = 2511.79297, z = 13224.1836, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_23"] = {x = 22720.4277, y = 2471.79297, z = 12869.8057, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_24"] = {x = 22488.541, y = 2469.33398, z = 12658.1963, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_27"] = {x = 20418.9414, y = 2691.85693, z = 14853.6543, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_25"] = {x = 20788.6094, y = 2681.85693, z = 14837.2402, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_26"] = {x = 20645.6426, y = 2611.85693, z = 14454.2529, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_28"] = {x = 15791.2969, y = 4132.79639, z = 11511.0664, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_29"] = {x = 15684.7949, y = 4138.38037, z = 11197.3047, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_30"] = {x = 15479.6074, y = 4113.22559, z = 11347.8193, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_31"] = {x = 17525.2891, y = 2552.03198, z = 15573.5205, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_33"] = {x = 17944.793, y = 2388.43384, z = 15840.1123, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_34"] = {x = 20953.5723, y = 1979.10498, z = 13376.1309, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_35"] = {x = 20913.0645, y = 2059.10498, z = 12597.6123, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_36"] = {x = 20329.8477, y = 1996.0271, z = 12655.7363, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP2_37"] = {x = 20530.5879, y = 2145.91724, z = 12100.1367, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_01"] = {x = 14195.748, y = 2637.31274, z = 14190.0039, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_02"] = {x = 13692.498, y = 2722.23584, z = 14470.666, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_03"] = {x = 13983.8545, y = 2692.23584, z = 14443.2539, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_04"] = {x = 13124.3271, y = 3041.81787, z = 15508.3994, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_05"] = {x = 13355.5146, y = 3034.94946, z = 15358.4238, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_06"] = {x = 13797.6377, y = 2688.04248, z = 14324.0918, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_07"] = {x = 25896.1758, y = 3164.57202, z = 15760.8408, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_08"] = {x = 25519.2969, y = 3112.51123, z = 15787.4336, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_09"] = {x = 25968.6113, y = 2931.08545, z = 16520.5898, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_10"] = {x = 25355.9609, y = 2969.47437, z = 16317.4434, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_11"] = {x = 27473.0469, y = 2983.71582, z = 16978.7695, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_12"] = {x = 27374.9395, y = 3096.86426, z = 16263.6113, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_13"] = {x = 27130.5781, y = 2938.43726, z = 16818.498, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_14"] = {x = 27563.1758, y = 2347.41187, z = 18252.5254, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_15"] = {x = 27161.7793, y = 2316.25391, z = 17965.8613, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_16"] = {x = 23990.25, y = 2491.04419, z = 17299.7969, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_17"] = {x = 23779.0156, y = 2501.68433, z = 17059.1289, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_18"] = {x = 23931.7754, y = 3767.1499, z = 23910.1875, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_19"] = {x = 24024.5176, y = 3844.79199, z = 24255.3887, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_20"] = {x = 23621.3887, y = 3799.80737, z = 24296.9941, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_21"] = {x = 20727.8652, y = 2672.55298, z = 15876.9912, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_22"] = {x = 20740.2695, y = 2673.27026, z = 16340.3584, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_23"] = {x = 10036.1064, y = 3461.1604, z = 19005.9102, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_24"] = {x = 10311.3447, y = 3515.91772, z = 18465.3066, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_25"] = {x = 8328.42285, y = 2318.39502, z = 19526.7402, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_26"] = {x = 8511.56445, y = 2265.34033, z = 18944.0938, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_27"] = {x = 8606.84863, y = 2260.49976, z = 19343.0645, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_28"] = {x = 14920.166, y = 4419.41504, z = 20448.291, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_29"] = {x = 15096.6504, y = 4374.9375, z = 20147.6211, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_30"] = {x = 15230.0234, y = 4420.47607, z = 20368.4277, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_31"] = {x = 12625.1064, y = 4014.91504, z = 21517.7441, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_32"] = {x = 12157.9063, y = 4034.91504, z = 21244.2402, adjacent =
	[
	]},
	["FP_ROAM_MEDIUMFOREST_KAP3_34"] = {x = 12832.4844, y = 4104.91504, z = 20858.0566, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FOREST_03"] = {x = 832.809204, y = 111.520325, z = 14671.625, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FOREST_04"] = {x = 1195.02307, y = 104.712341, z = 14673.3828, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FOREST_CAVE1_01"] = {x = 15414.4043, y = 2904.98584, z = 11293.7158, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FOREST_CAVE1_02"] = {x = 15758.0723, y = 2883.7041, z = 11165.3477, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FOREST_CAVE1_03"] = {x = 15665.8379, y = 2895.49756, z = 11467.3926, adjacent =
	[
	]},
	["FP_STAND_EGILL"] = {x = 31816.6582, y = 3472.50391, z = 9551.56152, adjacent =
	[
	]},
	["FP_SIT_CAMPFIRE_PAUSE_02"] = {x = 29708.6348, y = 3252.3667, z = 10106.7188, adjacent =
	[
	]},
	["FP_ROAM_NW_TAVERNE_BIGFARM_MONSTER_01_01"] = {x = 39343.375, y = 3616.27295, z = -5325.49756, adjacent =
	[
	]},
	["FP_ROAM_NW_TAVERNE_BIGFARM_MONSTER_01_02"] = {x = 39187.3516, y = 3692.55811, z = -5053.54639, adjacent =
	[
	]},
	["FP_ROAM_NW_TAVERNE_BIGFARM_MONSTER_01_03"] = {x = 39586.2148, y = 3614.27075, z = -5041.33057, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_TAVERNE_IN_01"] = {x = 38148.6758, y = 3835.56396, z = -1985.94031, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_TAVERNE_IN_02"] = {x = 38035.1875, y = 3825.56396, z = -2198.26855, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_62_02_01"] = {x = 26315.8203, y = 3983.396, z = 25788.8477, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_62_02_02"] = {x = 25930.2559, y = 3980.96875, z = 25827.0664, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_62_02_03"] = {x = 26205.0996, y = 3989.86963, z = 25355.832, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_14_MONSTER_04"] = {x = 29266.7051, y = 2102.35645, z = 13878.7617, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_14_MONSTER_03"] = {x = 29672.2813, y = 2123.36743, z = 14185.916, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_3_02"] = {x = 24101.6113, y = 1976.67126, z = 8732.74219, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_06_01"] = {x = 3196.01318, y = 138.603424, z = 14574.3574, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_06_02"] = {x = 3312.479, y = 133.305038, z = 14895.8721, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_06_03"] = {x = 3345.41089, y = 140.033752, z = 14669.0381, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_06_04"] = {x = 3177.76563, y = 138.111237, z = 14764.6289, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_06_05"] = {x = 3243.20947, y = 131.382568, z = 14998.4404, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_08_01"] = {x = 4152.13379, y = 155.638763, z = 16176.3447, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_SMFOREST_08_03"] = {x = 4181.62695, y = 137.409836, z = 16037.5313, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_13"] = {x = 7136.24219, y = 484.802795, z = 12635.9922, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_14"] = {x = 7485.40332, y = 470.059052, z = 12572.6445, adjacent =
	[
	]},
	["FP_ROAM_CITY_TO_FOREST_15"] = {x = 7348.52637, y = 461.578369, z = 12312.0479, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION3_01"] = {x = 11418.9746, y = 3633.08447, z = 22255.7227, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION3_02"] = {x = 11578.5068, y = 3749.39917, z = 21879.6816, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION3_03"] = {x = 11218.1748, y = 3598.4812, z = 22022.8164, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION3_04"] = {x = 11608.9346, y = 3714.79297, z = 22188.8047, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION10_01"] = {x = 15349.3242, y = 4092.60571, z = 23594.7734, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION10_02"] = {x = 15255.0527, y = 4006.11841, z = 24049.4063, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION10_03"] = {x = 15057.1631, y = 4155.52979, z = 23650.3633, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION10_04"] = {x = 15439.6543, y = 4000.48022, z = 23836.1855, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION11_01"] = {x = 15235.7275, y = 3866.31909, z = 25086.3398, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION10_02"] = {x = 15109.4365, y = 3949.01245, z = 24711.541, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT6_01"] = {x = 13060.8516, y = 2325.43359, z = 29254.3105, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT6_02"] = {x = 13338.1416, y = 2311.48511, z = 28750.666, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT6_03"] = {x = 12990.9053, y = 2335.427, z = 28905.3574, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT6_04"] = {x = 13286.6924, y = 2288.54053, z = 29279.4648, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT15_01"] = {x = 5703.7915, y = 2954.77344, z = 25010.0801, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT15_02"] = {x = 6111.66992, y = 2982.70605, z = 25068.293, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT15_03"] = {x = 5751.76318, y = 2918.86035, z = 25286.9785, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT15_04"] = {x = 6149.69189, y = 2982.70605, z = 25346.1973, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT8_M5_01"] = {x = 8843.4248, y = 3091.66821, z = 23819.9414, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT8_M5_02"] = {x = 8862.14453, y = 3025.31812, z = 24150.3555, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT8_M5_03"] = {x = 9179.49316, y = 3005.47266, z = 24128.7891, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT8_M5_04"] = {x = 9130.41211, y = 3082.71533, z = 23850.3594, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION16_01"] = {x = 17486.5254, y = 3661.5874, z = 22968.2402, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION16_02"] = {x = 17338.8984, y = 3671.69678, z = 22583.5605, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION16_03"] = {x = 17285.8379, y = 3694.28955, z = 23262.8906, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION16_04"] = {x = 17592.6191, y = 3639.22217, z = 23244.166, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT8_M3_01"] = {x = 10527.1738, y = 2769.48926, z = 24839.8203, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT8_M3_02"] = {x = 10214.3848, y = 2815.04297, z = 24575.2422, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT8_M3_03"] = {x = 10114.9268, y = 2765.52881, z = 25091.7344, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_80_1_MOVEMENT8_M3_04"] = {x = 10354.4795, y = 2742.75244, z = 25149.293, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_16_MONSTER_01"] = {x = 31369.1328, y = 2572.18335, z = 17828.0195, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_16_MONSTER_02"] = {x = 31621.9609, y = 2664.13013, z = 17099.9727, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_16_MONSTER_03"] = {x = 31080.6934, y = 2548.70825, z = 17636.6387, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_16_MONSTER_04"] = {x = 31352.582, y = 2641.62817, z = 17020.6855, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_16_MONSTER_05"] = {x = 31526.0898, y = 2590.77197, z = 17555.0469, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_16_MONSTER_06"] = {x = 31297.5566, y = 2568.80176, z = 17447.3477, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_16_MONSTER2_01"] = {x = 29211.1172, y = 2464.5957, z = 17975.6973, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_16_MONSTER2_02"] = {x = 29799.5332, y = 2555.54248, z = 18121.3047, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_16_MONSTER2_03"] = {x = 29603.7539, y = 2531.16675, z = 17775.627, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_16_MONSTER2_04"] = {x = 29405.6543, y = 2497.03711, z = 18296.4922, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_13_01"] = {x = 28156.6504, y = 2453.08618, z = 15071.0859, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_13_02"] = {x = 27683.6289, y = 2465.93335, z = 15271.4561, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_13_03"] = {x = 28111.6035, y = 2443.20581, z = 15459.7363, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_13_04"] = {x = 27787.0938, y = 2434.57202, z = 14848.7715, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_3_01"] = {x = 24330.0781, y = 2059.93774, z = 8461.30664, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_4_01"] = {x = 24219.9512, y = 1847.0813, z = 10018.8369, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_4_02"] = {x = 24223.6621, y = 1879.71655, z = 9634.15332, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_4_03"] = {x = 24603.0977, y = 1884.80591, z = 9689.98047, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_04_4_04"] = {x = 24565.4395, y = 1844.94714, z = 10061.2363, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_72_MONSTER_01"] = {x = 19762.0273, y = 2625.93799, z = 18158.8711, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_72_MONSTER_02"] = {x = 19822.0527, y = 2612.90894, z = 18508.4121, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_72_MONSTER_03"] = {x = 20057.1699, y = 2578.67432, z = 18158.8535, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_72_MONSTER_04"] = {x = 20053.002, y = 2580.62915, z = 18461.3633, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_62_06_01"] = {x = 23173.8848, y = 3513.97729, z = 22944.8633, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_62_06_02"] = {x = 22596.7227, y = 3384.97729, z = 23315.1777, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_62_06_03"] = {x = 22774.2344, y = 3387.97729, z = 22759.8672, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_62_06_04"] = {x = 22603.8281, y = 3367.97729, z = 22925.5977, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_56_MONSTER_01"] = {x = 22646.5195, y = 2729.68188, z = 19553.4727, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_56_MONSTER_02"] = {x = 22640.9043, y = 2755.29175, z = 19183.9922, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_56_MONSTER_03"] = {x = 23013.9199, y = 2661.21729, z = 19482.3008, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_56_MONSTER_04"] = {x = 23149.1641, y = 2647.25732, z = 19123.1504, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_27_03_01"] = {x = 14322.8398, y = 4184.00977, z = 19517.2266, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_27_03_02"] = {x = 13857.3652, y = 4140.96436, z = 19732.4277, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_27_03_03"] = {x = 14109.0879, y = 4170.40381, z = 19690.1426, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_27_03_04"] = {x = 13973.4063, y = 4099.47803, z = 19315.959, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_27_02_01"] = {x = 13213.6855, y = 3829.95972, z = 18619.4648, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_27_02_02"] = {x = 12988.5547, y = 3725.77075, z = 18837.0918, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_27_02_03"] = {x = 12720.1543, y = 3690.49829, z = 18602.6523, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_27_02_04"] = {x = 13026.3711, y = 3793.96826, z = 18376.916, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_LIGHTHOUSE_13_MONSTER7_01"] = {x = 8356.59961, y = 3397.73462, z = 15523.8682, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_LIGHTHOUSE_13_MONSTER7_02"] = {x = 8562.58105, y = 3408.45801, z = 15559.8252, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_LIGHTHOUSE_13_MONSTER7_03"] = {x = 8942.32324, y = 3479.86328, z = 15314.5537, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_LIGHTHOUSE_13_MONSTER7_04"] = {x = 8400.4043, y = 3353.67969, z = 15265.7852, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_LIGHTHOUSE_13_MONSTER8_01"] = {x = 9272.24219, y = 3953.85254, z = 16996.1113, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_LIGHTHOUSE_13_MONSTER8_02"] = {x = 9596.16406, y = 3909.68677, z = 16961.4883, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_LIGHTHOUSE_13_MONSTER8_03"] = {x = 9266.59277, y = 3872.09888, z = 16749.8203, adjacent =
	[
	]},
	["FP_ROAM_NW_CITY_TO_LIGHTHOUSE_13_MONSTER8_04"] = {x = 9503.00586, y = 3831.93823, z = 16519.0996, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_35_MONSTER_01"] = {x = 5323.98926, y = 2247.42432, z = 18620.584, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_35_MONSTER_02"] = {x = 5077.54443, y = 2292.14478, z = 18481.4961, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION_M_01"] = {x = 9673.3877, y = 3429.49829, z = 22628.1719, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION_M_02"] = {x = 10029.0518, y = 3355.45874, z = 22619.7129, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION_M_03"] = {x = 9567.54395, y = 3448.15186, z = 22220.3516, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION_M_04"] = {x = 10039.9258, y = 3393.76147, z = 22260.3945, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION19_01"] = {x = 16971.7949, y = 3874.18994, z = 20641.543, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION19_02"] = {x = 17196.0547, y = 3812.21045, z = 20867.8066, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION19_03"] = {x = 17431.2598, y = 3810.47607, z = 20337.123, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_31_NAVIGATION19_04"] = {x = 17145.8086, y = 3853.57007, z = 20394.916, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_18_MONSTER_01"] = {x = 16560.3711, y = 2641.69556, z = 15039.7422, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_18_MONSTER_02"] = {x = 16870.4629, y = 2630.73242, z = 14753.8896, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_18_MONSTER_03"] = {x = 16540.7012, y = 2660.87988, z = 14782.3945, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_18_MONSTER_04"] = {x = 16809.9375, y = 2648.09009, z = 15089.7168, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_72_MONSTER23_01"] = {x = 19248.4629, y = 2584.62988, z = 17353.8066, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_72_MONSTER23_02"] = {x = 19396.0059, y = 2626.69189, z = 17740.0332, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_72_MONSTER23_03"] = {x = 19131.7207, y = 2627.66895, z = 17601.377, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_72_MONSTER23_04"] = {x = 19537.1016, y = 2594.41016, z = 17657.3535, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_76_01"] = {x = 20050.9902, y = 2527.72461, z = 23425.2344, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_76_02"] = {x = 19699.5293, y = 2433.72461, z = 23521.4004, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_62_02_04"] = {x = 25990.5313, y = 3993.91455, z = 25404.2148, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_66_MONSTER_01"] = {x = 31948.3613, y = 4233.7251, z = 25593.5273, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_66_MONSTER_02"] = {x = 32237.1719, y = 4221.85107, z = 25752.3691, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_66_MONSTER_03"] = {x = 31706.8379, y = 4186.22998, z = 25804.0996, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_66_MONSTER_04"] = {x = 32048.4063, y = 4151.46289, z = 25988.6504, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_25_01_M_01"] = {x = 14511.0078, y = 4054.13843, z = 18750.418, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_25_01_M_02"] = {x = 14129.8428, y = 4014.5127, z = 18802.8965, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_PATH_25_01_M_03"] = {x = 14392.3242, y = 4036.2251, z = 18648.6211, adjacent =
	[
	]},
	["FP_EVENT_SPAWN_STONEGUARDIAN_ORNAMENT_FOREST_01"] = {x = 29360.3574, y = 4434.68115, z = 25165.8672, adjacent =
	[
	]},
	["FP_EVENT_STONEGUARDIAN_ORNAMENT_EFFECT_FOREST_01"] = {x = 29360.0508, y = 4395.68115, z = 25165.8008, adjacent =
	[
	]},
	["FP_ITEM_FOREST_STPLATE_01"] = {x = 34817.5352, y = -2263.67334, z = 1064.62854, adjacent =
	[
	]},
	["FP_ITEM_FOREST_STPLATE_02"] = {x = 31215.7773, y = -1647.79944, z = 3298.32617, adjacent =
	[
	]},
	["FP_ITEM_FOREST_STPLATE_03"] = {x = 16323.3438, y = 2667.34595, z = -8399.35742, adjacent =
	[
	]},
	["FP_ITEM_FOREST_STPLATE_04"] = {x = 31657.627, y = 4164.64502, z = -12917.0674, adjacent =
	[
	]},
	["FP_ITEM_FOREST_STPLATE_05"] = {x = 29311.7168, y = 4388.89941, z = 25263.7598, adjacent =
	[
	]},
	["FP_ITEM_FOREST_STPLATE_06"] = {x = 30007.582, y = 4433.71436, z = 25657.207, adjacent =
	[
	]},
	["FP_ITEM_FOREST_STPLATE_07"] = {x = 8391.16504, y = 2283.78833, z = 19878.7051, adjacent =
	[
	]},
	["FP_ITEM_FOREST_STPLATE_08"] = {x = 27059.293, y = 4420.86963, z = -4057.19385, adjacent =
	[
	]},
	["FP_STAND_NW_TAVERNE_IN_RANGERMEETING_06"] = {x = 37156.5898, y = 3802.10156, z = -2576.5625, adjacent =
	[
	]},
	["FP_PICK_NW_FARM2_FIELD_12"] = {x = 28859.2695, y = 3129.63745, z = 8599.97461, adjacent =
	[
	]},
	["FP_PICK_NW_FARM2_FIELD_13"] = {x = 28420.3496, y = 3085.89136, z = 7734.6123, adjacent =
	[
	]},
	["FP_STAND_PATRICK"] = {x = 74917.9297, y = 3483.5752, z = -14922.9219, adjacent =
	[
	]},
	["FP_ITEM_GREATPEASANT_FERNANDOSWEAPONS_04"] = {x = 49501.0859, y = 1219.75281, z = 5607.30957, adjacent =
	[
	]},
	["FP_ITEM_GREATPEASANT_FERNANDOSWEAPONS_03"] = {x = 49681.082, y = 1209.75281, z = 6059.21143, adjacent =
	[
	]},
	["FP_ITEM_GREATPEASANT_FERNANDOSWEAPONS_02"] = {x = 49035.2188, y = 1246.75281, z = 6518.03613, adjacent =
	[
	]},
	["FP_ITEM_GREATPEASANT_FERNANDOSWEAPONS_01"] = {x = 49012.0625, y = 1227.75281, z = 5977.81592, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_KITCHEN_04"] = {x = 72151.0547, y = 3185.84521, z = -9042.5625, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_KITCHEN_03"] = {x = 72296.1406, y = 3193.15381, z = -9063.70703, adjacent =
	[
	]},
	["FP_STAND_HODGES_01"] = {x = 72237.0234, y = 3223.96826, z = -10979.9092, adjacent =
	[
	]},
	["FP_STAND_BIGFARM_RAOUL"] = {x = 70917.75, y = 3176.68384, z = -11165.7813, adjacent =
	[
	]},
	["FP_PEE_BIGFARM_TREE"] = {x = 73172.0391, y = 3271.3584, z = -12013.5146, adjacent =
	[
	]},
	["FP_SMALLTALK_BIGFARMHUT_02"] = {x = 53537.3984, y = 1762.88721, z = -9979.61719, adjacent =
	[
	]},
	["FP_SMALLTALK_BIGFARMHUT_01"] = {x = 53372.0234, y = 1739.75305, z = -10037.582, adjacent =
	[
	]},
	["FP_STAND_SPIELPLATZ_06"] = {x = 65640.5703, y = 3808.77881, z = -24101.2773, adjacent =
	[
	]},
	["FP_STAND_SPIELPLATZ_05"] = {x = 65772.5391, y = 3790.12646, z = -24121.5137, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FOREST_03_NAVIGATION4"] = {x = 80026.4609, y = 4145.27588, z = -17149.6699, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FOREST_03_NAVIGATION3"] = {x = 79644.8438, y = 4109.67871, z = -17235.2266, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FOREST_03_NAVIGATION2"] = {x = 79936.8984, y = 4129.09521, z = -16708.1152, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FOREST_03_NAVIGATION1"] = {x = 79552.375, y = 4094.52075, z = -16751.6328, adjacent =
	[
	]},
	["FP_ROAM_MONSTERMILL_13"] = {x = 66925.4844, y = 2393.80273, z = -3285.7124, adjacent =
	[
	]},
	["FP_ROAM_MONSTERMILL_12"] = {x = 67181.5313, y = 2400.49756, z = -3472.85059, adjacent =
	[
	]},
	["FP_ROAM_MONSTERMILL_11"] = {x = 66599.3984, y = 2367.04053, z = -3848.11572, adjacent =
	[
	]},
	["FP_ROAM_MONSTERMILL_10"] = {x = 67047.5938, y = 2397.84717, z = -3958.65869, adjacent =
	[
	]},
	["FP_ROAM_MONSTERMILL_04"] = {x = 63905.1563, y = 2300.82837, z = -2211.00562, adjacent =
	[
	]},
	["FP_ROAM_MONSTERMILL_03"] = {x = 64192.6094, y = 2328.00806, z = -2364.74048, adjacent =
	[
	]},
	["FP_ROAM_MONSTERMILL_02"] = {x = 64495.5977, y = 2310.9104, z = -2149.67993, adjacent =
	[
	]},
	["FP_ROAM_MONSTERMILL_01"] = {x = 63984.8594, y = 2297.49219, z = -1911.67407, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_53"] = {x = 76019.7578, y = 3384.62256, z = -3363.34131, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_52"] = {x = 75342.875, y = 3357.29346, z = -3543.77002, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_51"] = {x = 76063.5313, y = 3400.45264, z = -3657.38989, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_50"] = {x = 75631.4609, y = 3348.19238, z = -3264.34058, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_44"] = {x = 77822.5156, y = 3364.80322, z = -2442.34912, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_42"] = {x = 77699.2266, y = 3393.97485, z = -2715.0957, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_41"] = {x = 78016.4688, y = 3451.65015, z = -3066.90039, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_40"] = {x = 78105.2578, y = 3393.99146, z = -2610.48022, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_38"] = {x = 75451.4922, y = 3247.83179, z = -604.812866, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_37"] = {x = 75936.4844, y = 3218.99414, z = -630.731079, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_36"] = {x = 75748.4063, y = 3228.60669, z = -464.90155, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_35"] = {x = 75565.8594, y = 3256.48462, z = -852.921021, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_33"] = {x = 74095.3281, y = 3219.31543, z = 548.646484, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_32"] = {x = 73658.5859, y = 3269.66895, z = 240.640167, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_31"] = {x = 73871.5625, y = 3271.83447, z = 245.708511, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_30"] = {x = 73774.5469, y = 3194.93164, z = 747.724731, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_24"] = {x = 69034.3828, y = 2149.05029, z = 2209.29858, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_23"] = {x = 69304.5469, y = 2205.29395, z = 1895.03699, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_22"] = {x = 69046.2656, y = 2185.86865, z = 1681.19897, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_21"] = {x = 68661.4531, y = 2138.18481, z = 1794.73193, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_19"] = {x = 70586.8594, y = 2717.12549, z = -610.895691, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_18"] = {x = 70562.7031, y = 2760.38013, z = -1076.07336, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_17"] = {x = 70877.0781, y = 2827.6687, z = -1021.58392, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_16"] = {x = 70855.7891, y = 2769.03442, z = -575.564575, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_15"] = {x = 68300.6875, y = 2335.26367, z = -1257.96631, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_13"] = {x = 68495.3594, y = 2335.2666, z = -883.496399, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_12"] = {x = 68023.6016, y = 2299.64941, z = -851.783081, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_11"] = {x = 68404.9297, y = 2324.24268, z = -649.72644, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_8"] = {x = 66105.4141, y = 2208.75293, z = -1140.13574, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_7"] = {x = 66245.5469, y = 2236.68994, z = -1331.07727, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_6"] = {x = 66525.1875, y = 2249.41162, z = -735.590576, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_5"] = {x = 66582.4531, y = 2284.74243, z = -1312.82166, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_4"] = {x = 65249.9531, y = 2140.68652, z = -384.358368, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_3"] = {x = 65608.875, y = 2102.71899, z = 275.682007, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_2"] = {x = 64934.3203, y = 2113.31079, z = -73.2280731, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT2_1"] = {x = 65568.4531, y = 2108.95117, z = -185.036911, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_03_MOVEMENT15"] = {x = 53252.207, y = 1545.39282, z = -478.957764, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_03_MOVEMENT12"] = {x = 53398.3125, y = 1621.21655, z = -840.68512, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_03_MOVEMENT11"] = {x = 53793.6953, y = 1613.90869, z = -285.702972, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_03_MOVEMENT20"] = {x = 53677.7344, y = 1640.92688, z = -678.854858, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_03_MOVEMENT13"] = {x = 53020.3242, y = 1647.29358, z = -3684.302, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_03_MOVEMENT12"] = {x = 53053.8594, y = 1619.54419, z = -3235.41553, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_03_MOVEMENT11"] = {x = 53499.4883, y = 1656.08569, z = -3622.12451, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_03_MOVEMENT10"] = {x = 53589.6133, y = 1649.69653, z = -3173.3252, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_03_MOVEMENT04"] = {x = 51938.4453, y = 1717.50623, z = -5923.32129, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_03_MOVEMENT03"] = {x = 51722.2109, y = 1712.64063, z = -5418.30518, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_03_MOVEMENT02"] = {x = 52270.8672, y = 1725.45215, z = -5967.0625, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_03_MOVEMENT01"] = {x = 52527.4219, y = 1686.29407, z = -5494.00488, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_15_04"] = {x = 39343.6758, y = 2885.42773, z = -23031.7305, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_15_03"] = {x = 38838.3047, y = 2811.76807, z = -22883.084, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_15_02"] = {x = 39431.375, y = 2863.31445, z = -22578.1348, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_15_01"] = {x = 38834.3984, y = 2781.56177, z = -22436.459, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_MOUNTAINLAKE_05"] = {x = 44269.9141, y = 2923.31787, z = -27602.5625, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_MOUNTAINLAKE_04"] = {x = 44387.1367, y = 2955.67822, z = -27189.6621, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_MOUNTAINLAKE_03"] = {x = 43850.3633, y = 2885.03467, z = -27141.7031, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_MOUNTAINLAKE_MONSTER_01"] = {x = 43922.9805, y = 2939.29736, z = -27510.2598, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_MOUNTAINLAKE_08"] = {x = 40474.8789, y = 2896.40063, z = -24406.8145, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_MOUNTAINLAKE_07"] = {x = 40569.0078, y = 2938.4729, z = -24028.2656, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_MOUNTAINLAKE_06"] = {x = 40195.7891, y = 2918.24438, z = -24066.5781, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_MOUNTAINLAKE_05"] = {x = 40041.5469, y = 2885.88623, z = -24564.7969, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_MOUNTAINLAKE_04"] = {x = 42678.8984, y = 2873.19849, z = -24814.75, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_MOUNTAINLAKE_03"] = {x = 43176.3203, y = 2873.20068, z = -25033.3691, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_MOUNTAINLAKE_02"] = {x = 43281.9063, y = 2877.24609, z = -24598.6465, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_MOUNTAINLAKE_01"] = {x = 42865.2266, y = 2882.89917, z = -24524.8574, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVERMID_04"] = {x = 47211.3281, y = 2507.86499, z = -14739.0254, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVERMID_03"] = {x = 47354.8906, y = 2525.26245, z = -14460.8604, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVERMID_02"] = {x = 45570.4844, y = 2524.4353, z = -16092.0801, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVERMID_01"] = {x = 45837.293, y = 2517.7002, z = -16158.0381, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_BIGWOOD_03_04"] = {x = 46229.8164, y = 2711.53711, z = -29050.875, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_BIGWOOD_03_03"] = {x = 46259.7656, y = 2738.14697, z = -28496.1055, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_BIGWOOD_03_02"] = {x = 45865.7031, y = 2733.82861, z = -28568.207, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_BIGWOOD_03_01"] = {x = 46597.4727, y = 2691.29126, z = -28771.9668, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_BIGWOOD_02_04"] = {x = 46653.293, y = 2748.85156, z = -25033.3965, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_BIGWOOD_02_03"] = {x = 46292.1875, y = 2740.36914, z = -25616.2148, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_BIGWOOD_02_02"] = {x = 46837.7305, y = 2799.68481, z = -25540.4727, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_BIGWOOD_02_01"] = {x = 46226.4102, y = 2734.17383, z = -25130.498, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_BIGWOOD_04_04"] = {x = 45876.8555, y = 2488.03833, z = -32763.0938, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_BIGWOOD_04_03"] = {x = 46311.832, y = 2445.53955, z = -32162.0801, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_BIGWOOD_04_02"] = {x = 46327.6172, y = 2452.25708, z = -32522.1309, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_BIGWOOD_04_01"] = {x = 45663.9375, y = 2478.0542, z = -32351.2559, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FARM3_03_04"] = {x = 56505.332, y = 1914.67371, z = -26616.0469, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FARM3_03_03"] = {x = 56503.3516, y = 1934.89697, z = -27179.4609, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FARM3_03_02"] = {x = 57130.0547, y = 1874.75635, z = -26956.3086, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FARM3_03_01"] = {x = 56920.1172, y = 1900.12817, z = -27226.0469, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_05_04"] = {x = 57456.6484, y = 1651.84705, z = -24530.1484, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_05_03"] = {x = 56744.793, y = 1659.54651, z = -24492.1484, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_05_02"] = {x = 57474.3398, y = 1702.58508, z = -25002.0137, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_05_01"] = {x = 56908.1836, y = 1706.6499, z = -24832.6367, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_ALLEE_08_N_5_04"] = {x = 56687.4141, y = 1664.63428, z = -18723.7402, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_ALLEE_08_N_5_03"] = {x = 57053.2578, y = 1730.26587, z = -18571.0215, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_ALLEE_08_N_5_02"] = {x = 56970.5391, y = 1719.08313, z = -18821.9043, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_ALLEE_08_N_5_01"] = {x = 56726.1797, y = 1711.39758, z = -18438.7051, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_ALLEE_08_N_2_04"] = {x = 55056.7461, y = 1829.29236, z = -15797.7188, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_ALLEE_08_N_2_03"] = {x = 54659.7539, y = 1743.44934, z = -16168.624, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_ALLEE_08_N_2_02"] = {x = 55392.1133, y = 1790.92639, z = -15941.3545, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_ALLEE_08_N_2_01"] = {x = 55357.2383, y = 1804.0697, z = -16463.2129, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_ALLEE_08_N4"] = {x = 55419.1914, y = 1784.71753, z = -13487.3193, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_ALLEE_08_N3"] = {x = 54995.6445, y = 1717.75928, z = -13577.833, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_ALLEE_08_N2"] = {x = 54987.6211, y = 1707.42261, z = -14181.8574, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_ALLEE_08_N1"] = {x = 55169.7617, y = 1749.70898, z = -13917.6182, adjacent =
	[
	]},
	["FP_ROAM_NW_CASTLEMINE_HUT_01_MONSTER_03"] = {x = 63654.7656, y = 3999.71777, z = -22952.2305, adjacent =
	[
	]},
	["FP_ROAM_NW_CASTLEMINE_HUT_01_MONSTER_02"] = {x = 63535.9609, y = 3956.4917, z = -23358.998, adjacent =
	[
	]},
	["FP_ROAM_NW_CASTLEMINE_HUT_01_MONSTER_01"] = {x = 63164.5703, y = 4042.94385, z = -23126.1445, adjacent =
	[
	]},
	["FP_ROAM_NW_CASTLEMINE_HUT_01_MONSTER_04"] = {x = 63261.543, y = 4056.09961, z = -22842.3359, adjacent =
	[
	]},
	["FP_ROAM_NW_CASTLEMINE_HUT_04"] = {x = 64494.7227, y = 3945.7356, z = -24274.2207, adjacent =
	[
	]},
	["FP_ROAM_NW_CASTLEMINE_HUT_03"] = {x = 64258.8906, y = 3922.6084, z = -24838.9512, adjacent =
	[
	]},
	["FP_ROAM_NW_CASTLEMINE_HUT_02"] = {x = 64576.6289, y = 3861.13794, z = -25014.9434, adjacent =
	[
	]},
	["FP_ROAM_NW_CASTLEMINE_HUT_01"] = {x = 64524.0977, y = 3936.24707, z = -24519.9004, adjacent =
	[
	]},
	["FP_ROAM_NW_CASTLEMINE_TROLL_05_04"] = {x = 67410.4844, y = 1505.90002, z = -27423.1934, adjacent =
	[
	]},
	["FP_ROAM_NW_CASTLEMINE_TROLL_05_03"] = {x = 67665.0938, y = 1512.47791, z = -27498.5527, adjacent =
	[
	]},
	["FP_ROAM_NW_CASTLEMINE_TROLL_05_02"] = {x = 67913.1484, y = 1513.4176, z = -27091.7949, adjacent =
	[
	]},
	["FP_ROAM_NW_CASTLEMINE_TROLL_05_01"] = {x = 67411.8203, y = 1519.99548, z = -27095.7012, adjacent =
	[
	]},
	["FP_ROAM_NW_CASTLEMINE_TROLL_02_04"] = {x = 71404.9531, y = 2037.15063, z = -21464.3613, adjacent =
	[
	]},
	["FP_ROAM_NW_CASTLEMINE_TROLL_02_03"] = {x = 71575.125, y = 2051.71216, z = -21295.6563, adjacent =
	[
	]},
	["FP_ROAM_NW_CASTLEMINE_TROLL_02_02"] = {x = 71149.4531, y = 1965.12073, z = -21154.457, adjacent =
	[
	]},
	["FP_ROAM_NW_CASTLEMINE_TROLL_02_01"] = {x = 70910.3438, y = 1945.12402, z = -21746.0059, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FOREST_03_NAVIGATION_04"] = {x = 84100.0156, y = 4350.59521, z = -15623.9424, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FOREST_03_NAVIGATION_03"] = {x = 84359.9766, y = 4330.49756, z = -16257.3262, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FOREST_03_NAVIGATION_02"] = {x = 83869.4063, y = 4339.63281, z = -15940.3135, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FOREST_03_NAVIGATION_01"] = {x = 84360.9297, y = 4360.64404, z = -15790.623, adjacent =
	[
	]},
	["FP_ROAM_NW_CRYPT_MONSTER06_04"] = {x = 81325.4297, y = 4010.99048, z = -7463.13623, adjacent =
	[
	]},
	["FP_ROAM_NW_CRYPT_MONSTER06_03"] = {x = 81882.6641, y = 4119.99268, z = -7474.52051, adjacent =
	[
	]},
	["FP_ROAM_NW_CRYPT_MONSTER06_02"] = {x = 81526.3594, y = 4016.62866, z = -7203.81396, adjacent =
	[
	]},
	["FP_ROAM_NW_CRYPT_MONSTER06_01"] = {x = 81857.6406, y = 4092.74707, z = -7263.97363, adjacent =
	[
	]},
	["FP_ROAM_NW_CRYPT_MONSTER04_04"] = {x = 80826.0078, y = 3721.4668, z = -4423.2627, adjacent =
	[
	]},
	["FP_ROAM_NW_CRYPT_MONSTER04_03"] = {x = 80470.3516, y = 3767.51196, z = -4847.55518, adjacent =
	[
	]},
	["FP_ROAM_NW_CRYPT_MONSTER04_02"] = {x = 79984.6016, y = 3710.19043, z = -4390.97266, adjacent =
	[
	]},
	["FP_ROAM_NW_CRYPT_MONSTER04_01"] = {x = 80706.4766, y = 3662.30835, z = -4101.08105, adjacent =
	[
	]},
	["FP_ROAM_NW_CRYPT_MONSTER08_04"] = {x = 78993.3594, y = 3716.32617, z = -4742.27441, adjacent =
	[
	]},
	["FP_ROAM_NW_CRYPT_MONSTER08_02"] = {x = 78704.8516, y = 3703.17041, z = -4928.05078, adjacent =
	[
	]},
	["FP_ROAM_NW_CRYPT_MONSTER08_026"] = {x = 79029.5469, y = 3779.28589, z = -5429.29639, adjacent =
	[
	]},
	["FP_ROAM_NW_CRYPT_MONSTER08_01"] = {x = 79222.3906, y = 3725.71777, z = -4917.08105, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FOREST_04"] = {x = 81625.8203, y = 4152.92871, z = -15118.1768, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FOREST_03"] = {x = 81127.5313, y = 4202.73193, z = -14571.8389, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FOREST_02"] = {x = 81221.2188, y = 4132.2583, z = -15196.3281, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FOREST_01"] = {x = 81609.7891, y = 4243.40527, z = -14615.5908, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_N_19"] = {x = 50496.5234, y = 1450.96973, z = 5106.06348, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_N_18"] = {x = 50706.625, y = 1270.29443, z = 5383.19775, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_N_17"] = {x = 50710.5898, y = 1178.09863, z = 5744.38086, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_N_16"] = {x = 50994.6211, y = 1285.87292, z = 5477.00098, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_N_15"] = {x = 50394.1211, y = 1276.16809, z = 5408.90381, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_N_8"] = {x = 55519.1211, y = 1396.82446, z = 7043.80811, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_N_7"] = {x = 55574.5586, y = 1479.79663, z = 6332.18994, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_N_6"] = {x = 55208.1992, y = 1387.37866, z = 6993.47314, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_N_5"] = {x = 56097.5469, y = 1548.66345, z = 6796.52686, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_N_4"] = {x = 54077.7344, y = 1357.96509, z = 6517.10889, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_N_3"] = {x = 53810.625, y = 1271.05566, z = 6990.37061, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_N_2"] = {x = 54340.9922, y = 1191.13599, z = 7244.146, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_N_1"] = {x = 54430.5391, y = 1359.36218, z = 6656.27344, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_MORE_09"] = {x = 53507.9805, y = 1593.06177, z = 2305.05933, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_MORE_08"] = {x = 53587.1367, y = 1594.93591, z = 2553.69312, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_MORE_07"] = {x = 53775.7148, y = 1612.91565, z = 2181.99805, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_MORE_06"] = {x = 53520.3594, y = 1618.00183, z = 1950.14148, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_MORE_05"] = {x = 53606.125, y = 1550.23328, z = 4098.25781, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_MORE_04"] = {x = 54303.3555, y = 1588.85071, z = 3685.11084, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_MORE_03"] = {x = 54081.4805, y = 1609.53394, z = 3206.33789, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_MORE_02"] = {x = 53779.1758, y = 1608.76794, z = 3493.02051, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_MONSTER_MORE_01"] = {x = 54317.1133, y = 1617.19434, z = 3409.49902, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT7_05"] = {x = 56407.168, y = 1672.10352, z = 2854.09497, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT7_03"] = {x = 56747.1055, y = 1730.37, z = 2883.58472, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT7_02"] = {x = 56186.6563, y = 1659.51111, z = 3091.63184, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM4_WOOD_NEARPEASANT7_01"] = {x = 56679.1641, y = 1711.8407, z = 3333.52295, adjacent =
	[
	]},
	["FP_ROAM_NW_SAGITTA_MOREMONSTER_02_4"] = {x = 64450.3594, y = 2365.38818, z = 5368.4375, adjacent =
	[
	]},
	["FP_ROAM_NW_SAGITTA_MOREMONSTER_02_3"] = {x = 64796.5703, y = 2375.16968, z = 5247.46289, adjacent =
	[
	]},
	["FP_ROAM_NW_SAGITTA_MOREMONSTER_02_2"] = {x = 65048.625, y = 2375.16968, z = 5051.47119, adjacent =
	[
	]},
	["FP_ROAM_NW_SAGITTA_MOREMONSTER_02_1"] = {x = 64589.2344, y = 2333.10962, z = 4731.16846, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_BLOODFLY06"] = {x = 45318.4492, y = 1919.07446, z = -6417.24365, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_BLOODFLY05"] = {x = 45808.8125, y = 1587.63757, z = -6874.88037, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_BLOODFLY04"] = {x = 46119.5039, y = 1613.0094, z = -6663.37939, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_BLOODFLY03"] = {x = 45535.9531, y = 1734.23035, z = -6737.12793, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_BLOODFLY02"] = {x = 45655.1758, y = 1831.01917, z = -6399.92627, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_BLOODFLY01"] = {x = 46067.5508, y = 1731.41248, z = -6468.89551, adjacent =
	[
	]},
	["FP_STAND_MALAKSVERSTECK_MALAK"] = {x = 66203.7813, y = 2399.27124, z = -20985.9453, adjacent =
	[
	]},
	["FP_ITEM_MALAKSVERSTECK"] = {x = 67168.4844, y = 2500.99658, z = -21736.293, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_MALAKSVERSTECK_06"] = {x = 67011.0625, y = 2479.2749, z = -22393.2188, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_MALAKSVERSTECK_05"] = {x = 66766, y = 2492.65771, z = -22094.4609, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_MALAKSVERSTECK_04"] = {x = 67051.1563, y = 2474.87915, z = -22049.7109, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_MALAKSVERSTECK_03"] = {x = 66655.2813, y = 2488.94189, z = -21904.5488, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_MALAKSVERSTECK_02"] = {x = 67317.1953, y = 2432.97437, z = -22518.3496, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_MALAKSVERSTECK_01"] = {x = 67280.3984, y = 2478.98511, z = -22307.0859, adjacent =
	[
	]},
	["FP_STAND_FARM4_FLEEDMT_04"] = {x = 61458.8203, y = 2014.82251, z = -2431.1792, adjacent =
	[
	]},
	["FP_STAND_FARM4_FLEEDMT_03"] = {x = 61414.6523, y = 2007.62915, z = -2936.08887, adjacent =
	[
	]},
	["FP_STAND_FARM4_FLEEDMT_02"] = {x = 61473, y = 2007.62915, z = -2615.95679, adjacent =
	[
	]},
	["FP_STAND_FARM4_FLEEDMT_01"] = {x = 61243.6602, y = 1989.84558, z = -2461.16895, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_FINAL_SPAWN_03"] = {x = 73367.9688, y = 2670.56396, z = -613.401062, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_FINAL_SPAWN_04"] = {x = 73481.9141, y = 2670.89233, z = 88.0476151, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_FINAL_SPAWN_05"] = {x = 74182.0859, y = 2670.89233, z = -59.3283997, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_FINAL_SPAWN_02"] = {x = 74129.875, y = 2669.8103, z = -1087.1792, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_FINAL_SPAWN_01"] = {x = 74330.4219, y = 2670.8103, z = -965.184387, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_02_SPAWN_07"] = {x = 73311.8906, y = 2667.60132, z = 306.343597, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_02_SPAWN_06"] = {x = 73465.7188, y = 2668.47754, z = 615.729614, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_02_SPAWN_05"] = {x = 73414.8516, y = 2665.82056, z = 949.791626, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_02_SPAWN_04"] = {x = 73139.3281, y = 2665.84009, z = 1134.88232, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_02_SPAWN_03"] = {x = 72638.625, y = 2673.771, z = 791.414673, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_02_SPAWN_02"] = {x = 72701.7891, y = 2674.65308, z = 489.981873, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_02_SPAWN_01"] = {x = 72966.9844, y = 2673.75659, z = 311.774414, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_01_SPAWN_07"] = {x = 73149.0234, y = 2668.10034, z = -779.631592, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_01_SPAWN_06"] = {x = 72801.6641, y = 2666.79028, z = -634.559814, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_01_SPAWN_05"] = {x = 72480.8359, y = 2666.78149, z = -698.210693, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_01_SPAWN_04"] = {x = 72287.4063, y = 2665.81885, z = -968.552917, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_01_SPAWN_03"] = {x = 72623.8281, y = 2667.74487, z = -1484.93774, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_01_SPAWN_02"] = {x = 72955.8125, y = 2670.62183, z = -1418.02917, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_01_SPAWN_01"] = {x = 73130.5313, y = 2667.73975, z = -1150.12024, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_03_SPAWNMAIN"] = {x = 74832.0469, y = 2670.50122, z = 374.075714, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_02_SPAWNMAIN"] = {x = 73038.8906, y = 2666.22437, z = 721.958862, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_01_SPAWNMAIN"] = {x = 72711.8672, y = 2669.43774, z = -1068.76685, adjacent =
	[
	]},
	["FP_NW_ITEM_CASTLEMINE2_EGG"] = {x = 61561.3828, y = 4410.16455, z = -38777, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE2_16"] = {x = 61264.1016, y = 4407.99365, z = -38458.6914, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE2_15"] = {x = 61248.1953, y = 4450.28271, z = -38060.1719, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE2_14"] = {x = 61571.0234, y = 4424.87646, z = -38380.9883, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE2_13"] = {x = 60875.2578, y = 4308.54883, z = -36772.2539, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE2_11"] = {x = 59645.0859, y = 4045.52295, z = -34743.1523, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE2_12"] = {x = 60426.6523, y = 4109.62793, z = -32776.5195, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE2_10"] = {x = 59728.7617, y = 4075.96533, z = -35283.6836, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE2_07"] = {x = 60745.0117, y = 4271.71387, z = -34440.4375, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE2_08"] = {x = 60583.8281, y = 4241.52393, z = -34171.0859, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE2_09"] = {x = 59826.4219, y = 4108.32178, z = -32721.4512, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE2_06"] = {x = 61202.7461, y = 4298.55762, z = -34708.6016, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE2_05"] = {x = 59255.0195, y = 4149.18555, z = -33080.1133, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE2_04"] = {x = 59125.043, y = 4151.7041, z = -33415.8125, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE2_03"] = {x = 59558.8633, y = 4168.2832, z = -33218.6211, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE2_02"] = {x = 61139.3398, y = 4020.80566, z = -32721.8008, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE2_01"] = {x = 61115.2734, y = 4020.80566, z = -32571.4395, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMILL_TROLL_05"] = {x = 66185.2422, y = 1459.87244, z = -32192.2441, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMILL_TROLL_04"] = {x = 65794.4453, y = 1483.25183, z = -31686.0938, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMILL_TROLL_03"] = {x = 66543.375, y = 1452.34583, z = -30082.7285, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMILL_TROLL_02"] = {x = 67618.1797, y = 1508.9021, z = -29781.0293, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMILL_TROLL_01"] = {x = 67193.6328, y = 1520.98022, z = -29399.0664, adjacent =
	[
	]},
	["FP_ROAM_BIGFARM_LAKE_CAVE_13"] = {x = 47580.2852, y = 2391.53809, z = -1074.8252, adjacent =
	[
	]},
	["FP_ROAM_BIGFARM_LAKE_CAVE_12"] = {x = 47788.4844, y = 2384.42285, z = -833.295044, adjacent =
	[
	]},
	["FP_ROAM_BIGFARM_LAKE_CAVE_11"] = {x = 48256.1133, y = 2381.78271, z = -1403.77893, adjacent =
	[
	]},
	["FP_ROAM_BIGFARM_LAKE_CAVE_10"] = {x = 47677.6094, y = 2383.91284, z = -1350.05066, adjacent =
	[
	]},
	["FP_ROAM_BIGFARM_LAKE_CAVE_09"] = {x = 47913.7344, y = 2400.02417, z = -1393.71118, adjacent =
	[
	]},
	["FP_ROAM_BIGFARM_LAKE_CAVE_08"] = {x = 49330.8047, y = 2346.30371, z = -706.076172, adjacent =
	[
	]},
	["FP_ROAM_BIGFARM_LAKE_CAVE_07"] = {x = 49471.293, y = 2358.30371, z = -1079.88171, adjacent =
	[
	]},
	["FP_ROAM_BIGFARM_LAKE_CAVE_06"] = {x = 49735.9688, y = 2348.30371, z = -962.153076, adjacent =
	[
	]},
	["FP_ROAM_BIGFARM_LAKE_CAVE_05"] = {x = 49613.7656, y = 2347.30371, z = -629.523743, adjacent =
	[
	]},
	["FP_ROAM_BIGFARM_LAKE_CAVE_04"] = {x = 48592.2383, y = 2373.30371, z = -3334.46826, adjacent =
	[
	]},
	["FP_ROAM_BIGFARM_LAKE_CAVE_03"] = {x = 48812.1484, y = 2374.30371, z = -3953.24902, adjacent =
	[
	]},
	["FP_NW_ITEM_BIGFARMLAKECAVE_EGG"] = {x = 50198.9336, y = 2346.30371, z = -3218.03882, adjacent =
	[
	]},
	["FP_ROAM_BIGFARM_LAKE_CAVE_02"] = {x = 50266.1758, y = 2358.30371, z = -3558.87354, adjacent =
	[
	]},
	["FP_ROAM_BIGFARM_LAKE_CAVE_01"] = {x = 49850.707, y = 2358.30371, z = -3338.46338, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE_12"] = {x = 61857.0664, y = 3949.18555, z = -25737.8164, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE_11"] = {x = 62006.9531, y = 3959.18555, z = -25926.9863, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE_09"] = {x = 63172.3047, y = 4078.80615, z = -27660.3477, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE_08"] = {x = 63127.7695, y = 4078.33838, z = -27445.584, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE_07"] = {x = 63457.3516, y = 4072.85107, z = -27610.9609, adjacent =
	[
	]},
	["FP_NW_ITEM_CASTLEMINE_EGG2"] = {x = 62788.75, y = 4072.67676, z = -28466.4805, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE_05"] = {x = 62668.2109, y = 4072.67676, z = -28482.957, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE_04"] = {x = 63015.9688, y = 4072.67676, z = -28442.1406, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE_03"] = {x = 60977.9336, y = 3865.68237, z = -27678.2988, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE_02"] = {x = 60866.6914, y = 3885.68237, z = -27860.6777, adjacent =
	[
	]},
	["FP_ROAM_CASTLEMINE_01"] = {x = 61168.9688, y = 3890.68237, z = -27928.2871, adjacent =
	[
	]},
	["FP_NW_ITEM_CASTLEMINE_EGG"] = {x = 61017.5117, y = 3902.68237, z = -28153.4824, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_08_02"] = {x = 66093.25, y = 2325.2937, z = 3123.9458, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_08_01"] = {x = 66190.2891, y = 2377.74756, z = 3700.50464, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_07_03"] = {x = 65001.2188, y = 2378.47314, z = 3420.20972, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_07_02"] = {x = 64478.2188, y = 2371.26514, z = 3348.00488, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_07_01"] = {x = 64456.8242, y = 2373.81104, z = 3878.38281, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_06_03"] = {x = 60545.3398, y = 2122.01416, z = 6169.18896, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_06_02"] = {x = 60844.2188, y = 2155.94019, z = 6641.65771, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_06_01"] = {x = 60420.625, y = 2086.104, z = 6447.42432, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_05_04"] = {x = 62441.3906, y = 2240.76685, z = 4763.98975, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_05_03"] = {x = 62350.5977, y = 2260.94214, z = 4383.94971, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_05_02"] = {x = 61584.5742, y = 2199.71387, z = 4774.50537, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_05_01"] = {x = 61927.4336, y = 2217.40234, z = 5028.08984, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_05_04"] = {x = 51803.6719, y = 3244.97729, z = 9822.7041, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_05_04"] = {x = 50649.6016, y = 3424.11841, z = 10097.5332, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_05_03"] = {x = 51293.6445, y = 3403.60791, z = 10288.4922, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_05_02"] = {x = 51293.9258, y = 3274.79761, z = 9699.77637, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_05_01"] = {x = 51649.9023, y = 3339.72827, z = 10067, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_04_03"] = {x = 48792.4336, y = 3597.75366, z = 11078.0781, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_04_02"] = {x = 48360.3555, y = 3538.29785, z = 10871.8066, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_04_01"] = {x = 48799.25, y = 3516.48071, z = 10708.3184, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_03_05"] = {x = 46266.332, y = 3287.3999, z = 5871.88135, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_03_04"] = {x = 46013.1094, y = 3399.8418, z = 6620.63135, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_03_03"] = {x = 45888.9648, y = 3309.10522, z = 6190.90088, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_03_02"] = {x = 46237.6875, y = 3430.54688, z = 6442.90283, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_03_01"] = {x = 45464.7578, y = 3307.30103, z = 6341.87549, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_ALVARES_02"] = {x = 74192.0078, y = 3292.47827, z = -14015.7441, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_ALVARES_01"] = {x = 74302.9922, y = 3295.38379, z = -14188.6416, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM3_REST_09"] = {x = 50601.1094, y = 3146.47388, z = -16254.1201, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM3_REST_08"] = {x = 48751.1055, y = 3080.04321, z = -16324.0928, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM3_REST_06"] = {x = 50256.8203, y = 3161.35376, z = -16337.0234, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM3_REST_07"] = {x = 50462.9453, y = 3179.35376, z = -16130.0957, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM3_REST_04"] = {x = 48753.0742, y = 3104.56958, z = -16087.4199, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM3_REST_03"] = {x = 48593.5234, y = 3084.56958, z = -15982.3887, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM3_REST_02"] = {x = 50294.1602, y = 2984.43237, z = -17876.498, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM3_REST_01"] = {x = 50474.8477, y = 2984.43237, z = -17885.8359, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_FARM3_PATH_05_02"] = {x = 47243.375, y = 2993.66284, z = -19673.1895, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_FARM3_PATH_05_01"] = {x = 47282.6641, y = 3013.44507, z = -19822.0918, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_FARM3_HOUSE_02"] = {x = 49575.1133, y = 2931.92603, z = -18319.1367, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_FARM3_HOUSE_01"] = {x = 49627.6094, y = 2946.92603, z = -18466.7031, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_FARM3_STABLE_OUT_02"] = {x = 48207.3516, y = 2952.11157, z = -17525.8164, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_FARM3_STABLE_OUT_01"] = {x = 48018.4492, y = 2952.11157, z = -17474.5352, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_OUT_05_05"] = {x = 48704.4648, y = 3071.97656, z = -20230.5176, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_OUT_05_03"] = {x = 48758.9688, y = 3067.24194, z = -19323.9941, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_OUT_05_04"] = {x = 49326.2734, y = 3098.98584, z = -19421.3711, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_OUT_05_02"] = {x = 49519.2852, y = 3148.98584, z = -19835.0645, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_OUT_05_01"] = {x = 49087.4648, y = 3078.98584, z = -19718.3965, adjacent =
	[
	]},
	["FP_STAND_NW_FARM3_BENGAR"] = {x = 49663.168, y = 2950.3374, z = -18401.2227, adjacent =
	[
	]},
	["FP_PICK_NW_FARM4_FIELD_07"] = {x = 62806.2227, y = 2285.11768, z = -6065.34082, adjacent =
	[
	]},
	["FP_PICK_NW_FARM4_FIELD_06"] = {x = 62064.8984, y = 2125.46582, z = -6099.52051, adjacent =
	[
	]},
	["FP_PICK_NW_FARM4_FIELD_05"] = {x = 62509.5391, y = 2195.15283, z = -5471.28516, adjacent =
	[
	]},
	["FP_PICK_NW_FARM4_FIELD_03"] = {x = 59714.8789, y = 2102.92163, z = -5919.59619, adjacent =
	[
	]},
	["FP_PICK_NW_FARM4_FIELD_02"] = {x = 59064.5, y = 2090.46875, z = -6160.76123, adjacent =
	[
	]},
	["FP_PICK_NW_FARM4_FIELD_01"] = {x = 59141.4102, y = 2077.80957, z = -5435.58057, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_PEPES_WOLFS_03"] = {x = 82036.7891, y = 4211.22998, z = -18467.793, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_PEPES_WOLFS_02"] = {x = 81885.5781, y = 4195.0542, z = -18981.3301, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_PEPES_WOLFS_01"] = {x = 80828, y = 4215.62695, z = -18638, adjacent =
	[
	]},
	["FP_STAND_BIGFARM_OUT_TORLOF"] = {x = 73861.2031, y = 3258.11182, z = -11275.5273, adjacent =
	[
	]},
	["FP_PICK_NW_BIGFARM_FIELD_01_03"] = {x = 65030.2383, y = 2470.21802, z = -9846.40723, adjacent =
	[
	]},
	["FP_PICK_NW_BIGFARM_FIELD_01_02"] = {x = 65308.0664, y = 2471.21802, z = -9942.87109, adjacent =
	[
	]},
	["FP_PICK_NW_BIGFARM_FIELD_01_01"] = {x = 65000.3984, y = 2460.21802, z = -10104.5518, adjacent =
	[
	]},
	["FP_STAND_NW_BIGFARM_HOUSE_16"] = {x = 75715.3828, y = 3525.72925, z = -9257.9873, adjacent =
	[
	]},
	["FP_STAND_NW_BIGFARM_KITCHEN_DAR"] = {x = 73518.8125, y = 3293.57983, z = -8388.79004, adjacent =
	[
	]},
	["FP_STAND_NW_BIGFARM_STABLE_01"] = {x = 71547.3125, y = 3172.80029, z = -13254.1221, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_STABLE_01_02"] = {x = 72382.3594, y = 3170.78467, z = -12948.291, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_STABLE_01_01"] = {x = 72247.75, y = 3173.78467, z = -12997.3477, adjacent =
	[
	]},
	["FP_STAND_NW_BIGFARM_HOUSE_06_01"] = {x = 75759.7813, y = 3525.00488, z = -11876.0977, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_KITCHEN_02"] = {x = 72742.7578, y = 3164.49487, z = -9653.44434, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_KITCHEN_01"] = {x = 72829.4297, y = 3185.88428, z = -9591.80566, adjacent =
	[
	]},
	["FP_STAND_NW_BIGFARM_KITCHEN_BAR_01"] = {x = 71901.0625, y = 3205.32764, z = -9038.76758, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_STABLE_01"] = {x = 71280.4063, y = 3190.38477, z = -11933.7363, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_STABLE_02"] = {x = 71204.0859, y = 3201.74072, z = -12097.5586, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP2_01"] = {x = 72339.6563, y = 3051.12231, z = -16757.9219, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP2_02"] = {x = 72426.6797, y = 3015.83887, z = -17282.8613, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP2_03"] = {x = 72625.4297, y = 3170.46411, z = -16499.7617, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP2_04"] = {x = 74803.4688, y = 3447.11621, z = -19425.6445, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP2_05"] = {x = 74916.9688, y = 3516.10718, z = -18773.0781, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP2_06"] = {x = 74089.2813, y = 3315.29346, z = -18658.2969, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP2_07"] = {x = 77361.3672, y = 3846.20288, z = -16937.0645, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP2_08"] = {x = 77216.9219, y = 3809.02295, z = -16265.5068, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP2_09"] = {x = 76619.3828, y = 3798.61279, z = -16398.0703, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP2_10"] = {x = 76500.1953, y = 3787.12109, z = -17009.1445, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP2_11"] = {x = 74996.4531, y = 3698.18872, z = -15942.3887, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP2_12"] = {x = 74388.0156, y = 3617.73193, z = -15797.0781, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP2_13"] = {x = 74154.1094, y = 3613.23022, z = -16789.3652, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP2_14"] = {x = 74967.3047, y = 3649.69702, z = -16748.6328, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP1_01"] = {x = 70237.1641, y = 2901.20752, z = -3885.54395, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP1_02"] = {x = 70731.5234, y = 3070.01294, z = -4113.70166, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP1_03"] = {x = 70435.875, y = 2914.21289, z = -3239.90186, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP1_04"] = {x = 72707, y = 3303, z = -4324, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP1_05"] = {x = 72466, y = 3271, z = -4328, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP1_06"] = {x = 72591.9297, y = 3276.59448, z = -5412.23486, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP1_07"] = {x = 72394, y = 3235, z = -3875, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP1_08"] = {x = 71479.1328, y = 3076.02588, z = -6184.34277, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP1_09"] = {x = 71132, y = 3066.79004, z = -6597, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP1_10"] = {x = 71198.5078, y = 3044.69287, z = -5723.67578, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_SHEEP1_11"] = {x = 70667.1953, y = 3065.50024, z = -6592.12109, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_KITCHEN_OUT_01"] = {x = 70540.4609, y = 3130.69385, z = -10086.3672, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_KITCHEN_OUT_02"] = {x = 70833.6875, y = 3110.20972, z = -10745.6211, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_KITCHEN_OUT_03"] = {x = 71167.8125, y = 3150.20972, z = -10615.1836, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_KITCHEN_OUT_04"] = {x = 70989.1484, y = 3160.20972, z = -10083.8203, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_SMITH_01"] = {x = 73153.0313, y = 3255.84131, z = -11211.0234, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_SMITH_02"] = {x = 73216.9141, y = 3250.84131, z = -11020.8105, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_HOUSE_OUT_01"] = {x = 73965.2813, y = 3265.16138, z = -11976.709, adjacent =
	[
	]},
	["FP_STAND_NW_BIGFARM_STABLE_OUT_01"] = {x = 72594.1953, y = 3246.69971, z = -11786.7002, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_KITCHEN_OUT_01"] = {x = 71681.6797, y = 3218.36475, z = -11227.1963, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_KITCHEN_OUT_02"] = {x = 71655.8828, y = 3212.36475, z = -11045.8369, adjacent =
	[
	]},
	["FP_STAND_NW_BIGFARM_VORPOSTEN1_02"] = {x = 68366.1563, y = 2482.45972, z = -11338.5713, adjacent =
	[
	]},
	["FP_STAND_NW_BIGFARM_VORPOSTEN1_01"] = {x = 68442.2813, y = 2540.00073, z = -11490.5703, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_BIGFARM_VORPOSTEN1_01"] = {x = 68294.2734, y = 2463.52222, z = -12195.8711, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_BIGFARM_VORPOSTEN1_02"] = {x = 68065.5781, y = 2463.52222, z = -12275.4043, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_HOUSE_SLD_01"] = {x = 75185.1797, y = 3545.93848, z = -12840.5898, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_HOUSE_SLD_02"] = {x = 75169.2422, y = 3545.93848, z = -12713.6201, adjacent =
	[
	]},
	["FP_STAND_NW_BIGFARM_HOUSE_UP2_01"] = {x = 75926.0469, y = 3852.84644, z = -12629.7568, adjacent =
	[
	]},
	["FP_STAND_NW_BIGFARM_HOUSE_LEE_GUARDING"] = {x = 75705.4375, y = 3542.78809, z = -13148.1436, adjacent =
	[
	]},
	["FP_STAND_NW_BIGFARM_HOUSE_ONAR_01"] = {x = 74827.8438, y = 3519.51563, z = -10028.5723, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_HOUSE_OUT_02"] = {x = 73937.0625, y = 3255.16138, z = -12176.3057, adjacent =
	[
	]},
	["FP_STAND_NW_BIGFARM_PEPE"] = {x = 76976.1328, y = 3766.64551, z = -16800.9336, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_PEPES_WOLFS_04"] = {x = 81977.0391, y = 4180.15771, z = -19509.7734, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_PEPES_WOLFS_05"] = {x = 81513.9688, y = 4235.22705, z = -18616.6953, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_PEPES_WOLFS_06"] = {x = 81598, y = 4241.79883, z = -18228, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FARM3_01"] = {x = 50841.2344, y = 3141.91846, z = -25587.3965, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FARM3_02"] = {x = 50731.1055, y = 3138.91846, z = -26072.168, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FARM3_03"] = {x = 50347.2891, y = 3116.33838, z = -25433.9258, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FARM3_04"] = {x = 50410.0195, y = 3104.33838, z = -25991.8125, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FARM3_05"] = {x = 50096.3906, y = 3092.86157, z = -25706.3535, adjacent =
	[
	]},
	["FP_PICK_NW_FARM3_FIELD_01"] = {x = 50395.9805, y = 3050.76514, z = -22143.0078, adjacent =
	[
	]},
	["FP_PICK_NW_FARM3_FIELD_02"] = {x = 50647.9375, y = 3070.18506, z = -21961.3398, adjacent =
	[
	]},
	["FP_PICK_NW_FARM3_FIELD_03"] = {x = 50082.2813, y = 3000.18506, z = -21915.3672, adjacent =
	[
	]},
	["FP_PICK_NW_FARM3_FIELD_04"] = {x = 50799.293, y = 3074.04565, z = -22353.373, adjacent =
	[
	]},
	["FP_PICK_NW_FARM3_FIELD_05"] = {x = 51272.1133, y = 3088.00024, z = -21792.1172, adjacent =
	[
	]},
	["FP_PICK_NW_FARM3_FIELD_06"] = {x = 51162.2656, y = 3077.4646, z = -21459.3945, adjacent =
	[
	]},
	["FP_PICK_NW_FARM3_FIELD_07"] = {x = 51672.125, y = 3057.59448, z = -21642.8008, adjacent =
	[
	]},
	["FP_PICK_NW_FARM3_FIELD_08"] = {x = 51348.8164, y = 3042.3999, z = -21343.3789, adjacent =
	[
	]},
	["FP_PICK_NW_FARM3_FIELD_09"] = {x = 51529.8477, y = 3099.40796, z = -23272.373, adjacent =
	[
	]},
	["FP_PICK_NW_FARM3_FIELD_10"] = {x = 51907.1875, y = 3070.67847, z = -23311.5195, adjacent =
	[
	]},
	["FP_PICK_NW_FARM3_FIELD_11"] = {x = 51716.5273, y = 3108.38428, z = -23644.8984, adjacent =
	[
	]},
	["FP_PICK_NW_FARM3_FIELD_12"] = {x = 51226.5547, y = 3101.80786, z = -23372.3203, adjacent =
	[
	]},
	["FP_PICK_NW_FARM3_FIELD_13"] = {x = 50524.4141, y = 3025.50122, z = -23616.9063, adjacent =
	[
	]},
	["FP_PICK_NW_FARM3_FIELD_14"] = {x = 50809.3242, y = 3061.77124, z = -23776.1699, adjacent =
	[
	]},
	["FP_PICK_NW_FARM3_FIELD_15"] = {x = 50493.8945, y = 2988.61108, z = -23987.5898, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_RUMBOLD_FLUCHT2_01"] = {x = 58812.4727, y = 1856.59961, z = -27537.5723, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_RUMBOLD_FLUCHT2_01"] = {x = 58958.8242, y = 1864.43945, z = -27541.8809, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_01_01"] = {x = 42351.4375, y = 3452.46216, z = -1451.75647, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_01_02"] = {x = 42331.8906, y = 3313.75708, z = -791.360596, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_01_03"] = {x = 42815.3633, y = 3389.58081, z = -1311.96338, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_01_04"] = {x = 42873.9805, y = 3339.18945, z = -898.232666, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_02_01"] = {x = 44751.4531, y = 2915.95801, z = 2133.1582, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_02_02"] = {x = 44142.6523, y = 3003.35669, z = 2968.92188, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_02_03"] = {x = 44005.8359, y = 2958.53394, z = 2451.10205, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_02_04"] = {x = 44990.1914, y = 3046.729, z = 3076.68604, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_01_01"] = {x = 45596.4805, y = 2580.89258, z = -3640.12183, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_01_02"] = {x = 46543.0898, y = 2448.68408, z = -3665.29224, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_01_03"] = {x = 45946.1836, y = 2492.35645, z = -4013.77466, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_01_04"] = {x = 46229.6602, y = 2468.67847, z = -4197.58643, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_02_01"] = {x = 44484.8086, y = 2118.4668, z = -6180.72314, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_02_02"] = {x = 44519.2539, y = 2070.90942, z = -6542.37939, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_02_03"] = {x = 44020.5039, y = 2131.2749, z = -6492.63916, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_02_04"] = {x = 44108.3438, y = 2095.1377, z = -6748.95166, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_03_01"] = {x = 50096.2969, y = 1998.85352, z = -5956.01123, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_03_02"] = {x = 49639.1133, y = 1971.15601, z = -6036.90625, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_03_03"] = {x = 49779.7539, y = 2085.05469, z = -5616.24902, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_03_04"] = {x = 50227.3242, y = 2082.19897, z = -5490.97314, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_04_01"] = {x = 47507.7813, y = 1628.74707, z = -10308.4834, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_04_02"] = {x = 48050.5234, y = 1636.47461, z = -10618.1104, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_05_01"] = {x = 53818.5391, y = 1829.46619, z = -8117.24902, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_05_02"] = {x = 54010.1133, y = 1812.88684, z = -7567.4126, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_05_03"] = {x = 53473.168, y = 1831.47009, z = -7647.34277, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_LAKE_MONSTER_05_04"] = {x = 53488.0313, y = 1835.87976, z = -7961.62646, adjacent =
	[
	]},
	["FP_ROAM_NW_TAVERNE_BIGFARM_MONSTER_01_01"] = {x = 39147.0508, y = 3674.12622, z = -5265.83789, adjacent =
	[
	]},
	["FP_ROAM_NW_TAVERNE_BIGFARM_MONSTER_01_02"] = {x = 39405.2305, y = 3728.91895, z = -4623.72412, adjacent =
	[
	]},
	["FP_ROAM_NW_TAVERNE_BIGFARM_MONSTER_01_03"] = {x = 38986.7031, y = 3723.58423, z = -4933.07178, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_01_01"] = {x = 60847.5117, y = 2152.95093, z = 3195.00317, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_01_02"] = {x = 60263.9102, y = 2137.30151, z = 3268.43042, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_01_03"] = {x = 60826.5742, y = 2170.18945, z = 3603.95776, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_01_04"] = {x = 60251.7461, y = 2159.31177, z = 3652.69067, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_02_01"] = {x = 59025.5234, y = 2029.29016, z = 4087.35938, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_02_02"] = {x = 58701.0391, y = 1994.3999, z = 3601.302, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_02_03"] = {x = 59057.7227, y = 2019.97949, z = 3696.97217, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_02_04"] = {x = 58507.5313, y = 1999.19165, z = 3996.02734, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_02_05"] = {x = 58724.4766, y = 2023.72205, z = 4245.6499, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_03_01"] = {x = 57121.375, y = 1834.40955, z = 4404.47656, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_03_02"] = {x = 57506.5039, y = 1894.75281, z = 4912.83496, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_03_03"] = {x = 56900.7813, y = 1786.55505, z = 4832.24414, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_04_01"] = {x = 55152.7227, y = 1546.10315, z = 4422.35352, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_04_02"] = {x = 55102.6797, y = 1505.8584, z = 4894.69873, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_04_03"] = {x = 54920.4688, y = 1503.59558, z = 4690.5498, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_09_01"] = {x = 67306.9844, y = 2245.52075, z = 2631.10742, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_09_02"] = {x = 67389.4844, y = 2228.01172, z = 2940.19214, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_10_01"] = {x = 70640.8516, y = 2531.95508, z = 1305.0177, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_10_02"] = {x = 70461.375, y = 2546.00195, z = 880.255005, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_10_03"] = {x = 70762.3828, y = 2597.6853, z = 993.092529, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_10_04"] = {x = 70372.4531, y = 2523.44922, z = 1117.21436, adjacent =
	[
	]},
	["FP_ROAM_NW_WOOD_MONSTER_10_05"] = {x = 70749.3438, y = 2617.57886, z = 661.22699, adjacent =
	[
	]},
	["FP_ROAM_FARM3_PATH_12_MONSTER_01MONSTER_01_01"] = {x = 39745.0273, y = 3392.67896, z = -10802.8994, adjacent =
	[
	]},
	["FP_ROAM_FARM3_PATH_12_MONSTER_01MONSTER_01_02"] = {x = 40176.0508, y = 3264.96533, z = -11300.9629, adjacent =
	[
	]},
	["FP_ROAM_FARM3_PATH_12_MONSTER_01MONSTER_01_03"] = {x = 39870.7813, y = 3279.09399, z = -11486.0215, adjacent =
	[
	]},
	["FP_ROAM_FARM3_PATH_12_MONSTER_01MONSTER_01_04"] = {x = 40088.6055, y = 3358.55737, z = -10730.1191, adjacent =
	[
	]},
	["FP_ROAM_FARM3_PATH_12_MONSTER_01MONSTER_02_01"] = {x = 40771.9063, y = 3050.98828, z = -13149.2256, adjacent =
	[
	]},
	["FP_ROAM_FARM3_PATH_12_MONSTER_01MONSTER_01_03"] = {x = 40727.8555, y = 3064.39038, z = -13825.4229, adjacent =
	[
	]},
	["FP_ROAM_FARM3_PATH_12_MONSTER_01MONSTER_01_04"] = {x = 40196.7969, y = 3078.40332, z = -13816.5596, adjacent =
	[
	]},
	["FP_ROAM_FARM3_PATH_12_MONSTER_01MONSTER_01_05"] = {x = 39974.1641, y = 3135.88794, z = -13156.2734, adjacent =
	[
	]},
	["FP_ROAM_FARM3_PATH_12_MONSTER_01MONSTER_03_01"] = {x = 39699.875, y = 3050.50586, z = -14904.7637, adjacent =
	[
	]},
	["FP_ROAM_FARM3_PATH_12_MONSTER_01MONSTER_03_02"] = {x = 38876.9102, y = 3115.5918, z = -14915.9609, adjacent =
	[
	]},
	["FP_ROAM_FARM3_PATH_12_MONSTER_01MONSTER_03_03"] = {x = 39115.8516, y = 3051.1355, z = -15476.9971, adjacent =
	[
	]},
	["FP_NW_ITEM_BIGFARMFORESTCAVE_EGG"] = {x = 85138.1016, y = 4285.6416, z = -9428.48633, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARMFORESTCAVE_01"] = {x = 85074.4063, y = 4303.88232, z = -9754.8457, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARMFORESTCAVE_02"] = {x = 85381.0313, y = 4295.08447, z = -9355.13281, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARMFORESTCAVE_03"] = {x = 85392.6641, y = 4291.08447, z = -9768.44824, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARMFORESTCAVE_04"] = {x = 85736.7578, y = 4291.08447, z = -9362.7666, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARMFORESTCAVE_05"] = {x = 84395.1719, y = 4285.08447, z = -10666.7373, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARMFORESTCAVE_06"] = {x = 84098.1563, y = 4275.08447, z = -10843.0381, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARMFORESTCAVE_07"] = {x = 84645.3828, y = 4265.08447, z = -10787.1904, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARMFORESTCAVE_08"] = {x = 84297.5859, y = 4268.08447, z = -11045.7314, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARMFORESTCAVE_09"] = {x = 85881.9766, y = 4287.74561, z = -11065.7119, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARMFORESTCAVE_10"] = {x = 85841.9844, y = 4287.74561, z = -11326.8467, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_01"] = {x = 63705.6719, y = 2385.38379, z = -12017.7803, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_02"] = {x = 61271.1016, y = 2241.61792, z = -8952.05273, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_03"] = {x = 52295.1172, y = 3160.43091, z = -26123.7402, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_04"] = {x = 41032.7734, y = 2868.85425, z = -22075.5684, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_05"] = {x = 38445.7539, y = 3838.14209, z = -7864.71338, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_06"] = {x = 47745.8789, y = 2122.12988, z = -5420.97607, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_07"] = {x = 57079.0742, y = 1689.49109, z = -20494.2324, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_08"] = {x = 71558.7734, y = 2340.07959, z = -19420.7441, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_09"] = {x = 65397.4688, y = 2461.60376, z = -18221.0098, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_10"] = {x = 56816.1953, y = 2022.32813, z = 9159.85352, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_11"] = {x = 45264.7383, y = 3429.3772, z = 7070.2041, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_12"] = {x = 38663.1563, y = 2742.07227, z = -19773.9336, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_13"] = {x = 44418.5273, y = 2808.93262, z = -23549.6797, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_14"] = {x = 48992.3555, y = 3022.71606, z = -24260.9121, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_01"] = {x = 52279.7305, y = 1804.13159, z = -7658.66602, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_02"] = {x = 52015.6641, y = 1754.13159, z = -8256.90625, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_03"] = {x = 44704.6016, y = 3496.55078, z = 7209.72314, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_04"] = {x = 45152.0977, y = 3416.55078, z = 6754.25977, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_05"] = {x = 59756.6719, y = 2133.64429, z = 5208.18018, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_06"] = {x = 59901.5625, y = 2143.64429, z = 5086.75, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_07"] = {x = 65026.2813, y = 2330.35913, z = -3050.21533, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_08"] = {x = 64850.0391, y = 2336.45703, z = -2732.11621, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_09"] = {x = 74074.5, y = 3320.85034, z = -5497.21289, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_10"] = {x = 77467.6016, y = 3750.02124, z = -20405.8574, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_11"] = {x = 67569.6797, y = 1647.66284, z = -24535.9375, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_12"] = {x = 65825.0625, y = 3956.05957, z = -21795.2559, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_14"] = {x = 54242.2422, y = 2544.4187, z = -28047.6953, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_15"] = {x = 51286.4531, y = 3033.48877, z = -15147.0166, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_16"] = {x = 44871.7305, y = 2608.47998, z = -15668.1328, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_17"] = {x = 39325.2734, y = 3084.77222, z = -14699.2119, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_19"] = {x = 44462.7695, y = 2992.43555, z = -104.162476, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_20"] = {x = 44535.8008, y = 3032.43555, z = -337.286743, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_21"] = {x = 51766.9883, y = 1556.0144, z = -11775.9736, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_22"] = {x = 53976.9063, y = 1699.29431, z = -2394.91992, adjacent =
	[
	]},
	["FP_STAND_DEMENTOR_KDF_23"] = {x = 51372.3125, y = 3106.00806, z = -15404.7061, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_03_SPAWN_01"] = {x = 74399.8672, y = 2664.63013, z = 461.867096, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_03_SPAWN_02"] = {x = 74587.3047, y = 2667.99561, z = 733.947632, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_03_SPAWN_03"] = {x = 74915.3359, y = 2670.53809, z = 794.38208, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_03_SPAWN_04"] = {x = 75227.3438, y = 2676.4751, z = 293.180023, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_03_SPAWN_05"] = {x = 75052.7266, y = 2674.77881, z = 33.9089584, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_03_SPAWN_06"] = {x = 74740.9922, y = 2667.99463, z = -26.5980644, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_03_SPAWN_07"] = {x = 74425.9375, y = 2668.16577, z = 114.928787, adjacent =
	[
	]},
	["EVT_CRYPT_ROOM_FINAL_SPAWN_06"] = {x = 74261.8672, y = 2667.87036, z = -1071.66602, adjacent =
	[
	]},
	["FP_ROAM_FARM4_SHEEP_01"] = {x = 58375.1016, y = 2001.57813, z = -3564.87695, adjacent =
	[
	]},
	["FP_ROAM_FARM4_SHEEP_02"] = {x = 58367.5156, y = 1989.8313, z = -3765.06763, adjacent =
	[
	]},
	["FP_ROAM_FARM4_SHEEP_03"] = {x = 58498.875, y = 1990.28882, z = -3432.7688, adjacent =
	[
	]},
	["FP_ROAM_FARM4_SHEEP_04"] = {x = 58850.9492, y = 1963.15527, z = -3661.14941, adjacent =
	[
	]},
	["FP_ROAM_FARM4_SHEEP_05"] = {x = 58684.3633, y = 1973.13159, z = -3885.60864, adjacent =
	[
	]},
	["FP_CAMPFIRE_FARM4_01"] = {x = 58171.7773, y = 1962.8501, z = -1178.3219, adjacent =
	[
	]},
	["FP_CAMPFIRE_FARM4_02"] = {x = 58397.7617, y = 2027.54224, z = -1169.32568, adjacent =
	[
	]},
	["FP_CAMPFIRE_FARM4_03"] = {x = 58531.3633, y = 2021.95032, z = -659.253052, adjacent =
	[
	]},
	["FP_CAMPFIRE_FARM4_05"] = {x = 58294.0156, y = 2005.17456, z = -560.363708, adjacent =
	[
	]},
	["FP_CAMPFIRE_FARM4_04"] = {x = 58577.8242, y = 2021.95032, z = -939.453125, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_03_01M1"] = {x = 47657.0547, y = 3390.31519, z = 4513.24805, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_03_01M2"] = {x = 47568.6758, y = 3425.88184, z = 5137.05518, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_03_01M3"] = {x = 47374.6211, y = 3393.16846, z = 4880.99023, adjacent =
	[
	]},
	["FP_ROAM_TAVERNE_TROLLAREA_03_01M4"] = {x = 47714.8594, y = 3403.02271, z = 4786.05078, adjacent =
	[
	]},
	["FP_ROAM_NW_TAVERNE_TROLLAREA_MONSTER_05_01M1"] = {x = 52372.8203, y = 2997.49414, z = 9035.80176, adjacent =
	[
	]},
	["FP_ROAM_NW_TAVERNE_TROLLAREA_MONSTER_05_01M2"] = {x = 52848.5625, y = 3026.64453, z = 9386.49121, adjacent =
	[
	]},
	["FP_ROAM_NW_TAVERNE_TROLLAREA_MONSTER_05_01M3"] = {x = 52594.6992, y = 3139.00781, z = 9656.5791, adjacent =
	[
	]},
	["FP_ROAM_NW_TAVERNE_TROLLAREA_MONSTER_05_01M4"] = {x = 52797.8203, y = 2930.72363, z = 9006.4248, adjacent =
	[
	]},
	["FP_ROAM_NW_SAGITTA_MOREMONSTER_01"] = {x = 63525.3438, y = 2268.69092, z = 6094.78027, adjacent =
	[
	]},
	["FP_ROAM_NW_SAGITTA_MOREMONSTER_02"] = {x = 63714.9102, y = 2231.60645, z = 6447.42773, adjacent =
	[
	]},
	["FP_ROAM_NW_SAGITTA_MOREMONSTER_03"] = {x = 63944.8047, y = 2258.97729, z = 6220.81885, adjacent =
	[
	]},
	["FP_ROAM_NW_SAGITTA_MOREMONSTER_04"] = {x = 63347.2617, y = 2251.91479, z = 6002.979, adjacent =
	[
	]},
	["FP_ROAM_NW_SAGITTA_MOREMONSTER_06"] = {x = 63380.2773, y = 2202.46948, z = 6472.25342, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_01"] = {x = 44022.8398, y = 2621.61938, z = -13882.9873, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_02"] = {x = 44364.2852, y = 2596.09473, z = -13408.2754, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_03"] = {x = 44627.3438, y = 2550.04956, z = -13847.6475, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_04"] = {x = 43871.9219, y = 2652.37915, z = -13570.377, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_05"] = {x = 43831.2383, y = 2608.58594, z = -15178.2979, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_06"] = {x = 44251.793, y = 2602.37646, z = -14949.7432, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_07"] = {x = 44124.1875, y = 2640.67847, z = -14571.2744, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_08"] = {x = 43670.918, y = 2683.41113, z = -14678.7969, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_09"] = {x = 41314.7383, y = 2727.86719, z = -17372.4102, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_10"] = {x = 41769.6016, y = 2676.98535, z = -17306.3535, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_11"] = {x = 41625.9141, y = 2702.42676, z = -17544.9258, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_12"] = {x = 41671.6133, y = 2685.46582, z = -17054.0723, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_15"] = {x = 39254.1211, y = 2757.99951, z = -18710.0371, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_16"] = {x = 39930.4219, y = 2724.9209, z = -18589.1953, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_17"] = {x = 39679.4766, y = 2722.37842, z = -18931.6504, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_19"] = {x = 39482.8086, y = 2757.99756, z = -18491.8086, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_20"] = {x = 38471.8594, y = 2724.729, z = -21590.2949, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_21"] = {x = 38738.6133, y = 2727.27637, z = -21193.7773, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_22"] = {x = 38396.8242, y = 2745.08252, z = -21024.1094, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_23"] = {x = 38264.1289, y = 2779.00439, z = -21231.3262, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_30"] = {x = 39847.9375, y = 2770.52563, z = -20323.0801, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_31"] = {x = 39698.5469, y = 2795.96704, z = -20675.5039, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_32"] = {x = 39445.3281, y = 2732.36597, z = -20108.0469, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_33"] = {x = 39431.9531, y = 2744.97192, z = -20745.3418, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_35"] = {x = 43703.3828, y = 2679.49951, z = -17914.6855, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_36"] = {x = 43245.2539, y = 2671.55298, z = -17907.6602, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_37"] = {x = 43739.1914, y = 2646.83032, z = -17648.3613, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_38"] = {x = 43490.5313, y = 2713.90234, z = -18168.7168, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_40"] = {x = 46986.0547, y = 2632.06763, z = -16540.8965, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_41"] = {x = 46670.5313, y = 2588.57495, z = -16469.9316, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_42"] = {x = 47204.707, y = 2544.15601, z = -16098.7715, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_PATH_11_SMALLRIVER_44"] = {x = 46964.6758, y = 2545.98291, z = -16080.9482, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FELDREUBER2"] = {x = 64311.5156, y = 2418.54346, z = -16140.5889, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FELDREUBER3"] = {x = 64738.0352, y = 2453.25806, z = -16156.8096, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FELDREUBER4"] = {x = 64578.0703, y = 2413.0625, z = -16921.5273, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FELDREUBER5"] = {x = 64850.7383, y = 2424.02417, z = -17010.8887, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FELDREUBER7"] = {x = 64563.4297, y = 2478.37329, z = -15493.1846, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FELDREUBER8"] = {x = 64293.0742, y = 2439.08936, z = -15580.7148, adjacent =
	[
	]},
	["FP_STAND_SPIELPLATZ"] = {x = 65893.9219, y = 3949.05933, z = -21271.9453, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FELDREUBER10"] = {x = 63802.125, y = 2450.74438, z = -13776.9189, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FELDREUBER11"] = {x = 64351.7617, y = 2460.55884, z = -13730.0605, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FELDREUBER13"] = {x = 66634.6094, y = 2402.1499, z = -16132.5576, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FELDREUBER14"] = {x = 66044.7266, y = 2424.25708, z = -15887.4756, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FELDREUBER15"] = {x = 66331.2813, y = 2397.34326, z = -16378.1299, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FELDREUBER16"] = {x = 66226.7266, y = 2426.18091, z = -15722.4561, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FELDREUBER17"] = {x = 66601.7656, y = 2408.9104, z = -15610.3174, adjacent =
	[
	]},
	["FP_CAMPFIRE_SPIELPLATZ_01"] = {x = 65050.7227, y = 3864.3064, z = -23753.4473, adjacent =
	[
	]},
	["FP_CAMPFIRE_SPIELPLATZ_02"] = {x = 64736.6367, y = 3895.05151, z = -23696.0859, adjacent =
	[
	]},
	["FP_CAMPFIRE_SPIELPLATZ_03"] = {x = 64897.9258, y = 3894.16553, z = -23864.6914, adjacent =
	[
	]},
	["FP_CAMPFIRE_SPIELPLATZ_04"] = {x = 64862.7148, y = 3868.56079, z = -23540.0078, adjacent =
	[
	]},
	["FP_SMALLTALK_SPIELPLATZ_01"] = {x = 65230.4844, y = 3900.9021, z = -22261.0566, adjacent =
	[
	]},
	["FP_SMALLTALK_SPIELPLATZ_02"] = {x = 64985.2773, y = 3913.84644, z = -22244.6172, adjacent =
	[
	]},
	["FP_ROAM_WP_BIGFARM_TAVERNCAVE2_02_01"] = {x = 35147.5234, y = 4117.51172, z = -12333.3555, adjacent =
	[
	]},
	["FP_ROAM_WP_BIGFARM_TAVERNCAVE2_02_02"] = {x = 34842.1133, y = 4115.62549, z = -12312.501, adjacent =
	[
	]},
	["FP_ROAM_WP_BIGFARM_TAVERNCAVE2_02_03"] = {x = 34944.9961, y = 4110.62549, z = -12472.4287, adjacent =
	[
	]},
	["FP_ROAM_WP_BIGFARM_TAVERNCAVE2_01_01"] = {x = 34727.1172, y = 4114.62549, z = -12544.0303, adjacent =
	[
	]},
	["FP_ROAM_WP_BIGFARM_TAVERNCAVE2_01_02"] = {x = 34885.7773, y = 4102.62549, z = -12650.6621, adjacent =
	[
	]},
	["FP_STAND_SPIELPLATZ_01"] = {x = 65173.6758, y = 3902.35547, z = -22155.2051, adjacent =
	[
	]},
	["FP_SMALLTALK_CASTLEMINE_03"] = {x = 67688.1172, y = 3979.88428, z = -22657.832, adjacent =
	[
	]},
	["FP_SMALLTALK_CASTLEMINE_04"] = {x = 67568.3516, y = 3990.36523, z = -22553.7793, adjacent =
	[
	]},
	["FP_SIT_CAMPFIRE_TOWER_01"] = {x = 68139.3125, y = 3977.88428, z = -22090.9941, adjacent =
	[
	]},
	["FP_CAMPFIRE_HODGES_01"] = {x = 72210.6016, y = 3162.68457, z = -10192.125, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_BIGWOOD_03_C_01"] = {x = 52149.3555, y = 2474.74756, z = -29372.623, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_BIGWOOD_03_C_02"] = {x = 51980.3086, y = 2474.74756, z = -29225.9238, adjacent =
	[
	]},
	["FP_ROAM_NW_FARM3_BIGWOOD_03_C_03"] = {x = 51844.4023, y = 2444.74756, z = -29464.7832, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FIELD_MONSTER_02_01"] = {x = 59254.2148, y = 2010.51904, z = -16244.8027, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FIELD_MONSTER_02_02"] = {x = 58838.5508, y = 2026.5542, z = -15984.3506, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FIELD_MONSTER_02_003"] = {x = 59428.0273, y = 2048.81641, z = -15648.7813, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FIELD_MONSTER_02_04"] = {x = 59046.1211, y = 2053.55151, z = -15527.2314, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FIELD_MONSTER_01_01"] = {x = 60476.7305, y = 1967.45776, z = -17233.0215, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FIELD_MONSTER_01_02"] = {x = 60314.3711, y = 1984.07397, z = -17671.2109, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FIELD_MONSTER_01_03"] = {x = 60076.1797, y = 2002.55688, z = -17260.9277, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FIELD_MONSTER_04_01"] = {x = 62529.1055, y = 2162.75854, z = -17111.5371, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FIELD_MONSTER_04_02"] = {x = 62993.625, y = 2241.18945, z = -17082.1797, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FIELD_MONSTER_04_03"] = {x = 62946.4258, y = 2184.69653, z = -17754.1621, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FIELD_MONSTER_03_01"] = {x = 57314.6797, y = 1806.41821, z = -15855.1025, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FIELD_MONSTER_03_02"] = {x = 57025.957, y = 1881.20129, z = -15447.3916, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGMILL_FIELD_MONSTER_03_03"] = {x = 57603.2852, y = 1870.07751, z = -15537.9189, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FIELD_MONSTER_01_01"] = {x = 65735.5625, y = 2456.67163, z = -6453.00635, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FIELD_MONSTER_01_02"] = {x = 65277.9609, y = 2443.67944, z = -6004.62939, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FIELD_MONSTER_01_03"] = {x = 65289.125, y = 2487.81372, z = -6626.73535, adjacent =
	[
	]},
	["FP_ROAM_NW_BIGFARM_FIELD_MONSTER_01_04"] = {x = 65633.0781, y = 2418.17578, z = -6150.66016, adjacent =
	[
	]},
	["FP_STAND_NW_BIGFARM_HOUSE_LEE_GUARDING"] = {x = 75906.1016, y = 3522.10059, z = -12866.5166, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_STABLE_OUT_03"] = {x = 72729.8359, y = 3185.24341, z = -12292.0557, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_BIGFARM_STABLE_OUT_031"] = {x = 72551.1719, y = 3172.88354, z = -12175.7148, adjacent =
	[
	]},
	["FP_EVENT_SPAWN_STONEGUARDIAN_ORNAMENT_BIGFARM_01"] = {x = 59529.4453, y = 2172.83594, z = -7200.30127, adjacent =
	[
	]},
	["FP_EVENT_STONEGUARDIAN_ORNAMENT_EFFECT_BIGFARM_01"] = {x = 59538.8047, y = 2150.83594, z = -7203.81494, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM4_WOOD_RANGERBANDITS_01"] = {x = 49244.1836, y = 1226.11719, z = 6054.32031, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_FARM4_WOOD_RANGERBANDITS_02"] = {x = 49451.2227, y = 1226.11719, z = 5784.67188, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_BIGMILL_FARM3_RANGERBANDITS_01"] = {x = 58435.9961, y = 1907.43945, z = -29025.6387, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_BIGMILL_FARM3_RANGERBANDITS_02"] = {x = 58232.3516, y = 1897.43945, z = -28726.3047, adjacent =
	[
	]},
	["FP_CAMPFIRE_NW_BIGMILL_FARM3_RANGERBANDITS_ELVRICH"] = {x = 57714.9063, y = 1955.52271, z = -28590.6543, adjacent =
	[
	]},
	["FP_ITEM_NW_FARM4_WOOD_LUCIASLETTER"] = {x = 50016.3594, y = 982.825745, z = 6948.44141, adjacent =
	[
	]},
	["FP_ITEM_GREATPEASANT_STPLATE_01"] = {x = 59415.5859, y = 2088.47119, z = -6854.45459, adjacent =
	[
	]},
	["FP_ITEM_GREATPEASANT_STPLATE_02"] = {x = 59305.082, y = 2106.47119, z = -7866.13184, adjacent =
	[
	]},
	["FP_ITEM_GREATPEASANT_STPLATE_03"] = {x = 75302.6016, y = 3310.17041, z = -1805.65491, adjacent =
	[
	]},
	["FP_ITEM_GREATPEASANT_STPLATE_04"] = {x = 64696.1523, y = 2408.91333, z = -17359.2246, adjacent =
	[
	]},
	["FP_ITEM_GREATPEASANT_STPLATE_05"] = {x = 61615.8945, y = 4274.73486, z = -35019.957, adjacent =
	[
	]},
	["FP_ITEM_GREATPEASANT_STPLATE_06"] = {x = 57358.3477, y = 2019.08008, z = -28604.4766, adjacent =
	[
	]},
	["FP_ITEM_GREATPEASANT_STPLATE_07"] = {x = 74251.1016, y = 3434.36694, z = -5837.60156, adjacent =
	[
	]},
	["FP_ITEM_GREATPEASANT_STPLATE_08"] = {x = 48185.293, y = 1422.14331, z = -13394.9365, adjacent =
	[
	]},
	["FP_ITEM_NW_FARM4_WOOD_FERNANDOLETTER"] = {x = 48843.9727, y = 1310.82129, z = 6480.19482, adjacent =
	[
	]},
	["FP_STAND_GUARDING_WINE_01"] = {x = 47768.2305, y = 5025.77539, z = 17389.8672, adjacent =
	[
	]},
	["FP_ITEM_KDFPLAYER"] = {x = 50948.2109, y = 5043.86035, z = 18181.6719, adjacent =
	[
	]},
	["FP_PRAY_MONASTERY_03"] = {x = 50188.4023, y = 5006.52246, z = 20526.3418, adjacent =
	[
	]},
	["FP_PRAY_ISGAROTH"] = {x = 38314.3164, y = 4373.22559, z = 9736.42578, adjacent =
	[
	]},
	["FP_SWEEP_NOV_01"] = {x = 46632.7031, y = 5003.99316, z = 20962.418, adjacent =
	[
	]},
	["FP_SWEEP_NOV_02"] = {x = 47799.0117, y = 5010.99316, z = 21522.2012, adjacent =
	[
	]},
	["FP_SWEEP_NOV_03"] = {x = 48935.2031, y = 5000.99316, z = 17188.2949, adjacent =
	[
	]},
	["FP_SWEEP_NOV_04"] = {x = 50132.4492, y = 5002.15967, z = 17939.1914, adjacent =
	[
	]},
	["FP_STAND_GUARDING_SERGIO"] = {x = 46854.6875, y = 5001.91357, z = 21325.8809, adjacent =
	[
	]},
	["FP_NW_ITEM_LIBRARY_SEAMAP2"] = {x = 51176.6289, y = 3577.05029, z = 19558.127, adjacent =
	[
	]},
	["FP_NW_ITEM_LIBRARY_SEAMAP"] = {x = 51073.0039, y = 3574.6748, z = 19517.6641, adjacent =
	[
	]},
	["FP_NW_ITEM_LIBRARY_IRDORATHBOOK"] = {x = 49596.832, y = 4339.08984, z = 21350.4121, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_30"] = {x = 48336.3906, y = 3531.04028, z = 17303.5195, adjacent =
	[
	]},
	["FP_HAMMER"] = {x = 46018.0859, y = 4337.26221, z = 19570.6563, adjacent =
	[
	]},
	["FP_STAND_MARDUK"] = {x = 47565.3086, y = 5002.27979, z = 20846.2676, adjacent =
	[
	]},
	["FP_PICK_HERB_04"] = {x = 48151.8633, y = 4941.25439, z = 18674.1133, adjacent =
	[
	]},
	["FP_PICK_HERB_03"] = {x = 48288.0156, y = 4936.47461, z = 18404.4043, adjacent =
	[
	]},
	["FP_PICK_HERB_02"] = {x = 48124.6953, y = 4927.90234, z = 18243.2656, adjacent =
	[
	]},
	["FP_PICK_HERB_01"] = {x = 47978.9414, y = 4893.94678, z = 18495.0195, adjacent =
	[
	]},
	["FP_ROAM_MONASTERY_05"] = {x = 47454.2305, y = 4899.07715, z = 19918.3887, adjacent =
	[
	]},
	["FP_ROAM_MONASTERY_04"] = {x = 47375.6406, y = 4896.31689, z = 20028.3242, adjacent =
	[
	]},
	["FP_ROAM_MONASTERY_03"] = {x = 47227.9141, y = 4926.85254, z = 19930.0723, adjacent =
	[
	]},
	["FP_ROAM_MONASTERY_02"] = {x = 47552.4492, y = 4912.9873, z = 19784.8633, adjacent =
	[
	]},
	["FP_ROAM_MONASTERY_01"] = {x = 47410.75, y = 4914.34766, z = 19669.5723, adjacent =
	[
	]},
	["FP_SMALLTALK_MONASTERY_01"] = {x = 48378.9609, y = 4899.06982, z = 20095.9766, adjacent =
	[
	]},
	["FP_SMALLTALK_MONASTERY_02"] = {x = 48466.375, y = 4917.91064, z = 20015.1055, adjacent =
	[
	]},
	["FP_SWEEP_MONASTERY_01"] = {x = 49786.8945, y = 5027.81299, z = 18485.1699, adjacent =
	[
	]},
	["FP_PRAY_MONASTERY_01"] = {x = 50442.4297, y = 5016.31201, z = 20245.4121, adjacent =
	[
	]},
	["FP_PRAY_MONASTERY_02"] = {x = 50138.625, y = 5023.31201, z = 20883.5879, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_02"] = {x = 49923.6875, y = 3472.91113, z = 22689.3574, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_01"] = {x = 50141.75, y = 3484.91113, z = 22680.5879, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_03"] = {x = 49965.8359, y = 3484.91113, z = 21601.584, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_24"] = {x = 49497.1797, y = 3506.19214, z = 22701.9238, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_04"] = {x = 49194.0781, y = 3476.91113, z = 23256.3281, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_06"] = {x = 49402.1875, y = 3501.92773, z = 20875.3027, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_07"] = {x = 49586.7969, y = 3496.5979, z = 21006.584, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_08"] = {x = 49893.6211, y = 3494.14526, z = 21096.8633, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_10"] = {x = 48165.9141, y = 3484.3584, z = 22127.627, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_11"] = {x = 46987.8516, y = 3486.03491, z = 22526.0879, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_12"] = {x = 46970.293, y = 3493.61401, z = 22296.6934, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_15"] = {x = 47112.4688, y = 3506.84351, z = 20063.2227, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_16"] = {x = 47215.4492, y = 3514.96655, z = 19655.5742, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_14"] = {x = 49417.8984, y = 3484.21851, z = 21752.6152, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_17"] = {x = 48233.3555, y = 3495.55273, z = 19734.2227, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_18"] = {x = 49370.5586, y = 3527.57959, z = 18001.1016, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_19"] = {x = 49564.3281, y = 3526.59497, z = 17927.5527, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_20"] = {x = 49922.1445, y = 3513.53101, z = 18637.6719, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_21"] = {x = 50902.7109, y = 3507.07495, z = 18397.3418, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_22"] = {x = 51565.9336, y = 3537.62476, z = 18691.1758, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_23"] = {x = 51261.7813, y = 3522.11426, z = 19339.5781, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_25"] = {x = 45687.8477, y = 3487.13892, z = 20964.4141, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_26"] = {x = 46062.5195, y = 3517.13892, z = 20246.793, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_27"] = {x = 45884.6992, y = 3501.13892, z = 20796.7891, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_28"] = {x = 47357.6445, y = 3497.7356, z = 19986.1035, adjacent =
	[
	]},
	["FP_ROAM_NW_KDF_LIBRARY_29"] = {x = 47477.6992, y = 3507.7356, z = 19389.6699, adjacent =
	[
	]},
	["FP_ITEM_MONASTERY_01"] = {x = 38304.5703, y = 4365.9751, z = 10112.5967, adjacent =
	[
	]},
	["FP_ITEM_MONASTERY_02"] = {x = 38850.3711, y = 3202.0105, z = 2211.42139, adjacent =
	[
	]},
	["FP_ITEM_MONASTERY_03"] = {x = 46758.4414, y = 4895.29395, z = 19130.1289, adjacent =
	[
	]},
	["FP_ITEM_MONASTERY_04"] = {x = 50094.0039, y = 4995.68994, z = 19756.9082, adjacent =
	[
	]},
	["FP_ITEM_PALFINALARMOR"] = {x = 47425.2656, y = 3582.38184, z = 17093.6992, adjacent =
	[
	]},
	["FP_ITEM_PALFINALWEAPON"] = {x = 47466.8633, y = 3584.38184, z = 17176.2363, adjacent =
	[
	]},
	["FP_ITEM_PALFINALSTUFF"] = {x = 47572.207, y = 3582.45361, z = 17168.6895, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_11_01"] = {x = 38152.1523, y = 4315.63672, z = 12477.1416, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_11_02"] = {x = 38314.4688, y = 4312.79932, z = 12098.7148, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_11_03"] = {x = 38485.9141, y = 4321.26172, z = 12400.7236, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_11_04"] = {x = 37909.2578, y = 4316.56445, z = 12307.4688, adjacent =
	[
	]},
	["FP_ROAM_NW_NW_PATH_TO_MONASTER_MONSTER22_01"] = {x = 37465.3516, y = 4324.68994, z = 12934.874, adjacent =
	[
	]},
	["FP_ROAM_NW_NW_PATH_TO_MONASTER_MONSTER22_02"] = {x = 37826.3477, y = 4316.1333, z = 12943.9561, adjacent =
	[
	]},
	["FP_ROAM_NW_NW_PATH_TO_MONASTER_MONSTER22_03"] = {x = 37701.7148, y = 4308.47461, z = 13139.4365, adjacent =
	[
	]},
	["FP_ROAM_NW_NW_PATH_TO_MONASTER_MONSTER22_04"] = {x = 37487.8477, y = 4324.17188, z = 12581.2588, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_01_01"] = {x = 39697.8203, y = 3487.08691, z = 4802.41797, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_01_02"] = {x = 39596.9688, y = 3498.87256, z = 4548.71777, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_01_03"] = {x = 39376.582, y = 3516.67871, z = 4677.84473, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_01_03"] = {x = 39422.2734, y = 3500.56885, z = 4919.12256, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_02_01"] = {x = 40288.2734, y = 3642.84961, z = 5901.31689, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_02_01"] = {x = 40057.1367, y = 3662.04028, z = 5934.13965, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_10_01"] = {x = 41400.082, y = 3988.92627, z = 7700.07031, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_10_02"] = {x = 41173.3555, y = 3947.38159, z = 7887.42432, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_10_03"] = {x = 40889.6523, y = 3931.28687, z = 7772.35059, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_10_04"] = {x = 40952.5195, y = 3961.11426, z = 7963.55957, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_08_01"] = {x = 41142.0742, y = 4243.16455, z = 11716.8916, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_08_02"] = {x = 41393.957, y = 4196.5498, z = 11568.665, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_08_03"] = {x = 41020.6328, y = 4239.80029, z = 11439.3896, adjacent =
	[
	]},
	["FP_ROAM_NW_PATH_TO_MONASTER_AREA_08_04"] = {x = 41233.1719, y = 4200.78857, z = 11313.124, adjacent =
	[
	]},
	["FP_ROAM_NW_SHRINE_MONSTER_01"] = {x = 37377.9766, y = 3332.72974, z = 2427.74365, adjacent =
	[
	]},
	["FP_ROAM_NW_SHRINE_MONSTER_02"] = {x = 37121.2695, y = 3285.1167, z = 2370.5083, adjacent =
	[
	]},
	["FP_ROAM_NW_SHRINE_MONSTER_03"] = {x = 37586.9453, y = 3298.81274, z = 2482.3252, adjacent =
	[
	]},
	["FP_ROAM_NW_SHRINE_MONSTER_04"] = {x = 37442.3945, y = 3331.09131, z = 2141.45801, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_CONNECT_MONSTER_01"] = {x = 39678.9922, y = 3402.33154, z = 923.646057, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_CONNECT_MONSTER_02"] = {x = 39393.5703, y = 3317.75244, z = 1325.99573, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_CONNECT_MONSTER_03"] = {x = 39390.3359, y = 3433.33521, z = 813.229553, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_CONNECT_MONSTER_04"] = {x = 39462.4102, y = 3390.10913, z = 1024.64172, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_CONNECT_MONSTER2_01"] = {x = 40801.5313, y = 3363.05225, z = 856.262756, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_CONNECT_MONSTER2_02"] = {x = 40593.6719, y = 3357.41382, z = 1100.84497, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_CONNECT_MONSTER2_03"] = {x = 40323.6328, y = 3389.36353, z = 906.103943, adjacent =
	[
	]},
	["FP_ROAM_NW_FOREST_CONNECT_MONSTER2_04"] = {x = 40527.332, y = 3401.98755, z = 622.477966, adjacent =
	[
	]},
	["FP_ITEM_KLOSTER_01"] = {x = 50741.3594, y = 4298.52148, z = 20154.4004, adjacent =
	[
	]},
	["FP_ITEM_KLOSTER_02"] = {x = 49739, y = 4292.55029, z = 17895.8887, adjacent =
	[
	]},
	["FP_STAND_JORGEN"] = {x = 41760.7109, y = 4231.32275, z = 15026.3984, adjacent =
	[
	]},
	["FP_STAND_PARLAN"] = {x = 48451.0586, y = 4900.33496, z = 19430.3613, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_25"] = {x = 33536, y = 4155, z = -37314, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_24"] = {x = 33342, y = 4393, z = -39059, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_23"] = {x = 33368, y = 4313, z = -38458, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_22"] = {x = 33396, y = 4216, z = -37889, adjacent =
	[
	]},
	["FP_CAMPFIRE_SHAMAN_05"] = {x = 31776, y = 4347, z = -38689, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_21"] = {x = 30286, y = 4854, z = -43759, adjacent =
	[
	]},
	["FP_CAMPFIRE_SHAMAN_04"] = {x = 31413, y = 4771, z = -42410, adjacent =
	[
	]},
	["FP_ROAM_PASS_WOLF_16"] = {x = 34725, y = 3867, z = -34662, adjacent =
	[
	]},
	["FP_ROAM_PASS_WOLF_15"] = {x = 34217, y = 3907, z = -34638, adjacent =
	[
	]},
	["FP_ROAM_PASS_WOLF_14"] = {x = 34709, y = 3847, z = -35125, adjacent =
	[
	]},
	["FP_ROAM_PASS_WOLF_13"] = {x = 33929, y = 3887, z = -35003, adjacent =
	[
	]},
	["FP_CAMPFIRE_SHAMAN_01"] = {x = 31425.0918, y = 4612.49219, z = -41299.7109, adjacent =
	[
	]},
	["FP_CAMPFIRE_SHAMAN_02"] = {x = 31816.541, y = 4632.49219, z = -41384.1523, adjacent =
	[
	]},
	["FP_CAMPFIRE_SHAMAN_03"] = {x = 31527.1016, y = 4573.86475, z = -41034.7734, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_01"] = {x = 30869.002, y = 4689, z = -41496, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_02"] = {x = 30912.5703, y = 4700.20313, z = -41895.3477, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_03"] = {x = 31119.7852, y = 4609.20313, z = -41101.1758, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_04"] = {x = 32523.3828, y = 4522.17578, z = -40675.6406, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_05"] = {x = 32948.1914, y = 4495.84717, z = -40128.0117, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_07"] = {x = 31849.6914, y = 4485.84717, z = -40242.8203, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_08"] = {x = 32090.5801, y = 4470.84717, z = -39881.8789, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_09"] = {x = 31657.4492, y = 4546.15967, z = -40591.4258, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_10"] = {x = 31569.3789, y = 4415.84717, z = -39241.1836, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_11"] = {x = 32200.6836, y = 4345.18701, z = -39254.3047, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_12"] = {x = 31240.7344, y = 4505.18701, z = -39816.3555, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_13"] = {x = 31003, y = 4865, z = -43745, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_14"] = {x = 30462.6055, y = 4878.15088, z = -43219.0977, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_15"] = {x = 31045.2871, y = 4762.59424, z = -42760.4883, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_16"] = {x = 30772.8477, y = 4752.59424, z = -42248.2773, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_17"] = {x = 30411.8379, y = 4815.56299, z = -42646.0195, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_18"] = {x = 31195.4375, y = 4538.66113, z = -40437.6055, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_19"] = {x = 30811.2324, y = 4578.66113, z = -40464.2188, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_20"] = {x = 31932.7012, y = 4578.66113, z = -41068.9531, adjacent =
	[
	]},
	["FP_STAND_GUARDING_PASS_01"] = {x = 36747.793, y = 3191, z = -26243.4355, adjacent =
	[
	]},
	["FP_STAND_GUARDING_PASS_02"] = {x = 36963.9883, y = 3177, z = -26384.0273, adjacent =
	[
	]},
	["FP_ROAM_WOLF_PASS_01"] = {x = 29429.002, y = 5416.00049, z = -35269, adjacent =
	[
	]},
	["FP_ROAM_WOLF_PASS_02"] = {x = 29877, y = 5436.00049, z = -35526, adjacent =
	[
	]},
	["FP_ROAM_WOLF_PASS_03"] = {x = 29741, y = 5416.00049, z = -35005, adjacent =
	[
	]},
	["FP_ROAM_WOLF_PASS_04"] = {x = 30043, y = 5456.00049, z = -35234, adjacent =
	[
	]},
	["FP_ROAM_WOLF_PASS_05"] = {x = 29725, y = 5416.00049, z = -35875, adjacent =
	[
	]},
	["FP_ROAM_WOLF_PASS_06"] = {x = 29240, y = 5416.00049, z = -35590, adjacent =
	[
	]},
	["FP_ROAM_WOLF_PASS_07"] = {x = 30216, y = 5399, z = -34367, adjacent =
	[
	]},
	["FP_ROAM_WOLF_PASS_08"] = {x = 30679.002, y = 5419, z = -34617, adjacent =
	[
	]},
	["FP_ROAM_PASS_GOBBO_01"] = {x = 37237, y = 4577, z = -37030, adjacent =
	[
	]},
	["FP_ROAM_PASS_GOBBO_02"] = {x = 37297, y = 4557, z = -37509, adjacent =
	[
	]},
	["FP_ROAM_PASS_GOBBO_03"] = {x = 36678, y = 4597, z = -37200, adjacent =
	[
	]},
	["FP_ROAM_PASS_GOBBO_04"] = {x = 36948, y = 4557, z = -36924, adjacent =
	[
	]},
	["FP_ROAM_PASS_GOBBO_05"] = {x = 36921, y = 4609, z = -37379, adjacent =
	[
	]},
	["FP_ROAM_PASS_RAT_01"] = {x = 36422, y = 4510, z = -33146, adjacent =
	[
	]},
	["FP_ROAM_PASS_RAT_02"] = {x = 36627, y = 4309, z = -32454, adjacent =
	[
	]},
	["FP_ROAM_PASS_RAT_03"] = {x = 36659, y = 3951, z = -31416, adjacent =
	[
	]},
	["FP_ROAM_PASS_RAT_04"] = {x = 36306, y = 3834, z = -30691, adjacent =
	[
	]},
	["FP_ROAM_PASS_WOLF_09"] = {x = 32340, y = 3456, z = -30118, adjacent =
	[
	]},
	["FP_ROAM_PASS_WOLF_10"] = {x = 33172, y = 3536, z = -30199.002, adjacent =
	[
	]},
	["FP_ROAM_PASS_WOLF_11"] = {x = 32627, y = 3376, z = -29744.002, adjacent =
	[
	]},
	["FP_ROAM_PASS_WOLF_12"] = {x = 33048, y = 3422, z = -29753, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_26"] = {x = 30825, y = 4707, z = -41612, adjacent =
	[
	]},
	["FP_ROAM_PASS_ORK_06"] = {x = 31026, y = 4667, z = -41642, adjacent =
	[
	]},
	["START_NW_ABANDONEDMINE_OW"] = {x = 28962.5195, y = 5409.06836, z = -35619.6719, adjacent =
	[
	]},
	["START_NW_ORETRAIL_OW"] = {x = 30602.0625, y = 4845.15723, z = -44545.1523, adjacent =
	[
	]},
	["FP_ITEM_PASS_01"] = {x = 36753.9609, y = 3243.35718, z = -25759.1563, adjacent =
	[
	]},
	["FP_STAND_SHIP_06"] = {x = -10061.3008, y = 407.926666, z = -17072.1172, adjacent =
	[
	]},
	["FP_STAND_SHIP_05"] = {x = -10222.8438, y = 414.556061, z = -16815.7695, adjacent =
	[
	]},
	["FP_SMALLTALK_SHIP_06"] = {x = -9731.33984, y = 434.575623, z = -16815.7441, adjacent =
	[
	]},
	["FP_SMALLTALK_SHIP_05"] = {x = -9617.85156, y = 434.506104, z = -16695.8223, adjacent =
	[
	]},
	["FP_STAND_SHIP_04"] = {x = -10238.5586, y = -86.1701889, z = -14780.9648, adjacent =
	[
	]},
	["FP_STAND_SHIP_03"] = {x = -9701.30469, y = -78.2851334, z = -13024.6201, adjacent =
	[
	]},
	["FP_SMALLTALK_SHIP_03"] = {x = -9620.99121, y = -83.669487, z = -13206.8398, adjacent =
	[
	]},
	["FP_SMALLTALK_SHIP_04"] = {x = -9798.50977, y = -77.4683914, z = -13266.2432, adjacent =
	[
	]},
	["FP_STAND_SHIP_01"] = {x = -9356.21387, y = 417.65564, z = -15979.9521, adjacent =
	[
	]},
	["FP_STAND_SHIP_02"] = {x = -9640.89063, y = 408.043701, z = -16082.8027, adjacent =
	[
	]},
	["FP_SMALLTALK_SHIP_02"] = {x = -9544.21094, y = 470.468018, z = -12512.8037, adjacent =
	[
	]},
	["FP_SMALLTALK_SHIP_01"] = {x = -9748.43652, y = 469.234436, z = -12446.9453, adjacent =
	[
	]},
	["FP_STAND_SHIP_07"] = {x = -9403.44336, y = -99.1801605, z = -15547.2393, adjacent =
	[
	]},
	["FP_STAND_SHIP_08"] = {x = -9234.1084, y = -96.1801605, z = -15098.7207, adjacent =
	[
	]},
	["FP_ITEM_SHIP_01"] = {x = -9465.70605, y = -580.659119, z = -13396.2344, adjacent =
	[
	]},
	["FP_ITEM_SHIP_02"] = {x = -9553.84961, y = -583.355164, z = -13499.5654, adjacent =
	[
	]},
	["FP_ITEM_SHIP_03"] = {x = -10629.1357, y = -581.659119, z = -13413.6836, adjacent =
	[
	]},
	["FP_ITEM_SHIP_05"] = {x = -10581.3926, y = -582.249023, z = -14383.7842, adjacent =
	[
	]},
	["FP_ITEM_SHIP_06"] = {x = -10627.3984, y = -582.249023, z = -16460.1797, adjacent =
	[
	]},
	["FP_ITEM_SHIP_04"] = {x = -10534.3076, y = -582.249023, z = -14418.1455, adjacent =
	[
	]},
	["FP_ITEM_SHIP_07"] = {x = -9353.82324, y = -578.915894, z = -14400.7676, adjacent =
	[
	]},
	["FP_ITEM_SHIP_08"] = {x = -9344.4834, y = -584.915894, z = -15033.1279, adjacent =
	[
	]},
	["FP_ITEM_SHIP_09"] = {x = -10014.5693, y = -99.6358643, z = -15591.9775, adjacent =
	[
	]},
	["FP_ITEM_SHIP_10"] = {x = -10765.2246, y = -93.0976105, z = -15562.4092, adjacent =
	[
	]},
	["FP_ITEM_SHIP_11"] = {x = -9749.79688, y = -32.4303741, z = -12033.0938, adjacent =
	[
	]},
	["FP_ITEM_SHIP_12"] = {x = -9403, y = -17, z = -17490, adjacent =
	[
	]},
	["FP_ITEM_SHIP_13"] = {x = -10436.6455, y = 574.26825, z = -18940.209, adjacent =
	[
	]},
	["FP_ITEM_SHIP_14"] = {x = -10152.0684, y = -97.6106186, z = -12825.1494, adjacent =
	[
	]},
	["FP_ITEM_SHIP_15"] = {x = -10361.1895, y = 1049.12439, z = -19341.6992, adjacent =
	[
	]},
	["FP_ITEM_SHIP_16"] = {x = -9437.38965, y = 62.4504852, z = -11830.6973, adjacent =
	[
	]},
	["FP_ITEM_TROLLAREA_STPLATE_14"] = {x = 36371.5469, y = 6269.21973, z = 29126.0703, adjacent =
	[
	]},
	["FP_ITEM_TROLLAREA_STPLATE_13"] = {x = 49484.1172, y = 7917.74609, z = 39454.668, adjacent =
	[
	]},
	["FP_ITEM_TROLLAREA_STPLATE_12"] = {x = 61477.8281, y = 7087.63477, z = 23227.8262, adjacent =
	[
	]},
	["FP_ITEM_TROLLAREA_STPLATE_11"] = {x = 73280.4766, y = 4737.98145, z = 19945.9727, adjacent =
	[
	]},
	["FP_ITEM_TROLLAREA_STPLATE_10"] = {x = 79520.3828, y = 5087.73438, z = 22557.2168, adjacent =
	[
	]},
	["FP_ITEM_TROLLAREA_STPLATE_09"] = {x = 80900.0547, y = 5278.55664, z = 33234.3281, adjacent =
	[
	]},
	["FP_ITEM_TROLLAREA_STPLATE_08"] = {x = 74402.2188, y = 6700.37793, z = 29248.2109, adjacent =
	[
	]},
	["FP_ITEM_TROLLAREA_STPLATE_07"] = {x = 73246.5078, y = 5488.25439, z = 24370.0234, adjacent =
	[
	]},
	["FP_ITEM_TROLLAREA_STPLATE_06"] = {x = 87895.4219, y = 3989.05469, z = 18110.3242, adjacent =
	[
	]},
	["FP_ITEM_TROLLAREA_STPLATE_05"] = {x = 83618.4688, y = 3495.3147, z = 29322.832, adjacent =
	[
	]},
	["FP_ITEM_TROLLAREA_STPLATE_04"] = {x = 82990.4531, y = 3567.05078, z = 21147.543, adjacent =
	[
	]},
	["FP_ITEM_TROLLAREA_STPLATE_03"] = {x = 81941.5156, y = 3562.2666, z = 21592.2344, adjacent =
	[
	]},
	["FP_ITEM_TROLLAREA_STPLATE_02"] = {x = 85537.6406, y = 3750.96582, z = 24539.4375, adjacent =
	[
	]},
	["FP_ITEM_TROLLAREA_STPLATE_01"] = {x = 85574.6016, y = 3787.44727, z = 19216.7676, adjacent =
	[
	]},
	["FP_ITEM_TROLLAREA_PORTALRITUAL_01"] = {x = 87196.9375, y = 4066.3291, z = 18118.1074, adjacent =
	[
	]},
	["EXIT_ADDON"] = {x = 82641.75, y = 3694.21191, z = 29223.4375, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_TROLLAREA_PORTALTEMPEL_02"] = {x = 83039.2578, y = 3510.17871, z = 27101.9375, adjacent =
	[
	]},
	["FP_SMALLTALK_NW_TROLLAREA_PORTALTEMPEL_01"] = {x = 83204.5156, y = 3510.17871, z = 26918.5156, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_32"] = {x = 82487.8047, y = 3484.11475, z = 22319.5273, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_31"] = {x = 82276.7188, y = 3484.11475, z = 22222.877, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_30"] = {x = 82016.5469, y = 3554.11475, z = 22230.3789, adjacent =
	[
	]},
	["FP_ITEM_NW_TROLLAREA_PORTALTEMPEL_01"] = {x = 82465.4219, y = 3845.24902, z = 17556.0742, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_DEADALLIGATOR"] = {x = 82541.5625, y = 3270.87378, z = 24935.4844, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_28"] = {x = 76806.0625, y = 4070.25586, z = 26997.0547, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_27"] = {x = 77507.4453, y = 4070.25586, z = 25875.5625, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_26"] = {x = 77210.1797, y = 4097.69238, z = 25939.373, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_25"] = {x = 82823.5391, y = 3660.28198, z = 20090.6309, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_24"] = {x = 83075.6406, y = 3680.27905, z = 20255.8555, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_23"] = {x = 83004.125, y = 3670.28052, z = 20523.0352, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_21"] = {x = 81898.4922, y = 3684.98047, z = 19013.4258, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_20"] = {x = 82209.2031, y = 3675.00854, z = 19106.0801, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_19"] = {x = 82011.9297, y = 3671.11084, z = 18769.5371, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_18"] = {x = 82739.9219, y = 3674.6748, z = 18955.9082, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_17"] = {x = 82845.6719, y = 3684.64673, z = 18568.5723, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_16"] = {x = 83080.4375, y = 3673.59326, z = 18854.5957, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_15"] = {x = 80704.3203, y = 3685.67334, z = 20460.7363, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_14"] = {x = 81051.0703, y = 3675.75879, z = 20374.8926, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_13"] = {x = 80751.5391, y = 3747.40088, z = 20671.0801, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_12"] = {x = 79055.4375, y = 3870.00586, z = 20177.5117, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_10"] = {x = 79374.9609, y = 3879.44385, z = 20018.9004, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_09"] = {x = 79334.9141, y = 3885.48877, z = 20329.3184, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_08"] = {x = 77995.2031, y = 3878.95801, z = 20103.8516, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_06"] = {x = 78299.3594, y = 3884.13965, z = 20267.2168, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_07"] = {x = 77711.7344, y = 3863.14331, z = 20153.0215, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_05"] = {x = 77870.0625, y = 3956.72192, z = 19744.0859, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_04"] = {x = 77935.3906, y = 3863.47534, z = 21800.0254, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_03"] = {x = 78086.5547, y = 3886.83911, z = 22194.3027, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_02"] = {x = 77709.7266, y = 3895.64575, z = 22233.7422, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PORTALTEMPEL_01"] = {x = 77692.5938, y = 3878.29272, z = 22003.6309, adjacent =
	[
	]},
	["FP_STAND"] = {x = 52019.8711, y = 7686.00684, z = 32451.3359, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RIVERSIDECAVE_07_03"] = {x = 66342.3594, y = 3243.21802, z = 13192.1777, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RIVERSIDECAVE_07_02"] = {x = 66625.7188, y = 3238.44604, z = 13138.6533, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RIVERSIDECAVE_07_01"] = {x = 66551.1484, y = 3242.39941, z = 13404.1279, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RIVERSIDECAVE_01_03"] = {x = 67214.6719, y = 3232.87891, z = 14765.168, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RIVERSIDECAVE_01_02"] = {x = 67548.3906, y = 3240.78271, z = 14737.2646, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RIVERSIDECAVE_01_01"] = {x = 67430.1953, y = 3233.31763, z = 15049.8701, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_09_01"] = {x = 60626.4297, y = 7089.44678, z = 24776.9414, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_08_03"] = {x = 61259.1289, y = 7088.42334, z = 23483.5645, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_08_02"] = {x = 61197.3242, y = 7090.85059, z = 23393.6719, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_08_01"] = {x = 61238.3906, y = 7091.79736, z = 23235.8828, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_02_03"] = {x = 57652.2539, y = 6943.5708, z = 26326.2832, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_02_02"] = {x = 57839.2539, y = 6950.3667, z = 26412.0195, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_02_01"] = {x = 57660.0703, y = 6945.56738, z = 26598.7539, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_03A_06"] = {x = 56914.4414, y = 6943.90674, z = 25611.8418, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_03A_05"] = {x = 57249.3516, y = 6942.14111, z = 25491.582, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_03A_04"] = {x = 57070.668, y = 6956.26611, z = 25344.498, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_03A_03"] = {x = 56766.3594, y = 6950.96924, z = 25393.6855, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_03A_02"] = {x = 56947.7422, y = 6943.90674, z = 25474.5293, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_03A_01"] = {x = 57082.1602, y = 6954.2417, z = 25680.9355, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_07_01"] = {x = 46762, y = 7780, z = 35705, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_05_01"] = {x = 47344.5508, y = 7798.53613, z = 34752.0391, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_10_01"] = {x = 46496.3281, y = 7771.59961, z = 35320.4531, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_03_02"] = {x = 46424.4883, y = 7791.65283, z = 33996.457, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_TROLLLAKECAVE_03_01"] = {x = 46331.8555, y = 7796.07422, z = 34239.918, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALFOREST_06_MONSTER_02"] = {x = 46367.5, y = 7798.646, z = 27913.8262, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALFOREST_06_MONSTER_01"] = {x = 46455.7852, y = 7804.90527, z = 27623.8711, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_22_MONSTER_04"] = {x = 68979.3516, y = 6641.9834, z = 34834.6719, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_22_MONSTER_03"] = {x = 69221.6484, y = 6604.8125, z = 34505.2188, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_22_MONSTER_02"] = {x = 68818.4141, y = 6611.65723, z = 34514.4102, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_22_MONSTER_01"] = {x = 69339.8906, y = 6623.39502, z = 34711.0898, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_01_04"] = {x = 64174.3086, y = 6992.08789, z = 36395.9961, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_01_03"] = {x = 63930.7031, y = 6936.26611, z = 36816.4336, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_01_02"] = {x = 64311.8516, y = 6993.71191, z = 36789.9336, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_01_01"] = {x = 63840.457, y = 6977.31445, z = 36285.9453, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALPATH_02_06"] = {x = 37782.7227, y = 6814.86328, z = 30371.4102, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALPATH_02_05"] = {x = 37765.9297, y = 6793.7124, z = 30115.3848, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALPATH_02_04"] = {x = 37125.9766, y = 6769.91016, z = 30285.4434, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALPATH_02_03"] = {x = 37521.0547, y = 6824.60205, z = 30368.8945, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALPATH_02_02"] = {x = 37392.3047, y = 6779.42236, z = 29995.5352, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALPATH_02_01"] = {x = 36987.3789, y = 6742.89258, z = 29970.7148, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUAL_08_04"] = {x = 36004.9922, y = 6873.21631, z = 34538.2969, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUAL_08_03"] = {x = 35441.9258, y = 6844.37354, z = 34715.2969, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUAL_08_02"] = {x = 35524.957, y = 6840.53369, z = 34399.5469, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUAL_08_01"] = {x = 35856.1563, y = 6877.05176, z = 34730.957, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUAL_13_04"] = {x = 38890.875, y = 6844.09766, z = 31860.1113, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUAL_13_03"] = {x = 39096.3125, y = 6829.73828, z = 32322.0918, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUAL_13_02"] = {x = 39212.6719, y = 6848.0083, z = 31772.3848, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUAL_13_01"] = {x = 39437.1563, y = 6831.65381, z = 32016.9648, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALPATH_04_06"] = {x = 40291.1523, y = 6748.03174, z = 30546.75, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALPATH_04_05"] = {x = 40069.6797, y = 6779.74854, z = 30644.1777, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALPATH_04_04"] = {x = 39690.4297, y = 6805.70264, z = 30345.9492, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALPATH_04_03"] = {x = 40200.4688, y = 6707.65479, z = 30335.3535, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALPATH_04_02"] = {x = 39882.707, y = 6748.9873, z = 30169.4023, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALPATH_04_01"] = {x = 39876.25, y = 6794.09131, z = 30459.6309, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALFOREST_04_MONSTER_04"] = {x = 46966.7578, y = 7801.26318, z = 32403.4395, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALFOREST_04_MONSTER_03"] = {x = 46524.9102, y = 7796.42578, z = 32545.2109, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALFOREST_04_MONSTER_02"] = {x = 46960.4805, y = 7787.28906, z = 32155.7598, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RITUALFOREST_04_MONSTER_01"] = {x = 46449.1836, y = 7794.03076, z = 32290.9824, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_BRIGDE_01_04"] = {x = 51992.4063, y = 7902.47656, z = 35501.5664, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_BRIGDE_01_03"] = {x = 52244.457, y = 7910.47852, z = 35939.5234, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_BRIGDE_01_02"] = {x = 51992.7656, y = 7906.82324, z = 35786.1992, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_BRIGDE_01_01"] = {x = 52324.4297, y = 7906.82178, z = 35559.0313, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_75_02"] = {x = 54390.1211, y = 6957.34326, z = 30031.293, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_75_01"] = {x = 54741.6836, y = 6955.21045, z = 30014.2285, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_15_MONSTER_04"] = {x = 63567.9688, y = 6549.13184, z = 32336.9395, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_15_MONSTER_03"] = {x = 63952.7539, y = 6473.30762, z = 31785.4414, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_15_MONSTER_02"] = {x = 64081.0039, y = 6525.38086, z = 32243.6641, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_15_MONSTER_01"] = {x = 63404.9688, y = 6541.74121, z = 32042.2031, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_71_MONSTER_04"] = {x = 60120.582, y = 6882.48877, z = 28642.1152, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_71_MONSTER_03"] = {x = 60004.2344, y = 6887.05615, z = 28411.7383, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_71_MONSTER_02"] = {x = 60293.8164, y = 6879.16895, z = 28211.5859, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_71_MONSTER2_01"] = {x = 60479.8281, y = 6864.77832, z = 28603.2852, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_71_MONSTER_04"] = {x = 58408.5195, y = 6885.32861, z = 29247.8652, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_71_MONSTER_03"] = {x = 58586.5352, y = 6896.08203, z = 29116.9961, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_71_MONSTER_02"] = {x = 58277.0938, y = 6852.06787, z = 29532.168, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_71_MONSTER_01"] = {x = 58745.043, y = 6859.90088, z = 29332.207, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_36_04"] = {x = 77337.5078, y = 5100.0083, z = 27981.5742, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_36_03"] = {x = 77338.4609, y = 5075.56982, z = 27603.1035, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_36_02"] = {x = 77078.8672, y = 5053.40186, z = 27315.7988, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_36_01"] = {x = 76791.6328, y = 5113.72363, z = 27620.8711, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_32_04"] = {x = 78411.4609, y = 4966.67529, z = 21188.3496, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_32_03"] = {x = 78452.4844, y = 4969.2168, z = 20943.8086, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_32_02"] = {x = 77901.9297, y = 4939.53369, z = 20960.7402, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_32_01"] = {x = 78121.125, y = 4941.85986, z = 21196.8926, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_14_04"] = {x = 73189.125, y = 5111.52441, z = 27011.168, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_14_03"] = {x = 73023.5625, y = 5104.94824, z = 27356.6445, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_14_02"] = {x = 72758.1563, y = 5123.7373, z = 27223.5996, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_14_01"] = {x = 72842.6719, y = 5134.07861, z = 26961.9883, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_17_04"] = {x = 68645.3828, y = 5104.41992, z = 27670.7578, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_17_03"] = {x = 69044.7969, y = 5099.82471, z = 27745.7676, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_17_02"] = {x = 68961.9688, y = 5061.20264, z = 27466.3945, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_17_01"] = {x = 68855.7891, y = 5123.43311, z = 27926.6465, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_04_04"] = {x = 69242.9141, y = 4415.04639, z = 25124.1602, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_04_03"] = {x = 69377.1406, y = 4366.87256, z = 24899.7656, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_04_02"] = {x = 69588.7969, y = 4453.18018, z = 25266.3809, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_04_01"] = {x = 69423.8125, y = 4472.73096, z = 25365.2852, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_38_MONSTER_04"] = {x = 71388.7344, y = 4438.08301, z = 22445.9902, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_38_MONSTER_03"] = {x = 71222.7266, y = 4399.26563, z = 22869.5488, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_38_MONSTER_02"] = {x = 70887.2188, y = 4370.56201, z = 22402.0898, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_38_MONSTER_01"] = {x = 70839.1563, y = 4368.30225, z = 22653.959, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_06_04"] = {x = 71665.8047, y = 4469.13379, z = 16641.6055, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_06_03"] = {x = 71818.4922, y = 4452.68945, z = 16544.4961, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_06_02"] = {x = 71502.8594, y = 4468.21826, z = 16480.082, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_06_01"] = {x = 71507.4297, y = 4477.26221, z = 16741.832, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_08"] = {x = 71950.1719, y = 4363.1001, z = 14976.6455, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_07"] = {x = 71584.7813, y = 4306.46289, z = 15132.5498, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_06"] = {x = 71528.4219, y = 4357.61963, z = 14840.9561, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_05"] = {x = 71884.1484, y = 4318.33691, z = 15143.7148, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RIVERSIDE_04"] = {x = 66413, y = 3241.7146, z = 17956.4512, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RIVERSIDE_03"] = {x = 66792.75, y = 3221.61719, z = 17787.3047, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RIVERSIDE_02"] = {x = 66895.9531, y = 3164.9751, z = 18042.6387, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RIVERSIDE_01"] = {x = 66530.2969, y = 3174.11035, z = 18133.748, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_NOVCHASE_02"] = {x = 64722.7852, y = 3198.11011, z = 18306.2988, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_NOVCHASE_01"] = {x = 64499.125, y = 3176.18384, z = 18264.9863, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_07_04"] = {x = 61569.1602, y = 3348.53589, z = 15478.917, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_07_03"] = {x = 61923.9844, y = 3432.9917, z = 15030.8711, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_07_02"] = {x = 61791.0469, y = 3356.70508, z = 15400.3076, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_07_01"] = {x = 61512.0859, y = 3395.71631, z = 15315.957, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_11_04"] = {x = 59743.625, y = 3521.11646, z = 14675.7393, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_11_03"] = {x = 59489.6211, y = 3543.2168, z = 14536.2207, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_11_02"] = {x = 59336.1367, y = 3610.54346, z = 14840.9766, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PLANE_11_01"] = {x = 59601.7656, y = 3592.51416, z = 14910.8594, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_66_MONSTER_03"] = {x = 59180.8555, y = 3136.66675, z = 11931.7295, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_66_MONSTER_02"] = {x = 59033.5273, y = 3138.36401, z = 12151.3154, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_66_MONSTER_01"] = {x = 58944.2148, y = 3128.79248, z = 11933.5918, adjacent =
	[
	]},
	["FP_NW_ITEM_TROLL_10"] = {x = 59692.2148, y = 6839.13721, z = 40667.0273, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_32"] = {x = 79204.5547, y = 5297.33008, z = 30202.6113, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_31"] = {x = 77783.7422, y = 5373.42188, z = 31427.9609, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_30"] = {x = 77168.7891, y = 5095.78418, z = 29620.7578, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_29"] = {x = 78146.9922, y = 5181.37354, z = 29832.6152, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_28"] = {x = 78498.7188, y = 5158.48926, z = 29109.582, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_27"] = {x = 79114.5859, y = 5318.48682, z = 28846.3477, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_26"] = {x = 75937.7578, y = 5311.2915, z = 31486.8574, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_25"] = {x = 75058.6016, y = 5214.65234, z = 32308.6836, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_24"] = {x = 73542.2344, y = 5100.22217, z = 30756.2285, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_23"] = {x = 72386.8359, y = 5101.27344, z = 30296.9531, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_22"] = {x = 74837.4141, y = 5115.8916, z = 31562.1914, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_21"] = {x = 74044.5078, y = 5229.18213, z = 32460.9941, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_20"] = {x = 73122.0313, y = 5222.16846, z = 32752.0547, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_19"] = {x = 73612.7813, y = 5132.5376, z = 31820.002, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_18"] = {x = 72533.2344, y = 5097.76416, z = 31257.2168, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_17"] = {x = 74206.2188, y = 5093.22656, z = 31139.2539, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_16"] = {x = 80033.1172, y = 4977.91553, z = 24030.582, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_15"] = {x = 79931.0625, y = 5072.59033, z = 21958.5039, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_14"] = {x = 79922.3438, y = 5003.32129, z = 23384.666, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_13"] = {x = 80395.6875, y = 5010.98145, z = 23061.2422, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_12"] = {x = 72139.5938, y = 5206.71387, z = 26240.8945, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_11"] = {x = 72323.1016, y = 5263.56104, z = 25711.5781, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_10"] = {x = 71623.4297, y = 5241.03809, z = 25810.8926, adjacent =
	[
	]},
	["FP_DEAD_SKELETON_01"] = {x = 83409.1172, y = 5268.23291, z = 36603.543, adjacent =
	[
	]},
	["FP_SHATTERED_GOLEM_04"] = {x = 81204.9063, y = 5315.92529, z = 32864.6289, adjacent =
	[
	]},
	["FP_SHATTERED_GOLEM_03"] = {x = 84031.8438, y = 5469.50635, z = 33006.7539, adjacent =
	[
	]},
	["FP_SHATTERED_GOLEM_02"] = {x = 82590.1875, y = 5290.43115, z = 33589.2227, adjacent =
	[
	]},
	["FP_SHATTERED_GOLEM_01"] = {x = 82720.4766, y = 5298.16553, z = 34281.75, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_26"] = {x = 82993.6719, y = 5273.19043, z = 36198.0625, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_25"] = {x = 83154.2578, y = 5307.93408, z = 36915.8125, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_24"] = {x = 82708.6719, y = 5305.65332, z = 33348.418, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_23"] = {x = 83089.7656, y = 5323.07861, z = 33690.1602, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_22"] = {x = 83059.6875, y = 5315.74268, z = 34443.6328, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_21"] = {x = 82251.4063, y = 5287.33447, z = 33827.9141, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_20"] = {x = 84278.5156, y = 5471.46289, z = 33754.4023, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_19"] = {x = 84758.9609, y = 5477.26904, z = 33170.4531, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_18"] = {x = 81763.9453, y = 5302.65186, z = 33380.8242, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_17"] = {x = 81571.7891, y = 5292.36182, z = 32738.5371, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_16"] = {x = 81332.625, y = 5292.22021, z = 33440.4922, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_15"] = {x = 80845.3828, y = 5323.15723, z = 32936.0664, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_14"] = {x = 79952.9531, y = 5524.99951, z = 34300.0898, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_13"] = {x = 79878.1641, y = 5498.92139, z = 33893.9531, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_12"] = {x = 79386.4453, y = 5497.5249, z = 34473.0352, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_11"] = {x = 79882.9219, y = 5493.70801, z = 35051.8789, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_10"] = {x = 79182.7344, y = 5516.21338, z = 34279, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_09"] = {x = 79580.7813, y = 5516.47705, z = 33788.1406, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_08"] = {x = 80715.3906, y = 5463.00684, z = 34327.2969, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_07"] = {x = 81440.8125, y = 5458.05762, z = 34480.8672, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_06"] = {x = 81198.2109, y = 5476.69043, z = 34272.9414, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_05"] = {x = 81217.25, y = 5446.09668, z = 34899.832, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_04"] = {x = 80445.1094, y = 5309.49951, z = 32057.7988, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_03"] = {x = 80019.2344, y = 5293.229, z = 32367.0742, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_02"] = {x = 80195.0547, y = 5311.85303, z = 31877.8457, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_CAVE_01"] = {x = 80258.9922, y = 5286.59473, z = 32562.1914, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_09"] = {x = 75886.9219, y = 5011.13428, z = 22567.2266, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_08"] = {x = 76067.9609, y = 4992.63037, z = 23425.0078, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_07"] = {x = 75340.9766, y = 5046.76953, z = 23989.8496, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_06"] = {x = 75411.4063, y = 5062.04932, z = 21990.4219, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_05"] = {x = 74921.4688, y = 5069.17285, z = 22943.5352, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_04"] = {x = 73061.2344, y = 4761.54443, z = 19438.4023, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_03"] = {x = 73427.8594, y = 4771.22607, z = 19001.5488, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_02"] = {x = 73461, y = 4779.38477, z = 19567.8672, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_RUINS_01"] = {x = 72661.9766, y = 4766.19727, z = 19735.1504, adjacent =
	[
	]},
	["FP_NW_ITEM_TROLL_06"] = {x = 36985.9258, y = 6984.51807, z = 32636.4453, adjacent =
	[
	]},
	["FP_NW_ITEM_TROLL_05"] = {x = 49976.3242, y = 7871.15723, z = 38622.5625, adjacent =
	[
	]},
	["FP_NW_ITEM_TROLL_04"] = {x = 75561.2188, y = 4921.50439, z = 25990.373, adjacent =
	[
	]},
	["FP_NW_ITEM_TROLL_03"] = {x = 61500.6289, y = 3337.64111, z = 17771.2422, adjacent =
	[
	]},
	["FP_NW_ITEM_TROLL_02"] = {x = 72220.7891, y = 4349.08057, z = 15117.542, adjacent =
	[
	]},
	["FP_NW_ITEM_TROLL_01"] = {x = 52431.6602, y = 7783.49805, z = 37670.1055, adjacent =
	[
	]},
	["FP_ROAM_RITUALFOREST_CAVE_11"] = {x = 36387.3672, y = 5293.35938, z = 29799.7207, adjacent =
	[
	]},
	["FP_ROAM_RITUALFOREST_CAVE_10"] = {x = 36201.7539, y = 5333.35938, z = 30043.9922, adjacent =
	[
	]},
	["FP_ROAM_RITUALFOREST_CAVE_09"] = {x = 37932.6016, y = 5165.65186, z = 30129.9746, adjacent =
	[
	]},
	["FP_ROAM_RITUALFOREST_CAVE_08"] = {x = 37947.1523, y = 5185.65186, z = 30407.9277, adjacent =
	[
	]},
	["FP_ROAM_RITUALFOREST_CAVE_07"] = {x = 39012.6719, y = 5245.65186, z = 30976.3047, adjacent =
	[
	]},
	["FP_ROAM_RITUALFOREST_CAVE_06"] = {x = 38931.8008, y = 5255.65186, z = 31191.8223, adjacent =
	[
	]},
	["FP_ROAM_RITUALFOREST_CAVE_05"] = {x = 35270.9336, y = 5174.13721, z = 31190.4219, adjacent =
	[
	]},
	["FP_ROAM_RITUALFOREST_CAVE_04"] = {x = 35074.7227, y = 5114.13721, z = 31478.873, adjacent =
	[
	]},
	["FP_ROAM_RITUALFOREST_CAVE_03"] = {x = 34994.2383, y = 5144.13721, z = 31181.5508, adjacent =
	[
	]},
	["FP_ROAM_RITUALFOREST_CAVE_02"] = {x = 36370.1992, y = 5583.72461, z = 32849.0742, adjacent =
	[
	]},
	["FP_ROAM_RITUALFOREST_CAVE_01"] = {x = 36174.7031, y = 5543.72461, z = 32647.4238, adjacent =
	[
	]},
	["FP_NW_ITEM_RITUALFOREST_CAVE_EGG"] = {x = 35734.0234, y = 5389.06348, z = 33067.4375, adjacent =
	[
	]},
	["FP_NW_ITEM_RIVERSIDE_EGG"] = {x = 68945.6094, y = 3322.23364, z = 14061.7695, adjacent =
	[
	]},
	["FP_NW_ITEM_MAGECAVE_EGG"] = {x = 66697.1406, y = 6895.4209, z = 43138.3984, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_SEA_04"] = {x = 62005.707, y = 6524.57031, z = 33774.1758, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_SEA_03"] = {x = 61605.2656, y = 6513.28613, z = 34112.3203, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_SEA_02"] = {x = 61397.7305, y = 6525.7002, z = 33462.75, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_SEA_01"] = {x = 61131.3398, y = 6514.30811, z = 32961.9727, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_20"] = {x = 65957.8359, y = 4554.65967, z = 15518.7031, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_19"] = {x = 66364.3047, y = 4558.90479, z = 15753.6396, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_18"] = {x = 66222.1406, y = 4610.15771, z = 15372.1104, adjacent =
	[
	]},
	["FP_ROAM_MAGECAVE_20"] = {x = 66452.9141, y = 6929.12793, z = 43596.9063, adjacent =
	[
	]},
	["FP_ROAM_MAGECAVE_19"] = {x = 66792.9531, y = 6929.12793, z = 43499.8203, adjacent =
	[
	]},
	["FP_ROAM_MAGECAVE_18"] = {x = 66205.9297, y = 6918.2085, z = 43445.4492, adjacent =
	[
	]},
	["FP_ROAM_MAGECAVE_16"] = {x = 66379.6172, y = 6947.46484, z = 43346.5078, adjacent =
	[
	]},
	["FP_ROAM_MAGECAVE_14"] = {x = 66509.1563, y = 6964.44922, z = 43910.4023, adjacent =
	[
	]},
	["FP_ROAM_MAGECAVE_13"] = {x = 64615.4766, y = 6917.71582, z = 43417.3047, adjacent =
	[
	]},
	["FP_ROAM_MAGECAVE_11"] = {x = 64311.168, y = 6883.93604, z = 43640.4961, adjacent =
	[
	]},
	["FP_ROAM_MAGECAVE_10"] = {x = 64483.9805, y = 6891.07275, z = 42924.5742, adjacent =
	[
	]},
	["FP_ROAM_MAGECAVE_09"] = {x = 64124.6055, y = 6901.79102, z = 43364.8867, adjacent =
	[
	]},
	["FP_ROAM_MAGECAVE_08"] = {x = 63440.8672, y = 6912.98047, z = 44640.9336, adjacent =
	[
	]},
	["FP_ROAM_MAGECAVE_07"] = {x = 63474.9961, y = 6894.74951, z = 44801.1914, adjacent =
	[
	]},
	["FP_ROAM_MAGECAVE_06"] = {x = 63371.418, y = 6869.43555, z = 44890.9219, adjacent =
	[
	]},
	["FP_ROAM_MAGECAVE_04"] = {x = 62310.9219, y = 6558.72412, z = 44938.6641, adjacent =
	[
	]},
	["FP_ROAM_MAGECAVE_03"] = {x = 62228.0586, y = 6553.20166, z = 45383.1797, adjacent =
	[
	]},
	["FP_ROAM_MAGECAVE_02"] = {x = 62130.25, y = 6571.1958, z = 45148.2422, adjacent =
	[
	]},
	["FP_ROAM_MAGECAVE_01"] = {x = 62399.4766, y = 6571.1958, z = 45269.6367, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_01"] = {x = 56557, y = 3467.74609, z = 12669, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_02"] = {x = 56073.6367, y = 3463.95752, z = 12617.3174, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_03"] = {x = 55949.8516, y = 3662.96484, z = 13186.1826, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_04"] = {x = 57046.7422, y = 3590.31641, z = 13040.0176, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_05"] = {x = 56583.8086, y = 3694.41675, z = 13307.2393, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_06"] = {x = 69048.5078, y = 3293.02075, z = 14411.335, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_07"] = {x = 69039.5938, y = 3288.47534, z = 14755.7842, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_08"] = {x = 66137.0391, y = 3467.21484, z = 16592.4883, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_09"] = {x = 66013.2188, y = 3489.11157, z = 16456.9199, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_10"] = {x = 65687.7813, y = 3490.89673, z = 16658.7598, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_11"] = {x = 66113.5234, y = 3449.22876, z = 16794.2969, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_12"] = {x = 65772.75, y = 3455.20117, z = 16918.5977, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_13"] = {x = 67155.9297, y = 4883.84277, z = 11464.0088, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_14"] = {x = 66920.5156, y = 4878.2959, z = 11061.2227, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_15"] = {x = 66736.9219, y = 4842.04346, z = 11293.793, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_16"] = {x = 66998.4063, y = 4846.53613, z = 11710.5215, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_17"] = {x = 66229.3672, y = 4945.0791, z = 11361.127, adjacent =
	[
	]},
	["FP_MAGICGOLEM"] = {x = 63704.4727, y = 4980.4834, z = 10376.542, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_SEA_05"] = {x = 61148.3008, y = 6505.4834, z = 34073.9688, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_SEA_06"] = {x = 61766.3555, y = 6507.02246, z = 33931.1055, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_SEA_07"] = {x = 61379.5039, y = 6531.15332, z = 33241.4688, adjacent =
	[
	]},
	["FP_ROAM_TROLLAREA_SEA_08"] = {x = 61517.7539, y = 6596.06543, z = 32648.7266, adjacent =
	[
	]},
	["FP_SMALLTALK_RITUALBANDITS_01"] = {x = 50488.6094, y = 7984.22949, z = 29091.2891, adjacent =
	[
	]},
	["FP_SMALLTALK_RITUALBANDITS_02"] = {x = 50268.8359, y = 7968.68359, z = 29388.0508, adjacent =
	[
	]},
	["FP_TROLLAREA_RITUAL_ITEM"] = {x = 37255.9844, y = 7078.37744, z = 32225.3867, adjacent =
	[
	]},
	["FP_NW_ITEM_TROLL_07"] = {x = 74251.8359, y = 6679.99414, z = 28911.5313, adjacent =
	[
	]},
	["FP_NW_ITEM_TROLL_08"] = {x = 74692.8906, y = 6679.41113, z = 28955.7676, adjacent =
	[
	]},
	["FP_NW_ITEM_TROLL_09"] = {x = 72323.9141, y = 5733.7749, z = 37000.5781, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_08_01"] = {x = 61029.5742, y = 6983.10205, z = 38373.2734, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_08_02"] = {x = 60673.793, y = 6959.62451, z = 38070.1484, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_08_03"] = {x = 61309.6367, y = 6982.11914, z = 38172.4492, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_08_04"] = {x = 60993.0664, y = 6965.46924, z = 37914.8047, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_72_01"] = {x = 56594.6289, y = 6570.50098, z = 31023.6406, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_72_02"] = {x = 57169.6523, y = 6610.35791, z = 31042.5762, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_72_03"] = {x = 56968.1875, y = 6669.72217, z = 30523.5313, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_PATH_72_04"] = {x = 57029.4414, y = 6642.5835, z = 30740.6113, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_BRIGDE_05_01"] = {x = 57480.6758, y = 7182.99951, z = 39116.4727, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_BRIGDE_05_02"] = {x = 56999.8125, y = 7282.77051, z = 39195.6914, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_BRIGDE_05_03"] = {x = 57209.9727, y = 7176.15283, z = 39400.4063, adjacent =
	[
	]},
	["FP_ROAM_NW_TROLLAREA_BRIGDE_05_04"] = {x = 57272.6992, y = 7251.46875, z = 38930.9766, adjacent =
	[
	]},
	["FP_STAND_RIO"] = {x = 86422.8281, y = 3749.05811, z = 24445.2383, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_30"] = {x = 21479.2559, y = 3439.82983, z = -11831.5986, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_29"] = {x = 21810.5176, y = 3539.28833, z = -11490.0205, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_28"] = {x = 21428.2285, y = 3524.87134, z = -11555.9375, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_27"] = {x = 21550.3535, y = 3569.08862, z = -11371.2959, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_26"] = {x = 20066.4609, y = 3805.6377, z = -11494.2559, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_25"] = {x = 19988.9883, y = 3537.02197, z = -12703.9365, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_24"] = {x = 24390.1875, y = 1840.34534, z = -17426.4766, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_23"] = {x = 24822.0938, y = 1773.05151, z = -17361.1855, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_22"] = {x = 24912.1504, y = 1695.56934, z = -18183.7188, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_21"] = {x = 24689.1719, y = 1751.25061, z = -17775.373, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_20"] = {x = 22311.4727, y = 2931.57446, z = -11979.7188, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_18"] = {x = 23607.1914, y = 2810.23682, z = -13375.9561, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_17"] = {x = 22809.9199, y = 2818.9668, z = -12974.583, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_16"] = {x = 22345.1211, y = 3205.22046, z = -10665.167, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_15"] = {x = 22530.584, y = 3172.53735, z = -10974.6348, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_14"] = {x = 22331.4648, y = 3136.97144, z = -11296.3672, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_13"] = {x = 22162.8496, y = 3223.48438, z = -11006.5586, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_12"] = {x = 22692.7402, y = 2870.74341, z = -12460.332, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_11"] = {x = 23158.8398, y = 2852.47949, z = -12731.9492, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_10"] = {x = 23289.5137, y = 2860.72314, z = -14202.0322, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_09"] = {x = 22523.4609, y = 2779.01685, z = -14153.8936, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_08"] = {x = 22339.752, y = 2774.6167, z = -13766.9131, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_07"] = {x = 22585.4121, y = 2802.49316, z = -13833.8672, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_06"] = {x = 21160.9785, y = 3222.53369, z = -13643.4551, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_05"] = {x = 21105.1152, y = 3288.56885, z = -14304.1689, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_04"] = {x = 20769.25, y = 3311.63647, z = -13898.0117, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_03"] = {x = 20814.0762, y = 3458.80249, z = -15951.0713, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_02"] = {x = 20944.9609, y = 3472.24048, z = -15162.7275, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_SECRET_01"] = {x = 20664.9883, y = 3457.90088, z = -15556.6875, adjacent =
	[
	]},
	["FP_ITEM_XARDAS_07"] = {x = 29932.3125, y = 5986.12305, z = -15472.6914, adjacent =
	[
	]},
	["FP_ITEM_XARDAS_06"] = {x = 28272.4668, y = 749.897461, z = -13940.1016, adjacent =
	[
	]},
	["FP_ITEM_XARDAS_05"] = {x = 29847.6582, y = 5985.77002, z = -15481.2852, adjacent =
	[
	]},
	["FP_ITEM_XARDAS_04"] = {x = 33224.4453, y = 4144.78174, z = -24000.2773, adjacent =
	[
	]},
	["FP_ITEM_XARDAS_03"] = {x = 17647.3398, y = 3248.05151, z = -21454.9023, adjacent =
	[
	]},
	["FP_ITEM_XARDAS_02"] = {x = 29711.6816, y = 5147.92773, z = -14570.4268, adjacent =
	[
	]},
	["FP_ROAM_XARDASCAVE_DJG_03"] = {x = 25970.9395, y = 974.527832, z = -9300.7959, adjacent =
	[
	]},
	["FP_ROAM_XARDASCAVE_DJG_02"] = {x = 25698.877, y = 975.527832, z = -9346.47168, adjacent =
	[
	]},
	["FP_ROAM_XARDASCAVE_DJG_01"] = {x = 26243.3281, y = 979.527832, z = -9500.94043, adjacent =
	[
	]},
	["FP_ITEM_XARDAS_01"] = {x = 26043.1172, y = 981.558044, z = -8832.80176, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_VALLEY_16"] = {x = 28876.832, y = 620.59314, z = -17263.1738, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_VALLEY_15"] = {x = 29186.6465, y = 590.59314, z = -16636.002, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_VALLEY_14"] = {x = 29067.1973, y = 701.59314, z = -17474.9258, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_VALLEY_13"] = {x = 28982.7813, y = 640.59314, z = -17014.791, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_VALLEY_12"] = {x = 27339.8535, y = 485.548462, z = -17954.0918, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_VALLEY_11"] = {x = 27588.0469, y = 505.548462, z = -17899.3105, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_VALLEY_10"] = {x = 27419.8691, y = 475.548462, z = -18263.418, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_VALLEY_09"] = {x = 27769.2773, y = 551.548462, z = -18093.4297, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_VALLEY_08"] = {x = 25669.4199, y = 475.548462, z = -17704.0078, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_VALLEY_07"] = {x = 26253.6367, y = 385.548462, z = -18138.3398, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_VALLEY_06"] = {x = 26390.5137, y = 395.548462, z = -17776.2168, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_VALLEY_05"] = {x = 25955.0391, y = 435.548462, z = -17708.4141, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_VALLEY_04"] = {x = 25625.7559, y = 490.736877, z = -14799.7002, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_VALLEY_03"] = {x = 25402.2969, y = 550.736877, z = -14569.7471, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_VALLEY_02"] = {x = 25413.5586, y = 450.736877, z = -15068.2432, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_VALLEY_01"] = {x = 25139.7441, y = 500.736877, z = -14808.0957, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_VALLEY_04"] = {x = 24766.5391, y = 858.684448, z = -11879.0059, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_VALLEY_03"] = {x = 24463.4902, y = 878.684448, z = -11996.4277, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_VALLEY_02"] = {x = 24801.3066, y = 848.684448, z = -11534.9648, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_VALLEY_01"] = {x = 24434.6523, y = 858.684448, z = -11578.3691, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_CAVE_14"] = {x = 24586.9766, y = 1015.41882, z = -9786.3418, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_CAVE_13"] = {x = 24022.9063, y = 995.418823, z = -9164.33398, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_CAVE_12"] = {x = 24576.6094, y = 995.565918, z = -9557.00391, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_CAVE_11"] = {x = 24075.4141, y = 985.565918, z = -9468.05664, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_CAVE_10"] = {x = 24277.0293, y = 975.565918, z = -9824.14258, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_CAVE_09"] = {x = 24299.0371, y = 985.565918, z = -9197.99512, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_MONSTER_02_03"] = {x = 13116.6406, y = 1586.66394, z = -23651.7598, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_MONSTER_02_02"] = {x = 13571.1514, y = 1539.9043, z = -23560.2051, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_MONSTER_02_01"] = {x = 13666.2119, y = 1619.87378, z = -23908.998, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_MONSTER_01_03"] = {x = 17013.8789, y = 1999.45667, z = -23592.6094, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_MONSTER_01_02"] = {x = 17286.0352, y = 2056.2832, z = -23691.9297, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_MONSTER_01_01"] = {x = 17052.1152, y = 2017.54443, z = -23095.9707, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_1_01"] = {x = 30635.8047, y = 4501.93115, z = -20662.0039, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_1_02"] = {x = 30214.3691, y = 4441.93115, z = -20706.9648, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_1_03"] = {x = 30497.6563, y = 4461.93115, z = -20331.4395, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_2_01"] = {x = 27668.4336, y = 3661.37793, z = -20344.0469, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_2_02"] = {x = 27913.2637, y = 3721.37793, z = -20125.7813, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_3_01"] = {x = 22663.1133, y = 2922.49438, z = -23480.3828, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_3_02"] = {x = 22932.3535, y = 2962.49438, z = -23225.8301, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_3_03"] = {x = 22404.1113, y = 2902.49438, z = -23327.043, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_3_04"] = {x = 22494.4102, y = 2887.49438, z = -23044.5898, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_CAVE_01"] = {x = 32581.7305, y = 3375.33228, z = -18166.9863, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_CAVE_02"] = {x = 32766.3555, y = 3385.33228, z = -18569.543, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_CAVE_03"] = {x = 32412.5586, y = 3395.33228, z = -18462.5781, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_CAVE_04"] = {x = 33038.8281, y = 3385.33228, z = -18237.2246, adjacent =
	[
	]},
	["FP_SIT_CAMPFIRE_LESTER"] = {x = 27936.0527, y = 754.468262, z = -14010.3848, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_4_02"] = {x = 30571.2266, y = 4465.98242, z = -19560.9473, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_4_03"] = {x = 31013.668, y = 4484.98242, z = -19678.6309, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_4_04"] = {x = 31018.8789, y = 4504.98242, z = -20029.5762, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_4_01"] = {x = 31148.9883, y = 4504.15771, z = -19245.1191, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_2_03"] = {x = 28344.0039, y = 3848.64136, z = -20271.3555, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_TOWER_2_04"] = {x = 28171.9805, y = 3805.13257, z = -20529.5449, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_GOBBO_01"] = {x = 24880.6836, y = 3144.11353, z = -21431.0742, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_GOBBO_02"] = {x = 24882.6074, y = 3164.11353, z = -21096.1016, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_GOBBO_03"] = {x = 25239.3105, y = 3184.11353, z = -21215.7266, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_MONSTER_01_04"] = {x = 17302.8027, y = 2018.78711, z = -23124.2148, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_MONSTER_02_04"] = {x = 13088.1631, y = 1554.0354, z = -23515.0977, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_CAVE_05"] = {x = 34133.4219, y = 3678.73438, z = -20303.7793, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_CAVE_06"] = {x = 34181.6445, y = 3678.73438, z = -19828.7539, adjacent =
	[
	]},
	["FP_ROAM_XARDAS_CAVE_07"] = {x = 33916.5273, y = 3674.73438, z = -20053.8496, adjacent =
	[
	]},
	["FP_ROAM_NW_XARDASTOWER_DEMON_01"] = {x = 29951.6914, y = 4311.20117, z = -14554.4736, adjacent =
	[
	]},
	["FP_ROAM_NW_XARDASTOWER_DEMON_02"] = {x = 29623.6973, y = 4292.0293, z = -15490.4678, adjacent =
	[
	]},
	["FP_ROAM_NW_XARDASTOWER_DEMON_03"] = {x = 30261.5742, y = 4312.50977, z = -15826.7031, adjacent =
	[
	]},
	["FP_ROAM_NW_XARDASTOWER_DEMONLORD_01"] = {x = 30184.8496, y = 5951.28857, z = -15680.6758, adjacent =
	[
	]},
	["FP_ROAM_NW_XARDASTOWER_DEMON_04"] = {x = 30333.0703, y = 5156.625, z = -15722.6553, adjacent =
	[
	]},
	["FP_ROAM_NW_XARDASTOWER_DEMON_05"] = {x = 29408.7324, y = 5165.625, z = -15494.3398, adjacent =
	[
	]},
	["FP_SIT_CAMPFIRE_BANDIT"] = {x = 25085.3242, y = 3184.54541, z = -21055.8398, adjacent =
	[
	]},
	["FP_ITEM_XARDAS_STPLATE_01"] = {x = 29592.4766, y = 5233.72266, z = -14989.4805, adjacent =
	[
	]},
	["FP_SPAWN_X_GUERTEL"] = {x = 30672.2715, y = 4330.43018, z = -15591.8662, adjacent =
	[
	]},

}

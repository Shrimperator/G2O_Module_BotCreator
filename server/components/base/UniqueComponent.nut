// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class UniqueComponent extends Component
{
    function tick(deltaTime)
    {
        m_DeltaTime = deltaTime;
    }

    // ------------------------------------------------------------------- //

    constructor(parent)
    {
        base.constructor();

        m_Parent = parent;
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- getters -- //
    function getParent() {return m_Parent;};
    function getDeltaTime() {return m_DeltaTime;};

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    m_Parent = null;
    m_DeltaTime = 0;
}

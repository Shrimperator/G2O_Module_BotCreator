// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class MonsterActionComponent extends ActionComponent
{
    function exIdle()
    {
        local tick = getTickCount();
        local parent = getParent();
        local pos = parent.getPosition();
        local world = parent.getWorld();

        if (isFirstExecution())
        {
            m_TargetFPName = WayPoints.getNearestFP(pos.x, pos.y, pos.z, world, m_FPSearchDist);
            m_TargetFP = WayPoints.getFPByName(world, m_TargetFPName);
        }

        if (m_TargetFP != null)
        {
            if (tick >= m_NextRefreshAniTick)
            {
                // -- stationary roam -- //
                if (rand() % 2 == 0)
                {
                    parent.stopAni();
                    parent.playRandomAni("roam");
                    m_Stationary = true;
                }
                // -- move to random point -- //
                else
                {
                    parent.stopAni();
                    parent.playRandomAni("walk");
                    m_Stationary = false;

                    local angle = rand() % 361;
                    local dist = rand() % m_FPRoamDist;
                    m_TargetPos = Util.moveOnAngle(m_TargetFP.x, m_TargetFP.y, m_TargetFP.z, angle, dist);
                }

                m_NextRefreshAniTick = tick + m_RereshAniInterval;
            }

            if (!m_Stationary)
            {
                local targetAngle = getVectorAngle(pos.x, pos.z, m_TargetPos.x, m_TargetPos.z);
                parent.setInterpAngle(targetAngle, m_RoamTurnSpeed);

                local targetDist = getDistance2d(pos.x, pos.z, m_TargetPos.x, m_TargetPos.z);
                // -- arrived at target -- //
                if (targetDist <= 50)
                {
                    parent.stopAni();
                }
            }
        }
        // -- no fp close enough. Play stationary idle anim -- //
        else if (tick >= m_NextRefreshAniTick)
        {
            parent.playRandomAni("roam");
            m_NextRefreshAniTick = tick + m_RereshAniInterval;
        }
    }

    // ------------------------------------------------------------------- //

    m_TargetFPName = null;
    m_TargetFP = null;
    m_TargetPos = null;

    m_Stationary = false;

    static m_RereshAniInterval = 5000;
    m_NextRefreshAniTick = getTickCount();

    // ------------------------------------------------------------------- //

    static m_FPSearchDist = 2000;
    static m_FPRoamDist = 1000;

    static m_RoamTurnSpeed = 0.002;
}

// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class PreyCombatComponent extends CombatComponent
{
    function handleCombat()
    {
        local tick = getTickCount();
        if (m_Enemy == null && tick >= m_FindEnemyNextTick)
        {
            local enemy = findEnemy();
            if (enemy != null)
                setEnemy(enemy);
        }

        if (m_Enemy != null)
        {
            switch (getCombatState())
            {
                case ECombatStatePrey.fleeStart:
                    exFleeStart();
                    break;
                case ECombatStatePrey.flee:
                    exFlee();
                    break;
                case ECombatStatePrey.fleeEnd:
                    exFleeEnd();
                    break;
            }
            return true;
        }
        return false;
    }

    // ------------------------------------------------------------------- //

    function setEnemy(enemy)
    {
        local parent = getParent();
        local oldEnemy = m_Enemy;

        local isNewEnemy = ( (enemy != null || oldEnemy != null) && enemy != oldEnemy) ||
            (enemy != null && oldEnemy != null && enemy.getID() == oldEnemy.getID());

        if (isNewEnemy)
        {
            m_Enemy = enemy;

            if (enemy == null)
                setCombatState(ECombatStatePrey.idle);
            else if (oldEnemy == null)
                startCombat();
        }
    }

    // ------------------------------------------------------------------- //

    function exFleeStart()
    {
        local parent = getParent();
        local pos = getParent().getPosition();
        local fleeStartWP = WayPoints.getNearestWP(pos.x, pos.y, pos.z, parent.getWorld());

        if (fleeStartWP == null)
            setCombatState(ECombatStatePrey.idle);
        else
        {
            local fleeEndWP = WayPoints.getRandomWPInRange(pos.x, pos.y, pos.z, parent.getWorld(), m_FleeRange);
            m_FleePath = WayPoints.getWPRoute(parent.getWorld(), fleeStartWP, fleeEndWP);

            parent.ignoreStuckOnce();
            setCombatState(ECombatStatePrey.flee);
        }
    }

    // ------------------------------------------------------------------- //

    function exFlee()
    {
        local parent = getParent();
        local pos = getParent().getPosition();

        if (m_FleePath == null || m_FleePath.len() == 0)
        {
            setCombatState(ECombatStatePrey.fleeEnd);
            return;
        }

        local currNodeName = m_FleePath.top();
        local currNode = WayPoints.getWPByName(parent.getWorld(), currNodeName);

        local targetAngle = getVectorAngle(pos.x, pos.z, currNode.x, currNode.z);
        parent.setInterpAngle(targetAngle, m_FleeTurnSpeed);

        parent.playRunAni();

        local targetDist = getDistance2d(pos.x, pos.z, currNode.x, currNode.z);
        // -- arrived at target -- //
        if (targetDist <= 50 || parent.isStuck())
        {
            parent.ignoreStuckOnce();
            m_FleePath.pop();
        }
    }

    // ------------------------------------------------------------------- //

    function exFleeEnd()
    {
        setEnemy(null);
    }

    // ------------------------------------------------------------------- //

    function startCombat()
    {
        setCombatState(ECombatStatePrey.fleeStart);
    }

    // ------------------------------------------------------------------- //

    function onReceiveDamage(attackerId, dmg, type)
    {
        if (!isFighting() && !getParent().isDead())
            setEnemy(BotEnemy(attackerId, getParent()), true);
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    m_FleePath = null;

    // ------------------------------------------------------------------- //

    static m_FleeRange = 5000;
    static m_FleeTurnSpeed = 0.01;
}

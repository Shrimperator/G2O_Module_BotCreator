// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class CombatComponent extends UniqueComponent
{
    function tick(deltaTime)
    {
        base.tick(deltaTime);

        return handleCombat();
    }

    // ------------------------------------------------------------------- //

    function handleCombat()
    {
        return false;
    }

    // ------------------------------------------------------------------- //

    function turnToEnemy(speed)
    {
        local parent = getParent();
        local enemy = getEnemy();
        parent.setInterpAngleId(enemy.getID(), speed);
    }

    // ------------------------------------------------------------------- //

    function stopTurningToEnemy()
    {
        local parent = getParent();
        parent.setInterpAngleId(-1, parent.getInterpAngleSpeed());
    }

    // ------------------------------------------------------------------- //

    function onDeath(attackerId)
    {
        stopCombat();
    }

    // ------------------------------------------------------------------- //

    // ------------------------------------------------------------------- //

    function onReceiveDamage(attackerId, dmg, type)
    {
    }

    // ------------------------------------------------------------------- //

    function onStartFight()
    {
    }

    // ------------------------------------------------------------------- //

    function onEndFight()
    {
    }

    // ------------------------------------------------------------------- //

    function canHit()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        local enemy = getEnemy();

        local pos = parent.getPosition();
        local enemyPos = enemy.getPosition();

        local angle = parent.getAngle();
        local targetAngle = getVectorAngle(pos.x, pos.z, enemyPos.x, enemyPos.z);

        local angleDelta = Util.getAngleDelta(angle, targetAngle);
        local dist = enemy.getDistance();

        local enemyAni = enemy.getAni();

        local enemyParrying = enemyAni in PARRY_ANIMS;

        return angleDelta <= typeComp.getMaxAttackAngle() && dist <= typeComp.getAttackRange() && !enemyParrying;
    }

    // ------------------------------------------------------------------- //

    function hit()
    {
        getEnemy().receiveHit(getParent().getID());
    }

    // ------------------------------------------------------------------- //

    function findEnemy()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        return getNearestEnemy(typeComp.getEnemyViewDist(), true);
    }

    // ------------------------------------------------------------------- //

    function stopCombat()
    {
    }

    // ------------------------------------------------------------------- //

    function startCombat()
    {
    }

    // ------------------------------------------------------------------- //

    function restartCombat()
    {
    }

    // ------------------------------------------------------------------- //

    function setEnemy(enemy, hostileImmediately = false)
    {
        m_Enemy = enemy;
    }

    // ------------------------------------------------------------------- //

    function clearEnemy()
    {
        setEnemy(null);
    }

    // ------------------------------------------------------------------- //

    function getEnemyDistance()
    {
        if (m_Enemy != null)
        {
            local pos = getPosition();
            local enemyPos = m_Enemy.getPosition();

            return getDistance3d(pos.x, pos.y, pos.z, enemyPos.x, enemyPos.y, enemyPos.z);
        }
        return -1;
    }

    // ------------------------------------------------------------------- //

    function getNearestEnemy(radius = 5000, alive = null)
    {
        local pos = getParent().getPosition();

        local enemyBot = getNearestEnemyBot(radius, alive);
        local enemyPlayer = getNearestEnemyPlayer(radius, alive);

        local botValid = enemyBot.isValid();
        local playerValid = enemyPlayer.isValid();

        if (botValid == playerValid)
        {
            // -- Both invalid -- //
            if (!botValid)
                return null;
            // -- Both valid. Chose closest -- //
            else
            {
                local botPos = enemyBot.getPosition();
                local botDist = getDistance3d(botPos.x, botPos.y, botPos.z, pos.x, pos.y, pos.z);

                local playerPos = enemyPlayer.getPosition();
                local playerDist = getDistance3d(playerPos.x, playerPos.y, playerPos.z, pos.x, pos.y, pos.z);

                if (botDist < playerDist)
                    return enemyBot;
                else
                    return enemyPlayer;
            }
        }
        // -- One is valid -- //
        else
        {
            if (!botValid)
                return enemyPlayer;
            return enemyBot;
        }
    }

    // ------------------------------------------------------------------- //

    function getNearestEnemyPlayer(radius = 5000, alive = null)
    {
        local lastDist = radius + 1;
        local lastPid = null;
        local botPos = getParent().getPosition();

        for(local pid = 0; pid < getMaxSlots(); pid++)
        {
            if (isPlayerConnected(pid) &&
            alive != isPlayerDead(pid))
            {
                local playerWorld = getPlayerWorld(pid);
                if (playerWorld == getParent().getWorld())
                {
                    local playerPos = getPlayerPosition(pid);
                    local dist = getDistance3d(botPos.x, botPos.y, botPos.z, playerPos.x, playerPos.y, playerPos.z);
                    if (dist <= radius && dist < lastDist && getParent().getFactionComp().isPlayerEnemy(pid))
                    {
                        lastPid = pid;
                        lastDist = dist;
                    }
                }
            }
        }

        return BotEnemy(lastPid, getParent());
    }

    // ------------------------------------------------------------------- //

    function getNearestEnemyBot(radius = 5000, alive = null)
    {
        local parent = getParent();
        local pos = parent.getPosition();

        local bots = BotAI.m_CellSystem.getContentNear(pos.x, pos.z, 5000);
        local lastDistance = null;
        local lastBotId = null;

        foreach (i, bot in bots)
        {
            if (bot != parent)
            {
                local otherPos = bot.getPosition();
                local currDistance = getDistance3d(pos.x, pos.y, pos.z, otherPos.x, otherPos.y, otherPos.z);

                if (currDistance <= radius && (currDistance < lastDistance || lastBotId == null) && parent.getFactionComp().isBotEnemy(bot) && (alive == null || !bot.isDead() ))
                {
                    lastDistance = currDistance;
                    lastBotId = bot.getID();
                }
            }
        }

        return BotEnemy(lastBotId, getParent());
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Setters -- //
    function setCombatState(state){m_CombatState = state;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Getters -- //
    function getEnemy(){return m_Enemy;}
    function getAttackRange(){return m_AttackRange;}
    function getCombatState(){return m_CombatState;}
    function isFighting() {return m_CombatState != COMBAT_IDLE;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    constructor(parent)
    {
        base.constructor(parent);
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    m_Enemy = null;
    m_CombatState = COMBAT_IDLE;

    m_CombatEndTick = 0;
    m_CombatStartPos = null;

    m_FindEnemyNextTick = getTickCount();

    // -- WARN -- //
        m_EndWarnTick = getTickCount();
        m_LastWarnDist = 0;
        m_MinWarnTime = getTickCount();

    // -- DRAW -- //
        m_LastDrawTick = 0;

    // -- ATTACK -- //
        m_AttackStateStartTick = 0;
        m_AttackState = EAttackState.ready;

    // -- EVADE -- //
        m_EvadeStateEndTick = 0;

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    static m_EvadeTime = 1000;
    static m_MinCancelChaseTime = 1000;

    static m_FindEnemyTickInterval = 3000;
}

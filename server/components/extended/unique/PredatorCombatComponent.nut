// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class PredatorCombatComponent extends CombatComponent
{
    // -- State machine:

    // -- idle -> warnStart/idle

    // -- warnStart -> warn
    // -- warn -> chaseStart/idle

    // -- chaseStart -> chase
    // -- chase -> attackStart/evadeStart/idle

    // -- evadeStart -> evade
    // -- evade -> chaseStart

    // -- attackStart -> attack
    // -- attack -> chaseStart/idle
    function handleCombat()
    {
        local tick = getTickCount();
        if ( (m_Enemy == null && tick >= m_FindEnemyNextTick) || tick >= m_EnemyDropTime)
        {
            local enemy = findEnemy();
            if (enemy != null)
                setEnemy(enemy);
        }

        if (m_Enemy != null)
        {
            switch (getCombatState())
            {
                case ECombatStatePredator.idle:
                    break;

                case ECombatStatePredator.warnStart:
                    exWarnStart();
                    break;
                case ECombatStatePredator.warn:
                    exWarn();
                    break;

                case ECombatStatePredator.chaseStart:
                    exChaseStart();
                    break;
                case ECombatStatePredator.chase:
                    exChase();
                    break;

                case ECombatStatePredator.evadeStart:
                    exEvadeStart();
                    break;
                case ECombatStatePredator.evade:
                    exEvade();
                    break;

                case ECombatStatePredator.attackStart:
                    exAttackStart();
                    break;
                case ECombatStatePredator.attack:
                    exAttack();
                    break;

                case EcombatMode.fleeStart:
                    exFleeStart();
                    break;
                case EcombatMode.flee:
                    exFlee();
                    break;
            }
            return true;
        }
        return false;
    }

    // ------------------------------------------------------------------- //

    function exWarnStart()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();

        parent.stopAni();
        m_EndWarnTick = getTickCount() + typeComp.getMaxWarnTime();

        setCombatState(ECombatStatePredator.warn);

        turnToEnemy(typeComp.getWarnTurnSpeed());
    }

    // ------------------------------------------------------------------- //

    function exWarn()
    {
        local tick = getTickCount();

        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        local enemy = getEnemy();

        local dist = enemy.getDistance();
        local speed = parent.getSpeed();

        local pos = parent.getPosition();
        local enemyPos = enemy.getPosition();

        local targetAngle = getVectorAngle(pos.x, pos.z, enemyPos.x, enemyPos.z);

        local facingAway = Util.getAngleDelta(targetAngle, parent.getMovementAngle()) > 90;
        local tooClose = dist < typeComp.getMinWarnDist();
        local moving = parent.getSpeed() > 0.0;
        local tookTooLong = tick > m_EndWarnTick;

        // -- change to chase state -- //
        if (dist > typeComp.getMaxWarnDist())
        {
            stopCombat();
            return;
        }
        else if (tick >= m_MinWarnTime && (tookTooLong || tooClose) && !(moving && facingAway))
        {
            m_CombatStartTick = getTickCount();
            setCombatState(ECombatStatePredator.chaseStart);
            return;
        }

        m_LastWarnDist = dist;
        parent.playRandomAni("warn");
    }

    // ------------------------------------------------------------------- //

    function exChaseStart()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();

        getParent().stopAni();
        getParent().ignoreStuckOnce();
        setCombatState(ECombatStatePredator.chase);
        m_CombatStartTick = getTickCount();

        turnToEnemy(typeComp.getChaseTurnSpeed());
    }

    // ------------------------------------------------------------------- //

    function exChase()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        local enemy = getEnemy();
        local maxMoveDist = typeComp.getMaxMoveDist();

        local pos = parent.getPosition();
        local dist = enemy.getDistance();

        local combatDist = getDistance2d(m_CombatStartPos.x, m_CombatStartPos.z, pos.x, pos.z);

        if (getTickCount() >= m_CombatStartTick + typeComp.getMaxChaseTime() && combatDist >= maxMoveDist)
        {
            // -- wait at least m_MinCancelChaseTime ms before chasing again -- //
            m_MinWarnTime = getTickCount() + m_MinCancelChaseTime;
            setCombatState(ECombatStatePredator.warnStart);
            return;
        }
        else if (dist > typeComp.getMaxChaseDist())
        {
            stopCombat();
            return;
        }
        else if (dist <= typeComp.getAttackRange())
        {
            setCombatState(ECombatStatePredator.attackStart);
            return;
        }
        else if (parent.isStuck())
        {
            setCombatState(ECombatStatePredator.evadeStart);
            return;
        }

        parent.playRunAni();
    }

    // ------------------------------------------------------------------- //

    function exEvadeStart()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();

        parent.ignoreStuckOnce();

        local rnd = rand() % 2;
        if (rnd == 0)
            parent.playRandomAni("jump");
        else
            parent.playRandomAni("strafe");

        turnToEnemy(typeComp.getChaseTurnSpeed());

        setCombatState(ECombatStatePredator.evade);
        m_EvadeStateEndTick = getTickCount() + m_EvadeTime;
    }

    // ------------------------------------------------------------------- //

    function exEvade()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();

        local tick = getTickCount();
        if (tick >= m_EvadeStateEndTick)
            setCombatState(ECombatStatePredator.chaseStart);
    }

    // ------------------------------------------------------------------- //

    function exAttackStart()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();

        turnToEnemy(typeComp.getAttackTurnSpeed());
        parent.stopAni();
        setCombatState(ECombatStatePredator.attack);
    }

    // ------------------------------------------------------------------- //

    // -- State machine:

    // -- ready -> swing/jumpBack/strafe
    // -- swing -> recover
    // -- recover -> ready
    function exAttack()
    {
        switch (m_AttackState)
        {
            case EAttackState.ready:
            attackReady();
            break;

            case EAttackState.swing:
            attackSwing();
            break;

            case EAttackState.recover:
            attackRecover();
            break;

            case EAttackState.jumpBack:
            attackJumpBack();
            break;

            case EAttackState.strafe:
            attackStrafe();
            break;
        }
    }

    // ------------------------------------------------------------------- //

    // -- Bot can potentially start attack -- //
    function attackReady()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        local enemy = getEnemy();

        if (enemy.getDistance() > typeComp.getAttackRange())
        {
            setCombatState(ECombatStatePredator.chaseStart);
            return;
        }

        local tooClose = enemy.getDistance() <= typeComp.getJumpBackRange();

        // -- jump back -- //
        if (tooClose && (rand() % 100.0) / 100.0 <= typeComp.getJumpBackChance())
        {
            m_AttackStateStartTick = getTickCount();
            parent.playRandomAni("jumpBack");
            m_AttackState = EAttackState.jumpBack;
        }
        // -- strafe -- //
        else if ((rand() % 100.0) / 100.0 <= typeComp.getStrafeChance())
        {
            m_AttackStateStartTick = getTickCount();
            parent.playRandomAni("strafe");
            m_AttackState = EAttackState.strafe;
        }
        // -- attack -- //
        else if (canHit())
        {
            parent.playRandomAni(parent.getAttackAniSet());
            stopTurningToEnemy();

            m_AttackStateStartTick = getTickCount();
            m_AttackState = EAttackState.swing;
        }
    }

    // ------------------------------------------------------------------- //

    // -- Bot is mid-swing -- //
    function attackSwing()
    {
        local tick = getTickCount();
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        local enemy = getEnemy();

        if (tick >= m_AttackStateStartTick + typeComp.getDamageDelay())
        {
            if (canHit())
            {
                enemy.receiveHit(parent.getID());
                callEvent("onBotHit", parent.getID(), enemy.getID());
            }
            turnToEnemy(typeComp.getAttackTurnSpeed());

            m_AttackStateStartTick = getTickCount();
            m_AttackState = EAttackState.recover;
        }
    }

    // ------------------------------------------------------------------- //

    // -- Bot has finished attacking. Don't attack for m_AttackRecoveryTime ms -- //
    function attackRecover()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();

        if (getEnemy().getDistance() > typeComp.getAttackRange())
        {
            setCombatState(ECombatStatePredator.chaseStart);
            return;
        }

        local tick = getTickCount();
        local enemy = getEnemy();

        if (tick >= m_AttackStateStartTick + typeComp.getAttackRecoveryTime())
        {
            m_AttackState = EAttackState.ready;
            parent.stopAni();
        }
    }

    // ------------------------------------------------------------------- //

    function attackJumpBack()
    {
        local tick = getTickCount();
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        if (tick >= m_AttackStateStartTick + typeComp.getJumpBackTime())
        {
            m_AttackState = EAttackState.ready;
            parent.stopAni();
        }
    }

    // ------------------------------------------------------------------- //

    function attackStrafe()
    {
        local tick = getTickCount();
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        if (tick >= m_AttackStateStartTick + typeComp.getStrafeTime())
        {
            m_AttackState = EAttackState.ready;
            parent.stopAni();
        }
    }

    // ------------------------------------------------------------------- //

    function turnToEnemy(speed)
    {
        local parent = getParent();
        local enemy = getEnemy();
        parent.setInterpAngleId(enemy.getID(), speed);
    }

    // ------------------------------------------------------------------- //

    function stopTurningToEnemy()
    {
        local parent = getParent();
        parent.setInterpAngleId(-1, parent.getInterpAngleSpeed());
    }

    // ------------------------------------------------------------------- //

    function onDeath(attackerId)
    {
        stopCombat();
    }

    // ------------------------------------------------------------------- //

    function onReceiveDamage(attackerId, dmg, type)
    {
        if (!isFighting() && !getParent().isDead())
            setEnemy(BotEnemy(attackerId, getParent()), true);
    }

    // ------------------------------------------------------------------- //

    function onStartFight()
    {
    }

    // ------------------------------------------------------------------- //

    function onEndFight()
    {
    }

    // ------------------------------------------------------------------- //

    function canHit()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        local enemy = getEnemy();

        local pos = parent.getPosition();
        local enemyPos = enemy.getPosition();

        local angle = parent.getAngle();
        local targetAngle = getVectorAngle(pos.x, pos.z, enemyPos.x, enemyPos.z);

        local angleDelta = Util.getAngleDelta(angle, targetAngle);
        local dist = enemy.getDistance();

        local enemyAni = enemy.getAni();

        local enemyParrying = enemyAni in PARRY_ANIMS;

        return angleDelta <= typeComp.getMaxAttackAngle() && dist <= typeComp.getAttackRange() && !enemyParrying;
    }

    // ------------------------------------------------------------------- //

    function hit()
    {
        getEnemy().receiveHit(getParent().getID());
    }

    // ------------------------------------------------------------------- //

    function findEnemy()
    {
        local parent = getParent();
        local typeComp = parent.getCombatTypeComp();
        return getNearestEnemy(typeComp.getEnemyViewDist(), true);
    }

    // ------------------------------------------------------------------- //

    function stopCombat()
    {
        local parent = getParent();

        m_Enemy = null;
        setCombatState(ECombatStatePredator.idle);
        stopTurningToEnemy();
        getParent().stopAni();

        parent.setWeaponMode(0);

        stopTurningToEnemy();

        getParent().getActionComp().clear();
        getParent().getScheduleComp().returnToSchedule(getTime());

        onEndFight();
    }

    // ------------------------------------------------------------------- //

    // -- hostileImmediately == true -> attack without going into warning stage -- //
    function startCombat(hostileImmediately = false)
    {
        local parent = getParent();
        local typeComp = getParent().getCombatTypeComp();

        m_CombatEndTick = getTickCount() + typeComp.getMaxChaseTime();
        m_CombatStartPos = m_Enemy.getPosition();

        m_CombatStartTick = getTickCount();

        if (isFighting() || hostileImmediately || !typeComp.getDoEverWarn())
        {
            setCombatState(ECombatStatePredator.chaseStart);
        }
        else
        {
            setCombatState(ECombatStatePredator.warnStart);
            onStartFight();
        }
    }

    // ------------------------------------------------------------------- //

    function restartCombat()
    {
        local parent = getParent();
        local typeComp = getParent().getCombatTypeComp();

        m_CombatStartTick = getTickCount();
        m_CombatStartPos = parent.getPosition();

        setCombatState(ECombatStatePredator.chaseStart);
    }

    // ------------------------------------------------------------------- //

    // -- hostileImmediately == true -> attack without going into warning stage -- //
    function setEnemy(enemy, hostileImmediately = false)
    {
        local parent = getParent();
        local oldEnemy = m_Enemy;
        local typeComp = getParent().getCombatTypeComp();

        local isNewEnemy = ( (enemy != null || oldEnemy != null) && enemy != oldEnemy) ||
            (enemy != null && oldEnemy != null && enemy.getID() == oldEnemy.getID());

        m_EnemyDropTime = getTickCount();

        if (isNewEnemy)
        {
            m_Enemy = enemy;

            if (enemy == null && parent.isInView() && !parent.isDead())
            {
                // -- Try finding new enemy -- //
                local newEnemy = findEnemy();
                if (newEnemy == null)
                    setCombatState(COMBAT_IDLE);
                else
                {
                    m_Enemy = newEnemy;
                    restartCombat();
                }
            }
            else if (enemy != null && oldEnemy == null)
            {
                startCombat(hostileImmediately);
            }
        }
    }

    // ------------------------------------------------------------------- //

    function clearEnemy()
    {
        setEnemy(null);
    }

    // ------------------------------------------------------------------- //

    function getEnemyDistance()
    {
        if (m_Enemy != null)
        {
            local pos = getPosition();
            local enemyPos = m_Enemy.getPosition();

            return getDistance3d(pos.x, pos.y, pos.z, enemyPos.x, enemyPos.y, enemyPos.z);
        }
        return null;
    }

    // ------------------------------------------------------------------- //

    function getNearestEnemy(radius = 5000, alive = null)
    {
        local pos = getParent().getPosition();

        local enemyBot = getNearestEnemyBot(radius, alive);
        local enemyPlayer = getNearestEnemyPlayer(radius, alive);

        local botValid = enemyBot.isValid();
        local playerValid = enemyPlayer.isValid();

        if (botValid == playerValid)
        {
            // -- Both invalid -- //
            if (!botValid)
                return null;
            // -- Both valid. Chose closest -- //
            else
            {
                local botPos = enemyBot.getPosition();
                local botDist = getDistance3d(botPos.x, botPos.y, botPos.z, pos.x, pos.y, pos.z);

                local playerPos = enemyPlayer.getPosition();
                local playerDist = getDistance3d(playerPos.x, playerPos.y, playerPos.z, pos.x, pos.y, pos.z);

                if (botDist < playerDist)
                    return enemyBot;
                else
                    return enemyPlayer;
            }
        }
        // -- One is valid -- //
        else
        {
            if (!botValid)
                return enemyPlayer;
            return enemyBot;
        }
    }

    // ------------------------------------------------------------------- //

    function getNearestEnemyPlayer(radius = 5000, alive = null)
    {
        local lastDist = radius + 1;
        local lastPid = null;
        local botPos = getParent().getPosition();

        for(local pid = 0; pid < getMaxSlots(); pid++)
        {
            if (isPlayerConnected(pid) &&
            alive != isPlayerDead(pid))
            {
                local playerWorld = getPlayerWorld(pid);
                if (playerWorld == getParent().getWorld())
                {
                    local playerPos = getPlayerPosition(pid);
                    local dist = getDistance3d(botPos.x, botPos.y, botPos.z, playerPos.x, playerPos.y, playerPos.z);
                    if (dist <= radius && dist < lastDist && getParent().getFactionComp().isPlayerEnemy(pid))
                    {
                        lastPid = pid;
                        lastDist = dist;
                    }
                }
            }
        }

        return BotEnemy(lastPid, getParent());
    }

    // ------------------------------------------------------------------- //

    function getNearestEnemyBot(radius = 5000, alive = null)
    {
        local parent = getParent();
        local pos = parent.getPosition();

        local bots = Bot.m_CellSystem.getContentNear(pos.x, pos.z, 5000);
        local lastDistance = null;
        local lastBotId = null;

        foreach (i, bot in bots)
        {
            if (bot != parent)
            {
                local otherPos = bot.getPosition();
                local currDistance = getDistance3d(pos.x, pos.y, pos.z, otherPos.x, otherPos.y, otherPos.z);

                if (currDistance <= radius && (currDistance < lastDistance || lastBotId == null) && parent.getFactionComp().isBotEnemy(bot) && (alive == null || !bot.isDead() ))
                {
                    lastDistance = currDistance;
                    lastBotId = bot.getID();
                }
            }
        }

        return BotEnemy(lastBotId, getParent());
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Setters -- //
    function setCombatState(state){m_CombatState = state;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- Getters -- //
    function getEnemy(){return m_Enemy;}
    function getAttackRange(){return m_AttackRange;}
    function getCombatState(){return m_CombatState;}
    function isFighting() {return m_CombatState != COMBAT_IDLE;}

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    constructor(parent)
    {
        base.constructor(parent);
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    m_Enemy = null;
    m_CombatState = COMBAT_IDLE;

    m_CombatStartTick = getTickCount();
    m_CombatStartPos = null;
    m_EnemyDropTime = 0;

    // -- WARN -- //
        m_EndWarnTick = getTickCount();
        m_LastWarnDist = 0;
        m_MinWarnTime = getTickCount();

    // -- ATTACK -- //
        m_AttackStateStartTick = 0;
        m_AttackState = EAttackState.ready;

    // -- EVADE -- //
        m_EvadeStateEndTick = 0;

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    static m_EvadeTime = 1000;
    static m_MinCancelChaseTime = 1000;
}

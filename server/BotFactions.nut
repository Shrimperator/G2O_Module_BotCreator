// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

function initFactions()
{
    local player = Faction("player");

    local humanFriendly = Faction("humanfriendly");
    local humanBandit = Faction("humanbandit");

    local monster = Faction("monster");

    local prey = Faction("prey");

    local orc = Faction("orc");

    local skeleton = Faction("skeleton");

    // ---- //

    humanFriendly.addEnemyFaction(humanBandit);
    humanFriendly.addEnemyFaction(monster);
    humanFriendly.addEnemyFaction(orc);
    humanFriendly.addEnemyFaction(skeleton);

    humanBandit.addEnemyFaction(player);
    humanBandit.addEnemyFaction(humanFriendly);
    humanBandit.addEnemyFaction(monster);
    humanBandit.addEnemyFaction(orc);
    humanBandit.addEnemyFaction(skeleton);

    monster.addEnemyFaction(player);
    monster.addEnemyFaction(humanFriendly);
    monster.addEnemyFaction(humanBandit);
    monster.addEnemyFaction(skeleton);
    monster.addEnemyFaction(prey);

    orc.addEnemyFaction(player);
    orc.addEnemyFaction(humanFriendly);
    orc.addEnemyFaction(humanBandit);
    orc.addEnemyFaction(skeleton);

    skeleton.addEnemyFaction(player);
    skeleton.addEnemyFaction(humanFriendly);
    skeleton.addEnemyFaction(humanBandit);
    skeleton.addEnemyFaction(monster);
    skeleton.addEnemyFaction(orc);
    skeleton.addEnemyFaction(prey);

    prey.addEnemyFaction(monster);
}

// ------------------------------------------------------------------- //

addEventHandler("onInit", function()
{
    initFactions();
});

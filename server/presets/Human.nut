// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online BotCreator Scripts          -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

// -- Monastery -- //
class BotNovice extends BotHuman
{
    constructor(name, x = 0, y = 0, z = 0, angle = 0)
    {
        base.constructor(name, x, y, z, angle);

        setStrength(35);
        setDexterity(20);
        equipArmor(Items.id("ITAR_NOV_L"));
        equipMeleeWeapon(Items.id("ITMW_ADDON_HACKER_1H_02"));
        setVisuals("Hum_Body_Naked0", 1, "Hum_Head_Fighter", 14);

        getFactionComp().addToFaction("HumanFriendly");
    }
}
